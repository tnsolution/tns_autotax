﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class SectionImport_Info
    {
        #region [ Field Name ]
        private int _SectionNo = 0;
        private string _FileName = "";
        private int _AmountData = 0;
        private int _AmountAnalysis = 0;
        private string _UserKey = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _Message = "";
        private bool _IsAddNew = true;
        #endregion

        #region [ Constructor Get Information ]
        public SectionImport_Info()
        {
        }
        public SectionImport_Info(int SectionNo)
        {
            _IsAddNew = true;
            string zSQL = "SELECT * FROM SectionImport WHERE SectionNo = @SectionNo";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _SectionNo = int.Parse(zReader["SectionNo"].ToString());
                    _FileName = zReader["FileName"].ToString();
                    _AmountData = int.Parse(zReader["AmountData"].ToString());
                    _AmountAnalysis = int.Parse(zReader["AmountAnalysis"].ToString());
                    _UserKey = zReader["UserKey"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _IsAddNew = false;
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int SectionNo
        {
            get { return _SectionNo; }
            set { _SectionNo = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public int AmountData
        {
            get { return _AmountData; }
            set { _AmountData = value; }
        }

        public int AmountAnalysis
        {
            get { return _AmountAnalysis; }
            set { _AmountAnalysis = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SectionImport ("
        + " SectionNo,FileName,AmountData  ,AmountAnalysis  ,UserKey ) "
         + " VALUES ( "
         + "@SectionNo,@FileName,@AmountData  ,@AmountAnalysis  ,@UserKey ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = _SectionNo;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SectionImport SET "
                        + " FileName = @FileName,"
                        + " AmountData = @AmountData,"
                        + " AmountAnalysis = @AmountAnalysis,"
                        + " UserKey = @UserKey"
                       + " WHERE SectionNo = @SectionNo";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = _SectionNo;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_IsAddNew)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SectionImport WHERE SectionNo = @SectionNo";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = _SectionNo;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
