﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HHT.Library.System;
namespace HHT.Library.Bank
{
    public class Item_Bill
    {
        private int _ID = 0;

        private string _WordCode = "";
        private string _Content = "";
        private string _Value = "";
        private string _Extent = "";

        private int _CategoryID = 0;
        private string _CategoryName = "";
        private string _Description = "";
        private bool _IsNumber;
        private string _CorrectName = "";
        private string _CorrectID = "";
        private string _CorrectValue = "";
        private int _Index = -1;
        public Item_Bill()
        {

        }
        public Item_Bill(string content, bool isNumber)
        {
            int n = content.Length;
            int i = n - 1;
            if (content.Length >= 2)
            {
                while (i > 0)
                {
                    int zAscii = (int)content[i];
                    if ((zAscii > 47 && zAscii < 58) || (zAscii > 64 && zAscii < 91))
                    {
                        break;
                    }
                    else
                        i--;
                }
                content = content.Substring(0, i + 1);
                i = 0;
                while (i < n)
                {
                    int zAscii = (int)content[i];
                    if ((zAscii > 47 && zAscii < 58) || (zAscii > 64 && zAscii < 91))
                    {
                        break;
                    }
                    else
                        i++;
                }
                _Content = content.Substring(i, content.Length - i);
            }
            else
                _Content = content;
            _IsNumber = isNumber;
        }
        public bool Anlysis(int id, string[] ListCode)
        {
            bool zResult = false;
            foreach (string zCode in ListCode)
            {
                int n = zCode.Length;
                int zIndex = Content.IndexOf(zCode, 0);
                if (zIndex >= 0)
                {
                    _ID = id;
                    _WordCode = _Content.Substring(zIndex, n);
                    zResult = true;
                    break;
                }
            }
            return zResult;
        }
        
        public bool CheckCategoryID_InList(int[] ListID)
        {
            for(int i=0;i< ListID.Length;i++)
            {
                if (_CategoryID == ListID[i])
                    return true;
            }
            return false;
        }

        #region [ Properties ]
        public int ID
        {
            set { _ID = value; }
            get { return _ID; }
        }
        public string Content
        {
            set { _Content = value; }
            get { return _Content; }
        }
        public string Value
        {
            set { _Value = value; }
            get { return _Value; }
        }
        public string CorrectValue
        {
            set { _CorrectValue = value; }
            get { return _CorrectValue; }
        }
        public string CorrectName
        {
            set { _CorrectName = value; }
            get { return _CorrectName; }
        }
        public string CorrectID
        {
            set { _CorrectID = value; }
            get { return _CorrectID; }
        }
        public string Extent
        {
            set { _Extent = value; }
            get { return _Extent; }
        }
        public string WordCode
        {
            set { _WordCode = value; }
            get { return _WordCode; }
        }
        public int CategoryID
        {
            set { _CategoryID = value; }
            get { return _CategoryID; }
        }
        public string CategoryName
        {
            set { _CategoryName = value; }
            get { return _CategoryName; }
        }
        public string Description
        {
            set { _Description = value; }
            get { return _Description; }
        }
        public bool IsNumber
        {
            set { _IsNumber = value; }
            get { return _IsNumber; }
        }
        public int Index
        {
            set { _Index = value; }
            get { return _Index; }
        }
        #endregion
    }
}
