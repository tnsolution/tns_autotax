﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class OrderTranfer_Data
    {
        public static int AmountSection(int SectionNo)
        {
            int zAmount = 0;
            string zResult = "";
            string zSQL = "SELECT COUNT(*) FROM BNK_OrderTranfer WHERE SectionNo = @SectionNo";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            int.TryParse(zResult, out zAmount);
            return zAmount;
        }
        public static int Insert_Web(string SQL)
        {
            int zAmount = 0;
            string zResult = "";
            string zSQL = "Insert_Web";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            int.TryParse(zResult, out zAmount);
            return zAmount;
        }
        public static int Insert_IPICAS(string SQL)
        {
            int zAmount = 0;
            string zResult = "";
            string zSQL = "Insert_IPICAS";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            int.TryParse(zResult, out zAmount);
            return zAmount;
        }
        public static int MaxSection()
        {
            int zResult = 0;
            string zSQL = "SELECT TOP 1 SectionNo FROM dbo.BNK_OrderTranfer ORDER BY SectionNo DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    zResult = int.Parse(zReader["SectionNo"].ToString());
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult + 1;
        }

        public static DataTable ListAnalysis(int SectionNo)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM BNK_OrderTranfer 
                            WHERE SectionNo = @SectionNo ORDER BY trdate,trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                zConnect.Close();
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListAnalysis(int SectionNo, int StatusAnalysis, int CategoryOrder)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM BNK_OrderTranfer 
                            WHERE SectionNo = @SectionNo ";
            if (StatusAnalysis > 0)
                zSQL += "AND StatusAnalysis = @StatusAnalysis ";
            if (CategoryOrder > 0)
                zSQL += "AND CategoryOrder = @CategoryOrder ";

            zSQL += "ORDER BY trdate,trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = StatusAnalysis - 1;
                zCommand.Parameters.Add("@CategoryOrder", SqlDbType.Int).Value = CategoryOrder;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListAnalysis(int SectionNo, int StatusAnalysis)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT trref,trdate,traamt,ordcust,bencust,remtbk,remntbk,name_16,remark,StatusAnalysis FROM BNK_OrderTranfer WHERE SectionNo = @SectionNo AND StatusAnalysis = @StatusAnalysis ";
                    zSQL += "ORDER BY trdate,trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = StatusAnalysis;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListAnalysis(int StatusAnalysis,string ContentSearch)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT trref,trdate,traamt,ordcust,bencust,remtbk,remntbk,name_16,remark,StatusAnalysis FROM BNK_OrderTranfer "
                          + "WHERE (remark LIKE @ContentSearch OR ordcust LIKE @ContentSearch) AND StatusAnalysis = @StatusAnalysis ";
            zSQL += "ORDER BY trdate,trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + ContentSearch +"%";
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = StatusAnalysis;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string[] ListLHXNK()
        {

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM BNK_LHXNK ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            string[] zResult = new string[zTable.Rows.Count];
            int i = 0;
            foreach (DataRow zRow in zTable.Rows)
            {
                zResult[i] = zRow["ID"].ToString();
                i++;
            }
            return zResult;
        }
        public static string SearchDepartment(string DepartmentName)
        {
            DepartmentName = DepartmentName.ToUpper();
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM GOV_Department_Code";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {

                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
                zConnect.Close();
            }
            int zIndex;
            foreach (DataRow zRow in zTable.Rows)
            {
                zIndex = DepartmentName.IndexOf(zRow["WordCode"].ToString());
                if (zIndex >= 0)
                {
                    zResult = zRow["ValueCode"].ToString();
                    break;
                }
            }
            return zResult;
        }
        public static string SearchMaQHNS_Of_Department(string DepartmentName)
        {
            DepartmentName = DepartmentName.ToUpper();
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM GOV_Department_Code ORDER BY  [Rank] DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {

                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
                zConnect.Close();
            }
            int zIndex;
            foreach (DataRow zRow in zTable.Rows)
            {
                zIndex = DepartmentName.IndexOf(zRow["WordCode"].ToString());
                if (zIndex >= 0)
                {
                    zResult = zRow["ValueCode"].ToString();
                    break;
                }
            }
            //----------------------------------------------------------------

            return zResult;
        }
        #region [So Sánh 2 Bảng Table]
        public static DataTable List_SQL(string SoCT)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM [dbo].[BNK_Order_Detail] A
LEFT JOIN [dbo].[BNK_Order] B ON B.trref = A.trref
WHERE B.So_CT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SoCT", SqlDbType.Int).Value = SoCT;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                zConnect.Close();
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Excel(string SoCT)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM [dbo].[BNK_Order_Detail_Web] WHERE SoCT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SoCT", SqlDbType.Int).Value = SoCT;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                zConnect.Close();
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion


        #region [Load 2 Bảng Table]
        public static DataTable List_SQL_1(string SoCT)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.NDKT_ID AS TieuMuc, A.SoTien AS SoTien FROM [dbo].[BNK_Order_Detail] A
LEFT JOIN [dbo].[BNK_Order] B ON B.trref = A.trref
WHERE B.So_CT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SoCT", SqlDbType.Int).Value = SoCT;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                zConnect.Close();
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Excel_2(string SoCT)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT NDKT , Sotien FROM [dbo].[BNK_Order_Detail_Web] WHERE SoCT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SoCT", SqlDbType.Int).Value = SoCT;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                zConnect.Close();
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        public static ArrayList ItemAnalysis_2(string trref)
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT  * FROM BNK_OrderTranfer_Detail A "
                        + " LEFT JOIN Item_Categories B ON A.CategoryID = B.CategoryID "
                        + " WHERE A.trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = trref;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            Item_Bill zItem;
            ArrayList zResult = new ArrayList();
            foreach (DataRow zRow in zTable.Rows)
            {
                zItem = new Item_Bill();
                zItem.CategoryID = (int)zRow["CategoryID"];
                zItem.Content = zRow["ContentItem"].ToString();
                zItem.CategoryName = zRow["CategoryName"].ToString();
                zItem.Value = zRow["ContentValue"].ToString();
                zResult.Add(zItem);
            }
            return zResult;
        }
        public static string GetMST(string trref)
        {
            string zResult = "";
            string zSQL = "SELECT MST FROM BNK_OrderTranfer WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = trref;
                var zOjb = zCommand.ExecuteScalar();
                if (zOjb != null)
                    zResult = zOjb.ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

    }
}
