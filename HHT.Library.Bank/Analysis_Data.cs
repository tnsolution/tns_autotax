﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
   public class Analysis_Data
    {
        public static DataTable ListWordCode_RuleItem(int CategoryID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[Rule_Items] A "
                            + " LEFT JOIN [dbo].[WordCodes] B ON A.ItemKey = B.RuleID " 
                            + " WHERE ItemKey = @ItemKey ORDER BY B.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = CategoryID;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListWordCodeName(int CategoryID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, B.LengMax,B.RuleID, C.WordCode FROM [dbo].[Item_Categories] A 
                            INNER JOIN[dbo].[Rule] B ON A.CategoryID = B.CategoryID
                            INNER JOIN [dbo].[WordCodes] C ON C.RuleID = B.RuleID
                            WHERE RuleStyle = 1 AND @CategoryID = A.CategoryID
                            ORDER BY A.Rank, B.Rank, C.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListWordCodeName_Ext(int CategoryID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, B.LengMax,B.RuleID, C.WordCode FROM [dbo].[Item_Categories] A 
                            INNER JOIN[dbo].[Rule] B ON A.CategoryID = B.CategoryID
                            INNER JOIN [dbo].[WordCodes_Ext] C ON C.RuleID = B.RuleID
                            WHERE RuleStyle = 1 AND @CategoryID = A.CategoryID
                            ORDER BY A.Rank, B.Rank, C.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCategory()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[Item_Categories] 
                            ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }



    }
}
