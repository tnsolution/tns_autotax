﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;
namespace HHT.Library.Bank
{
    public class Rule_Group_Info
    {

        #region [ Field Name ]
        private int _Auto = 0;
        private int _GroupID = 0;
        private string _GroupSort = "";
        private string _ItemRepeat = "";
        private int _Version = 0;
        private int _Rank = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Rule_Group_Info()
        {
        }
        public Rule_Group_Info(int Auto)
        {
            string zSQL = "SELECT * FROM Rule_Group WHERE Auto = @Auto";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = Auto;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Auto = int.Parse(zReader["Auto"].ToString());
                    _GroupID = int.Parse(zReader["GroupID"].ToString());
                    _GroupSort = zReader["GroupSort"].ToString();
                    _ItemRepeat = zReader["ItemRepeat"].ToString();
                    _Version = int.Parse(zReader["Version"].ToString());
                    _Rank = int.Parse(zReader["Rank"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int Auto
        {
            get { return _Auto; }
            set { _Auto = value; }
        }
        public int GroupID
        {
            get { return _GroupID; }
            set { _GroupID = value; }
        }
        public string GroupSort
        {
            get { return _GroupSort; }
            set { _GroupSort = value; }
        }
        public string ItemRepeat
        {
            get { return _ItemRepeat; }
            set { _ItemRepeat = value; }
        }
        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Rule_Group ("
                        + " GroupID ,GroupSort ,ItemRepeat ,Version ,Rank ) "
                        + " VALUES ( "
                        + "@GroupID ,@GroupSort ,@ItemRepeat ,@Version ,@Rank ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = _GroupID;
                zCommand.Parameters.Add("@GroupSort", SqlDbType.NVarChar).Value = _GroupSort;
                zCommand.Parameters.Add("@ItemRepeat", SqlDbType.NVarChar).Value = _ItemRepeat;
                zCommand.Parameters.Add("@Version", SqlDbType.Int).Value = _Version;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Rule_Group SET "
                        + " GroupID = @GroupID,"
                        + " GroupSort = @GroupSort,"
                        + " ItemRepeat = @ItemRepeat,"
                        + " Version = @Version,"
                        + " Rank = @Rank"
                       + " WHERE Auto = @Auto";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = _Auto;
                zCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = _GroupID;
                zCommand.Parameters.Add("@GroupSort", SqlDbType.NVarChar).Value = _GroupSort;
                zCommand.Parameters.Add("@ItemRepeat", SqlDbType.NVarChar).Value = _ItemRepeat;
                zCommand.Parameters.Add("@Version", SqlDbType.Int).Value = _Version;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_Auto == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Rule_Group WHERE Auto = @Auto";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = _Auto;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
