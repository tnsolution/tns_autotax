﻿using HHT.Connect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace HHT.Library.Bank
{
    public class Order_Object
    {
        private Order_Info _OrderHeader;
        private List<Order_Detail_Info> _OrderDetail;
        private string _Remark;
        private DateTime _trdate;
        private List<Item_Bill> _Remark_Items;
        private string _RuleID;
        private string _Message = "";
        public Order_Object()
        {
            _OrderHeader = new Order_Info();
            _OrderDetail = new List<Order_Detail_Info>();
            _Remark_Items = new List<Item_Bill>();
            _Remark = "";
            _RuleID = "";
        }
        public string Remark
        {
            set { _Remark = value; }
            get { return _Remark; }
        }
        public DateTime trdate
        {
            set { _trdate = value; }
            get { return _trdate; }
        }
        public Order_Info Header
        {
            set { _OrderHeader = value; }
            get { return _OrderHeader; }
        }
        public List<Order_Detail_Info> Items
        {
            set { _OrderDetail = value; }
            get { return _OrderDetail; }
        }
        public List<Item_Bill> Remark_Items
        {
            set { _Remark_Items = value; }
            get { return _Remark_Items; }
        }
        public string RuleID
        {
            set { _RuleID = value; }
            get { return _RuleID; }
        }
        public string DeleteAllDetail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_Order_Detail WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _OrderHeader.trref;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public bool Save()
        {
            bool zResult = false;
            if (Header.IsHaveRecord)
            {
                _OrderHeader.Update();
                if (_OrderHeader.Code == "200")
                {
                    zResult = true;
                    DeleteAllDetail();
                    for (int i = 0; i < _OrderDetail.Count; i++)
                    {
                        _OrderDetail[i].trref = _OrderHeader.trref;
                        _OrderDetail[i].Create();
                        if(_OrderDetail[i].Code == "501")
                        {
                            zResult = false;
                        }
                    }
                }
                else
                {
                    zResult = false;
                }
            }
            else
            {
                _OrderHeader.Create();
                if (_OrderHeader.Code == "201")
                {
                    zResult = true;
                    for (int i = 0; i < _OrderDetail.Count; i++)
                    {
                        _OrderDetail[i].trref = _OrderHeader.trref;
                        _OrderDetail[i].Create();
                        if (_OrderDetail[i].Code == "501")
                        {
                            zResult = false;
                        }
                    }
                }
                else
                {
                    zResult = false;
                }
            }
            return zResult;

        }
    }
}
