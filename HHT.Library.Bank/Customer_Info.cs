﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;
namespace HHT.Library.Bank
{
    public class Customer_Info
    {

        #region [ Field Name ]
        private string _CustomerKey = "";
        private string _CustomerName = "";
        private string _CustomerTax = "";
        private string _Address = "";
        private string _DistricID = "";
        private string _Province = "";
        private string _Format = "";
        private int _CompanyService = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Customer_Info()
        {
            Guid zNewID = Guid.NewGuid();
            _CustomerKey = zNewID.ToString();
        }
        public Customer_Info(string Name)
        {
            string zSQL = "SELECT * FROM [dbo].[CRM_Customer] WHERE CustomerName = @CustomerName ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Name;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _CustomerKey = zReader["CustomerKey"].ToString();
                    _CustomerName = zReader["CustomerName"].ToString();
                    _CustomerTax = zReader["CustomerTax"].ToString();
                    _Address = zReader["Address"].ToString();
                    _DistricID = zReader["DistricID"].ToString();
                    _Province = zReader["Province"].ToString();
                    _Format = zReader["Format"].ToString();
                    _CompanyService = int.Parse(zReader["CompanyService"].ToString());
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public string CustomerTax
        {
            get { return _CustomerTax; }
            set { _CustomerTax = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string DistricID
        {
            get { return _DistricID; }
            set { _DistricID = value; }
        }
        public string Province
        {
            get { return _Province; }
            set { _Province = value; }
        }
        public string Format
        {
            get { return _Format; }
            set { _Format = value; }
        }
        public int CompanyService
        {
            get { return _CompanyService; }
            set { _CompanyService = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[CRM_Customer] ("
        + " CustomerName ,CustomerTax ,Address ,DistricID ,Province ,Format ,CompanyService ) "
         + " VALUES ( "
         + "@CustomerName ,@CustomerTax ,@Address ,@DistricID ,@Province ,@Format ,@CompanyService ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@CustomerTax", SqlDbType.NVarChar).Value = _CustomerTax;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@DistricID", SqlDbType.NVarChar).Value = _DistricID;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Format", SqlDbType.NVarChar).Value = _Format;
                zCommand.Parameters.Add("@CompanyService", SqlDbType.Int).Value = _CompanyService;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[CRM_Customer] ("
        + " CustomerKey ,CustomerName ,CustomerTax ,Address ,DistricID ,Province ,Format ,CompanyService ) "
         + " VALUES ( "
         + "@CustomerKey ,@CustomerName ,@CustomerTax ,@Address ,@DistricID ,@Province ,@Format ,@CompanyService ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@CustomerTax", SqlDbType.NVarChar).Value = _CustomerTax;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@DistricID", SqlDbType.NVarChar).Value = _DistricID;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Format", SqlDbType.NVarChar).Value = _Format;
                zCommand.Parameters.Add("@CompanyService", SqlDbType.Int).Value = _CompanyService;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE [dbo].[CRM_Customer] SET "
                        + " CustomerName = @CustomerName,"
                        + " CustomerTax = @CustomerTax,"
                        + " Address = @Address,"
                        + " DistricID = @DistricID,"
                        + " Province = @Province,"
                        + " Format = @Format,"
                        + " CompanyService = @CompanyService"
                       + " WHERE CustomerKey = @CustomerKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@CustomerTax", SqlDbType.NVarChar).Value = _CustomerTax;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@DistricID", SqlDbType.NVarChar).Value = _DistricID;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Format", SqlDbType.NVarChar).Value = _Format;
                zCommand.Parameters.Add("@CompanyService", SqlDbType.Int).Value = _CompanyService;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE [dbo].[CRM_Customer] Set RecordStatus = 99 WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[CRM_Customer] WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = new Guid(_CustomerKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
