﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using HHT.Connect;
namespace HHT.Library.Bank
{
    public class GovTax_Account_Info
    {

        #region [ Field Name ]
        private string _AccountID = "";
        private string _AccountName = "";
        private string _TKNS = "";
        private string _QHNS = "";
        private string _DepartmentID = "";
        private string _DepartmentName = "";
        private string _TreasuryID = "";
        private string _TreasuryName = "";
        private string _DBHC = "";
        private int _CategoryTax = 0;
        private string _Message = "";
        

        #endregion

        #region [ Constructor Get Information ]
        public GovTax_Account_Info()
        {
        }
        public GovTax_Account_Info(string AccountID)
        {
            string zSQL = "";

            zSQL = " SELECT * FROM FNC_GovTax_Account A"
                        + " LEFT JOIN GOV_TreasuryVN B ON A.TreasuryID = B.TreasuryID"
                        + " WHERE AccountID = @AccountID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = AccountID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AccountID = zReader["AccountID"].ToString();
                    _AccountName = zReader["AccountName"].ToString();
                    _TKNS = _AccountID.Substring(0, 4) ;
                    _QHNS = zReader["QHNS"].ToString();
                    _DepartmentID = zReader["DepartmentID"].ToString();
                    _DepartmentName = zReader["DepartmentName"].ToString();
                    _TreasuryID = zReader["TreasuryID"].ToString();
                    _TreasuryName = zReader["TreasuryName"].ToString();
                    _DBHC = zReader["DBHC"].ToString();
                    _CategoryTax = (int)zReader["CategoryTax"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public string AccountID
        {
            get { return _AccountID; }
            set { _AccountID = value; }
        }
        public string AccountName
        {
            get { return _AccountName; }
            set { _AccountName = value; }
        }
        public string QHNS
        {
            get { return _QHNS; }
            set { _QHNS = value; }
        }
        public string TKNS
        {
            get { return _TKNS; }
            set { _TKNS = value; }
        }
        public string DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }
       
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public string TreasuryID
        {
            get { return _TreasuryID; }
            set { _TreasuryID = value; }
        }
        public string TreasuryName
        {
            get { return _TreasuryName; }
            set { _TreasuryName = value; }
        }
        public string DBHC
        {
            get { return _DBHC; }
            set { _DBHC = value; }
        }
        public string CategoryTax
        {
            get { return _CategoryTax.ToString().PadLeft(2, '0'); }
            set { _CategoryTax = int.Parse(value); }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            _Message = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_GovTax_Account ("
        + " AccountID,AccountName ,QHNS ,DepartmentID,DepartmentName ,TreasuryID, DBHC,CategoryTax ) "
         + " VALUES ( "
         + "@AccountID,@AccountName ,@QHNS ,@DepartmentID,@DepartmentName ,@TreasuryID, @DBHC,@CategoryTax ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = _AccountID;
                zCommand.Parameters.Add("@AccountName", SqlDbType.NVarChar).Value = _AccountName;
                zCommand.Parameters.Add("@QHNS", SqlDbType.NVarChar).Value = _QHNS;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = _DepartmentID;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName;
                zCommand.Parameters.Add("@TreasuryID", SqlDbType.NVarChar).Value = _TreasuryID;
                zCommand.Parameters.Add("@DBHC", SqlDbType.NVarChar).Value = _DBHC;
                zCommand.Parameters.Add("@CategoryTax", SqlDbType.Int).Value = _CategoryTax;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            _Message = "";
            string zSQL = "UPDATE FNC_GovTax_Account SET "
                        + " AccountName = @AccountName,"
                        + " QHNS = @QHNS,"
                        + " DepartmentID = @DepartmentID,"
                        + " DepartmentName = @DepartmentName,"
                        + " TreasuryID = @TreasuryID,"
                        + " DBHC = @DBHC,"
                        + " CategoryTax = @CategoryTax"
                       + " WHERE AccountID = @AccountID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = _AccountID;
                zCommand.Parameters.Add("@AccountName", SqlDbType.NVarChar).Value = _AccountName;
                zCommand.Parameters.Add("@QHNS", SqlDbType.NVarChar).Value = _QHNS;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = _DepartmentID;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName;
                zCommand.Parameters.Add("@TreasuryID", SqlDbType.NVarChar).Value = _TreasuryID;
                zCommand.Parameters.Add("@DBHC", SqlDbType.NVarChar).Value = _DBHC;
                zCommand.Parameters.Add("@CategoryTax", SqlDbType.Int).Value = _CategoryTax;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_AccountID.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_GovTax_Account WHERE AccountID = @AccountID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = _AccountID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
