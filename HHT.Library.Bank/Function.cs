﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace HHT.Library.Bank
{
    public class Function
    {
        public static bool IsSpecialWord(char Word)
        {
            string zIsSpecialWord = "./";
            foreach (char zChar in zIsSpecialWord)
            {
                if (Word == zChar)
                    return true;
            }
            return false;
        }

        public static ArrayList SplipItem_1(string Content)
        {
            ArrayList zListItem = new ArrayList();

            int i = 0, n = Content.Length;
            while (i < n)
            {
                char zChar = Content[i];
                if (!IsSpecialWord(zChar))
                {
                    int zNumber = 0;
                    if (int.TryParse(zChar.ToString(), out zNumber))
                    {
                        string zListNumber = zChar.ToString();
                        i++;
                        while (i < n)
                        {
                            zChar = Content[i];
                            if (int.TryParse(zChar.ToString(), out zNumber))
                            {
                                zListNumber += zChar;
                            }
                            else
                            {
                                if (!IsSpecialWord(zChar))
                                    break;
                                else
                                    zListNumber += zChar;
                            }
                            i++;
                        }
                        zListItem.Add(new Item_Bill(zListNumber, true));
                    }
                    else
                    {
                        string zListChar = zChar.ToString();
                        i++;
                        while (i < n)
                        {
                            zChar = Content[i];
                            if (!int.TryParse(zChar.ToString(), out zNumber))
                            {
                                zListChar += zChar;
                            }
                            else
                            {
                                break;
                            }
                            i++;
                        }
                        zListItem.Add(new Item_Bill(zListChar, false));
                    }
                }
                else
                    i++;
            }
            return zListItem;
        }
        public static ArrayList SplipItem(string Content)
        {
            ArrayList zListItem = new ArrayList();

            int i = 0, n = Content.Length;
            while (i < n)
            {
                int zAscii = (int)Content[i];
                string zListChar = "";
                // La chu cai
                while ((zAscii < 48 || zAscii > 57) & i < n)
                {
                    zListChar += Content[i].ToString();

                    i++;
                    if (i < n)
                        zAscii = (int)Content[i];

                }
                zListItem.Add(new Item_Bill(zListChar, false));
                if (i < n)
                {
                    string zListNumber = "";
                    while ((zAscii < 65 || zAscii > 90) & i < n)
                    {
                        zListNumber += Content[i].ToString();
                        i++;
                        if (i < n)
                            zAscii = (int)Content[i];
                    }
                    zListItem.Add(new Item_Bill(zListNumber, true));
                }

            }
            return zListItem;
        }
        public static ArrayList SplipOnlyOneItem(string Content)
        {
            ArrayList zItem = new ArrayList();
            if (Content.Trim().Length == 0)
                return zItem;
            int i = 0, n = Content.Length;

            int zAscii = (int)Content[i];
            string zListChar = "";
            // La chu cai
            while ((zAscii < 48 || zAscii > 57) & i < n)
            {
                zListChar += Content[i].ToString();

                i++;
                if (i < n)
                    zAscii = (int)Content[i];

            }
            zItem.Add(new Item_Bill(zListChar, false));
            if (i < n)
            {
                string zListNumber = Content.Substring(i, n - i);
                zItem.Add(new Item_Bill(zListNumber, true));
            }
            else
                zItem.Add(new Item_Bill("0", true));


            return zItem;
        }
        public static Item_Bill SplipCodeAndValue(string Content)
        {
            Item_Bill zItem = new Item_Bill();
            if (Content.Trim().Length == 0)
                return zItem;
            int i = 0, n = Content.Length;

            int zAscii = (int)Content[i];
            string zListChar = "";
            // La chu cai
            while ((zAscii < 48 || zAscii > 57) & i < n)
            {
                zListChar += Content[i].ToString();

                i++;
                if (i < n)
                    zAscii = (int)Content[i];

            }
            zItem.Content = zListChar.Trim();
            if (i < n)
            {
                string zListNumber = Content.Substring(i, n - i);
                zItem.Value = zListNumber.Trim();
            }
            return zItem;
        }
        public static List<string> Split_All_Item(string Content)
        {
            List<string> zListItem = new List<string>();

            int i = 0, n = Content.Length;
            while (i < n)
            {
                int zAscii = (int)Content[i];
                string zListChar = "";
                // La chu cai
                while ((zAscii < 48 || zAscii > 57) & i < n)
                {
                    zListChar += Content[i].ToString();

                    i++;
                    if (i < n)
                        zAscii = (int)Content[i];

                }
                zListItem.Add(zListChar.Trim());
                if (i < n)
                {
                    string zListNumber = "";
                    while ((zAscii < 65 || zAscii > 90) & i < n)
                    {
                        zListNumber += Content[i].ToString();
                        i++;
                        if (i < n)
                            zAscii = (int)Content[i];
                    }
                    zListItem.Add(zListNumber.Trim());
                }

            }
            return zListItem;
        }
        public static string GetOnlyNumberInFormat(string Content)
        {
            char[] SpecialChars = { '.' };
            string zValue = "";
            int n = Content.Length;
            int i = 0;
            int zNumber;
            bool zFound = false;
            while (i < n)
            {
                zNumber = 0;
                zFound = true;
                string zChar = Content[i].ToString();
                if (int.TryParse(zChar, out zNumber))
                {
                    zValue += zChar;
                }
                else
                {
                    zFound = false;
                    foreach (char zItem in SpecialChars)
                    {
                        if (zItem.ToString() == zChar)
                        {
                            zFound = true;
                            break;
                        }
                    }
                }
                if (zFound)
                    i++;
                else
                    break;
            }

            return zValue;
        }
        public static string GetAllNumberInSide(string Content)
        {
            string zValue = "";
            int n = Content.Length;
            int i = 0;
            int zNumber;
            while (i < n)
            {
                zNumber = 0;
                string zChar = Content[i].ToString();
                if (int.TryParse(zChar, out zNumber))
                {
                    zValue += zChar;
                }
                i++;
            }

            return zValue;
        }
        public static string GetCorrectDate(string Content)
        {
            string zTemp = "";
            int zNumber;
            for (int i = 0; i < Content.Length; i++)
            {
                if (Content[i] == '.' || Content[i] == '/' || Content[i] == '-')
                {
                    zTemp += Content[i];
                }
                else
                {

                        if (int.TryParse(Content[i].ToString(), out zNumber))
                        {
                            zTemp += Content[i];
                        }
                    //else
                    //zTemp += " ";  // đông sửa 25/05/2021

                }
            }
            Content = zTemp;
            string[] zDateString = Content.Split('.');
            if (zDateString.Length != 3)
            {
                zDateString = Content.Split('/');
            }
            if (zDateString.Length != 3)
            {
                zDateString = Content.Split('-');
            }
            if (zDateString.Length != 3)
            {
                zDateString = Content.Split(' ');
            }

            for (int i = 0; i < zDateString.Length; i++)
            {
                zDateString[i] = zDateString[i].Trim().PadLeft(2, '0');
            }

            DateTime zDateTime;
            string strDate = "";
            bool zIsFound = false;
            if (zDateString.Length == 3)
            {
                if (zDateString[2].Length == 2)
                    strDate = zDateString[0].PadLeft(2,'0') + "/" + zDateString[1].PadLeft(2, '0') + "/20" + zDateString[2];
                else
                    strDate = zDateString[0].PadLeft(2, '0') + "/" + zDateString[1].PadLeft(2, '0') + "/" + zDateString[2];
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 8) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("dd/MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
            }

            if (!zIsFound && Content.Length == 8) //20180522
            {
                strDate = Content.Substring(6, 2) + "/" + Content.Substring(4, 2) + "/" + Content.Substring(0, 4);
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 8) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("dd/MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
            }
            if (!zIsFound && Content.Length == 8) //22052018
            {
                strDate = Content.Substring(0, 2) + "/" + Content.Substring(2, 2) + "/" + Content.Substring(4, 4);
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 8) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("dd/MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
            }

            return strDate;
        }
        public static string GetCorrectMonthYear(string Content)
        {
            CultureInfo enUS = new CultureInfo("en-US"); // is up to you
            string[] zDate1 = Content.Split('.');
            string[] zDate2 = Content.Split('/');
            string[] zDate3 = Content.Split('-');
            DateTime zDateTime;
            string strDate = "";
            bool zIsFound = false;
            if (zDate1.Length == 2)
            {
                strDate = "01/" + zDate1[0] + "/" + zDate1[1];
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 3) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
                else
                    strDate = "";
            }
            if (!zIsFound && zDate2.Length == 2)
            {
                strDate = "01/" + zDate2[0] + "/" + zDate2[1];
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 3) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
                else
                    strDate = "";
            }
            if (!zIsFound && zDate3.Length == 2)
            {
                strDate = "01/" + zDate3[0] + "/" + zDate3[1];
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 8) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
                else
                    strDate = "";
            }
            if (!zIsFound && Content.Length == 6)
            {
                strDate = "01/" + Content.Substring(0, 2) + "/" + Content.Substring(2, 4);
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 8) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
                else
                    strDate = "";
            }
            if (!zIsFound && zDate2.Length == 3)
            {
                if (zDate2[0] == "00")
                    zDate2[0] = "01";
                if (zDate2[1] == "Q1")
                    zDate2[1] = "03";
                if (zDate2[1] == "Q2")
                    zDate2[1] = "06";
                if (zDate2[1] == "Q3")
                    zDate2[1] = "09";
                if (zDate2[1] == "Q4")
                    zDate2[1] = "12";
                if (zDate2[1] == "CN")
                    zDate2[1] = "12";
                if (zDate2[1] == "QT")
                    zDate2[1] = "12";

                strDate = zDate2[0] + "/" + zDate2[1] + "/" + zDate2[2];
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out zDateTime))
                {
                    if ((zDateTime.Year > DateTime.Now.Year - 8) && (zDateTime.Year <= DateTime.Now.Year))
                    {
                        strDate = zDateTime.ToString("MM/yyyy");
                        zIsFound = true;
                    }
                    else
                        strDate = "";
                }
                else
                {
                    strDate = "";
                }

            }

            return strDate;
        }

        public static bool CheckCorrectID(string Content)
        {
            bool zResult = true;
            char zAscii = Content[0];
            if ((zAscii > 47 && zAscii < 58) || (zAscii > 64 && zAscii < 91))
            {
                zResult = false;
            }
            zAscii = Content[Content.Length - 1];
            if ((zAscii > 47 && zAscii < 58) || (zAscii > 64 && zAscii < 91))
            {
                zResult = false;
            }
            return zResult;
        }

        public static bool ConvertDateVN(string s, out DateTime Result)
        {
            Result = new DateTime();
            bool zCorrect = true;
            if (s.Length != 10)
            {
                zCorrect = false;
            }
            else
            {
                int zDay = 0, zMonth = 0, zYear = 0;
                int.TryParse(s.Substring(0, 2), out zDay);
                int.TryParse(s.Substring(3, 2), out zMonth);
                int.TryParse(s.Substring(6, 4), out zYear);
                try
                {
                    Result = new DateTime(zYear, zMonth, zDay);
                }
                catch (Exception ex)
                {
                    zCorrect = false;
                }
            }

            return zCorrect;
        }
        public static bool ConvertDateTimeVN(string s, out DateTime Result)
        {
            Result = new DateTime();
            bool zCorrect = true;
            if (s.Length < 14)
            {
                zCorrect = false;
            }
            else
            {
                string[] zData = s.Split(' ');
                s = zData[0].Trim();
                int zDay = 0, zMonth = 0, zYear = 0, HH = 0, MM = 0;
                int.TryParse(s.Substring(0, 2), out zDay);
                int.TryParse(s.Substring(3, 2), out zMonth);
                int.TryParse(s.Substring(6, 4), out zYear);
                s = zData[1].Trim();
                int.TryParse(s.Substring(0, 2), out HH);
                int.TryParse(s.Substring(3, 2), out MM);
                try
                {
                    Result = new DateTime(zYear, zMonth, zDay, HH, MM, 0);
                }
                catch (Exception ex)
                {
                    zCorrect = false;
                }
            }

            return zCorrect;
        }

        public static string RemoveSpace(string Content)
        {
            string zResult = "";
            for (int i = 0; i < Content.Length; i++)
            {
                if (Content[i] != ' ')
                {
                    zResult += Content[i];
                }
            }
            return zResult;
        }
        public static string RemoveCharNotNumber(string Content)
        {
            string zResult = "";
            for (int i = 0; i < Content.Length; i++)
            {
                int zNumber = 0;
                if (int.TryParse(Content[i].ToString(), out zNumber))
                {
                    zResult += Content[i];
                }
            }
            return zResult;
        }


    }
}
