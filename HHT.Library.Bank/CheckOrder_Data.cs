﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class CheckOrder_Data
    {
        public static DataTable OrderOnWeb(DateTime NgayCT)
        {
            string zDate = NgayCT.Year + NgayCT.Month.ToString().PadLeft(2, '0') + NgayCT.Day.ToString().PadLeft(2, '0');
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM BNK_OrderWeb "
                + " WHERE NgayKB = @NgayKB ORDER BY TongTien";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static double OrderOnWeb_TotalMoney(DateTime NgayCT)
        {
            string zDate = NgayCT.Year + NgayCT.Month.ToString().PadLeft(2, '0') + NgayCT.Day.ToString().PadLeft(2, '0');
            double zTotal = 0;
            string zSQL = "SELECT Sum(TongTien) FROM BNK_OrderWeb WHERE NgayKB = @NgayKB ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                string zReslut = zCommand.ExecuteScalar().ToString();
                double.TryParse(zReslut, out zTotal);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTotal;
        }
        public static double OrderOnWeb_TotalRecord(DateTime NgayCT)
        {
            string zDate = NgayCT.Year + NgayCT.Month.ToString().PadLeft(2, '0') + NgayCT.Day.ToString().PadLeft(2, '0');
            double zTotal = 0;
            string zSQL = "SELECT Count(*) FROM BNK_OrderWeb WHERE NgayKB = @NgayKB ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                string zReslut = zCommand.ExecuteScalar().ToString();
                double.TryParse(zReslut, out zTotal);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTotal;
        }

        public static double OrderOnWeb_TotalLoaiThue(DateTime NgayCT, string LoaiThue)
        {
            string zDate = NgayCT.Year + NgayCT.Month.ToString().PadLeft(2, '0') + NgayCT.Day.ToString().PadLeft(2, '0');
            double zTotal = 0;
            string zSQL = "SELECT Count(*) FROM BNK_OrderWeb WHERE NgayKB = @NgayKB AND LoaiThue = @LoaiTHue";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                zCommand.Parameters.Add("@LoaiThue", SqlDbType.NVarChar).Value = LoaiThue;
                string zReslut = zCommand.ExecuteScalar().ToString();
                double.TryParse(zReslut, out zTotal);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTotal;
        }

        public static DataTable OrderOnWeb_ItemMutil(DateTime NgayCT)
        {
            string zDate = NgayCT.Year + NgayCT.Month.ToString().PadLeft(2, '0') + NgayCT.Day.ToString().PadLeft(2, '0');
            DataTable zTable = new DataTable();
            string zSQL = "SELECT COUNT(1) AS SoLuong,So_CT AS '',SoBT, NgayKB,TKNSNN,MaCQT,SoTK,NgayTK,LHXNK,MaSoThue,TenCTy,TongTien,MaDBHC,LoaiThue "
                        + " FROM[dbo].[BNK_OrderWeb] "
                        + " WHERE NgayKB = @NgayKB"
                        + " GROUP BY NgayKB,TKNSNN,MaCQT,SoTK,NgayTK,LHXNK,MaSoThue,TenCTy,TongTien,MaDBHC,LoaiThue "
                        + " HAVING COUNT(1) > 1 ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable OrderOnWeb_ItemMutil_Detail(string NgayKB, string SoTK, string MaSoThue, double TongTien)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM BNK_OrderWeb "
                + " WHERE NgayKB = @NgayKB AND SoTK = @SoTK AND MaSoThue = @MaSoThue AND TongTien=@TongTien";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = NgayKB;
                zCommand.Parameters.Add("@SoTK", SqlDbType.NVarChar).Value = SoTK;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = MaSoThue;
                zCommand.Parameters.Add("@TongTien", SqlDbType.Money).Value = TongTien;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //----------------SQL-------------------
        public static DataTable OrderOnSQL(DateTime trdate)
        {
            // string zDate = NgayCT.Year + NgayCT.Month.ToString().PadLeft(2, '0') + NgayCT.Day.ToString().PadLeft(2, '0');
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.trref,A.trdate,A.traamt,UPPER(A.ordcust) AS Ordcust, "
                        + " B.SO_BT,B.Ma_ST,UPPER(B.TenCongTy) AS TenCongTy , "
                        + " TK_ThuNS,CQThuID, LoaiThueID , ToKhai,NgayDK,LH_ID,So_CT FROM[dbo].[BNK_OrderTranfer] A "
                        + " LEFT JOIN[BNK_Order] B ON A.trref = B.trref "
                        + " WHERE trdate = @trdate ORDER BY traamt";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = trdate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static double OrderOnSQL_TotalMoney(DateTime trdate)
        {
            double zTotal = 0;
            string zSQL = "SELECT Sum(traamt) FROM BNK_OrderTranfer WHERE trdate = @trdate ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = trdate;
                string zReslut = zCommand.ExecuteScalar().ToString();
                double.TryParse(zReslut, out zTotal);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTotal;
        }
        public static double OrderOnSQL_TotalRecord(DateTime trdate)
        {
            double zTotal = 0;
            string zSQL = "SELECT count(*) FROM BNK_OrderTranfer WHERE trdate = @trdate ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = trdate;
                string zReslut = zCommand.ExecuteScalar().ToString();
                double.TryParse(zReslut, out zTotal);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTotal;
        }

        //----------------IPCAS-------------------
        public static DataTable IPCAS(DateTime trdt)
        {
            string zDate = trdt.ToString("dd/MM/yyyy");
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  A.tomgntno AS tomgntno,A.trdt AS trdt,A.trccyamt AS trccyamt,A.Rem AS Rem,B.ordcust AS ordcust,C.* FROM [dbo].[IPCAS] A "
                        + " LEFT JOIN [dbo].[BNK_OrderTranfer] B ON A.tomgntno = B.trref  "
                        + " LEFT JOIN [dbo].[BNK_Order] C ON B.trref = C.trref "
                        + " WHERE trdt = @trdt ORDER BY A.trccyamt";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trdt", SqlDbType.NVarChar).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable IPCAS_(DateTime trdt)
        {
            string zDate = trdt.ToString("dd/MM/yyyy");
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM [dbo].[IPCAS] WHERE trdt = @trdt ORDER BY trccyamt";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trdt", SqlDbType.NVarChar).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
