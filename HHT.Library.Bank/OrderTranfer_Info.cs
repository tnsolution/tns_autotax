﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class OrderTranfer_Info
    {
        #region [ Field Name ]
        private string _trref = "";
        private DateTime _trdate;
        private string _traccy = "";
        private double _traamt = 0;
        private double _usdamt = 0;
        private string _ordcust = "";
        private string _bencust = "";
        private string _benacct = "";
        private string _remtbk = "";
        private string _rmbsbk = "";
        private string _trfbk = "";
        private string _custno = "";
        private string _traaccd = "";
        private string _thrref = "";
        private DateTime? _remtdt = null;
        private string _remark = "";
        private string _remntbk = "";
        private string _rembk1 = "";
        private string _feebk = "";
        private string _name_1 = "";
        private string _name_2 = "";
        private string _name_3 = "";
        private string _name_4 = "";
        private string _name_7 = "";
        private string _name_9 = "";
        private string _name_10 = "";
        private string _name_15 = "";
        private string _name_16 = "";
        private int _StatusAnalysis = 0;
        private int _SectionNo = 0;
        private string _UserKey;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public OrderTranfer_Info()
        {
        }
        public OrderTranfer_Info(string trref)
        {
            string zSQL = "SELECT * FROM BNK_OrderTranfer WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = trref;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _trref = zReader["trref"].ToString();
                    if (zReader["trdate"] != DBNull.Value)
                        _trdate = (DateTime)zReader["trdate"];
                    else
                        _trdate = new DateTime();
                    _traccy = zReader["traccy"].ToString();
                    _traamt = double.Parse(zReader["traamt"].ToString());
                    _usdamt = double.Parse(zReader["usdamt"].ToString());
                    _ordcust = zReader["ordcust"].ToString();
                    _bencust = zReader["bencust"].ToString();
                    _benacct = zReader["benacct"].ToString();
                    _remtbk = zReader["remtbk"].ToString();
                    _rmbsbk = zReader["rmbsbk"].ToString();
                    _trfbk = zReader["trfbk"].ToString();
                    _custno = zReader["custno"].ToString();
                    _traaccd = zReader["traaccd"].ToString();
                    _thrref = zReader["thrref"].ToString();
                    if (zReader["remtdt"] != DBNull.Value)
                        _remtdt = (DateTime)zReader["remtdt"];
                    _remark = zReader["remark"].ToString().ToUpper();
                    _remntbk = zReader["remntbk"].ToString();
                    _rembk1 = zReader["rembk1"].ToString();
                    _feebk = zReader["feebk"].ToString();
                    _name_1 = zReader["name_1"].ToString();
                    _name_2 = zReader["name_2"].ToString();
                    _name_3 = zReader["name_3"].ToString();
                    _name_4 = zReader["name_4"].ToString();
                    _name_7 = zReader["name_7"].ToString();
                    _name_9 = zReader["name_9"].ToString();
                    _name_10 = zReader["name_10"].ToString();
                    _name_15 = zReader["name_15"].ToString();
                    _name_16 = zReader["name_16"].ToString();
                    _StatusAnalysis = (int)zReader["StatusAnalysis"];
                    _SectionNo = (int)zReader["SectionNo"];
                    _UserKey = zReader["UserKey"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public string trref
        {
            get { return _trref; }
            set { _trref = value; }
        }
        public DateTime trdate
        {
            get { return _trdate; }
            set { _trdate = value; }
        }
        public string traccy
        {
            get { return _traccy; }
            set { _traccy = value; }
        }
        public double traamt
        {
            get { return _traamt; }
            set { _traamt = value; }
        }
        public double usdamt
        {
            get { return _usdamt; }
            set { _usdamt = value; }
        }
        public string ordcust
        {
            get { return _ordcust; }
            set { _ordcust = value; }
        }
        public string bencust
        {
            get { return _bencust; }
            set { _bencust = value; }
        }
        public string benacct
        {
            get { return _benacct; }
            set { _benacct = value; }
        }
        public string remtbk
        {
            get { return _remtbk; }
            set { _remtbk = value; }
        }
        public string rmbsbk
        {
            get { return _rmbsbk; }
            set { _rmbsbk = value; }
        }
        public string trfbk
        {
            get { return _trfbk; }
            set { _trfbk = value; }
        }
        public string custno
        {
            get { return _custno; }
            set { _custno = value; }
        }
        public string traaccd
        {
            get { return _traaccd; }
            set { _traaccd = value; }
        }
        public string thrref
        {
            get { return _thrref; }
            set { _thrref = value; }
        }
        public DateTime? remtdt
        {
            get { return _remtdt; }
            set { _remtdt = value; }
        }
        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }
        public string remntbk
        {
            get { return _remntbk; }
            set { _remntbk = value; }
        }
        public string rembk1
        {
            get { return _rembk1; }
            set { _rembk1 = value; }
        }
        public string feebk
        {
            get { return _feebk; }
            set { _feebk = value; }
        }
        public string name_1
        {
            get { return _name_1; }
            set { _name_1 = value; }
        }
        public string name_2
        {
            get { return _name_2; }
            set { _name_2 = value; }
        }
        public string name_3
        {
            get { return _name_3; }
            set { _name_3 = value; }
        }
        public string name_4
        {
            get { return _name_4; }
            set { _name_4 = value; }
        }
        public string name_7
        {
            get { return _name_7; }
            set { _name_7 = value; }
        }
        public string name_9
        {
            get { return _name_9; }
            set { _name_9 = value; }
        }
        public string name_10
        {
            get { return _name_10; }
            set { _name_10 = value; }
        }
        public string name_15
        {
            get { return _name_15; }
            set { _name_15 = value; }
        }
        public string name_16
        {
            get { return _name_16; }
            set { _name_16 = value; }
        }
        public int StatusAnalysis
        {
            get { return _StatusAnalysis; }
            set { _StatusAnalysis = value; }
        }
        public int SectionNo
        {
            get { return _SectionNo; }
            set { _SectionNo = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO BNK_OrderTranfer ("
            + " trref,trdate ,traccy ,traamt ,usdamt ,ordcust ,bencust ,benacct ,remtbk ,rmbsbk ,trfbk ,custno ,traaccd ,thrref ,remtdt ,remark ,remntbk ,rembk1 ,feebk ,name_1 ,name_2 ,name_3 ,name_4 ,name_7 ,name_9 ,name_10 ,name_15 ,name_16 ,UserKey  ) "
            + " VALUES ( "
            + "@trref,@trdate ,@traccy ,@traamt ,@usdamt ,@ordcust ,@bencust ,@benacct ,@remtbk ,@rmbsbk ,@trfbk ,@custno ,@traaccd ,@thrref ,@remtdt ,@remark ,@remntbk ,@rembk1 ,@feebk ,@name_1 ,@name_2 ,@name_3 ,@name_4 ,@name_7 ,@name_9 ,@name_10 ,@name_15 ,@name_16 ,@UserKey  ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                if (_trdate == null)
                    zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = _trdate;
                zCommand.Parameters.Add("@traccy", SqlDbType.NChar).Value = _traccy;
                zCommand.Parameters.Add("@traamt", SqlDbType.Money).Value = _traamt;
                zCommand.Parameters.Add("@usdamt", SqlDbType.Money).Value = _usdamt;
                zCommand.Parameters.Add("@ordcust", SqlDbType.NVarChar).Value = _ordcust;
                zCommand.Parameters.Add("@bencust", SqlDbType.NVarChar).Value = _bencust;
                zCommand.Parameters.Add("@benacct", SqlDbType.NVarChar).Value = _benacct;
                zCommand.Parameters.Add("@remtbk", SqlDbType.NVarChar).Value = _remtbk;
                zCommand.Parameters.Add("@rmbsbk", SqlDbType.NVarChar).Value = _rmbsbk;
                zCommand.Parameters.Add("@trfbk", SqlDbType.NVarChar).Value = _trfbk;
                zCommand.Parameters.Add("@custno", SqlDbType.NVarChar).Value = _custno;
                zCommand.Parameters.Add("@traaccd", SqlDbType.NVarChar).Value = _traaccd;
                zCommand.Parameters.Add("@thrref", SqlDbType.NVarChar).Value = _thrref;
                if (_remtdt == null)
                    zCommand.Parameters.Add("@remtdt", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@remtdt", SqlDbType.Date).Value = _remtdt;
                zCommand.Parameters.Add("@remark", SqlDbType.NVarChar).Value = _remark;
                zCommand.Parameters.Add("@remntbk", SqlDbType.NVarChar).Value = _remntbk;
                zCommand.Parameters.Add("@rembk1", SqlDbType.NVarChar).Value = _rembk1;
                zCommand.Parameters.Add("@feebk", SqlDbType.NVarChar).Value = _feebk;
                zCommand.Parameters.Add("@name_1", SqlDbType.NVarChar).Value = _name_1;
                zCommand.Parameters.Add("@name_2", SqlDbType.NVarChar).Value = _name_2;
                zCommand.Parameters.Add("@name_3", SqlDbType.NVarChar).Value = _name_3;
                zCommand.Parameters.Add("@name_4", SqlDbType.NVarChar).Value = _name_4;
                zCommand.Parameters.Add("@name_7", SqlDbType.NVarChar).Value = _name_7;
                zCommand.Parameters.Add("@name_9", SqlDbType.NVarChar).Value = _name_9;
                zCommand.Parameters.Add("@name_10", SqlDbType.NVarChar).Value = _name_10;
                zCommand.Parameters.Add("@name_15", SqlDbType.NChar).Value = _name_15;
                zCommand.Parameters.Add("@name_16", SqlDbType.NVarChar).Value = _name_16;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE BNK_OrderTranfer SET "
                        + " trdate = @trdate,"
                        + " traccy = @traccy,"
                        + " traamt = @traamt,"
                        + " usdamt = @usdamt,"
                        + " ordcust = @ordcust,"
                        + " bencust = @bencust,"
                        + " benacct = @benacct,"
                        + " remtbk = @remtbk,"
                        + " rmbsbk = @rmbsbk,"
                        + " trfbk = @trfbk,"
                        + " custno = @custno,"
                        + " traaccd = @traaccd,"
                        + " thrref = @thrref,"
                        + " remtdt = @remtdt,"
                        + " remark = @remark,"
                        + " remntbk = @remntbk,"
                        + " rembk1 = @rembk1,"
                        + " feebk = @feebk,"
                        + " name_1 = @name_1,"
                        + " name_2 = @name_2,"
                        + " name_3 = @name_3,"
                        + " name_4 = @name_4,"
                        + " name_7 = @name_7,"
                        + " name_9 = @name_9,"
                        + " name_10 = @name_10,"
                        + " name_15 = @name_15,"
                        + " name_16 = @name_16,"
                        + " UserKey = @UserKey"
                       + " WHERE trref = @trref";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                if (_trdate == null)
                    zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@trdate", SqlDbType.Date).Value = _trdate;
                zCommand.Parameters.Add("@traccy", SqlDbType.NChar).Value = _traccy;
                zCommand.Parameters.Add("@traamt", SqlDbType.Money).Value = _traamt;
                zCommand.Parameters.Add("@usdamt", SqlDbType.Money).Value = _usdamt;
                zCommand.Parameters.Add("@ordcust", SqlDbType.NVarChar).Value = _ordcust;
                zCommand.Parameters.Add("@bencust", SqlDbType.NVarChar).Value = _bencust;
                zCommand.Parameters.Add("@benacct", SqlDbType.NVarChar).Value = _benacct;
                zCommand.Parameters.Add("@remtbk", SqlDbType.NVarChar).Value = _remtbk;
                zCommand.Parameters.Add("@rmbsbk", SqlDbType.NVarChar).Value = _rmbsbk;
                zCommand.Parameters.Add("@trfbk", SqlDbType.NVarChar).Value = _trfbk;
                zCommand.Parameters.Add("@custno", SqlDbType.NVarChar).Value = _custno;
                zCommand.Parameters.Add("@traaccd", SqlDbType.NVarChar).Value = _traaccd;
                zCommand.Parameters.Add("@thrref", SqlDbType.NVarChar).Value = _thrref;
                if (_remtdt == null)
                    zCommand.Parameters.Add("@remtdt", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@remtdt", SqlDbType.Date).Value = _remtdt;
                zCommand.Parameters.Add("@remark", SqlDbType.NVarChar).Value = _remark;
                zCommand.Parameters.Add("@remntbk", SqlDbType.NVarChar).Value = _remntbk;
                zCommand.Parameters.Add("@rembk1", SqlDbType.NVarChar).Value = _rembk1;
                zCommand.Parameters.Add("@feebk", SqlDbType.NVarChar).Value = _feebk;
                zCommand.Parameters.Add("@name_1", SqlDbType.NVarChar).Value = _name_1;
                zCommand.Parameters.Add("@name_2", SqlDbType.NVarChar).Value = _name_2;
                zCommand.Parameters.Add("@name_3", SqlDbType.NVarChar).Value = _name_3;
                zCommand.Parameters.Add("@name_4", SqlDbType.NVarChar).Value = _name_4;
                zCommand.Parameters.Add("@name_7", SqlDbType.NVarChar).Value = _name_7;
                zCommand.Parameters.Add("@name_9", SqlDbType.NVarChar).Value = _name_9;
                zCommand.Parameters.Add("@name_10", SqlDbType.NVarChar).Value = _name_10;
                zCommand.Parameters.Add("@name_15", SqlDbType.NChar).Value = _name_15;
                zCommand.Parameters.Add("@name_16", SqlDbType.NVarChar).Value = _name_16;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_trref.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_OrderTranfer WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
        public string UpdateStatus()
        {
            string zSQL = "UPDATE BNK_OrderTranfer SET "
                        + " StatusAnalysis = @StatusAnalysis "
                        + " WHERE trref = @trref";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = _StatusAnalysis;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateMST(string MST)
        {
            string zSQL = "UPDATE BNK_OrderTranfer SET "
                        + " MST = @MST "
                        + " WHERE trref = @trref";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@CategoryOrder", SqlDbType.NVarChar).Value = MST;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
