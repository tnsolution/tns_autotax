﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class Order_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM BNK_Order ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int SectionNo, int StatusOrder, int StatusAnalysis, string LoaiThueID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*, B.ordcust,B.remark FROM BNK_Order A"
                + " LEFT JOIN BNK_OrderTranfer B ON A.trref = B.trref"
                + " WHERE A.SectionNo=@SectionNo ";
            if (StatusOrder >= 0)
                zSQL += " AND A.OrderStatus = @OrderStatus";
            if (StatusAnalysis > 0)
                zSQL += " AND A.StatusAnalysis = @StatusAnalysis";
            if (LoaiThueID != "00")
                zSQL += " AND A.LoaiThueID = @LoaiThueID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = StatusOrder;
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = StatusAnalysis;
                zCommand.Parameters.Add("@LoaiThueID", SqlDbType.NVarChar).Value = LoaiThueID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int SectionNo, int StatusOrder)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM BNK_Order "
                + " WHERE SectionNo=@SectionNo ";
            if (StatusOrder >= 0)
                zSQL += " AND OrderStatus = @OrderStatus";
           
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = StatusOrder;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListOrderDetail(string OrderID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM BNK_Order_Detail WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = OrderID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Amount_So_CT_OnWeb(string So_CT)
        {
            int zAmount = 0;
            string zSQL = "SELECT count(*) AS Amount  FROM [dbo].[BNK_Order] WHERE @SO_CT = SO_CT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@So_CT", SqlDbType.NVarChar).Value = So_CT;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    zAmount = int.Parse(zReader["Amount"].ToString());

                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                string _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
            return zAmount;
        }

        public static DataTable List_CT(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = "SELECT So_BT AS SoBT, So_CT AS SoCT , Ma_ST AS MST , ToKhai AS ToKhai , NgayDK AS NgayDK , LH_ID AS LHID, SoTien AS SoTien FROM BNK_Order  WHERE CreatedOn BETWEEN @FromDate AND @ToDate ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_CT_Excel(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            string zDate = zFromDate.Year + zFromDate.Month.ToString().PadLeft(2, '0') + zFromDate.Day.ToString().PadLeft(2, '0');
            string zSQL = @"
SELECT  SoCT AS SoCT , SoTK AS SoTK , SoTK AS SoTK, Ngay AS Ngay, LHXNK AS LHXNK ,
TenLH AS TenLH ,MaSoThue AS MaSoThue ,TenCTy AS TenCTy , TongTien AS TongTien
FROM BNK_OrderWeb  WHERE NgayKB = @NgayKB
Group By [SoCT],[SoTK],[Ngay],[LHXNK],[TenLH],[MaSoThue],[TenCTy],[TongTien]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }



        public static DataTable List_Excel(DateTime FromDate, DateTime ToDate, double MoneyFrom, double MoneyTo, int Catergory)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            string zDate = zFromDate.Year + zFromDate.Month.ToString().PadLeft(2, '0') + zFromDate.Day.ToString().PadLeft(2, '0');
            string zSQL = @"SELECT  SoCT AS SoCT , SoTK AS SoTK , SoTK AS SoTK, Ngay AS Ngay, LHXNK AS LHXNK ,";
            zSQL += "TenLH AS TenLH ,MaSoThue AS MaSoThue ,TenCTy AS TenCTy , TongTien AS TongTien ";
            zSQL += "FROM BNK_OrderWeb WHERE NgayKB = @NgayKB ";
            if (MoneyTo > 0)
            {
                zSQL += " AND TongTien BETWEEN @MoneyFrom AND @MoneyTo ";
            }
            if (Catergory > 0)
            {
                zSQL += "";
            }

            zSQL += "Group By[SoCT],[SoTK],[Ngay],[LHXNK],[TenLH],[MaSoThue],[TenCTy],[TongTien]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NgayKB", SqlDbType.NVarChar).Value = zDate;
                zCommand.Parameters.Add("@MoneyFrom", SqlDbType.Money).Value = MoneyFrom;
                zCommand.Parameters.Add("@MoneyTo", SqlDbType.Money).Value = MoneyTo;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable List_CT(DateTime FromDate, DateTime ToDate, double MoneyFrom, double MoneyTo, int Catergory)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = @"SELECT So_BT AS SoBT, So_CT AS SoCT , Ma_ST AS MST ,
ToKhai AS ToKhai , NgayDK AS NgayDK ,LH_ID AS LHID, SoTien AS SoTien FROM BNK_Order A 
LEFT JOIN [dbo].[BNK_OrderTranfer] B ON A.trref = B.trref 
WHERE B.trdate BETWEEN @FromDate AND @ToDate ";
            if (MoneyTo > 0)
                zSQL += @"AND SoTien BETWEEN @MoneyFrom AND @MoneyTo";
            if (Catergory > 0)
                zSQL += @"";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = zToDate;
                zCommand.Parameters.Add("@MoneyFrom", SqlDbType.Money).Value = MoneyFrom;
                zCommand.Parameters.Add("@MoneyTo", SqlDbType.Money).Value = MoneyTo;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable CheckVer2(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"SELECT So_BT AS SoBT, So_CT AS SoCT , Ma_ST AS MST ,SoTien AS SoTien FROM BNK_Order 
WHERE CreatedOn BETWEEN @FromDate AND @ToDate AND StatusAnalysis = 2 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
