﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;
using System.Collections;

namespace HHT.Library.Bank
{
    public class Rule_Data
    {
        #region [ Old Code]
        public static DataTable Item_Categories()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM Item_Categories ORDER BY Rank,Style";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable RuleGroup(int GroupID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM Rule_Group WHERE GroupID = @GroupID ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = GroupID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #endregion

        public static DataTable List_Rule(int CategoryID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM Rule_Format WHERE CategoryID = @CategoryID ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string Get_Rule_Format(string ID)
        {
            string zResult = "";
            string zSQL = "SELECT * FROM Rule_Format WHERE RuleID = @RuleID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            zCommand.Parameters.Add("@RuleID", SqlDbType.NVarChar).Value = ID;
            SqlDataReader zReader = zCommand.ExecuteReader();
            if (zReader.HasRows)
            {
                zReader.Read();
                zResult = zReader["Format"].ToString();
            }
            return zResult;
        }
        public static void AnalysisRule(string RuleID, string StringSpecial, Order_Object Order)
        {
            Order.RuleID = RuleID;
            Order.Remark_Items = new List<Item_Bill>();
            Order.Items = new List<Order_Detail_Info>();

            if (RuleID == "10001" || RuleID == "10002" || RuleID == "10003")
            {
                RuleID = "10000";
            }
            switch (RuleID)
            {
                case "10000":
                    if (Order.Remark.Contains("(C") || Order.Remark.Contains("(TM"))
                        RunRule_10001(Order, StringSpecial[0]);
                    else
                        RunRule_10011(Order, StringSpecial[0]);
                    break;

                case "80001":
                    RunRule_80001(Order); // Rule Char-Number
                    break;
                case "30000":
                    RunRule_30000(Order, StringSpecial);
                    break;
                case "60000":
                    RunRule_60000(Order, StringSpecial);
                    break;
                case "90001":
                    RunRule_90001(Order);
                    break;
            }
        }
        public static void RunRule_10001(Order_Object Order, char CharSpecial)
        {
            List<string> zListGroup = new List<string>();

            int zBegin, zEnd;
            bool zFirst = true;

            zBegin = Order.Remark.IndexOf('(');
            while (zBegin > 0)
            {
                zEnd = Order.Remark.IndexOf(')', zBegin);
                if (zFirst)
                {
                    zListGroup.Add(Order.Remark.Substring(0, zBegin));
                    zFirst = false;
                }
                if (zEnd > 0)
                {
                    zListGroup.Add(Order.Remark.Substring(zBegin + 1, zEnd - zBegin - 1));
                }
                else
                {
                    zEnd = Order.Remark.Length - 1;
                    zListGroup.Add(Order.Remark.Substring(zBegin + 1, zEnd - zBegin - 1));
                }

                zBegin = Order.Remark.IndexOf('(', zEnd);
            }
            string[] zHeaderItem;
            if (!zFirst)
                zHeaderItem = zListGroup[0].Split(CharSpecial);
            else
                zHeaderItem = Order.Remark.Split('+');

            Item_Bill zItem = new Item_Bill();
            foreach (string zOneLine in zHeaderItem)
            {
                zItem = Function.SplipCodeAndValue(zOneLine);
                if (zItem.Content.Trim().Length > 0)
                    Order.Remark_Items.Add(zItem);
            }
            #region[ Find Header]
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                string zValue = "";
                DateTime zDate;
                if (Order.Header.NgayNT == null)
                {
                    if (CheckItemNgayNT(zItem.Content, zItem.Value, out zDate))
                    {
                        zItem.CategoryID = 8;
                        zItem.CategoryName = "Ngày Nộp Tiền";
                        zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                        Order.Header.NgayNT = zDate;
                        goto Next;
                    }
                }
                if (Order.Header.MST.Length == 0)
                {
                    if (CheckItemMST(zItem.Content, zItem.Value, out zValue))
                    {
                        zItem.CategoryID = 1;
                        zItem.CategoryName = "Mã Số Thuế";
                        zItem.CorrectValue = zValue;
                        Order.Header.MST = zValue;
                        goto Next;
                    }
                }
                if (Order.Header.DVSDNS_ID.Length == 0)
                {
                    if (CheckItemDVSDNS(zItem.Content, zItem.Value, out zValue))
                    {
                        string zTK_KhoBac = Order.Header.TKThuNS_ID + ".0." + zValue;
                        GovTax_Account_Info zTaxAccount = new GovTax_Account_Info(zTK_KhoBac);

                        if (zTaxAccount.AccountName.Trim().Length > 5)
                        {
                            Order.Header.TKThuNS_ID = zTaxAccount.TKNS;
                            Order.Header.TKThuNS_Name = zTaxAccount.AccountName;
                            Order.Header.CQThu_ID = zTaxAccount.DepartmentID;
                            Order.Header.CQThu_Name = zTaxAccount.DepartmentName;
                            Order.Header.DVSDNS_ID = zTaxAccount.QHNS;
                            Order.Header.LoaiThue_ID = zTaxAccount.CategoryTax;
                            Order.Header.NHNN_ID = zTaxAccount.TreasuryID;
                            Order.Header.NHNN_Name = zTaxAccount.TreasuryName;
                            Order.Header.DBHC_ID = zTaxAccount.DBHC;
                            zItem.CategoryName = "DVSDNS + CQT";
                            zItem.CategoryID = 9;
                            goto Next;
                        }
                    }
                }
                if (Order.Header.LoaiThue_ID == "04")
                {
                    if (Order.Header.ToKhai.Length == 0)
                    {
                        if (CheckItemToKhai(zItem.Content, zItem.Value, out zValue))
                        {
                            zItem.CategoryID = 2;
                            zItem.CategoryName = "Tờ Khai";
                            zItem.CorrectValue = zValue;
                            Order.Header.ToKhai = zValue;
                            goto Next;
                        }
                    }
                    if (Order.Header.NgayDK == null)
                    {
                        if (CheckItemNgayDK(zItem.Content, zItem.Value, out zDate))
                        {
                            zItem.CategoryID = 13;
                            zItem.CategoryName = "Ngày Đăng Ký";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayDK = zDate;
                            goto Next;
                        }
                    }
                    if (Order.Header.LH_ID.Length == 0)
                    {
                        string zLH_ID;
                        string zLH_Name;

                        if (CheckItemLHXNK(zItem.Content, zItem.Value, out zLH_ID, out zLH_Name))
                        {
                            zItem.CategoryID = 3;
                            zItem.CategoryName = "LH XNK";
                            zItem.CorrectID = zLH_ID;
                            zItem.CorrectName = zLH_Name;

                            Order.Header.LH_ID = zLH_ID;
                            Order.Header.LH_Name = zLH_Name;
                            goto Next;
                        }
                    }
                }

            Next:
                ;
            }
            #endregion

            #region [ Find Chuong - Tieu Muc - So tien - ky theu ]
            int k = Order.Remark_Items.Count;

            for (int i = 1; i < zListGroup.Count; i++)
            {
                char zChar = '+';
                int zBegin1 = zListGroup[i].IndexOf('+');
                int zBegin2 = zListGroup[i].IndexOf('-');
                if (zBegin1 == -1 && zBegin2 > 0)
                {
                    zChar = '-';
                }
                else
                {
                    if (zBegin1 > 0 && zBegin2 > 0 && zBegin2 < zBegin1)
                        zChar = '-';
                }

                string[] zDetailItem = zListGroup[i].Split(zChar);
                Order_Detail_Info zOrderItem = new Order_Detail_Info();
                string zID, zName;
                double zSoTien = 0;
                string zValue;
                if (zDetailItem.Length == 3)
                {
                    if (zDetailItem[0].Substring(0, 2) == "TM" && zDetailItem[1].Substring(0, 2) == "ST" && zDetailItem[2].Substring(0, 1) == "T")
                    {
                        zDetailItem[2] = zDetailItem[2].Insert(0, "S");
                    }
                }
                foreach (string zOneLine in zDetailItem)
                {
                    zItem = Function.SplipCodeAndValue(zOneLine);
                    if (zItem.Content.Trim().Length > 0)
                    {
                        if (zOrderItem.ChuongID.Length == 0)
                        {
                            if (CheckItemChuong(zItem.Content, zItem.Value, out zID, out zName))
                            {
                                zItem.CategoryID = 12;
                                zItem.CategoryName = "Chương";
                                zItem.CorrectID = zID;
                                zItem.CorrectName = zName;

                                zOrderItem.ChuongID = zID;
                                zOrderItem.ChuongName = zName;

                                goto Next;
                            }
                        }
                        if (zOrderItem.NDKT_ID.Length == 0)
                        {
                            if (CheckItemNDKT(zItem.Content, zItem.Value, out zID, out zName))
                            {
                                zItem.CategoryID = 10;
                                zItem.CategoryName = "Tiểu Mục";
                                zItem.CorrectID = zID;
                                zItem.CorrectName = zName;
                                zOrderItem.NDKT_ID = zID;
                                zOrderItem.NDKT_Name = zName;
                                goto Next;
                            }
                        }
                        if (zOrderItem.KyThue.Length == 0)
                        {
                            if (CheckItemKyThue(zItem.Content, zItem.Value, out zValue))
                            {
                                zItem.CategoryID = 14;
                                zItem.CategoryName = "Kỳ Thuế";
                                zItem.CorrectValue = zValue;
                                zOrderItem.KyThue = zValue;
                                goto Next;
                            }
                        }
                        if (zOrderItem.SoTien == 0)
                        {
                            if (CheckItemSoTien(zItem.Content, zItem.Value, out zSoTien))
                            {
                                zItem.CategoryID = 11;
                                zItem.CategoryName = "Số Tiền";
                                zItem.CorrectValue = zSoTien.ToString("#,###,###");
                                zOrderItem.SoTien = zSoTien;
                                goto Next;
                            }
                        }

                    Next:
                        ;
                        Order.Remark_Items.Add(zItem);
                    }
                }
                if (zOrderItem.NDKT_ID.Length > 0 && zOrderItem.SoTien > 0)
                    Order.Items.Add(zOrderItem);
            }
            k = 0;
            for (int i = 0; i < Order.Items.Count; i++)
            {
                if (Order.Items[i].ChuongID.Length == 0)
                {
                    k++;
                }
            }
            if (k == Order.Items.Count)
            {
                bool zFound = false;
                for (int i = 0; i < Order.Remark_Items.Count; i++)
                {
                    zItem = Order.Remark_Items[i];
                    string zID = "";
                    string zName = "";
                    if (zItem.CategoryID == 0)
                    {
                        if (CheckItemChuong(zItem.Content, zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryID = 12;
                            zItem.CategoryName = "Chương";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;
                            zFound = true;
                            break;
                        }
                    }

                }
                if (zFound)
                {
                    for (int i = 0; i < Order.Items.Count; i++)
                    {
                        Order.Items[i].ChuongID = zItem.CorrectID;
                        Order.Items[i].ChuongName = zItem.CorrectName;
                    }
                }
            }
            #endregion

        }
        public static void RunRule_10011(Order_Object Order, char CharSpecial)
        {
            string zValue = "";
            string[] zListItem = Order.Remark.Split(CharSpecial);

            Item_Bill zItem = new Item_Bill();
            foreach (string zOneLine in zListItem)
            {
                zItem = Function.SplipCodeAndValue(zOneLine);
                if (zItem.Content.Trim().Length > 0)
                    Order.Remark_Items.Add(zItem);
            }
            #region [ Check Infor Normal ]
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                zValue = "";
                DateTime zDate;
                if (Order.Header.NgayNT == null)
                {
                    if (CheckItemNgayNT(zItem.Content, zItem.Value, out zDate))
                    {
                        zItem.CategoryID = 8;
                        zItem.CategoryName = "Ngày Nộp Tiền";
                        zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                        Order.Header.NgayNT = zDate;
                        goto Next;
                    }
                }
                if (Order.Header.MST.Length == 0)
                {
                    if (CheckItemMST(zItem.Content, zItem.Value, out zValue))
                    {
                        zItem.CategoryID = 1;
                        zItem.CategoryName = "Mã Số Thuế";
                        zItem.CorrectValue = zValue;
                        Order.Header.MST = zValue;
                        goto Next;
                    }
                }
                if (Order.Header.DVSDNS_ID.Length == 0)
                {
                    if (CheckItemDVSDNS(zItem.Content, zItem.Value, out zValue))
                    {
                        string zTK_KhoBac = Order.Header.TKThuNS_ID + ".0." + zValue;
                        GovTax_Account_Info zTaxAccount = new GovTax_Account_Info(zTK_KhoBac);

                        if (zTaxAccount.AccountName.Trim().Length > 5)
                        {
                            Order.Header.TKThuNS_ID = zTaxAccount.TKNS;
                            Order.Header.TKThuNS_Name = zTaxAccount.AccountName;
                            Order.Header.CQThu_ID = zTaxAccount.DepartmentID;
                            Order.Header.CQThu_Name = zTaxAccount.DepartmentName;
                            Order.Header.DVSDNS_ID = zTaxAccount.QHNS;
                            Order.Header.LoaiThue_ID = zTaxAccount.CategoryTax;
                            Order.Header.NHNN_ID = zTaxAccount.TreasuryID;
                            Order.Header.NHNN_Name = zTaxAccount.TreasuryName;
                            Order.Header.DBHC_ID = zTaxAccount.DBHC;
                            zItem.CategoryName = "DVSDNS + CQT";
                            zItem.CategoryID = 9;
                            goto Next;
                        }
                    }
                }

                if (Order.Header.LoaiThue_ID == "04")
                {
                    if (Order.Header.ToKhai.Length == 0)
                    {
                        if (CheckItemToKhai(zItem.Content, zItem.Value, out zValue))
                        {
                            zItem.CategoryID = 2;
                            zItem.CategoryName = "Tờ Khai";
                            zItem.CorrectValue = zValue;
                            Order.Header.ToKhai = zValue;
                            goto Next;
                        }
                    }
                    if (Order.Header.NgayDK == null)
                    {
                        if (CheckItemNgayDK(zItem.Content, zItem.Value, out zDate))
                        {
                            zItem.CategoryID = 13;
                            zItem.CategoryName = "Ngày Đăng Ký";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayDK = zDate;
                            goto Next;
                        }
                    }
                    if (Order.Header.LH_ID.Length == 0)
                    {
                        string zLH_ID;
                        string zLH_Name;

                        if (CheckItemLHXNK(zItem.Content, zItem.Value, out zLH_ID, out zLH_Name))
                        {
                            zItem.CategoryID = 3;
                            zItem.CategoryName = "LH XNK";
                            zItem.CorrectID = zLH_ID;
                            zItem.CorrectName = zLH_Name;

                            Order.Header.LH_ID = zLH_ID;
                            Order.Header.LH_Name = zLH_Name;
                            goto Next;
                        }
                    }
                }

            Next:
                ;
            }
            string zID, zName;
            double zSoTien = 0;
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                if (zItem.CategoryID == 0)
                {

                    if (CheckItemChuong(zItem.Content, zItem.Value, out zID, out zName))
                    {
                        zItem.CategoryID = 12;
                        zItem.CategoryName = "Chương";
                        zItem.CorrectID = zID;
                        zItem.CorrectName = zName;

                    }

                    if (CheckItemNDKT(zItem.Content, zItem.Value, out zID, out zName))
                    {
                        zItem.CategoryID = 10;
                        zItem.CategoryName = "Tiểu Mục";
                        zItem.CorrectID = zID;
                        zItem.CorrectName = zName;
                    }

                    if (CheckItemKyThue(zItem.Content, zItem.Value, out zValue))
                    {
                        zItem.CategoryID = 14;
                        zItem.CategoryName = "Kỳ Thuế";
                        zItem.CorrectValue = zValue;
                    }

                    if (CheckItemSoTien(zItem.Content, zItem.Value, out zSoTien))
                    {
                        zItem.CategoryID = 11;
                        zItem.CategoryName = "Số Tiền";
                        zItem.CorrectValue = zSoTien.ToString();
                    }

                }
            }
            #endregion

            #region [ Check Miss ]
            if (Order.Header.ToKhai.Length == 0)
            {
                for (int i = 0; i < Order.Remark_Items.Count; i++)
                {
                    zItem = Order.Remark_Items[i];
                    if (zItem.CategoryID == 0)
                    {
                        string zRemain = "";
                        if (CheckItemToKhai_Ext_1(zItem.Content, zItem.Value, out zValue, out zRemain))
                        {
                            zItem.CategoryID = 2;
                            zItem.CategoryName = "Tờ Khai";
                            zItem.CorrectValue = zValue;
                            Order.Header.ToKhai = zValue;
                            int zIndex = i;
                            Item_Bill zItemNext = Function.SplipCodeAndValue(zRemain);
                            Order.Remark_Items.Insert(zIndex + 1, zItemNext);
                        }
                    }
                }
            }
            if (Order.Header.NgayDK == null)
            {
                for (int i = 0; i < Order.Remark_Items.Count; i++)
                {
                    zItem = Order.Remark_Items[i];
                    if (zItem.CategoryID == 2)
                    {
                        if (i < Order.Remark_Items.Count - 1)
                        {
                            Item_Bill zItemNgayDK = Order.Remark_Items[i + 1];
                            string zstrDate = Function.GetCorrectDate(zItemNgayDK.Value);
                            DateTime zDate;
                            if (Function.ConvertDateVN(zstrDate, out zDate))
                            {
                                zItemNgayDK.CategoryID = 13;
                                zItemNgayDK.CategoryName = "Ngày Đăng Ký";
                                zItemNgayDK.CorrectValue = zDate.ToString("dd/MM/yyyy");
                                Order.Header.NgayDK = zDate;
                                break;
                            }
                        }
                    }
                }
            }
            #endregion

            #region [ Check Detail ]
            List<Item_Bill> zChuong = new List<Item_Bill>();
            List<Item_Bill> zTM = new List<Item_Bill>();
            List<Item_Bill> zST = new List<Item_Bill>();
            List<Item_Bill> zKT = new List<Item_Bill>();
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                if (zItem.CategoryID == 12)
                    zChuong.Add(zItem);
                if (zItem.CategoryID == 10)
                    zTM.Add(zItem);
                if (zItem.CategoryID == 14)
                    zKT.Add(zItem);
                if (zItem.CategoryID == 11)
                    zST.Add(zItem);
            }
            int zMax = zChuong.Count;
            if (zTM.Count > zMax)
                zMax = zTM.Count;

            for (int i = 0; i < zMax; i++)
            {
                Order_Detail_Info zOrderItem = new Order_Detail_Info();
                if (i <= zChuong.Count - 1)
                {
                    zOrderItem.ChuongID = zChuong[i].CorrectID;
                    zOrderItem.ChuongName = zChuong[i].CorrectName;
                }
                if (i <= zTM.Count - 1)
                {
                    zOrderItem.NDKT_ID = zTM[i].CorrectID;
                    zOrderItem.NDKT_Name = zTM[i].CorrectName;
                }
                if (i <= zKT.Count - 1)
                {
                    zOrderItem.KyThue = zKT[i].CorrectValue;
                }
                if (i <= zST.Count - 1)
                {
                    zOrderItem.SoTien = double.Parse(zST[i].CorrectValue);
                }
                Order.Items.Add(zOrderItem);
            }

            #endregion

        }
        public static void RunRule_80001(Order_Object Order)
        {
            string zValue = "";
            Order.RuleID = "80001";
            List<string> zListItem = Function.Split_All_Item(Order.Remark.ToUpper());
            Item_Bill zItem = new Item_Bill();
            for (int i = 0; i < zListItem.Count; i++)
            {
                zItem = new Item_Bill();
                zItem.Content = zListItem[i];
                if (i < zListItem.Count - 1)
                {
                    i++;
                    zItem.Value = RemoveChar(zListItem[i]);
                }
                Order.Remark_Items.Add(zItem);
            }
          
            #region [ Check Infor Normal ]
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                zValue = "";
                DateTime zDate;
                if (Order.Header.NgayNT == null)
                {
                    if (CheckItemNgayNT(zItem.Content, zItem.Value, out zDate))
                    {
                        zItem.CategoryID = 8;
                        zItem.CategoryName = "Ngày Nộp Tiền";
                        zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                        Order.Header.NgayNT = zDate;
                        goto Next;
                    }
                }
                if (Order.Header.MST.Length == 0)
                {
                    if (CheckItemMST(zItem.Content, zItem.Value, out zValue))
                    {
                        zItem.CategoryID = 1;
                        zItem.CategoryName = "Mã Số Thuế";
                        zItem.CorrectValue = zValue;
                        Order.Header.MST = zValue;
                        goto Next;
                    }
                }
                if (Order.Header.DVSDNS_ID.Length == 0)
                {
                    if (CheckItemDVSDNS(zItem.Content, zItem.Value, out zValue))
                    {
                        string zTK_KhoBac = Order.Header.TKThuNS_ID + ".0." + zValue;
                        GovTax_Account_Info zTaxAccount = new GovTax_Account_Info(zTK_KhoBac);

                        if (zTaxAccount.AccountName.Trim().Length > 5)
                        {
                            Order.Header.TKThuNS_ID = zTaxAccount.TKNS;
                            Order.Header.TKThuNS_Name = zTaxAccount.AccountName;
                            Order.Header.CQThu_ID = zTaxAccount.DepartmentID;
                            Order.Header.CQThu_Name = zTaxAccount.DepartmentName;
                            Order.Header.DVSDNS_ID = zTaxAccount.QHNS;
                            Order.Header.LoaiThue_ID = zTaxAccount.CategoryTax;
                            Order.Header.NHNN_ID = zTaxAccount.TreasuryID;
                            Order.Header.NHNN_Name = zTaxAccount.TreasuryName;
                            Order.Header.DBHC_ID = zTaxAccount.DBHC;
                            zItem.CategoryName = "DVSDNS + CQT";
                            zItem.CategoryID = 9;
                            goto Next;
                        }
                    }
                }

                if (Order.Header.LoaiThue_ID == "04")
                {
                    if (Order.Header.ToKhai.Length == 0)
                    {
                        if (CheckItemToKhai(zItem.Content, zItem.Value, out zValue))
                        {
                            zItem.CategoryID = 2;
                            zItem.CategoryName = "Tờ Khai";
                            zItem.CorrectValue = zValue;
                            Order.Header.ToKhai = zValue;
                            goto Next;
                        }
                    }
                    if (Order.Header.NgayDK == null)
                    {
                        
                        if (CheckItemNgayDK(zItem.Content, zItem.Value, out zDate))
                        {
                            zItem.CategoryID = 13;
                            zItem.CategoryName = "Ngày Đăng Ký";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayDK = zDate;
                            goto Next;
                        }
                        else
                        {
                            if (zItem.Content == "N")
                            {
                                if (CheckItemNgayDK("NDK", zItem.Value, out zDate))
                                {
                                    zItem.CategoryID = 13;
                                    zItem.CategoryName = "Ngày Đăng Ký";
                                    zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                                    Order.Header.NgayDK = zDate;
                                    goto Next;
                                }
                            }
                        }

                    }
                    if (Order.Header.LH_ID.Length == 0)
                    {
                        string zLH_ID;
                        string zLH_Name;

                        if (CheckItemLHXNK(zItem.Content, zItem.Value, out zLH_ID, out zLH_Name))
                        {
                            zItem.CategoryID = 3;
                            zItem.CategoryName = "LH XNK";
                            zItem.CorrectID = zLH_ID;
                            zItem.CorrectName = zLH_Name;

                            Order.Header.LH_ID = zLH_ID;
                            Order.Header.LH_Name = zLH_Name;
                            goto Next;
                        }
                    }
                }

            Next:
                ;
            }
            string zID, zName;
            double zSoTien = 0;
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                if (zItem.CategoryID == 0)
                {

                    if (CheckItemChuong(zItem.Content, zItem.Value, out zID, out zName))
                    {
                        zItem.CategoryID = 12;
                        zItem.CategoryName = "Chương";
                        zItem.CorrectID = zID;
                        zItem.CorrectName = zName;

                    }

                    if (CheckItemNDKT(zItem.Content, zItem.Value, out zID, out zName))
                    {
                        zItem.CategoryID = 10;
                        zItem.CategoryName = "Tiểu Mục";
                        zItem.CorrectID = zID;
                        zItem.CorrectName = zName;
                    }

                    if (CheckItemKyThue(zItem.Content, zItem.Value, out zValue))
                    {
                        zItem.CategoryID = 14;
                        zItem.CategoryName = "Kỳ Thuế";
                        zItem.CorrectValue = zValue;
                    }

                    if (CheckItemSoTien(zItem.Content, zItem.Value, out zSoTien))
                    {
                        zItem.CategoryID = 11;
                        zItem.CategoryName = "Số Tiền";
                        zItem.CorrectValue = zSoTien.ToString();
                    }

                }
            }
            #endregion
            Item_Bill zItem_TK;
            Item_Bill zItem_NDK;
            Item_Bill zItem_NT;
            DateTime zDateCheck;
            List<int> zDateIndex = new List<int>();
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                zValue = "";
                if (zItem.Content == "NGAY")
                {
                    zDateIndex.Add(i);
                }
            }
            if (zDateIndex.Count == 2)
            {
                if (zDateIndex[0] >= 1)
                {
                    zItem_TK = Order.Remark_Items[zDateIndex[0] - 1];
                    if (zItem_TK.CategoryID == 2)
                    {
                        //Xóa dấu cuối cùng
                        Order.Remark_Items[zDateIndex[0]].Value = RemoveChar(Order.Remark_Items[zDateIndex[0]].Value);
                        //
                        if ( CheckItemNgayDK("NDK", Order.Remark_Items[zDateIndex[0]].Value, out zDateCheck))
                        {
                            Order.Remark_Items[zDateIndex[0]].CategoryName = "Ngày đăng kí";
                            Order.Remark_Items[zDateIndex[0]].CategoryID = 2;
                            Order.Remark_Items[zDateIndex[0]].CorrectValue = zDateCheck.ToString("dd/MM/yyyy");
                            Order.Header.NgayDK = zDateCheck;
                        }
                        if (CheckItemNgayNT("NNT", Order.Remark_Items[zDateIndex[1]].Value, out zDateCheck))
                        {
                            Order.Remark_Items[zDateIndex[1]].CategoryName = "Ngày nộp tiền";
                            Order.Remark_Items[zDateIndex[1]].CategoryID = 8;
                            Order.Remark_Items[zDateIndex[1]].CorrectValue = zDateCheck.ToString("dd/MM/yyyy");
                            Order.Header.NgayNT = zDateCheck;
                        }
                    }
                    else
                    {
                        zItem_TK = Order.Remark_Items[zDateIndex[1] - 1];
                        if (zItem_TK.CategoryID == 2)
                        {
                            //Xóa dấu cuối cùng
                             Order.Remark_Items[zDateIndex[1]].Value = RemoveChar(Order.Remark_Items[zDateIndex[1]].Value);
                            
                            if (CheckItemNgayDK("NDK", Order.Remark_Items[zDateIndex[1]].Value, out zDateCheck))
                            {
                                Order.Remark_Items[zDateIndex[1]].CategoryName = "Ngày đăng kí";
                                Order.Remark_Items[zDateIndex[1]].CategoryID = 2;
                                Order.Remark_Items[zDateIndex[1]].CorrectValue = zDateCheck.ToString("dd/MM/yyyy");
                                Order.Header.NgayDK = zDateCheck;

                            }
                            if (CheckItemNgayNT("NNT", Order.Remark_Items[zDateIndex[0]].Value, out zDateCheck))
                            {
                                Order.Remark_Items[zDateIndex[0]].CategoryName = "Ngày Nộp Tiền";
                                Order.Remark_Items[zDateIndex[0]].CategoryID = 8;
                                Order.Remark_Items[zDateIndex[0]].CorrectValue = zDateCheck.ToString("dd/MM/yyyy");
                                Order.Header.NgayNT = zDateCheck;
                            }
                        }
                    }

                }
            }

            #region [ Check Detail ]
            List<Item_Bill> zChuong = new List<Item_Bill>();
            List<Item_Bill> zTM = new List<Item_Bill>();
            List<Item_Bill> zST = new List<Item_Bill>();
            List<Item_Bill> zKT = new List<Item_Bill>();
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                if (zItem.CategoryID == 12)
                    zChuong.Add(zItem);
                if (zItem.CategoryID == 10)
                    zTM.Add(zItem);
                if (zItem.CategoryID == 14)
                    zKT.Add(zItem);
                if (zItem.CategoryID == 11)
                    zST.Add(zItem);
            }
            int zMax = zChuong.Count;
            if (zTM.Count > zMax)
                zMax = zTM.Count;

            for (int i = 0; i < zMax; i++)
            {
                Order_Detail_Info zOrderItem = new Order_Detail_Info();
                if (i <= zChuong.Count - 1)
                {
                    zOrderItem.ChuongID = zChuong[i].CorrectID;
                    zOrderItem.ChuongName = zChuong[i].CorrectName;
                }
                if (i <= zTM.Count - 1)
                {
                    zOrderItem.NDKT_ID = zTM[i].CorrectID;
                    zOrderItem.NDKT_Name = zTM[i].CorrectName;
                }
                if (i <= zKT.Count - 1)
                {
                    zOrderItem.KyThue = zKT[i].CorrectValue;
                }
                if (i <= zST.Count - 1)
                {
                    zOrderItem.SoTien = double.Parse(zST[i].CorrectValue);
                }
                Order.Items.Add(zOrderItem);
            }

            #endregion
        }


        public static void RunRule_30000(Order_Object Order, string Format_Data)
        {
            string zValue = "";
            // Order.RuleID = "30001";

            string[] zFormat = Format_Data.Split('|');
            string[] zFormat_Index = new string[zFormat.Length];

            #region [ Analysic ]
            Item_Bill zItemPre;
            Item_Bill zItemNow;
            string[] zOne;

            zItemPre = new Item_Bill();
            zOne = zFormat[0].Split('#');
            zItemPre.Content = zOne[0].Trim();
            zItemPre.CategoryID = int.Parse(zOne[1]);
            zItemPre.Index = Order.Remark.IndexOf(zItemPre.Content);

            for (int i = 1; i < zFormat.Length - 1; i++)
            {
                zOne = zFormat[i].Split('#');
                zItemNow = new Item_Bill();

                zItemNow.Content = zOne[0].Trim();
                zItemNow.CategoryID = int.Parse(zOne[1]);
                int zLenNext = 0;
                if (zItemNow.Content[0] == '*')
                {
                    zLenNext = int.Parse(zItemNow.Content.Substring(1, 2));
                    zItemNow.Index = zItemPre.Index + 1 + zLenNext;
                    zItemNow.Content = "";
                }
                else
                    zItemNow.Index = Order.Remark.IndexOf(zItemNow.Content, zItemPre.Index + zItemPre.Content.Length);

                if (zItemNow.Index < 0)
                {
                    Order.Remark_Items.Add(zItemPre);
                    break;
                }
                int zStartIndex = zItemPre.Index + zItemPre.Content.Length;
                int zLen = zItemNow.Index - zStartIndex;
                zValue = Order.Remark.Substring(zStartIndex, zLen);
                if (zOne.Length == 2)
                {
                    if (zItemPre.Value.Length == 0)
                        zItemPre.Value = zValue;
                }
                else
                {
                    string[] zTwoValue = zValue.Split(zOne[2][0]);
                    if (zItemPre.Value.Length == 0)
                        zItemPre.Value = zTwoValue[0];
                    if (zItemNow.Value.Length == 0)
                        zItemNow.Value = zTwoValue[1];
                }

                Order.Remark_Items.Add(zItemPre);
                if (i == zFormat.Length - 2)
                    Order.Remark_Items.Add(zItemNow);
                zItemPre = zItemNow;
            }
            zItemNow = Order.Remark_Items[Order.Remark_Items.Count - 1];

            int zIndex = zItemNow.Index + zItemNow.Content.Length;
            string zEnd = zFormat[zFormat.Length - 1];
            if (zEnd.Length == 1)
            {
                if (zEnd == "N")//end by number
                {
                    string zListNumber = "";
                    while (zIndex < Order.Remark.Length)
                    {
                        int zNumber;
                        if (Order.Remark[zIndex] != '.')
                        {
                            if (int.TryParse(Order.Remark[zIndex].ToString(), out zNumber))
                            {
                                zListNumber += Order.Remark[zIndex];
                            }
                            else
                                break;
                        }
                        zIndex++;
                    }
                    zItemNow.Value = zListNumber;
                }
                if (zEnd == "E")
                {
                    zItemNow.Value = Order.Remark.Substring(zIndex, Order.Remark.Length - zIndex);
                }
            }
            else
            {

            }
            #endregion

            #region [ Check Infor Normal ]
            Item_Bill zItem = new Item_Bill();
            string zID = "";
            string zName = "";
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                zValue = "";
                zID = "";
                zName = "";
                DateTime zDate;
                switch (zItem.CategoryID)
                {
                    case 1: // MST
                        if (CheckItemMST(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Mã Số Thuế";
                            zItem.CorrectValue = zValue;
                            Order.Header.MST = zValue;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 2: // To khai
                        if (CheckItemToKhai(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Tờ Khai";
                            zItem.CorrectValue = zValue;
                            Order.Header.ToKhai = zValue;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 3: //Loại Hình XNK
                        if (CheckItemLHXNK(zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryName = "LH XNK";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;

                            Order.Header.LH_ID = zID;
                            Order.Header.LH_Name = zName;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 4: //DBHC
                        break;
                    case 5: //Tài Khoản Ngân Sách
                        break;
                    case 6: //Cơ Quan Quản Lý Thu
                        if (CheckItemCQQLThu(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Cơ Quan Quản Lý Thu";
                            zItem.CorrectID = zItem.Value;
                            zItem.CorrectName = zValue;
                            Order.Header.CQThu_ID = zItem.Value;
                            Order.Header.CQThu_Name = zValue;
                        }
                        break;
                    case 7: //Loại Thuế
                        break;
                    case 8: //Ngày Nộp Tiền
                        if (CheckItemNgayNT(zItem.Value, out zDate))
                        {
                            zItem.CategoryName = "Ngày Nộp Tiền";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayNT = zDate;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 9: //DNSDNS
                        break;
                    case 10: //Tiểu Mục
                        if (CheckItemNDKT(zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryID = 10;
                            zItem.CategoryName = "Tiểu Mục";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 11:
                        double zSoTien;
                        if (CheckItemSoTien(zItem.Value, out zSoTien))
                        {
                            zItem.CategoryID = 11;
                            zItem.CategoryName = "Số Tiền";
                            zItem.CorrectValue = zSoTien.ToString();
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 12:
                        if (CheckItemChuong(zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryID = 12;
                            zItem.CategoryName = "Chương";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 13: // Ngay DK
                        if (CheckItemNgayDK(zItem.Value, out zDate))
                        {
                            zItem.CategoryName = "Ngày Đăng Ký";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayDK = zDate;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 14: //Ky Thue
                        if (CheckItemKyThue(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Kỳ Thuế";
                            zItem.CorrectValue = zValue;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;


                }



            }
            #endregion

            #region [ Check Detail ]
            List<Item_Bill> zChuong = new List<Item_Bill>();
            List<Item_Bill> zTM = new List<Item_Bill>();
            List<Item_Bill> zST = new List<Item_Bill>();
            List<Item_Bill> zKT = new List<Item_Bill>();
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                if (zItem.CategoryID == 12)
                    zChuong.Add(zItem);
                if (zItem.CategoryID == 10)
                    zTM.Add(zItem);
                if (zItem.CategoryID == 14)
                    zKT.Add(zItem);
                if (zItem.CategoryID == 11)
                    zST.Add(zItem);
            }
            int zMax = zChuong.Count;
            if (zTM.Count > zMax)
                zMax = zTM.Count;

            for (int i = 0; i < zMax; i++)
            {
                Order_Detail_Info zOrderItem = new Order_Detail_Info();
                if (i <= zChuong.Count - 1)
                {
                    zOrderItem.ChuongID = zChuong[i].CorrectID;
                    zOrderItem.ChuongName = zChuong[i].CorrectName;
                }
                if (i <= zTM.Count - 1)
                {
                    zOrderItem.NDKT_ID = zTM[i].CorrectID;
                    zOrderItem.NDKT_Name = zTM[i].CorrectName;
                }
                if (i <= zKT.Count - 1)
                {
                    zOrderItem.KyThue = zKT[i].CorrectValue;
                }
                if (i <= zST.Count - 1)
                {
                    zOrderItem.SoTien = double.Parse(zST[i].CorrectValue);
                }
                Order.Items.Add(zOrderItem);
            }

            #endregion
        }
        public static void RunRule_60000(Order_Object Order, string Format_Data)
        {
            string zValue = "";
            // Order.RuleID = "30001";

            string[] zFormat = Format_Data.Split('|');
            string[] zFormat_Index = new string[zFormat.Length];

            #region [ Analysic ]
            Item_Bill zItemPre;
            Item_Bill zItemNow;
            string[] zOne;

            zItemPre = new Item_Bill();
            zOne = zFormat[0].Split('#');
            zItemPre.Content = zOne[0].Trim();
            zItemPre.CategoryID = int.Parse(zOne[1]);
            zItemPre.Index = Order.Remark.IndexOf(zItemPre.Content);

            for (int i = 1; i < zFormat.Length - 1; i++)
            {
                zOne = zFormat[i].Split('#');
                zItemNow = new Item_Bill();

                zItemNow.Content = zOne[0].Trim();
                zItemNow.CategoryID = int.Parse(zOne[1]);
                int zLenNext = 0;
                if (zItemNow.Content[0] == '*')
                {
                    zLenNext = int.Parse(zItemNow.Content.Substring(1, 2));
                    zItemNow.Index = zItemPre.Index + 1 + zLenNext;
                    zItemNow.Content = "";
                }
                else
                    zItemNow.Index = Order.Remark.IndexOf(zItemNow.Content, zItemPre.Index + zItemPre.Content.Length);

                if (zItemNow.Index < 0)
                {
                    Order.Remark_Items.Add(zItemPre);
                    break;
                }
                int zStartIndex = zItemPre.Index + zItemPre.Content.Length;
                int zLen = zItemNow.Index - zStartIndex;
                zValue = Order.Remark.Substring(zStartIndex, zLen);
                if (zOne.Length == 2)
                {
                    if (zItemPre.Value.Length == 0)
                        zItemPre.Value = zValue;
                }
                else
                {
                    string[] zTwoValue = zValue.Split(zOne[2][0]);
                    if (zItemPre.Value.Length == 0)
                        zItemPre.Value = zTwoValue[0];
                    if (zItemNow.Value.Length == 0)
                        zItemNow.Value = zTwoValue[1];
                }

                Order.Remark_Items.Add(zItemPre);
                if (i == zFormat.Length - 2)
                    Order.Remark_Items.Add(zItemNow);
                zItemPre = zItemNow;
            }
            zItemNow = Order.Remark_Items[Order.Remark_Items.Count - 1];

            int zIndex = zItemNow.Index + zItemNow.Content.Length;
            string zEnd = zFormat[zFormat.Length - 1];
            if (zEnd.Length == 1)
            {
                if (zEnd == "N")//end by number
                {
                    string zListNumber = "";
                    while (zIndex < Order.Remark.Length)
                    {
                        int zNumber;
                        char zOneChar = Order.Remark[zIndex];
                        if (zOneChar == '.' || zOneChar == ' ')
                        {
                            zIndex++;
                        }
                        else
                        {
                            if (int.TryParse(Order.Remark[zIndex].ToString(), out zNumber))
                            {
                                zListNumber += Order.Remark[zIndex];
                                zIndex++;
                            }
                            else
                                break;
                        }

                    }
                    zItemNow.Value = zListNumber;
                }
                if (zEnd == "E")
                {
                    zItemNow.Value = Order.Remark.Substring(zIndex, Order.Remark.Length - zIndex);
                }
            }
            else
            {

            }
            #endregion

            #region [ Check Infor Normal ]
            Item_Bill zItem = new Item_Bill();
            string zID = "";
            string zName = "";
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                zValue = "";
                zID = "";
                zName = "";
                DateTime zDate;
                switch (zItem.CategoryID)
                {
                    case 1: // MST
                        if (CheckItemMST(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Mã Số Thuế";
                            zItem.CorrectValue = zValue;
                            Order.Header.MST = zValue;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 2: // To khai
                        if (Order.Header.SoTien % 20000 == 0)
                        {
                            if (CheckItemNhieuToKhai(zItem.Value, Order.Header.SoTien / 20000, out zValue))
                            {
                                zItem.CategoryName = "Tờ Khai";
                                zItem.CorrectValue = zValue;
                                Order.Header.ToKhai = zValue;
                            }
                            else
                            {
                                zItem.CategoryName = "0";
                                zItem.CategoryID = 0;
                            }
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 3: //Loại Hình XNK
                        if (CheckItemLHXNK(zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryName = "LH XNK";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;

                            Order.Header.LH_ID = zID;
                            Order.Header.LH_Name = zName;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 4: //DBHC
                        break;
                    case 5: //Tài Khoản Ngân Sách
                        break;
                    case 6: //Cơ Quan Quản Lý Thu
                        if (CheckItemCQQLThu(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Cơ Quan Quản Lý Thu";
                            zItem.CorrectID = zItem.Value;
                            zItem.CorrectName = zValue;
                            Order.Header.CQThu_ID = zItem.Value;
                            Order.Header.CQThu_Name = zValue;
                        }
                        break;
                    case 7: //Loại Thuế
                        break;
                    case 8: //Ngày Nộp Tiền
                        if (CheckItemNgayNT(zItem.Value, out zDate))
                        {
                            zItem.CategoryName = "Ngày Nộp Tiền";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayNT = zDate;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 9: //DNSDNS
                        break;
                    case 10: //Tiểu Mục
                        if (CheckItemNDKT(zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryID = 10;
                            zItem.CategoryName = "Tiểu Mục";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 11:
                        double zSoTien;
                        if (CheckItemSoTien(zItem.Value, out zSoTien))
                        {
                            zItem.CategoryID = 11;
                            zItem.CategoryName = "Số Tiền";
                            zItem.CorrectValue = zSoTien.ToString();
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 12:
                        if (CheckItemChuong(zItem.Value, out zID, out zName))
                        {
                            zItem.CategoryID = 12;
                            zItem.CategoryName = "Chương";
                            zItem.CorrectID = zID;
                            zItem.CorrectName = zName;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 13: // Ngay DK
                        if (CheckItemNgayDK(zItem.Value, out zDate))
                        {
                            zItem.CategoryName = "Ngày Đăng Ký";
                            zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                            Order.Header.NgayDK = zDate;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;
                    case 14: //Ky Thue
                        if (CheckItemKyThue(zItem.Value, out zValue))
                        {
                            zItem.CategoryName = "Kỳ Thuế";
                            zItem.CorrectValue = zValue;
                        }
                        else
                        {
                            zItem.CategoryName = "0";
                            zItem.CategoryID = 0;
                        }
                        break;


                }



            }
            #endregion

            #region [ Check Detail ]
            List<Item_Bill> zChuong = new List<Item_Bill>();
            List<Item_Bill> zTM = new List<Item_Bill>();
            List<Item_Bill> zST = new List<Item_Bill>();
            List<Item_Bill> zKT = new List<Item_Bill>();
            for (int i = 0; i < Order.Remark_Items.Count; i++)
            {
                zItem = Order.Remark_Items[i];
                if (zItem.CategoryID == 12)
                    zChuong.Add(zItem);
                if (zItem.CategoryID == 10)
                    zTM.Add(zItem);
                if (zItem.CategoryID == 14)
                    zKT.Add(zItem);
                if (zItem.CategoryID == 11)
                    zST.Add(zItem);
            }
            int zMax = zChuong.Count;
            if (zTM.Count > zMax)
                zMax = zTM.Count;

            for (int i = 0; i < zMax; i++)
            {
                Order_Detail_Info zOrderItem = new Order_Detail_Info();
                if (i <= zChuong.Count - 1)
                {
                    zOrderItem.ChuongID = zChuong[i].CorrectID;
                    zOrderItem.ChuongName = zChuong[i].CorrectName;
                }
                if (i <= zTM.Count - 1)
                {
                    zOrderItem.NDKT_ID = zTM[i].CorrectID;
                    zOrderItem.NDKT_Name = zTM[i].CorrectName;
                }
                if (i <= zKT.Count - 1)
                {
                    zOrderItem.KyThue = zKT[i].CorrectValue;
                }
                if (i <= zST.Count - 1)
                {
                    zOrderItem.SoTien = double.Parse(zST[i].CorrectValue);
                }
                Order.Items.Add(zOrderItem);
            }

            #endregion
        }
        public static void RunRule_90001(Order_Object Order)
        {
            string zValue = "";
            Order.RuleID = "90001";
            int zBegin;
            int zIndex = 0;
            int n;
            Item_Bill zItem;
            List<Item_Bill> zListItem;
            #region [ find MST ]
            if (Find_MST(Order.Remark, out zValue, out zItem))
            {
                Order.Remark_Items.Add(zItem);
                Order.Remark = zValue;
            }
            if (Find_TK_NDK_LH(Order.Remark, out zValue, out zListItem))
            {
                Order.Remark_Items.AddRange(zListItem);
            }
            #endregion
        }

        #region [ Function Find Value ]
        public static bool CheckItemMST(string ContentCode, string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(1);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                if (zValue.Length == 10 || zValue.Length == 13)
                {
                    Result = zValue;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemNgayNT(string ContentCode, string Value, out DateTime Result)
        {
            bool zFound = false;
            Result = new DateTime();
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(8);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zstrDate = Function.GetCorrectDate(Value);
                DateTime zDate;
                if (Function.ConvertDateVN(zstrDate, out zDate))
                {
                    Result = zDate;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemDBHC(string ContentCode, string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(4);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                if (zValue.Length == 3)
                {
                    Result = zValue;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemDVSDNS(string ContentCode, string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(9);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                Result = zValue;
            }
            return zFound;
        }
        public static bool CheckItemCQQLThu(string ContentCode, string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(6);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                if (zValue.Length == 10 || zValue.Length == 13)
                {
                    Result = zValue;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemToKhai(string ContentCode, string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(2);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                if (zValue.Length == 11 || zValue.Length == 12)
                {
                    Result = zValue;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemToKhai_Ext_1(string ContentCode, string Value, out string Result, out string Remain)
        {
            bool zFound = false;
            Result = "";
            Remain = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(2);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetOnlyNumberInFormat(Value);
                if (zValue.Length == 11 || zValue.Length == 12)
                {
                    Result = zValue;
                    Remain = Value.Substring(zValue.Length, Value.Length - zValue.Length);
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemNgayDK(string ContentCode, string Value, out DateTime Result)
        {
            bool zFound = false;
            Result = new DateTime();

            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(13);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }

            if (zFound)
            {
                string zstrDate = Function.GetCorrectDate(Value);
                DateTime zDate;
                if (Function.ConvertDateVN(zstrDate, out zDate))
                {
                    Result = zDate;
                }
                else
                    zFound = false;

            }
            return zFound;
        }
        public static bool CheckItemLHXNK(string ContentCode, string Value, out string ID, out string Name)
        {
            bool zFound = false;
            ID = "";
            Name = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(3);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zTemp;
                zTemp = ContentCode.Substring(ContentCode.Length - 1, 1);
                string zValue = Function.GetAllNumberInSide(Value);
                zValue = zTemp + zValue;
                if (zValue == "D01")
                    zValue = "A11";
                string zName = LHXNK_Name(zValue);
                if (zName.Length > 2)
                {
                    ID = zValue;
                    Name = zName;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemChuong(string ContentCode, string Value, out string ID, out string Name)
        {
            bool zFound = false;
            ID = "";
            Name = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(12);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()) && zRow["WordCode"].ToString().Length >= ContentCode.Length - 1)
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                string zChuongName = Chuong_Name(zValue);
                if (zChuongName.Length > 2)
                {
                    Name = zChuongName;
                    ID = zValue;
                }
                else
                    zFound = false;

            }
            return zFound;
        }
        public static bool CheckItemNDKT(string ContentCode, string Value, out string ID, out string Name)
        {
            bool zFound = false;
            ID = "";
            Name = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(10);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetAllNumberInSide(Value);
                string zNDKT_Name = NDKT_Name(zValue);
                if (zNDKT_Name.Length > 2)
                {
                    Name = zNDKT_Name;
                    ID = zValue;
                }
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemKyThue(string ContentCode, string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(14);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zValue = Function.GetCorrectMonthYear(Value);
                if (zValue.Length > 0)
                    Result = zValue;
                else
                    zFound = false;
            }
            return zFound;
        }
        public static bool CheckItemSoTien(string ContentCode, string Value, out double Result)
        {
            bool zFound = false;
            Result = 0;
            if (Value.Length <= 2)
                return zFound;

            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(11);
            string[] zListNotMoney = { "MA SO" };
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                if (ContentCode.Contains(zRow["WordCode"].ToString()))
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                string zstrValue = Function.GetAllNumberInSide(Value);
                double zValue;

                if (double.TryParse(zstrValue, out zValue))
                {
                    Result = zValue;
                }
                else
                    zFound = false;

            }
            return zFound;
        }

        #endregion

        #region [ Function Find Value With Category]
        public static bool CheckItemMST(string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            string zValue = Function.GetAllNumberInSide(Value);
            if (zValue.Length == 10 || zValue.Length == 13)
            {
                Result = zValue;
                zFound = true;
            }

            return zFound;
        }
        public static bool CheckItemNgayNT(string Value, out DateTime Result)
        {
            bool zFound = false;
            Result = new DateTime();

            string zstrDate = Function.GetCorrectDate(Value);
            DateTime zDate;
            if (Function.ConvertDateVN(zstrDate, out zDate))
            {
                Result = zDate;
                zFound = true;
            }
            return zFound;
        }
        public static bool CheckItemDBHC(string Value, out string Result)
        {
            bool zFound = false;
            Result = "";
            string zValue = Function.GetAllNumberInSide(Value);
            if (zValue.Length == 3)
            {
                Result = zValue;
                zFound = true;
            }
            return zFound;
        }
        public static bool CheckItemDVSDNS(string Value, out string Result)
        {
            bool zFound = false;

            string zValue = Function.GetAllNumberInSide(Value);
            Result = zValue;
            zFound = true;
            return zFound;
        }
        public static bool CheckItemCQQLThu(string Value, out string Result)
        {
            bool zFound = false;
            Result = "";

            string zValue = CQQLThu_Name(Value);
            if (zValue.Length > 2)
            {
                Result = zValue;
                zFound = true;
            }
            return zFound;
        }
        public static bool CheckItemToKhai(string Value, out string Result)
        {
            bool zFound = false;
            Result = "";

            string zValue = Function.GetAllNumberInSide(Value);
            if (zValue.Length == 11 || zValue.Length == 12)
            {
                Result = zValue;
                zFound = true;
            }

            return zFound;
        }
        public static bool CheckItemNhieuToKhai(string Value, double TongSoTK, out string Result)
        {
            bool zFound = false;
            Result = "";
            string[] zListTK = Value.Split(';');
            if (zListTK.Length != TongSoTK)
            {
                zListTK = Value.Split(',');
                if (zListTK.Length != TongSoTK)
                    zListTK = Value.Split('-');
            }
            if (zListTK.Length != TongSoTK)
                return zFound;
            int zAmountCorrect = 0;
            for (int i = 0; i < TongSoTK; i++)
            {
                string zValue = Function.GetAllNumberInSide(zListTK[i]);
                if (zValue.Length == 11 || zValue.Length == 12)
                {
                    zAmountCorrect++;
                }
            }
            if (zAmountCorrect == TongSoTK)
            {
                Result = "9999999999";
                zFound = true;
            }


            return zFound;
        }
        public static bool CheckItemNgayDK(string Value, out DateTime Result)
        {
            bool zFound = false;
            Result = new DateTime();

            string zstrDate = Function.GetCorrectDate(Value);
            DateTime zDate;
            if (Function.ConvertDateVN(zstrDate, out zDate))
            {
                Result = zDate;
                zFound = true;
            }

            return zFound;
        }
        public static bool CheckItemLHXNK(string Value, out string ID, out string Name)
        {
            bool zFound = false;
            ID = "";
            Name = "";
            int zNumber;
            string zTemp = "";
            if (Value.Length > 3)
            {
                for (int i = Value.Length - 1; i > 1; i--)
                {
                    if (int.TryParse(Value[i].ToString(), out zNumber)
                        && int.TryParse(Value[i - 1].ToString(), out zNumber)
                        && !int.TryParse(Value[i - 2].ToString(), out zNumber))
                    {
                        zTemp = Value[i - 2].ToString() + Value[i - 1].ToString() + Value[i].ToString();

                    }
                }
            }
            else
                zTemp = Value;
            if (zTemp == "D01")
                zTemp = "A11";
            string zName = LHXNK_Name(zTemp);
            if (zName.Length > 2)
            {
                ID = zTemp;
                Name = zName;
                zFound = true;
            }

            return zFound;
        }
        public static bool CheckItemChuong(string Value, out string ID, out string Name)
        {
            bool zFound = false;
            ID = "";
            Name = "";
            string zValue = Function.GetAllNumberInSide(Value);
            string zChuongName = Chuong_Name(zValue);
            if (zChuongName.Length > 2)
            {
                Name = zChuongName;
                ID = zValue;
                zFound = true;
            }

            return zFound;
        }
        public static bool CheckItemNDKT(string Value, out string ID, out string Name)
        {
            bool zFound = false;
            ID = "";
            Name = "";

            string zValue = Function.GetAllNumberInSide(Value);
            string zNDKT_Name = NDKT_Name(zValue);
            if (zNDKT_Name.Length > 2)
            {
                Name = zNDKT_Name;
                ID = zValue;
                zFound = true;
            }

            return zFound;
        }
        public static bool CheckItemKyThue(string Value, out string Result)
        {
            bool zFound = false;
            Result = "";

            string zValue = Function.GetCorrectMonthYear(Value);
            if (zValue.Length > 0)
            {
                Result = zValue;
                zFound = true;
            }
            return zFound;
        }
        public static bool CheckItemSoTien(string Value, out double Result)
        {
            bool zFound = false;
            Result = 0;
            if (Value.Length <= 2)
                return zFound;
            string zListNumber = "";
            int zIndex = 0;
            Value = Value.Trim();
            if (Value[0] == ':')
                Value = Value.Substring(1, Value.Length - 1);

            while (zIndex < Value.Length)
            {
                int zNumber;
                if (Value[zIndex] != '.')
                {
                    if (int.TryParse(Value[zIndex].ToString(), out zNumber))
                    {
                        zListNumber += Value[zIndex];
                    }
                    else
                        break;
                }
                zIndex++;
            }
            double zValue = 0;
            if (double.TryParse(zListNumber, out zValue))
            {
                Result = zValue;
                zFound = true;
            }

            return zFound;
        }

        #endregion

        #region [ Query database]
        public static string Chuong_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT Description FROM BNK_Chuong WHERE ID = '" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static string NDKT_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT Description FROM BNK_NDKT WHERE ID = '" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static string LHXNK_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT Description FROM BNK_LHXNK WHERE ID = '" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static string CQQLThu_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT TOP 1 DepartmentName FROM [dbo].[FNC_GovTax_Account] WHERE DepartmentID ='" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        #endregion

        public static bool Find_MST(string Content, out string RemainContent, out Item_Bill ItemMST)
        {
            bool zResult = false;
            ItemMST = new Item_Bill();
            RemainContent = "";

            bool zFound = false;
            int zBegin = 0;
            string zWordCode = ""; ;
            int n = Content.Length;

            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(1);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                zWordCode = zRow["WordCode"].ToString();
                zBegin = Content.IndexOf(zWordCode);
                if (zBegin > 0)
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                int k = zBegin + zWordCode.Length;
                int zNumber;
                string zListNumber = "";
                while (k < n)
                {
                    char zChar = Content[k];
                    if (zChar != '.')
                    {
                        if (int.TryParse(zChar.ToString(), out zNumber))
                        {
                            zListNumber += zChar;
                        }
                        else
                        {
                            break;
                        }
                    }
                    k++;
                }
                if (zListNumber.Length == 10 || zListNumber.Length == 13)
                {
                    ItemMST.CategoryID = 1;
                    ItemMST.CategoryName = "Mã Số Thuế";
                    ItemMST.CorrectValue = zListNumber;
                    ItemMST.Content = zWordCode;
                    for (int i = 0; i < zBegin; i++)
                    {
                        RemainContent += Content[i];
                    }
                    for (int i = k; i < n; i++)
                    {
                        RemainContent += Content[i];
                    }
                    zResult = true;
                }
            }
            return zResult;
        }
        public static bool Find_TK_NDK_LH(string Content, out string RemainContent, out List<Item_Bill> Items)
        {
            bool zResult = false;
            Items = new List<Item_Bill>();
            RemainContent = "";

            bool zFound = false;
            int zBegin = 0;
            string zWordCode = ""; ;
            int n = 0;
            int zNumber;
            string zListNumber = "";
            string zValue;
            DateTime zDate;
            DataTable zListWordcodeForName = Analysis_Data.ListWordCode_RuleItem(2);
            foreach (DataRow zRow in zListWordcodeForName.Rows)
            {
                zWordCode = zRow["WordCode"].ToString();
                zBegin = Content.IndexOf(zWordCode);
                if (zBegin > 0)
                {
                    zFound = true;
                    break;
                }
            }
            if (zFound)
            {
                //--------------TK_Number/Date/LH-----------
                string zTKContent = "";
                string zNgayDK = "";
                string zLHContent = "";
                int k = zBegin + zWordCode.Length;
                int zIndexOfLH = -1;
                int zIndexOfDate = Content.IndexOf('/', k);
                if (zIndexOfDate > 0)
                {
                    zIndexOfLH = Content.IndexOf('/', zIndexOfDate + 1);
                }
                if (zIndexOfDate > 0 && zIndexOfLH > 0)
                {
                    zTKContent = Content.Substring(k, zIndexOfDate - k);
                    zNgayDK = Content.Substring(zIndexOfDate + 1, zIndexOfLH - zIndexOfDate - 1);
                    int j = zIndexOfLH + 1;
                    bool zHaveNumber = false;
                    n = Content.Length;
                    while (j < n)
                    {
                        if (int.TryParse(Content[j].ToString(), out zNumber))
                        {
                            if (!zHaveNumber)
                                zHaveNumber = true;
                            zLHContent += Content[j];

                        }
                        else
                        {
                            if (zHaveNumber)
                                break;
                            zLHContent += Content[j];
                        }
                        j++;
                    }

                }
                Item_Bill zItem;

                if (zTKContent.Length > 0 && CheckItemToKhai(zTKContent, out zValue))
                {
                    zItem = new Item_Bill();
                    zItem.CategoryID = 2;
                    zItem.CategoryName = "Tờ Khai";
                    zItem.Content = zWordCode;
                    zItem.CorrectValue = zValue;
                    Items.Add(zItem);
                    if (zNgayDK.Length > 0 && CheckItemNgayDK(zNgayDK, out zDate))
                    {
                        zItem = new Item_Bill();
                        zItem.CategoryID = 13;
                        zItem.CategoryName = "Ngày Đăng Ký";
                        zItem.Content = "NgayDK";
                        zItem.CorrectValue = zDate.ToString("dd/MM/yyyy");
                        Items.Add(zItem);
                        string zID = "";
                        if (zLHContent.Length > 0)
                        {
                            zID = zLHContent.Substring(zLHContent.Length - 3, 3);
                            string zName = LHXNK_Name(zID);
                            if (zName.Length > 5)
                            {
                                zItem = new Item_Bill();
                                zItem.CategoryID = 3;
                                zItem.CategoryName = "LH XNK";
                                zItem.Content = "LH XNK";
                                zItem.CorrectID = zID;
                                zItem.CorrectName = zName;
                                Items.Add(zItem);
                                zResult = true;
                            }
                        }
                    }
                }


            }
            return zResult;
        }

        public static List<string> ListStringNeedRomove()
        {
            List<string> zResult = new List<string>();
            DataTable zTable = new DataTable();
            string SQL = "SELECT StringRemove FROM StringNeedRemove";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();
            foreach (DataRow zRow in zTable.Rows)
            {
                zResult.Add(zRow[0].ToString());
            }
            return zResult;
        }


        //Xóa kí tự thừa
        public static string RemoveChar(string s)
        {
            string zResult = "";

            //Xóa dấu cuối cùng
            if (s.Length > 0)
            {
                if (s.Substring((s.Length - 1), 1) == "+" || s.Substring((s.Length - 1), 1) == ")")
                {
                    zResult = s.Remove((s.Length - 1));
                }
            }
            return zResult;
        }

    }
}
