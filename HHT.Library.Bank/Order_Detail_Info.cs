﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using HHT.Connect;
namespace HHT.Library.Bank
{
    public class Order_Detail_Info
    {

        #region [ Field Name ]
        private string _AutoKey;
        private string _trref = "";
        private string _ChuongID = "";
        private string _ChuongName = "";
        private string _NDKT_ID = "";
        private string _NDKT_Name = "";
        private double _SoTien = 0;
        private string _KyThue = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Order_Detail_Info()
        {
        }
        public Order_Detail_Info(string Key)
        {
            string zSQL = "SELECT * FROM BNK_Order_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = new Guid(Key);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = zReader["AutoKey"].ToString();
                    _trref = zReader["trref"].ToString();
                    _ChuongID = zReader["ChuongID"].ToString();
                    _ChuongName = zReader["ChuongName"].ToString();
                    _NDKT_ID = zReader["NDKT_ID"].ToString();
                    _NDKT_Name = zReader["NDKT_Name"].ToString();
                    _SoTien = double.Parse(zReader["SoTien"].ToString());
                    _KyThue = zReader["KyThue"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Order_Detail_Info(DataRow DetailInfo)
        {

            _AutoKey = DetailInfo["AutoKey"].ToString();
            _trref = DetailInfo["trref"].ToString();
            _ChuongID = DetailInfo["ChuongID"].ToString();
            _ChuongName = DetailInfo["ChuongName"].ToString();
            _NDKT_ID = DetailInfo["NDKT_ID"].ToString();
            _NDKT_Name = DetailInfo["NDKT_Name"].ToString();
            _SoTien = double.Parse(DetailInfo["SoTien"].ToString());
            _KyThue = DetailInfo["KyThue"].ToString();

        }

     

        #endregion

        #region [ Properties ]
        public string AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string trref
        {
            get { return _trref; }
            set { _trref = value; }
        }
        public string ChuongID
        {
            get { return _ChuongID; }
            set { _ChuongID = value; }
        }
        public string ChuongName
        {
            get { return _ChuongName; }
            set { _ChuongName = value; }
        }
        public string NDKT_ID
        {
            get { return _NDKT_ID; }
            set { _NDKT_ID = value; }
        }
        public string NDKT_Name
        {
            get { return _NDKT_Name; }
            set { _NDKT_Name = value; }
        }
        public double SoTien
        {
            get { return _SoTien; }
            set { _SoTien = value; }
        }
        public string KyThue
        {
            get { return _KyThue; }
            set { _KyThue = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO BNK_Order_Detail ("
                        + " trref ,ChuongID ,ChuongName ,NDKT_ID ,NDKT_Name ,SoTien ,KyThue ) "
                        + " VALUES ( "
                        + "@trref ,@ChuongID ,@ChuongName ,@NDKT_ID ,@NDKT_Name ,@SoTien ,@KyThue ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@ChuongID", SqlDbType.NVarChar).Value = _ChuongID;
                zCommand.Parameters.Add("@ChuongName", SqlDbType.NVarChar).Value = _ChuongName;
                zCommand.Parameters.Add("@NDKT_ID", SqlDbType.NVarChar).Value = _NDKT_ID;
                zCommand.Parameters.Add("@NDKT_Name", SqlDbType.NVarChar).Value = _NDKT_Name;
                zCommand.Parameters.Add("@SoTien", SqlDbType.Money).Value = _SoTien;
                zCommand.Parameters.Add("@KyThue", SqlDbType.NVarChar).Value = _KyThue;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE BNK_Order_Detail SET "
                        + " trref = @trref,"
                        + " ChuongID = @ChuongID,"
                        + " ChuongName = @ChuongName,"
                        + " NDKT_ID = @NDKT_ID,"
                        + " NDKT_Name = @NDKT_Name,"
                        + " SoTien = @SoTien,"
                        + " KyThue = @KyThue"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = new Guid(_AutoKey);
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@ChuongID", SqlDbType.NVarChar).Value = _ChuongID;
                zCommand.Parameters.Add("@ChuongName", SqlDbType.NVarChar).Value = _ChuongName;
                zCommand.Parameters.Add("@NDKT_ID", SqlDbType.NVarChar).Value = _NDKT_ID;
                zCommand.Parameters.Add("@NDKT_Name", SqlDbType.NVarChar).Value = _NDKT_Name;
                zCommand.Parameters.Add("@SoTien", SqlDbType.Money).Value = _SoTien;
                zCommand.Parameters.Add("@KyThue", SqlDbType.NVarChar).Value = _KyThue;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey.Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_Order_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = new Guid(_AutoKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
