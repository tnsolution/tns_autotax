﻿using HHT.Connect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace HHT.Library.Bank
{
    public class Data_Fail
    {
        #region [ Field Name ]
        private int _Auto = 0;
        private string _trref = "";
        private string _NHNN_ID = "";
        private string _NHNN_Name = "";
        private int _KieuThu = 0;
        private string _NHPV_ID = "";
        private string _NHPV_Name = "";
        private DateTime _NgayNT;
        private string _Ma_ST = "";
        private string _TenCongTy = "";
        private string _TK_ThuNS = "";
        private string _TK_ThuNS_Name = "";
        private string _DBHC_ID = "";
        private string _DBHC_Name = "";
        private string _CQThuID = "";
        private string _CQThuName = "";
        private string _LoaiThueID = "";
        private string _LoaiThueName = "";
        private string _ToKhai = "";
        private DateTime _NgayDK;
        private string _LH_ID = "";
        private string _LH_Name = "";
        private double _SoTien = 0;
        private string _MessageReturn = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Auto
        {
            get { return _Auto; }
            set { _Auto = value; }
        }
        public string trref
        {
            get { return _trref; }
            set { _trref = value; }
        }
        public string NHNN_ID
        {
            get { return _NHNN_ID; }
            set { _NHNN_ID = value; }
        }
        public string NHNN_Name
        {
            get { return _NHNN_Name; }
            set { _NHNN_Name = value; }
        }
        public int KieuThu
        {
            get { return _KieuThu; }
            set { _KieuThu = value; }
        }
        public string NHPV_ID
        {
            get { return _NHPV_ID; }
            set { _NHPV_ID = value; }
        }
        public string NHPV_Name
        {
            get { return _NHPV_Name; }
            set { _NHPV_Name = value; }
        }
        public DateTime NgayNT
        {
            get { return _NgayNT; }
            set { _NgayNT = value; }
        }
        public string Ma_ST
        {
            get { return _Ma_ST; }
            set { _Ma_ST = value; }
        }
        public string TenCongTy
        {
            get { return _TenCongTy; }
            set { _TenCongTy = value; }
        }
        public string TK_ThuNS
        {
            get { return _TK_ThuNS; }
            set { _TK_ThuNS = value; }
        }
        public string TK_ThuNS_Name
        {
            get { return _TK_ThuNS_Name; }
            set { _TK_ThuNS_Name = value; }
        }
        public string DBHC_ID
        {
            get { return _DBHC_ID; }
            set { _DBHC_ID = value; }
        }
        public string DBHC_Name
        {
            get { return _DBHC_Name; }
            set { _DBHC_Name = value; }
        }
        public string CQThuID
        {
            get { return _CQThuID; }
            set { _CQThuID = value; }
        }
        public string CQThuName
        {
            get { return _CQThuName; }
            set { _CQThuName = value; }
        }
        public string LoaiThueID
        {
            get { return _LoaiThueID; }
            set { _LoaiThueID = value; }
        }
        public string LoaiThueName
        {
            get { return _LoaiThueName; }
            set { _LoaiThueName = value; }
        }
        public string ToKhai
        {
            get { return _ToKhai; }
            set { _ToKhai = value; }
        }
        public DateTime NgayDK
        {
            get { return _NgayDK; }
            set { _NgayDK = value; }
        }
        public string LH_ID
        {
            get { return _LH_ID; }
            set { _LH_ID = value; }
        }
        public string LH_Name
        {
            get { return _LH_Name; }
            set { _LH_Name = value; }
        }
        public double SoTien
        {
            get { return _SoTien; }
            set { _SoTien = value; }
        }
        public string MessageReturn
        {
            get { return _MessageReturn; }
            set { _MessageReturn = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Data_Fail()
        {
        }
        public Data_Fail(int Auto)
        {
            string zSQL = "SELECT * FROM Data_Fail WHERE Auto = @Auto";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = Auto;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["Auto"] != DBNull.Value)
                        _Auto = int.Parse(zReader["Auto"].ToString());
                    _trref = zReader["trref"].ToString();
                    _NHNN_ID = zReader["NHNN_ID"].ToString();
                    _NHNN_Name = zReader["NHNN_Name"].ToString();
                    if (zReader["KieuThu"] != DBNull.Value)
                        _KieuThu = int.Parse(zReader["KieuThu"].ToString());
                    _NHPV_ID = zReader["NHPV_ID"].ToString();
                    _NHPV_Name = zReader["NHPV_Name"].ToString();
                    if (zReader["NgayNT"] != DBNull.Value)
                        _NgayNT = (DateTime)zReader["NgayNT"];
                    _Ma_ST = zReader["Ma_ST"].ToString();
                    _TenCongTy = zReader["TenCongTy"].ToString();
                    _TK_ThuNS = zReader["TK_ThuNS"].ToString();
                    _TK_ThuNS_Name = zReader["TK_ThuNS_Name"].ToString();
                    _DBHC_ID = zReader["DBHC_ID"].ToString();
                    _DBHC_Name = zReader["DBHC_Name"].ToString();
                    _CQThuID = zReader["CQThuID"].ToString();
                    _CQThuName = zReader["CQThuName"].ToString();
                    _LoaiThueID = zReader["LoaiThueID"].ToString();
                    _LoaiThueName = zReader["LoaiThueName"].ToString();
                    _ToKhai = zReader["ToKhai"].ToString();
                    if (zReader["NgayDK"] != DBNull.Value)
                        _NgayDK = (DateTime)zReader["NgayDK"];
                    _LH_ID = zReader["LH_ID"].ToString();
                    _LH_Name = zReader["LH_Name"].ToString();
                    if (zReader["SoTien"] != DBNull.Value)
                        _SoTien = double.Parse(zReader["SoTien"].ToString());
                    _MessageReturn = zReader["MessageReturn"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Data_Fail(string Auto)
        {
            string zSQL = "SELECT * FROM Data_Fail WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.Int).Value = Auto;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["Auto"] != DBNull.Value)
                        _Auto = int.Parse(zReader["Auto"].ToString());
                    _trref = zReader["trref"].ToString();
                    _NHNN_ID = zReader["NHNN_ID"].ToString();
                    _NHNN_Name = zReader["NHNN_Name"].ToString();
                    if (zReader["KieuThu"] != DBNull.Value)
                        _KieuThu = int.Parse(zReader["KieuThu"].ToString());
                    _NHPV_ID = zReader["NHPV_ID"].ToString();
                    _NHPV_Name = zReader["NHPV_Name"].ToString();
                    if (zReader["NgayNT"] != DBNull.Value)
                        _NgayNT = (DateTime)zReader["NgayNT"];
                    _Ma_ST = zReader["Ma_ST"].ToString();
                    _TenCongTy = zReader["TenCongTy"].ToString();
                    _TK_ThuNS = zReader["TK_ThuNS"].ToString();
                    _TK_ThuNS_Name = zReader["TK_ThuNS_Name"].ToString();
                    _DBHC_ID = zReader["DBHC_ID"].ToString();
                    _DBHC_Name = zReader["DBHC_Name"].ToString();
                    _CQThuID = zReader["CQThuID"].ToString();
                    _CQThuName = zReader["CQThuName"].ToString();
                    _LoaiThueID = zReader["LoaiThueID"].ToString();
                    _LoaiThueName = zReader["LoaiThueName"].ToString();
                    _ToKhai = zReader["ToKhai"].ToString();
                    if (zReader["NgayDK"] != DBNull.Value)
                        _NgayDK = (DateTime)zReader["NgayDK"];
                    _LH_ID = zReader["LH_ID"].ToString();
                    _LH_Name = zReader["LH_Name"].ToString();
                    if (zReader["SoTien"] != DBNull.Value)
                        _SoTien = double.Parse(zReader["SoTien"].ToString());
                    _MessageReturn = zReader["MessageReturn"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Data_Fail ("
        + " trref ,NHNN_ID ,NHNN_Name ,KieuThu ,NHPV_ID ,NHPV_Name ,NgayNT ,Ma_ST ,TenCongTy ,TK_ThuNS ,TK_ThuNS_Name ,DBHC_ID ,DBHC_Name ,CQThuID ,CQThuName ,LoaiThueID ,LoaiThueName ,ToKhai ,NgayDK ,LH_ID ,LH_Name ,SoTien ,MessageReturn ,CreatedOn ,CreatedBy ) "
         + " VALUES ( "
         + "@trref ,@NHNN_ID ,@NHNN_Name ,@KieuThu ,@NHPV_ID ,@NHPV_Name ,@NgayNT ,@Ma_ST ,@TenCongTy ,@TK_ThuNS ,@TK_ThuNS_Name ,@DBHC_ID ,@DBHC_Name ,@CQThuID ,@CQThuName ,@LoaiThueID ,@LoaiThueName ,@ToKhai ,@NgayDK ,@LH_ID ,@LH_Name ,@SoTien ,@MessageReturn ,@CreatedOn ,@CreatedBy ) "
         + " SELECT Auto FROM Data_Fail WHERE Auto = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = _Auto;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@NHNN_ID", SqlDbType.NVarChar).Value = _NHNN_ID;
                zCommand.Parameters.Add("@NHNN_Name", SqlDbType.NVarChar).Value = _NHNN_Name;
                zCommand.Parameters.Add("@KieuThu", SqlDbType.Int).Value = _KieuThu;
                zCommand.Parameters.Add("@NHPV_ID", SqlDbType.NVarChar).Value = _NHPV_ID;
                zCommand.Parameters.Add("@NHPV_Name", SqlDbType.NVarChar).Value = _NHPV_Name;
                if (_NgayNT == DateTime.MinValue)
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = _NgayNT;
                zCommand.Parameters.Add("@Ma_ST", SqlDbType.NVarChar).Value = _Ma_ST;
                zCommand.Parameters.Add("@TenCongTy", SqlDbType.NVarChar).Value = _TenCongTy;
                zCommand.Parameters.Add("@TK_ThuNS", SqlDbType.NVarChar).Value = _TK_ThuNS;
                zCommand.Parameters.Add("@TK_ThuNS_Name", SqlDbType.NVarChar).Value = _TK_ThuNS_Name;
                zCommand.Parameters.Add("@DBHC_ID", SqlDbType.NVarChar).Value = _DBHC_ID;
                zCommand.Parameters.Add("@DBHC_Name", SqlDbType.NVarChar).Value = _DBHC_Name;
                zCommand.Parameters.Add("@CQThuID", SqlDbType.NVarChar).Value = _CQThuID;
                zCommand.Parameters.Add("@CQThuName", SqlDbType.NVarChar).Value = _CQThuName;
                zCommand.Parameters.Add("@LoaiThueID", SqlDbType.NVarChar).Value = _LoaiThueID;
                zCommand.Parameters.Add("@LoaiThueName", SqlDbType.NVarChar).Value = _LoaiThueName;
                zCommand.Parameters.Add("@ToKhai", SqlDbType.NVarChar).Value = _ToKhai;
                if (_NgayDK == DateTime.MinValue)
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = _NgayDK;
                zCommand.Parameters.Add("@LH_ID", SqlDbType.NVarChar).Value = _LH_ID;
                zCommand.Parameters.Add("@LH_Name", SqlDbType.NVarChar).Value = _LH_Name;
                zCommand.Parameters.Add("@SoTien", SqlDbType.Money).Value = _SoTien;
                zCommand.Parameters.Add("@MessageReturn", SqlDbType.NVarChar).Value = _MessageReturn;
                if (_CreatedOn == DateTime.MinValue)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                _Auto = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE Data_Fail SET "
                        + " trref = @trref,"
                        + " NHNN_ID = @NHNN_ID,"
                        + " NHNN_Name = @NHNN_Name,"
                        + " KieuThu = @KieuThu,"
                        + " NHPV_ID = @NHPV_ID,"
                        + " NHPV_Name = @NHPV_Name,"
                        + " NgayNT = @NgayNT,"
                        + " Ma_ST = @Ma_ST,"
                        + " TenCongTy = @TenCongTy,"
                        + " TK_ThuNS = @TK_ThuNS,"
                        + " TK_ThuNS_Name = @TK_ThuNS_Name,"
                        + " DBHC_ID = @DBHC_ID,"
                        + " DBHC_Name = @DBHC_Name,"
                        + " CQThuID = @CQThuID,"
                        + " CQThuName = @CQThuName,"
                        + " LoaiThueID = @LoaiThueID,"
                        + " LoaiThueName = @LoaiThueName,"
                        + " ToKhai = @ToKhai,"
                        + " NgayDK = @NgayDK,"
                        + " LH_ID = @LH_ID,"
                        + " LH_Name = @LH_Name,"
                        + " SoTien = @SoTien,"
                        + " MessageReturn = @MessageReturn,"
                        + " CreatedOn = @CreatedOn,"
                        + " CreatedBy = @CreatedBy"
                       + " WHERE Auto = @Auto";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = _Auto;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@NHNN_ID", SqlDbType.NVarChar).Value = _NHNN_ID;
                zCommand.Parameters.Add("@NHNN_Name", SqlDbType.NVarChar).Value = _NHNN_Name;
                zCommand.Parameters.Add("@KieuThu", SqlDbType.Int).Value = _KieuThu;
                zCommand.Parameters.Add("@NHPV_ID", SqlDbType.NVarChar).Value = _NHPV_ID;
                zCommand.Parameters.Add("@NHPV_Name", SqlDbType.NVarChar).Value = _NHPV_Name;
                if (_NgayNT == DateTime.MinValue)
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = _NgayNT;
                zCommand.Parameters.Add("@Ma_ST", SqlDbType.NVarChar).Value = _Ma_ST;
                zCommand.Parameters.Add("@TenCongTy", SqlDbType.NVarChar).Value = _TenCongTy;
                zCommand.Parameters.Add("@TK_ThuNS", SqlDbType.NVarChar).Value = _TK_ThuNS;
                zCommand.Parameters.Add("@TK_ThuNS_Name", SqlDbType.NVarChar).Value = _TK_ThuNS_Name;
                zCommand.Parameters.Add("@DBHC_ID", SqlDbType.NVarChar).Value = _DBHC_ID;
                zCommand.Parameters.Add("@DBHC_Name", SqlDbType.NVarChar).Value = _DBHC_Name;
                zCommand.Parameters.Add("@CQThuID", SqlDbType.NVarChar).Value = _CQThuID;
                zCommand.Parameters.Add("@CQThuName", SqlDbType.NVarChar).Value = _CQThuName;
                zCommand.Parameters.Add("@LoaiThueID", SqlDbType.NVarChar).Value = _LoaiThueID;
                zCommand.Parameters.Add("@LoaiThueName", SqlDbType.NVarChar).Value = _LoaiThueName;
                zCommand.Parameters.Add("@ToKhai", SqlDbType.NVarChar).Value = _ToKhai;
                if (_NgayDK == DateTime.MinValue)
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = _NgayDK;
                zCommand.Parameters.Add("@LH_ID", SqlDbType.NVarChar).Value = _LH_ID;
                zCommand.Parameters.Add("@LH_Name", SqlDbType.NVarChar).Value = _LH_Name;
                zCommand.Parameters.Add("@SoTien", SqlDbType.Money).Value = _SoTien;
                zCommand.Parameters.Add("@MessageReturn", SqlDbType.NVarChar).Value = _MessageReturn;
                if (_CreatedOn == DateTime.MinValue)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_Auto == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Data_Fail WHERE Auto = @Auto";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = _Auto;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
