﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class TimerDelay_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _TimerWaitMST = 0;
        private int _TimerSleepAfterMST = 0;
        private int _TimerWaitFill = 0;
        private int _TimerSleepAfterFill = 0;
        private int _TimerWaitSave = 0;
        private int _TimerSleepAfterSave = 0;
        private int _TimerLoad = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public TimerDelay_Info()
        {
        }
        public TimerDelay_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM TimerDelay WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _TimerWaitMST = int.Parse(zReader["TimerWaitMST"].ToString());
                    _TimerSleepAfterMST = int.Parse(zReader["TimerSleepAfterMST"].ToString());
                    _TimerWaitFill = int.Parse(zReader["TimerWaitFill"].ToString());
                    _TimerSleepAfterFill = int.Parse(zReader["TimerSleepAfterFill"].ToString());
                    _TimerWaitSave = int.Parse(zReader["TimerWaitSave"].ToString());
                    _TimerSleepAfterSave = int.Parse(zReader["TimerSleepAfterSave"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int TimerWaitMST
        {
            get { return _TimerWaitMST; }
            set { _TimerWaitMST = value; }
        }
        public int TimerSleepAfterMST
        {
            get { return _TimerSleepAfterMST; }
            set { _TimerSleepAfterMST = value; }
        }
        public int TimerWaitFill
        {
            get { return _TimerWaitFill; }
            set { _TimerWaitFill = value; }
        }
        public int TimerSleepAfterFill
        {
            get { return _TimerSleepAfterFill; }
            set { _TimerSleepAfterFill = value; }
        }
        public int TimerWaitSave
        {
            get { return _TimerWaitSave; }
            set { _TimerWaitSave = value; }
        }
        public int TimerSleepAfterSave
        {
            get { return _TimerSleepAfterSave; }
            set { _TimerSleepAfterSave = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int TimerLoad { get => _TimerLoad; set => _TimerLoad = value; }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TimerDelay ("
        + " TimerWaitMST ,TimerSleepAfterMST ,TimerWaitFill ,TimerSleepAfterFill ,TimerWaitSave ,TimerSleepAfterSave ) "
         + " VALUES ( "
         + "@TimerWaitMST ,@TimerSleepAfterMST ,@TimerWaitFill ,@TimerSleepAfterFill ,@TimerWaitSave ,@TimerSleepAfterSave ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TimerWaitMST", SqlDbType.Int).Value = _TimerWaitMST;
                zCommand.Parameters.Add("@TimerSleepAfterMST", SqlDbType.Int).Value = _TimerSleepAfterMST;
                zCommand.Parameters.Add("@TimerWaitFill", SqlDbType.Int).Value = _TimerWaitFill;
                zCommand.Parameters.Add("@TimerSleepAfterFill", SqlDbType.Int).Value = _TimerSleepAfterFill;
                zCommand.Parameters.Add("@TimerWaitSave", SqlDbType.Int).Value = _TimerWaitSave;
                zCommand.Parameters.Add("@TimerSleepAfterSave", SqlDbType.Int).Value = _TimerSleepAfterSave;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE TimerDelay SET "
                        + " TimerWaitMST = @TimerWaitMST,"
                        + " TimerSleepAfterMST = @TimerSleepAfterMST,"
                        + " TimerWaitFill = @TimerWaitFill,"
                        + " TimerSleepAfterFill = @TimerSleepAfterFill,"
                        + " TimerWaitSave = @TimerWaitSave,"
                        + " TimerSleepAfterSave = @TimerSleepAfterSave"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TimerWaitMST", SqlDbType.Int).Value = _TimerWaitMST;
                zCommand.Parameters.Add("@TimerSleepAfterMST", SqlDbType.Int).Value = _TimerSleepAfterMST;
                zCommand.Parameters.Add("@TimerWaitFill", SqlDbType.Int).Value = _TimerWaitFill;
                zCommand.Parameters.Add("@TimerSleepAfterFill", SqlDbType.Int).Value = _TimerSleepAfterFill;
                zCommand.Parameters.Add("@TimerWaitSave", SqlDbType.Int).Value = _TimerWaitSave;
                zCommand.Parameters.Add("@TimerSleepAfterSave", SqlDbType.Int).Value = _TimerSleepAfterSave;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update_Load()
        {
            string zSQL = "UPDATE TimerDelay SET "
                        + " TimerWaitMST = @TimerWaitMST,"
                        + " TimerLoad = @TimerLoad"
                       + " WHERE AutoKey = 1";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TimerWaitMST", SqlDbType.Int).Value = _TimerWaitMST;
                zCommand.Parameters.Add("@TimerLoad", SqlDbType.Int).Value = _TimerLoad;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TimerDelay WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
