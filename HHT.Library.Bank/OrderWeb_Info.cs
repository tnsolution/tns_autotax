﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.BNK
{
    public class OrderWeb_Info
    {

        #region [ Field Name ]
        private int _AuTo = 0;
        private string _SoCT = "";
        private string _SoTK = "";
        private string _Ngay = "";
        private string _LHXNK = "";
        private string _TenLH = "";
        private string _MaSoThue = "";
        private string _TenCTy = "";
        private double _TongTien = 0;
        private DateTime _CreatedOn;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AuTo
        {
            get { return _AuTo; }
            set { _AuTo = value; }
        }
        public string SoCT
        {
            get { return _SoCT; }
            set { _SoCT = value; }
        }
        public string SoTK
        {
            get { return _SoTK; }
            set { _SoTK = value; }
        }
        public string Ngay
        {
            get { return _Ngay; }
            set { _Ngay = value; }
        }
        public string LHXNK
        {
            get { return _LHXNK; }
            set { _LHXNK = value; }
        }
        public string TenLH
        {
            get { return _TenLH; }
            set { _TenLH = value; }
        }
        public string MaSoThue
        {
            get { return _MaSoThue; }
            set { _MaSoThue = value; }
        }
        public string TenCTy
        {
            get { return _TenCTy; }
            set { _TenCTy = value; }
        }
        public double TongTien
        {
            get { return _TongTien; }
            set { _TongTien = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public OrderWeb_Info()
        {
        }
        public OrderWeb_Info(int AuTo)
        {
            string zSQL = "SELECT * FROM BNK_OrderWeb WHERE AuTo = @AuTo";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AuTo", SqlDbType.Int).Value = AuTo;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AuTo"] != DBNull.Value)
                        _AuTo = int.Parse(zReader["AuTo"].ToString());
                    _SoCT = zReader["SoCT"].ToString();
                    _SoTK = zReader["SoTK"].ToString();
                    _Ngay = zReader["Ngay"].ToString();
                    _LHXNK = zReader["LHXNK"].ToString();
                    _TenLH = zReader["TenLH"].ToString();
                    _MaSoThue = zReader["MaSoThue"].ToString();
                    _TenCTy = zReader["TenCTy"].ToString();
                    if (zReader["TongTien"] != DBNull.Value)
                        _TongTien = double.Parse(zReader["TongTien"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }


        public OrderWeb_Info(string So_Ct)
        {
            string zSQL = "SELECT * FROM BNK_OrderWeb WHERE SoCT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SoCT", SqlDbType.NVarChar).Value = So_Ct;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AuTo"] != DBNull.Value)
                        _AuTo = int.Parse(zReader["AuTo"].ToString());
                    _SoCT = zReader["SoCT"].ToString();
                    _SoTK = zReader["SoTK"].ToString();
                    _Ngay = zReader["Ngay"].ToString();
                    _LHXNK = zReader["LHXNK"].ToString();
                    _TenLH = zReader["TenLH"].ToString();
                    _MaSoThue = zReader["MaSoThue"].ToString();
                    _TenCTy = zReader["TenCTy"].ToString();
                    if (zReader["TongTien"] != DBNull.Value)
                        _TongTien = double.Parse(zReader["TongTien"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO BNK_OrderWeb ("
        + " SoCT ,SoTK ,Ngay ,LHXNK ,TenLH ,MaSoThue ,TenCTy ,TongTien ,CreatedOn ) "
         + " VALUES ( "
         + "@SoCT ,@SoTK ,@Ngay ,@LHXNK ,@TenLH ,@MaSoThue ,@TenCTy ,@TongTien ,@CreatedOn ) "
         + " SELECT AuTo FROM BNK_OrderWeb WHERE AuTo = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AuTo", SqlDbType.Int).Value = _AuTo;
                zCommand.Parameters.Add("@SoCT", SqlDbType.NVarChar).Value = _SoCT;
                zCommand.Parameters.Add("@SoTK", SqlDbType.NVarChar).Value = _SoTK;
                zCommand.Parameters.Add("@Ngay", SqlDbType.NVarChar).Value = _Ngay;
                zCommand.Parameters.Add("@LHXNK", SqlDbType.NVarChar).Value = _LHXNK;
                zCommand.Parameters.Add("@TenLH", SqlDbType.NVarChar).Value = _TenLH;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = _MaSoThue;
                zCommand.Parameters.Add("@TenCTy", SqlDbType.NVarChar).Value = _TenCTy;
                zCommand.Parameters.Add("@TongTien", SqlDbType.Money).Value = _TongTien;
                if (_CreatedOn == DateTime.MinValue)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                _AuTo = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE BNK_OrderWeb SET "
                        + " SoCT = @SoCT,"
                        + " SoTK = @SoTK,"
                        + " Ngay = @Ngay,"
                        + " LHXNK = @LHXNK,"
                        + " TenLH = @TenLH,"
                        + " MaSoThue = @MaSoThue,"
                        + " TenCTy = @TenCTy,"
                        + " TongTien = @TongTien,"
                        + " CreatedOn = @CreatedOn"
                       + " WHERE AuTo = @AuTo";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AuTo", SqlDbType.Int).Value = _AuTo;
                zCommand.Parameters.Add("@SoCT", SqlDbType.NVarChar).Value = _SoCT;
                zCommand.Parameters.Add("@SoTK", SqlDbType.NVarChar).Value = _SoTK;
                zCommand.Parameters.Add("@Ngay", SqlDbType.NVarChar).Value = _Ngay;
                zCommand.Parameters.Add("@LHXNK", SqlDbType.NVarChar).Value = _LHXNK;
                zCommand.Parameters.Add("@TenLH", SqlDbType.NVarChar).Value = _TenLH;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = _MaSoThue;
                zCommand.Parameters.Add("@TenCTy", SqlDbType.NVarChar).Value = _TenCTy;
                zCommand.Parameters.Add("@TongTien", SqlDbType.Money).Value = _TongTien;
                if (_CreatedOn == DateTime.MinValue)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_AuTo == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_OrderWeb WHERE AuTo = @AuTo";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AuTo", SqlDbType.Int).Value = _AuTo;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
