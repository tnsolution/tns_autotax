﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using HHT.Connect;

namespace HHT.Library.Bank
{
    public class Rule_Info
    {

        #region [ Field Name ]
        private int _RuleID = 0;
        private string _RuleName = "";
        private int _RuleStyle = 0;
        private int _LengMax = 0;
        private int[] _LengValue;
        private int _CategoryID = 0;
        private int _Rank = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Rule_Info()
        {
        }
        public Rule_Info(int RuleID, int RuleStyle)
        {
            string zSQL = "SELECT * FROM [dbo].[Rule] WHERE RuleID = @RuleID AND RuleStyle = @RuleStyle";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RuleID", SqlDbType.Int).Value = RuleID;
                zCommand.Parameters.Add("@RuleStyle", SqlDbType.Int).Value = RuleStyle;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RuleID = int.Parse(zReader["RuleID"].ToString());
                    _RuleName = zReader["RuleName"].ToString();
                    _LengMax = int.Parse(zReader["LengMax"].ToString());
                    string[] zLengthValue = zReader["LengValue"].ToString().Split(';');
                    _LengValue = new int[zLengthValue.Length];
                    for (int i = 0; i < zLengthValue.Length; i++)
                    {
                        _LengValue[i] = int.Parse(zLengthValue[i]);
                    }
                    _RuleStyle = int.Parse(zReader["RuleStyle"].ToString());
                    _CategoryID = int.Parse(zReader["CategoryID"].ToString());
                    _Rank = int.Parse(zReader["Rank"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        public Rule_Info(DataRow zReader)
        {
            try
            {
                _RuleID = int.Parse(zReader["RuleID"].ToString());
                _RuleStyle = int.Parse(zReader["RuleStyle"].ToString());
                _RuleName = zReader["RuleName"].ToString();
                _LengMax = int.Parse(zReader["LengMax"].ToString());
                string[] zLengthValue = zReader["LengValue"].ToString().Split(';');
                _LengValue = new int[zLengthValue.Length];
                for (int i = 0; i < zLengthValue.Length; i++)
                {
                    _LengValue[i] = int.Parse(zLengthValue[i]);
                }
                _CategoryID = int.Parse(zReader["CategoryID"].ToString());

                _Rank = int.Parse(zReader["Rank"].ToString());

            }
            catch (Exception Err) { _Message = Err.ToString(); }

        }
        #endregion

        #region [ Properties ]
        public int RuleID
        {
            get { return _RuleID; }
            set { _RuleID = value; }
        }
        public string RuleName
        {
            get { return _RuleName; }
            set { _RuleName = value; }
        }
        public int RuleStyle
        {
            get { return _RuleStyle; }
            set { _RuleStyle = value; }
        }
        
        public int LengMax
        {
            get { return _LengMax; }
            set { _LengMax = value; }
        }
        public int[] LengValue
        {
            get { return _LengValue; }
            set { _LengValue = value; }
        }

        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]


        #endregion
    }
}
