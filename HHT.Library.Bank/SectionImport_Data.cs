﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT.Library.Bank
{
    public class SectionImport_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = "SELECT A.*,B.UserName, dbo.GetAmountSuccess(SectionNo) AS AmountSuccess FROM SectionImport A"
                        + " LEFT JOIN SYS_User B ON A.UserKey = B.UserKey"
                        + " WHERE CreatedOn BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListNew(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            /*  string zSQL = "SELECT A.*,B.UserName, dbo.GetAmount_StatusAnalysis(SectionNo,1) AS AmountAnalysis_1,dbo.GetAmount_StatusAnalysis(SectionNo,2) AS AmountAnalysis_2 FROM SectionImport A"
                          + " LEFT JOIN SYS_User B ON A.UserKey = B.UserKey"
                          + " WHERE CreatedOn BETWEEN @FromDate AND @ToDate";
                          */
            string zSQL = "SELECT A.*,B.UserName, dbo.GetAmount_StatusAnalysis(SectionNo,0) AS AmountFail,0 AS AmountAnalysis_2 FROM SectionImport A"
           + " LEFT JOIN SYS_User B ON A.UserKey = B.UserKey"
           + " WHERE CreatedOn BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListToWeb(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = "SELECT *, dbo.GetAmountToWeb(SectionNo) AS AmountToWeb, dbo.GetAmountWebSuccess(SectionNo) AS AmountSuccess FROM SectionImport WHERE CreatedOn BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable CountDataImportList(int SectionNo)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT COUNT(*) FROM SectionImport ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Export_Excel(DateTime FromDate , DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = "SELECT * FROM BNK_OrderTranfer WHERE CreatedOn BETWEEN @FromDate AND @ToDate AND StatusAnalysis = 0 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Export_Excel_2(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = "SELECT * FROM BNK_OrderTranfer WHERE CreatedOn BETWEEN @FromDate AND @ToDate AND StatusAnalysis = 2 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Export_Section(int SectionNo)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.trref,B.name_16,A.NgayNT,A.So_CT,
A.So_BT,A.SoTien,B.remark,B.ordcust,B.bencust 
FROM [dbo].[BNK_Order] A 
LEFT JOIN [dbo].[BNK_OrderTranfer] B ON A.trref = B.trref 
WHERE A.SectionNo = @SectionNo AND A.StatusAnalysis = 1  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = SectionNo;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        // đông 02/04/2021
        public static string  Delete_Export_Excel(DateTime FromDate, DateTime ToDate)
        {
            string zResult = "";
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            string zSQL = "UPDATE BNK_OrderTranfer SET StatusAnalysis=-1 WHERE CreatedOn BETWEEN @FromDate AND @ToDate AND StatusAnalysis = 0 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            return zResult;
        }

    }
}
