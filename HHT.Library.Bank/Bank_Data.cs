﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HHT.Connect;
using System.Data.SqlClient;

namespace HHT.Library.Bank
{
    public class Bank_Data
    {
        public static DataTable TaiKhoanNSNN(string OrderBy)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[FNC_GovTax_Account] A";
            zSQL += " LEFT JOIN GOV_TreasuryVN B ON A.TreasuryID = B.TreasuryID ";
            zSQL += " ORDER BY " + OrderBy;
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable TaiKhoanNSNN(string SearchContent, string OrderBy)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[FNC_GovTax_Account] A";
            zSQL += " LEFT JOIN GOV_TreasuryVN B ON A.TreasuryID = B.TreasuryID ";
            zSQL += " WHERE AccountID LIKE @SearchContent OR AccountName LIKE @SearchContent OR QHNS LIKE @SearchContent";
            zSQL += " OR DepartmentName LIKE @SearchContent OR TreasuryName LIKE @SearchContent ";
            zSQL += " ORDER BY " + OrderBy;
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SearchContent", SqlDbType.NVarChar).Value = "%" + SearchContent +"%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
