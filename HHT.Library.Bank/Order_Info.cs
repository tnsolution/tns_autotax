﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

using HHT.Connect;
namespace HHT.Library.Bank
{
    public class Order_Info
    {

        #region [ Field Name ]
        private string _trref = "";
        private string _So_CT = "";
        private string _So_BT = "";

        private string _NHNN_ID = "";
        private string _NHNN_Name = "";
        private int _HinhThucThu = 0;
        private string _NHPV_ID = "";
        private string _NHPV_Name = "";
        private DateTime? _NgayNT = null;

        private string _MST = "";
        private string _TenCty = "";
        private string _DiaChi = "";

        private string _MST_NopThay = "";
        private string _TenCty_NopThay = "";
        private string _DiaChi_NopThay = "";

        private string _TKThuNS_ID = "";
        private string _TKThuNS_Name = "";
        private string _DVSDNS_ID = "";
        private string _DVSDNS_Name = "";

        private string _DBHC_ID = "";
        private string _DBHC_Name = "";
        private string _CQThu_ID = "";
        private string _CQThu_Name = "";

        private string _LoaiThue_ID = "";
        private string _LoaiThue_Name = "";
        private string _ToKhai = "";
        private int _ToKhaiIndex = -1;
        private DateTime? _NgayDK = null;
        private string _LH_ID = "";
        private string _LH_Name = "";
        private double _SoTien = 0;
        private int _SectionNo = 0;
        private int _OrderStatus = 0;
        private int _StatusAnalysis = 0;

        private string _MessageReturn = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _Message = "";

        #endregion

        #region [ Constructor Get Information ]
        public Order_Info()
        {
        }

        public Order_Info(string ID)
        {
            string zSQL = "SELECT * FROM BNK_Order WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _trref = zReader["trref"].ToString();
                    _trref = zReader["trref"].ToString();
                    _So_CT = zReader["So_CT"].ToString();
                    _So_BT = zReader["So_BT"].ToString();
                    _NHNN_ID = zReader["NHNN_ID"].ToString();
                    _NHNN_Name = zReader["NHNN_Name"].ToString();
                    _HinhThucThu = int.Parse(zReader["HinhThucThu"].ToString());
                    _NHPV_ID = zReader["NHPV_ID"].ToString();
                    _NHPV_Name = zReader["NHPV_Name"].ToString();
                    if (zReader["NgayNT"] != DBNull.Value)
                        _NgayNT = (DateTime)zReader["NgayNT"];
                    _MST = zReader["MST"].ToString();
                    _TenCty = zReader["TenCty"].ToString();
                    _DiaChi = zReader["DiaChi"].ToString();
                    _MST_NopThay = zReader["MST_NopThay"].ToString();
                    _TenCty_NopThay = zReader["TenCty_NopThay"].ToString();
                    _DiaChi_NopThay = zReader["DiaChi_NopThay"].ToString();
                    _TKThuNS_ID = zReader["TKThuNS_ID"].ToString();
                    _TKThuNS_Name = zReader["TKThuNS_Name"].ToString();
                    _DVSDNS_ID = zReader["DVSDNS_ID"].ToString();
                    _DVSDNS_Name = zReader["DVSDNS_Name"].ToString();
                    _DBHC_ID = zReader["DBHC_ID"].ToString();
                    _DBHC_Name = zReader["DBHC_Name"].ToString();
                    _CQThu_ID = zReader["CQThu_ID"].ToString();
                    _CQThu_Name = zReader["CQThu_Name"].ToString();
                    _LoaiThue_ID = zReader["LoaiThue_ID"].ToString();
                    _LoaiThue_Name = zReader["LoaiThue_Name"].ToString();
                    _ToKhai = zReader["ToKhai"].ToString();
                    if (zReader["NgayDK"] != DBNull.Value)
                        _NgayDK = (DateTime)zReader["NgayDK"];
                    _LH_ID = zReader["LH_ID"].ToString();
                    _LH_Name = zReader["LH_Name"].ToString();
                    _SoTien = double.Parse(zReader["SoTien"].ToString());
                    _SectionNo = int.Parse(zReader["SectionNo"].ToString());
                    _OrderStatus = int.Parse(zReader["OrderStatus"].ToString());
                    _StatusAnalysis = int.Parse(zReader["StatusAnalysis"].ToString());
                    _MessageReturn = zReader["MessageReturn"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _Message = "200 OK";
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]

        public string trref
        {
            get { return _trref; }
            set { _trref = value; }
        }
        public string So_CT
        {
            get { return _So_CT; }
            set { _So_CT = value; }
        }
        public string So_BT
        {
            get { return _So_BT; }
            set { _So_BT = value; }
        }
        public string NHNN_ID
        {
            get { return _NHNN_ID; }
            set { _NHNN_ID = value; }
        }
        public string NHNN_Name
        {
            get { return _NHNN_Name; }
            set { _NHNN_Name = value; }
        }
        public int HinhThucThu
        {
            get { return _HinhThucThu; }
            set { _HinhThucThu = value; }
        }
        public string NHPV_ID
        {
            get { return _NHPV_ID; }
            set { _NHPV_ID = value; }
        }
        public string NHPV_Name
        {
            get { return _NHPV_Name; }
            set { _NHPV_Name = value; }
        }
        public DateTime? NgayNT
        {
            get { return _NgayNT; }
            set { _NgayNT = value.Value; }
        }
        public string MST
        {
            get { return _MST; }
            set { _MST = value; }
        }
        public string TenCty
        {
            get { return _TenCty; }
            set { _TenCty = value; }
        }
        public string DiaChi
        {
            get { return _DiaChi; }
            set { _DiaChi = value; }
        }
        public string MST_NopThay
        {
            get { return _MST_NopThay; }
            set { _MST_NopThay = value; }
        }
        public string TenCty_NopThay
        {
            get { return _TenCty_NopThay; }
            set { _TenCty_NopThay = value; }
        }
        public string DiaChi_NopThay
        {
            get { return _DiaChi_NopThay; }
            set { _DiaChi_NopThay = value; }
        }
        public string TKThuNS_ID
        {
            get { return _TKThuNS_ID; }
            set { _TKThuNS_ID = value; }
        }
        public string TKThuNS_Name
        {
            get { return _TKThuNS_Name; }
            set { _TKThuNS_Name = value; }
        }
        public string DVSDNS_ID
        {
            get { return _DVSDNS_ID; }
            set { _DVSDNS_ID = value; }
        }
        public string DVSDNS_Name
        {
            get { return _DVSDNS_Name; }
            set { _DVSDNS_Name = value; }
        }
        public string DBHC_ID
        {
            get { return _DBHC_ID; }
            set { _DBHC_ID = value; }
        }
        public string DBHC_Name
        {
            get { return _DBHC_Name; }
            set { _DBHC_Name = value; }
        }
        public string CQThu_ID
        {
            get { return _CQThu_ID; }
            set { _CQThu_ID = value; }
        }
        public string CQThu_Name
        {
            get { return _CQThu_Name; }
            set { _CQThu_Name = value; }
        }
        public string LoaiThue_ID
        {
            get { return _LoaiThue_ID; }
            set { _LoaiThue_ID = value; }
        }
        public string LoaiThue_Name
        {
            get { return _LoaiThue_Name; }
            set { _LoaiThue_Name = value; }
        }
        public string ToKhai
        {
            get { return _ToKhai; }
            set { _ToKhai = value; }
        }
        public DateTime? NgayDK
        {
            get { return _NgayDK; }
            set { _NgayDK = value; }
        }
        public string LH_ID
        {
            get { return _LH_ID; }
            set { _LH_ID = value; }
        }
        public string LH_Name
        {
            get { return _LH_Name; }
            set { _LH_Name = value; }
        }
        public double SoTien
        {
            get { return _SoTien; }
            set { _SoTien = value; }
        }
        public int SectionNo
        {
            get { return _SectionNo; }
            set { _SectionNo = value; }
        }
        public int OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }
        public int StatusAnalysis
        {
            get { return _StatusAnalysis; }
            set { _StatusAnalysis = value; }
        }
        public string MessageReturn
        {
            get { return _MessageReturn; }
            set { _MessageReturn = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[BNK_Order] ("
            + " trref ,So_CT ,So_BT ,NHNN_ID ,NHNN_Name ,HinhThucThu ,NHPV_ID ,NHPV_Name ,NgayNT ,MST ,TenCty ,DiaChi ,MST_NopThay ,TenCty_NopThay ,DiaChi_NopThay ,TKThuNS_ID ,TKThuNS_Name ,DVSDNS_ID ,DVSDNS_Name ,DBHC_ID ,DBHC_Name ,CQThu_ID ,CQThu_Name ,LoaiThue_ID ,LoaiThue_Name ,ToKhai ,NgayDK ,LH_ID ,LH_Name ,SoTien ,SectionNo ,OrderStatus ,StatusAnalysis ,MessageReturn ,CreatedBy ) "
            + " VALUES ( "
            + "@trref ,@So_CT ,@So_BT ,@NHNN_ID ,@NHNN_Name ,@HinhThucThu ,@NHPV_ID ,@NHPV_Name ,@NgayNT ,@MST ,@TenCty ,@DiaChi ,@MST_NopThay ,@TenCty_NopThay ,@DiaChi_NopThay ,@TKThuNS_ID ,@TKThuNS_Name ,@DVSDNS_ID ,@DVSDNS_Name ,@DBHC_ID ,@DBHC_Name ,@CQThu_ID ,@CQThu_Name ,@LoaiThue_ID ,@LoaiThue_Name ,@ToKhai ,@NgayDK ,@LH_ID ,@LH_Name ,@SoTien ,@SectionNo ,@OrderStatus ,@StatusAnalysis ,@MessageReturn ,@CreatedBy ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@So_CT", SqlDbType.NVarChar).Value = _So_CT;
                zCommand.Parameters.Add("@So_BT", SqlDbType.NVarChar).Value = _So_BT;
                zCommand.Parameters.Add("@NHNN_ID", SqlDbType.NVarChar).Value = _NHNN_ID;
                zCommand.Parameters.Add("@NHNN_Name", SqlDbType.NVarChar).Value = _NHNN_Name;
                zCommand.Parameters.Add("@HinhThucThu", SqlDbType.Int).Value = _HinhThucThu;
                zCommand.Parameters.Add("@NHPV_ID", SqlDbType.NVarChar).Value = _NHPV_ID;
                zCommand.Parameters.Add("@NHPV_Name", SqlDbType.NVarChar).Value = _NHPV_Name;
                if (_NgayNT == null)
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = _NgayNT;
                zCommand.Parameters.Add("@MST", SqlDbType.NVarChar).Value = _MST;
                zCommand.Parameters.Add("@TenCty", SqlDbType.NVarChar).Value = _TenCty;
                zCommand.Parameters.Add("@DiaChi", SqlDbType.NVarChar).Value = _DiaChi;
                zCommand.Parameters.Add("@MST_NopThay", SqlDbType.NVarChar).Value = _MST_NopThay;
                zCommand.Parameters.Add("@TenCty_NopThay", SqlDbType.NChar).Value = _TenCty_NopThay;
                zCommand.Parameters.Add("@DiaChi_NopThay", SqlDbType.NVarChar).Value = _DiaChi_NopThay;
                zCommand.Parameters.Add("@TKThuNS_ID", SqlDbType.NVarChar).Value = _TKThuNS_ID;
                zCommand.Parameters.Add("@TKThuNS_Name", SqlDbType.NVarChar).Value = _TKThuNS_Name;
                zCommand.Parameters.Add("@DVSDNS_ID", SqlDbType.NVarChar).Value = _DVSDNS_ID;
                zCommand.Parameters.Add("@DVSDNS_Name", SqlDbType.NVarChar).Value = _DVSDNS_Name;
                zCommand.Parameters.Add("@DBHC_ID", SqlDbType.NVarChar).Value = _DBHC_ID;
                zCommand.Parameters.Add("@DBHC_Name", SqlDbType.NVarChar).Value = _DBHC_Name;
                zCommand.Parameters.Add("@CQThu_ID", SqlDbType.NVarChar).Value = _CQThu_ID;
                zCommand.Parameters.Add("@CQThu_Name", SqlDbType.NVarChar).Value = _CQThu_Name;
                zCommand.Parameters.Add("@LoaiThue_ID", SqlDbType.NVarChar).Value = _LoaiThue_ID;
                zCommand.Parameters.Add("@LoaiThue_Name", SqlDbType.NVarChar).Value = _LoaiThue_Name;
                zCommand.Parameters.Add("@ToKhai", SqlDbType.NVarChar).Value = _ToKhai;
                if (_NgayDK == null)
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = _NgayDK;
                zCommand.Parameters.Add("@LH_ID", SqlDbType.NVarChar).Value = _LH_ID;
                zCommand.Parameters.Add("@LH_Name", SqlDbType.NVarChar).Value = _LH_Name;
                zCommand.Parameters.Add("@SoTien", SqlDbType.Money).Value = _SoTien;
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = _SectionNo;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = _OrderStatus;
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = _StatusAnalysis;
                zCommand.Parameters.Add("@MessageReturn", SqlDbType.NVarChar).Value = _MessageReturn;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE [dbo].[BNK_Order] SET "
                        + " So_CT = @So_CT,"
                        + " So_BT = @So_BT,"
                        + " NHNN_ID = @NHNN_ID,"
                        + " NHNN_Name = @NHNN_Name,"
                        + " HinhThucThu = @HinhThucThu,"
                        + " NHPV_ID = @NHPV_ID,"
                        + " NHPV_Name = @NHPV_Name,"
                        + " NgayNT = @NgayNT,"
                        + " MST = @MST,"
                        + " TenCty = @TenCty,"
                        + " DiaChi = @DiaChi,"
                        + " MST_NopThay = @MST_NopThay,"
                        + " TenCty_NopThay = @TenCty_NopThay,"
                        + " DiaChi_NopThay = @DiaChi_NopThay,"
                        + " TKThuNS_ID = @TKThuNS_ID,"
                        + " TKThuNS_Name = @TKThuNS_Name,"
                        + " DVSDNS_ID = @DVSDNS_ID,"
                        + " DVSDNS_Name = @DVSDNS_Name,"
                        + " DBHC_ID = @DBHC_ID,"
                        + " DBHC_Name = @DBHC_Name,"
                        + " CQThu_ID = @CQThu_ID,"
                        + " CQThu_Name = @CQThu_Name,"
                        + " LoaiThue_ID = @LoaiThue_ID,"
                        + " LoaiThue_Name = @LoaiThue_Name,"
                        + " ToKhai = @ToKhai,"
                        + " NgayDK = @NgayDK,"
                        + " LH_ID = @LH_ID,"
                        + " LH_Name = @LH_Name,"
                        + " SoTien = @SoTien,"
                        + " SectionNo = @SectionNo,"
                        + " OrderStatus = @OrderStatus,"
                        + " StatusAnalysis = @StatusAnalysis,"
                        + " MessageReturn = @MessageReturn "
                        + " WHERE trref = @trref";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@So_CT", SqlDbType.NVarChar).Value = _So_CT;
                zCommand.Parameters.Add("@So_BT", SqlDbType.NVarChar).Value = _So_BT;
                zCommand.Parameters.Add("@NHNN_ID", SqlDbType.NVarChar).Value = _NHNN_ID;
                zCommand.Parameters.Add("@NHNN_Name", SqlDbType.NVarChar).Value = _NHNN_Name;
                zCommand.Parameters.Add("@HinhThucThu", SqlDbType.Int).Value = _HinhThucThu;
                zCommand.Parameters.Add("@NHPV_ID", SqlDbType.NVarChar).Value = _NHPV_ID;
                zCommand.Parameters.Add("@NHPV_Name", SqlDbType.NVarChar).Value = _NHPV_Name;
                if (_NgayNT == null)
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayNT", SqlDbType.Date).Value = _NgayNT;
                zCommand.Parameters.Add("@MST", SqlDbType.NVarChar).Value = _MST;
                zCommand.Parameters.Add("@TenCty", SqlDbType.NVarChar).Value = _TenCty;
                zCommand.Parameters.Add("@DiaChi", SqlDbType.NVarChar).Value = _DiaChi;
                zCommand.Parameters.Add("@MST_NopThay", SqlDbType.NVarChar).Value = _MST_NopThay;
                zCommand.Parameters.Add("@TenCty_NopThay", SqlDbType.NChar).Value = _TenCty_NopThay;
                zCommand.Parameters.Add("@DiaChi_NopThay", SqlDbType.NVarChar).Value = _DiaChi_NopThay;
                zCommand.Parameters.Add("@TKThuNS_ID", SqlDbType.NVarChar).Value = _TKThuNS_ID;
                zCommand.Parameters.Add("@TKThuNS_Name", SqlDbType.NVarChar).Value = _TKThuNS_Name;
                zCommand.Parameters.Add("@DVSDNS_ID", SqlDbType.NVarChar).Value = _DVSDNS_ID;
                zCommand.Parameters.Add("@DVSDNS_Name", SqlDbType.NVarChar).Value = _DVSDNS_Name;
                zCommand.Parameters.Add("@DBHC_ID", SqlDbType.NVarChar).Value = _DBHC_ID;
                zCommand.Parameters.Add("@DBHC_Name", SqlDbType.NVarChar).Value = _DBHC_Name;
                zCommand.Parameters.Add("@CQThu_ID", SqlDbType.NVarChar).Value = _CQThu_ID;
                zCommand.Parameters.Add("@CQThu_Name", SqlDbType.NVarChar).Value = _CQThu_Name;
                zCommand.Parameters.Add("@LoaiThue_ID", SqlDbType.NVarChar).Value = _LoaiThue_ID;
                zCommand.Parameters.Add("@LoaiThue_Name", SqlDbType.NVarChar).Value = _LoaiThue_Name;
                zCommand.Parameters.Add("@ToKhai", SqlDbType.NVarChar).Value = _ToKhai;
                if (_NgayDK == null)
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayDK", SqlDbType.Date).Value = _NgayDK;
                zCommand.Parameters.Add("@LH_ID", SqlDbType.NVarChar).Value = _LH_ID;
                zCommand.Parameters.Add("@LH_Name", SqlDbType.NVarChar).Value = _LH_Name;
                zCommand.Parameters.Add("@SoTien", SqlDbType.Money).Value = _SoTien;
                zCommand.Parameters.Add("@SectionNo", SqlDbType.Int).Value = _SectionNo;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = _OrderStatus;
                zCommand.Parameters.Add("@StatusAnalysis", SqlDbType.Int).Value = _StatusAnalysis;
                zCommand.Parameters.Add("@MessageReturn", SqlDbType.NVarChar).Value = _MessageReturn;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_Order WHERE trref = @trref";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@trref", SqlDbType.Int).Value = _trref;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public bool IsHaveRecord
        {
            get
            {
                bool zResult = false;
                string zSQL = "SELECT * FROM BNK_Order WHERE trref = @trref";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zResult = true;
                }
                return zResult;
            }

        }
        public string UpdateMessage(int Status, string SoCT, string Mess, string SoBT)
        {
            _OrderStatus = Status;
            _So_CT = SoCT;
            _So_BT = SoBT;
            string zSQL = "UPDATE BNK_Order SET "
                        + " So_CT = @So_CT,"
                        + " So_BT = @So_BT,"
                        + " OrderStatus = @OrderStatus,"
                        + " TenCty = @TenCty,"
                        + " MessageReturn = @MessageReturn"
                        + " WHERE trref = @trref";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@OrderStatus", SqlDbType.Int).Value = _OrderStatus;
                zCommand.Parameters.Add("@So_CT", SqlDbType.NVarChar).Value = _So_CT;
                zCommand.Parameters.Add("@So_BT", SqlDbType.NVarChar).Value = _So_BT;
                zCommand.Parameters.Add("@TenCty", SqlDbType.NVarChar).Value = _TenCty;
                zCommand.Parameters.Add("@MessageReturn", SqlDbType.NVarChar).Value = Mess;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
