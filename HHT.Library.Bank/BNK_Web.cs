﻿using HHT.Connect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace HHT.Library.Bank
{
    public class BNK_Web
    {
        #region [ Field Name ]
        private int _STT = 0;
        private string _SHKB = "";
        private string _TenNganHang = "";
        private string _MaNv = "";
        private string _TenNv = "";
        private string _MaKS = "";
        private string _TenKs = "";
        private string _SoCT = "";
        private string _SoBT = "";
        private string _TKThuNS = "";
        private string _MaCQ = "";
        private string _TenCQThu = "";
        private string _SoTK = "";
        private DateTime _NgayTK;
        private string _LHXNK = "";
        private string _LHXNK_VietTat = "";
        private string _TenLHXNK = "";
        private string _MaSoThue = "";
        private string _TenNguoiNopThue = "";
        private string _DiaChi = "";
        private string _TongTien = "";
        private string _SysTrae = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int STT
        {
            get { return _STT; }
            set { _STT = value; }
        }
        public string SHKB
        {
            get { return _SHKB; }
            set { _SHKB = value; }
        }
        public string TenNganHang
        {
            get { return _TenNganHang; }
            set { _TenNganHang = value; }
        }
        public string MaNv
        {
            get { return _MaNv; }
            set { _MaNv = value; }
        }
        public string TenNv
        {
            get { return _TenNv; }
            set { _TenNv = value; }
        }
        public string MaKS
        {
            get { return _MaKS; }
            set { _MaKS = value; }
        }
        public string TenKs
        {
            get { return _TenKs; }
            set { _TenKs = value; }
        }
        public string SoCT
        {
            get { return _SoCT; }
            set { _SoCT = value; }
        }
        public string SoBT
        {
            get { return _SoBT; }
            set { _SoBT = value; }
        }
        public string TKThuNS
        {
            get { return _TKThuNS; }
            set { _TKThuNS = value; }
        }
        public string MaCQ
        {
            get { return _MaCQ; }
            set { _MaCQ = value; }
        }
        public string TenCQThu
        {
            get { return _TenCQThu; }
            set { _TenCQThu = value; }
        }
        public string SoTK
        {
            get { return _SoTK; }
            set { _SoTK = value; }
        }
        public DateTime NgayTK
        {
            get { return _NgayTK; }
            set { _NgayTK = value; }
        }
        public string LHXNK
        {
            get { return _LHXNK; }
            set { _LHXNK = value; }
        }
        public string LHXNK_VietTat
        {
            get { return _LHXNK_VietTat; }
            set { _LHXNK_VietTat = value; }
        }
        public string TenLHXNK
        {
            get { return _TenLHXNK; }
            set { _TenLHXNK = value; }
        }
        public string MaSoThue
        {
            get { return _MaSoThue; }
            set { _MaSoThue = value; }
        }
        public string TenNguoiNopThue
        {
            get { return _TenNguoiNopThue; }
            set { _TenNguoiNopThue = value; }
        }
        public string DiaChi
        {
            get { return _DiaChi; }
            set { _DiaChi = value; }
        }
        public string TongTien
        {
            get { return _TongTien; }
            set { _TongTien = value; }
        }
        public string SysTrae
        {
            get { return _SysTrae; }
            set { _SysTrae = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public BNK_Web()
        {
        }
        public BNK_Web(int STT)
        {
            string zSQL = "SELECT * FROM BNK_Web WHERE STT = @STT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@STT", SqlDbType.Int).Value = STT;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["STT"] != DBNull.Value)
                        _STT = int.Parse(zReader["STT"].ToString());
                    _SHKB = zReader["SHKB"].ToString();
                    _TenNganHang = zReader["TenNganHang"].ToString();
                    _MaNv = zReader["MaNv"].ToString();
                    _TenNv = zReader["TenNv"].ToString();
                    _MaKS = zReader["MaKS"].ToString();
                    _TenKs = zReader["TenKs"].ToString();
                    _SoCT = zReader["SoCT"].ToString();
                    _SoBT = zReader["SoBT"].ToString();
                    _TKThuNS = zReader["TKThuNS"].ToString();
                    _MaCQ = zReader["MaCQ"].ToString();
                    _TenCQThu = zReader["TenCQThu"].ToString();
                    _SoTK = zReader["SoTK"].ToString();
                    if (zReader["NgayTK"] != DBNull.Value)
                        _NgayTK = (DateTime)zReader["NgayTK"];
                    _LHXNK = zReader["LHXNK"].ToString();
                    _LHXNK_VietTat = zReader["LHXNK_VietTat"].ToString();
                    _TenLHXNK = zReader["TenLHXNK"].ToString();
                    _MaSoThue = zReader["MaSoThue"].ToString();
                    _TenNguoiNopThue = zReader["TenNguoiNopThue"].ToString();
                    _DiaChi = zReader["DiaChi"].ToString();
                    _TongTien = zReader["TongTien"].ToString();
                    _SysTrae = zReader["SysTrae"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public BNK_Web(string So_CT)
        {
            string zSQL = "SELECT * FROM BNK_Web WHERE SoCT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SoCT", SqlDbType.Int).Value = So_CT;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["STT"] != DBNull.Value)
                        _STT = int.Parse(zReader["STT"].ToString());
                    _SHKB = zReader["SHKB"].ToString();
                    _TenNganHang = zReader["TenNganHang"].ToString();
                    _MaNv = zReader["MaNv"].ToString();
                    _TenNv = zReader["TenNv"].ToString();
                    _MaKS = zReader["MaKS"].ToString();
                    _TenKs = zReader["TenKs"].ToString();
                    _SoCT = zReader["SoCT"].ToString();
                    _SoBT = zReader["SoBT"].ToString();
                    _TKThuNS = zReader["TKThuNS"].ToString();
                    _MaCQ = zReader["MaCQ"].ToString();
                    _TenCQThu = zReader["TenCQThu"].ToString();
                    _SoTK = zReader["SoTK"].ToString();
                    if (zReader["NgayTK"] != DBNull.Value)
                        _NgayTK = (DateTime)zReader["NgayTK"];
                    _LHXNK = zReader["LHXNK"].ToString();
                    _LHXNK_VietTat = zReader["LHXNK_VietTat"].ToString();
                    _TenLHXNK = zReader["TenLHXNK"].ToString();
                    _MaSoThue = zReader["MaSoThue"].ToString();
                    _TenNguoiNopThue = zReader["TenNguoiNopThue"].ToString();
                    _DiaChi = zReader["DiaChi"].ToString();
                    _TongTien = zReader["TongTien"].ToString();
                    _SysTrae = zReader["SysTrae"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO BNK_Web ("
        + " SHKB ,TenNganHang ,MaNv ,TenNv ,MaKS ,TenKs ,SoCT ,SoBT ,TKThuNS ,MaCQ ,TenCQThu ,SoTK ,NgayTK ,LHXNK ,LHXNK_VietTat ,TenLHXNK ,MaSoThue ,TenNguoiNopThue ,DiaChi ,TongTien ,SysTrae ) "
         + " VALUES ( "
         + "@SHKB ,@TenNganHang ,@MaNv ,@TenNv ,@MaKS ,@TenKs ,@SoCT ,@SoBT ,@TKThuNS ,@MaCQ ,@TenCQThu ,@SoTK ,@NgayTK ,@LHXNK ,@LHXNK_VietTat ,@TenLHXNK ,@MaSoThue ,@TenNguoiNopThue ,@DiaChi ,@TongTien ,@SysTrae ) "
         + " SELECT STT FROM BNK_Web WHERE STT = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@STT", SqlDbType.Int).Value = _STT;
                zCommand.Parameters.Add("@SHKB", SqlDbType.NVarChar).Value = _SHKB;
                zCommand.Parameters.Add("@TenNganHang", SqlDbType.NVarChar).Value = _TenNganHang;
                zCommand.Parameters.Add("@MaNv", SqlDbType.NVarChar).Value = _MaNv;
                zCommand.Parameters.Add("@TenNv", SqlDbType.NVarChar).Value = _TenNv;
                zCommand.Parameters.Add("@MaKS", SqlDbType.NVarChar).Value = _MaKS;
                zCommand.Parameters.Add("@TenKs", SqlDbType.NVarChar).Value = _TenKs;
                zCommand.Parameters.Add("@SoCT", SqlDbType.NVarChar).Value = _SoCT;
                zCommand.Parameters.Add("@SoBT", SqlDbType.NVarChar).Value = _SoBT;
                zCommand.Parameters.Add("@TKThuNS", SqlDbType.NVarChar).Value = _TKThuNS;
                zCommand.Parameters.Add("@MaCQ", SqlDbType.NVarChar).Value = _MaCQ;
                zCommand.Parameters.Add("@TenCQThu", SqlDbType.NVarChar).Value = _TenCQThu;
                zCommand.Parameters.Add("@SoTK", SqlDbType.NVarChar).Value = _SoTK;
                if (_NgayTK == DateTime.MinValue)
                    zCommand.Parameters.Add("@NgayTK", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayTK", SqlDbType.DateTime).Value = _NgayTK;
                zCommand.Parameters.Add("@LHXNK", SqlDbType.NVarChar).Value = _LHXNK;
                zCommand.Parameters.Add("@LHXNK_VietTat", SqlDbType.NVarChar).Value = _LHXNK_VietTat;
                zCommand.Parameters.Add("@TenLHXNK", SqlDbType.NVarChar).Value = _TenLHXNK;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = _MaSoThue;
                zCommand.Parameters.Add("@TenNguoiNopThue", SqlDbType.NVarChar).Value = _TenNguoiNopThue;
                zCommand.Parameters.Add("@DiaChi", SqlDbType.NVarChar).Value = _DiaChi;
                zCommand.Parameters.Add("@TongTien", SqlDbType.NVarChar).Value = _TongTien;
                zCommand.Parameters.Add("@SysTrae", SqlDbType.NVarChar).Value = _SysTrae;
                _STT = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE BNK_Web SET "
                        + " SHKB = @SHKB,"
                        + " TenNganHang = @TenNganHang,"
                        + " MaNv = @MaNv,"
                        + " TenNv = @TenNv,"
                        + " MaKS = @MaKS,"
                        + " TenKs = @TenKs,"
                        + " SoCT = @SoCT,"
                        + " SoBT = @SoBT,"
                        + " TKThuNS = @TKThuNS,"
                        + " MaCQ = @MaCQ,"
                        + " TenCQThu = @TenCQThu,"
                        + " SoTK = @SoTK,"
                        + " NgayTK = @NgayTK,"
                        + " LHXNK = @LHXNK,"
                        + " LHXNK_VietTat = @LHXNK_VietTat,"
                        + " TenLHXNK = @TenLHXNK,"
                        + " MaSoThue = @MaSoThue,"
                        + " TenNguoiNopThue = @TenNguoiNopThue,"
                        + " DiaChi = @DiaChi,"
                        + " TongTien = @TongTien,"
                        + " SysTrae = @SysTrae"
                       + " WHERE STT = @STT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@STT", SqlDbType.Int).Value = _STT;
                zCommand.Parameters.Add("@SHKB", SqlDbType.NVarChar).Value = _SHKB;
                zCommand.Parameters.Add("@TenNganHang", SqlDbType.NVarChar).Value = _TenNganHang;
                zCommand.Parameters.Add("@MaNv", SqlDbType.NVarChar).Value = _MaNv;
                zCommand.Parameters.Add("@TenNv", SqlDbType.NVarChar).Value = _TenNv;
                zCommand.Parameters.Add("@MaKS", SqlDbType.NVarChar).Value = _MaKS;
                zCommand.Parameters.Add("@TenKs", SqlDbType.NVarChar).Value = _TenKs;
                zCommand.Parameters.Add("@SoCT", SqlDbType.NVarChar).Value = _SoCT;
                zCommand.Parameters.Add("@SoBT", SqlDbType.NVarChar).Value = _SoBT;
                zCommand.Parameters.Add("@TKThuNS", SqlDbType.NVarChar).Value = _TKThuNS;
                zCommand.Parameters.Add("@MaCQ", SqlDbType.NVarChar).Value = _MaCQ;
                zCommand.Parameters.Add("@TenCQThu", SqlDbType.NVarChar).Value = _TenCQThu;
                zCommand.Parameters.Add("@SoTK", SqlDbType.NVarChar).Value = _SoTK;
                if (_NgayTK == DateTime.MinValue)
                    zCommand.Parameters.Add("@NgayTK", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayTK", SqlDbType.DateTime).Value = _NgayTK;
                zCommand.Parameters.Add("@LHXNK", SqlDbType.NVarChar).Value = _LHXNK;
                zCommand.Parameters.Add("@LHXNK_VietTat", SqlDbType.NVarChar).Value = _LHXNK_VietTat;
                zCommand.Parameters.Add("@TenLHXNK", SqlDbType.NVarChar).Value = _TenLHXNK;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = _MaSoThue;
                zCommand.Parameters.Add("@TenNguoiNopThue", SqlDbType.NVarChar).Value = _TenNguoiNopThue;
                zCommand.Parameters.Add("@DiaChi", SqlDbType.NVarChar).Value = _DiaChi;
                zCommand.Parameters.Add("@TongTien", SqlDbType.NVarChar).Value = _TongTien;
                zCommand.Parameters.Add("@SysTrae", SqlDbType.NVarChar).Value = _SysTrae;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_STT == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_Web WHERE STT = @STT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@STT", SqlDbType.Int).Value = _STT;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
