﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using HHT.Connect;

namespace HHT.Library.Bank
{ 
    public class RuleFormat_Info
    {

        #region [ Field Name ]
        private string _RuleID = "";
        private string _Signal = "";
        private string _Format = "";
        private int _CategoryID = 0;
        private int _Rank = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public RuleFormat_Info()
        {
        }
        public RuleFormat_Info(string RuleID)
        {
            string zSQL = "SELECT * FROM [dbo].[Rule_Format] WHERE RuleID = @RuleID ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RuleID", SqlDbType.NVarChar).Value = RuleID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RuleID = zReader["RuleID"].ToString();
                    _Signal = zReader["Signal"].ToString();
                    _Format = zReader["Format"].ToString();
                    _CategoryID = int.Parse(zReader["CategoryID"].ToString());
                    _Rank = int.Parse(zReader["Rank"].ToString());
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public string RuleID
        {
            get { return _RuleID; }
            set { _RuleID = value; }
        }
        public string Signal
        {
            get { return _Signal; }
            set { _Signal = value; }
        }
        public string Format
        {
            get { return _Format; }
            set { _Format = value; }
        }
        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

     

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[Rule_Format] ("
        + " RuleID ,Signal ,Format ,CategoryID ,Rank ) "
         + " VALUES ( "
         + "@RuleID ,@Signal ,@Format ,@CategoryID ,@Rank ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RuleID", SqlDbType.NVarChar).Value = _RuleID;
                zCommand.Parameters.Add("@Signal", SqlDbType.NVarChar).Value = _Signal;
                zCommand.Parameters.Add("@Format", SqlDbType.NVarChar).Value = _Format;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = _CategoryID;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE [dbo].[Rule_Format] SET "
                        + " Signal = @Signal,"
                        + " Format = @Format,"
                        + " CategoryID = @CategoryID,"
                        + " Rank = @Rank"
                       + " WHERE RuleID = @RuleID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RuleID", SqlDbType.NVarChar).Value = _RuleID;
                zCommand.Parameters.Add("@Signal", SqlDbType.NVarChar).Value = _Signal;
                zCommand.Parameters.Add("@Format", SqlDbType.NVarChar).Value = _Format;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = _CategoryID;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[Rule_Format] WHERE RuleID = @RuleID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RuleID", SqlDbType.NVarChar).Value = _RuleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
