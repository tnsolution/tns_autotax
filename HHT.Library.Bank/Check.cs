﻿using HHT.Connect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace HHT.Library.Bank
{
    public class Check
    {
        #region [ Field Name ]
        private double _Sotien = 0;
        private string _NDKT = "";
        private string _So_CT = "";
        private string _So_BT = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Check()
        {
        }
        public Check(string So_CT)
        {
            string zSQL = @"SELECT A.NDKT_ID , A.SoTien FROM [dbo].[BNK_Order_Detail] A 
LEFT JOIN[dbo].[BNK_Order] B ON B.trref = A.trref
WHERE So_CT = @SoCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SoCT", SqlDbType.NVarChar).Value = So_CT;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _NDKT = zReader["NDKT_ID"].ToString();
                    _Sotien = Double.Parse(zReader["SoTien"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        #endregion


        #region [ Properties ]
        public string So_BT { get => _So_BT; set => _So_BT = value; }
        public string So_CT { get => _So_CT; set => _So_CT = value; }
        public string NDKT { get => _NDKT; set => _NDKT = value; }
        public double Sotien { get => _Sotien; set => _Sotien = value; }
        #endregion
    }
}
