﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;
namespace HHT.Library.Bank
{
    public class OrderTranfer_Detail_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _trref = "";
        private string _ContentItem = "";
        private int _CategoryID = 0;
        private string _ContentValue = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public OrderTranfer_Detail_Info()
        {
        }
        public OrderTranfer_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM BNK_OrderTranfer_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _trref = zReader["trref"].ToString();
                    _ContentItem = zReader["ContentItem"].ToString();
                    _CategoryID = int.Parse(zReader["CategoryID"].ToString());
                    _ContentValue = zReader["ContentValue"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string trref
        {
            get { return _trref; }
            set { _trref = value; }
        }
        public string ContentItem
        {
            get { return _ContentItem; }
            set { _ContentItem = value; }
        }
        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public string ContentValue
        {
            get { return _ContentValue; }
            set { _ContentValue = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO BNK_OrderTranfer_Detail ("
                        + " trref,ContentItem,CategoryID ,ContentValue ) "
                         + " VALUES ( "
                         + "@trref,@ContentItem ,@CategoryID ,@ContentValue ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@ContentItem", SqlDbType.NVarChar).Value = _ContentItem;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = _CategoryID;
                zCommand.Parameters.Add("@ContentValue", SqlDbType.NVarChar).Value = _ContentValue;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE BNK_OrderTranfer_Detail SET "
                        + " trref = @trref,"
                        + " ContentItem = @ContentItem,"
                        + " CategoryID = @CategoryID,"
                        + " ContentValue = @ContentValue"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = _trref;
                zCommand.Parameters.Add("@ContentItem", SqlDbType.NVarChar).Value = _ContentItem;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Int).Value = _CategoryID;
                zCommand.Parameters.Add("@ContentValue", SqlDbType.NVarChar).Value = _ContentValue;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM BNK_OrderTranfer_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
