﻿using HHT.Connect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace HHT.Library.Bank
{
    public class Order_Check
    {
        #region [ Field Name ]
        private int _Auto = 0;
        private string _trref = "";
        private string _So_CT = "";
        private string _So_BT = "";
        private int _SectionNo = 0;
        private int _OrderStatus = 0;
        private int _StatusAnalysis = 0;
        private string _NHNN_ID = "";
        private string _NHNN_Name = "";
        private int _KieuThu = 0;
        private string _NHPV_ID = "";
        private string _NHPV_Name = "";
        private DateTime _NgayNT;
        private string _Ma_ST = "";
        private string _TenCongTy = "";
        private string _TK_ThuNS = "";
        private string _TK_ThuNS_Name = "";
        private string _DBHC_ID = "";
        private string _DBHC_Name = "";
        private string _CQThuID = "";
        private string _CQThuName = "";
        private string _LoaiThueID = "";
        private string _LoaiThueName = "";
        private string _ToKhai = "";
        private int _ToKhaiIndex = -1;
        private DateTime? _NgayDK = null;
        private string _LH_ID = "";
        private string _LH_Name = "";
        private double _SoTien = 0;
        private string _MessageReturn = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Order_Check()
        {
        }
        public Order_Check(int Auto)
        {
            string zSQL = "SELECT * FROM BNK_Order WHERE Auto = @Auto";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Auto", SqlDbType.Int).Value = Auto;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Auto = int.Parse(zReader["Auto"].ToString());
                    _trref = zReader["trref"].ToString();
                    _SectionNo = int.Parse(zReader["SectionNo"].ToString());
                    _So_CT = zReader["So_CT"].ToString();
                    _OrderStatus = int.Parse(zReader["OrderStatus"].ToString());
                    _StatusAnalysis = int.Parse(zReader["StatusAnalysis"].ToString());
                    _NHNN_ID = zReader["NHNN_ID"].ToString();
                    _NHNN_Name = zReader["NHNN_Name"].ToString();
                    _KieuThu = int.Parse(zReader["KieuThu"].ToString());
                    _NHPV_ID = zReader["NHPV_ID"].ToString();
                    _NHPV_Name = zReader["NHPV_Name"].ToString();
                    if (zReader["NgayNT"] != DBNull.Value)
                        _NgayNT = (DateTime)zReader["NgayNT"];
                    _Ma_ST = zReader["Ma_ST"].ToString();
                    _TenCongTy = zReader["TenCongTy"].ToString();
                    _TK_ThuNS = zReader["TK_ThuNS"].ToString();
                    _TK_ThuNS_Name = zReader["TK_ThuNS_Name"].ToString();
                    _DBHC_ID = zReader["DBHC_ID"].ToString();
                    _DBHC_Name = zReader["DBHC_Name"].ToString();
                    _CQThuID = zReader["CQThuID"].ToString();
                    _CQThuName = zReader["CQThuName"].ToString();
                    _LoaiThueID = zReader["LoaiThueID"].ToString();
                    _LoaiThueName = zReader["LoaiThueName"].ToString();
                    _ToKhai = zReader["ToKhai"].ToString();
                    if (zReader["NgayDK"] != DBNull.Value)
                        _NgayDK = (DateTime)zReader["NgayDK"];
                    _LH_ID = zReader["LH_ID"].ToString();
                    _LH_Name = zReader["LH_Name"].ToString();
                    _SoTien = double.Parse(zReader["SoTien"].ToString());
                    _MessageReturn = zReader["MessageReturn"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Order_Check(string OrderID)
        {
            string zSQL = "SELECT * FROM BNK_Order WHERE So_CT = @So_CT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@So_CT", SqlDbType.NVarChar).Value = OrderID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Auto = int.Parse(zReader["Auto"].ToString());
                    _trref = zReader["trref"].ToString();
                    _SectionNo = int.Parse(zReader["SectionNo"].ToString());
                    _OrderStatus = int.Parse(zReader["OrderStatus"].ToString());
                    _StatusAnalysis = int.Parse(zReader["StatusAnalysis"].ToString());
                    _NHNN_ID = zReader["NHNN_ID"].ToString();
                    _So_CT = zReader["So_CT"].ToString();
                    _NHNN_Name = zReader["NHNN_Name"].ToString();
                    _KieuThu = int.Parse(zReader["KieuThu"].ToString());
                    _NHPV_ID = zReader["NHPV_ID"].ToString();
                    _NHPV_Name = zReader["NHPV_Name"].ToString();
                    _NgayNT = (DateTime)zReader["NgayNT"];
                    _Ma_ST = zReader["Ma_ST"].ToString();
                    _TenCongTy = zReader["TenCongTy"].ToString();
                    _TK_ThuNS = zReader["TK_ThuNS"].ToString();
                    _TK_ThuNS_Name = zReader["TK_ThuNS_Name"].ToString();
                    _DBHC_ID = zReader["DBHC_ID"].ToString();
                    _DBHC_Name = zReader["DBHC_Name"].ToString();
                    _CQThuID = zReader["CQThuID"].ToString();
                    _CQThuName = zReader["CQThuName"].ToString();
                    _LoaiThueID = zReader["LoaiThueID"].ToString();
                    _LoaiThueName = zReader["LoaiThueName"].ToString();
                    _ToKhai = zReader["ToKhai"].ToString();
                    if (zReader["NgayDK"] != DBNull.Value)
                        _NgayDK = (DateTime)zReader["NgayDK"];
                    _LH_ID = zReader["LH_ID"].ToString();
                    _LH_Name = zReader["LH_Name"].ToString();
                    _SoTien = double.Parse(zReader["SoTien"].ToString());
                    _MessageReturn = zReader["MessageReturn"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int Auto
        {
            get { return _Auto; }
            set { _Auto = value; }
        }
        public string trref
        {
            get { return _trref; }
            set { _trref = value; }
        }
        public int SectionNo
        {
            get { return _SectionNo; }
            set { _SectionNo = value; }
        }
        public string So_CT
        {
            get { return _So_CT; }
            set { _So_CT = value; }
        }
        public int OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }
        public int StatusAnalysis
        {
            get { return _StatusAnalysis; }
            set { _StatusAnalysis = value; }
        }
        public string NHNN_ID
        {
            get { return _NHNN_ID; }
            set { _NHNN_ID = value; }
        }
        public string NHNN_Name
        {
            get { return _NHNN_Name; }
            set { _NHNN_Name = value; }
        }
        public int KieuThu
        {
            get { return _KieuThu; }
            set { _KieuThu = value; }
        }
        public string NHPV_ID
        {
            get { return _NHPV_ID; }
            set { _NHPV_ID = value; }
        }
        public string NHPV_Name
        {
            get { return _NHPV_Name; }
            set { _NHPV_Name = value; }
        }
        public DateTime NgayNT
        {
            get { return _NgayNT; }
            set { _NgayNT = value; }
        }
        public string ParseNgayNT
        {
            set
            {
                CultureInfo enUS = new CultureInfo("en-US"); // is up to you
                DateTime zTime;
                if (DateTime.TryParseExact(value, "dd/MM/yyyy", enUS, DateTimeStyles.None, out zTime))
                {
                    _NgayNT = zTime;
                }
            }
        }
        public string Ma_ST
        {
            get { return _Ma_ST; }
            set { _Ma_ST = value; }
        }
        public string TenCongTy
        {
            get { return _TenCongTy; }
            set { _TenCongTy = value; }
        }
        public string TK_ThuNS
        {
            get { return _TK_ThuNS; }
            set { _TK_ThuNS = value; }
        }
        public string TK_ThuNS_Name
        {
            get { return _TK_ThuNS_Name; }
            set { _TK_ThuNS_Name = value; }
        }
        public string DBHC_ID
        {
            get { return _DBHC_ID; }
            set { _DBHC_ID = value; }
        }
        public string DBHC_Name
        {
            get { return _DBHC_Name; }
            set { _DBHC_Name = value; }
        }
        public string CQThuID
        {
            get { return _CQThuID; }
            set { _CQThuID = value; }
        }
        public string CQThuName
        {
            get { return _CQThuName; }
            set { _CQThuName = value; }
        }
        public string LoaiThueID
        {
            get { return _LoaiThueID; }
            set { _LoaiThueID = value; }
        }
        public string LoaiThueName
        {
            get { return _LoaiThueName; }
            set { _LoaiThueName = value; }
        }
        public string ToKhai
        {
            get { return _ToKhai; }
            set { _ToKhai = value; }
        }
        public int ToKhaiIndex
        {
            get { return _ToKhaiIndex; }
            set { _ToKhaiIndex = value; }
        }
        public DateTime? NgayDK
        {
            get { return _NgayDK; }
            set { _NgayDK = value; }
        }
        public string ParseNgayDK
        {
            set
            {
                CultureInfo enUS = new CultureInfo("en-US"); // is up to you
                DateTime zTime;
                if (DateTime.TryParseExact(value, "dd/MM/yyyy", enUS, DateTimeStyles.None, out zTime))
                {
                    _NgayDK = zTime;
                }
            }
        }
        public bool IsNgayDK
        {
            get
            {
                bool zResult = true;
                if (_NgayDK == null)
                    zResult = false;
                else
                {
                    if (_NgayDK.Value.Year > DateTime.Now.Year)
                        zResult = false;
                    if (_NgayDK.Value.Year < DateTime.Now.Year - 6)
                        zResult = false;
                }
                return zResult;
            }
        }
        public string LH_ID
        {
            get { return _LH_ID; }
            set { _LH_ID = value; }
        }
        public string LH_Name
        {
            get { return _LH_Name; }
            set { _LH_Name = value; }
        }
        public double SoTien
        {
            get { return _SoTien; }
            set { _SoTien = value; }
        }
        public string MessageReturn
        {
            get { return _MessageReturn; }
            set { _MessageReturn = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string So_BT { get => _So_BT; set => _So_BT = value; }
        #endregion
    }
}

