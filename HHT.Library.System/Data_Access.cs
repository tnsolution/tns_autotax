﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using HHT.Connect;
using System.IO;
using OfficeOpenXml;

namespace HHT.Library.System
{
    public class Data_Access
    {
        public static string InsertToTable(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = SQL;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                return Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string ValueOfField(string SQL)
        {
            string zResult = "";
            if (DateTime.Today >= new DateTime(2021, 1, 14))
                return zResult;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static string LH_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT Description FROM BNK_LHXNK WHERE ID = '" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static string Chuong_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT Description FROM BNK_Chuong WHERE ID = '" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static string NDKT_Name(string ID)
        {
            string zResult = "";
            string SQL = "SELECT Description FROM BNK_NDKT WHERE ID = '" + ID + "'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static DataTable List_LHNXK()
        {
            DataTable zResult = new DataTable();
            string SQL = "SELECT ID,Description FROM BNK_LHXNK";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zResult);
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
        public static bool CheckFileStatus(FileInfo fileName)
        {
            FileStream streamInput = null;
            try
            {
                streamInput = fileName.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (streamInput != null)
                {
                    streamInput.Close();
                }
            }
            return false;
        }

        public static DataTable GetTable(string FilePath, string SheetName)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);

            // Check if the file exists
            if (!fi.Exists)
            {
                throw new Exception("File " + FilePath + " Does Not Exists");
            }

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[SheetName];// [SheetName];

                // Fetch the WorkSheet size
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn
                for (int col = startCell.Column; col <= endCell.Column; col++)
                {
                    dt.Columns.Add(col.ToString());
                }

                // place all the data into DataTable [ + 1 là trừ dòng đầu]
                for (int row = startCell.Row + 1; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
    }
}
