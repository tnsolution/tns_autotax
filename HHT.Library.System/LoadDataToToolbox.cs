﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

using HHT.Connect;

namespace HHT.Library.System
{
    public class Item
    {
        private string _ID = null;
        private string _Name = null;
        private object _Value = null;

        public Item()
        {
        }

        #region [ Properties ]

        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        #endregion
    }
    public class LoadDataToToolbox
    {
        private static string zConnectionString = ConnectDataBase.ConnectionString;
        public static ArrayList GetDataBase(string SQL, int Parent)
        {
            string nSQL = SQL + " WHERE Parent=@Parent";

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
            zCommand.CommandType = CommandType.Text;

            zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;
            SqlDataReader zReader = zCommand.ExecuteReader();

            ArrayList ListItems = new ArrayList();
            while (zReader.Read())
            {
                Item li = new Item();
                li.Name = zReader[1].ToString().Trim();
                li.Value = zReader[0].ToString();
                ListItems.Add(li);
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();
            return ListItems;
        }

        public static void ListViewData(ListView LV, string SQL)
        {

            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;
                //  MessageBox.Show(  mReader.FieldCount.ToString());


                int i = 1;
                string StyleField;
                LV.Items.Clear();
                while (zReader.Read())
                {

                    lvi = new ListViewItem();
                    lvi.Text = zReader[1].ToString();
                    lvi.Tag = zReader[0]; // Set the tag to 

                    lvi.ForeColor = Color.DarkBlue;
                    lvi.BackColor = Color.White;

                    lvi.ImageIndex = 0;

                    for (i = 2; i < zReader.FieldCount; i++)
                    {

                        lvsi = new ListViewItem.ListViewSubItem();
                        StyleField = zReader.GetDataTypeName(i).ToString();


                        switch (StyleField)
                        {
                            case "nvarchar":
                                lvsi.Text = zReader[i].ToString().Trim();
                                break;
                            case "datetime":
                                lvsi.Text = DateTime.Parse(zReader[i].ToString()).ToShortDateString();
                                break;

                            case "int":
                                lvsi.Text = string.Format("{0:###,###,###}", zReader[i]);
                                break;

                            default:
                                lvsi.Text = zReader[i].ToString().Trim();
                                break;
                        }
                        lvsi.ForeColor = Color.Silver;
                        lvi.SubItems.Add(lvsi);

                    }
                    LV.Items.Add(lvi);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ListViewData(ListView LV, string SQL, Color color)
        {

            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;
                //  MessageBox.Show(  mReader.FieldCount.ToString());


                int i = 1;
                string StyleField;
                LV.Items.Clear();
                while (zReader.Read())
                {

                    lvi = new ListViewItem();
                    lvi.Text = zReader[1].ToString();
                    lvi.Tag = zReader[0]; // Set the tag to 

                    lvi.ForeColor = color;
                    lvi.BackColor = Color.White;

                    lvi.ImageIndex = 0;

                    for (i = 2; i < zReader.FieldCount; i++)
                    {

                        lvsi = new ListViewItem.ListViewSubItem();
                        StyleField = zReader.GetDataTypeName(i).ToString();

                        switch (StyleField)
                        {
                            case "nvarchar":
                                lvsi.Text = zReader[i].ToString().Trim();
                                break;
                            case "datetime":
                                lvsi.Text = DateTime.Parse(zReader[i].ToString()).ToShortDateString();
                                break;

                            case "int":
                                lvsi.Text = string.Format("{0:###,###,###}", zReader[i]);
                                break;

                            default:
                                lvsi.Text = zReader[i].ToString().Trim();
                                break;
                        }
                        lvsi.ForeColor = color;
                        lvi.SubItems.Add(lvsi);

                    }
                    LV.Items.Add(lvi);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ListBoxData(ListBox LB, string SQL)
        {

            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                Item item;

                ArrayList ListItems = new ArrayList();

                while (zReader.Read())
                {

                    item = new Item();

                    item.Name = zReader[1].ToString();
                    item.Value = zReader[0];

                    ListItems.Add(item);

                }

                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                LB.DataSource = ListItems;
                LB.DisplayMember = "Name";
                LB.ValueMember = "Value";

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        public static void ComboBoxData(ComboBox CB, int Month, int Year)
        {
            ArrayList ListItems = new ArrayList();
            int n = 12;
            int nMonthFinacial = 1;
            Item li;
            for (int i = Month; i <= n; i++)
            {
                li = new Item();
                li.Value = nMonthFinacial;
                if (i > 9)
                    li.Name = i.ToString() + "/" + Year.ToString();
                else
                    li.Name = "0" + i.ToString() + "/" + Year.ToString();

                ListItems.Add(li);

                nMonthFinacial++;
            }
            if (Month > 1)
            {
                Year = Year + 1;
                for (int i = 1; i < Month; i++)
                {
                    li = new Item();
                    li.Value = nMonthFinacial;
                    if (i > 9)
                        li.Name = i.ToString() + "/" + Year.ToString();
                    else
                        li.Name = "0" + i.ToString() + "/" + Year.ToString();

                    ListItems.Add(li);
                    nMonthFinacial++;
                }
            }

            if (ListItems.Count > 0)
            {
                CB.DataSource = ListItems;
                CB.DisplayMember = "Name";
                CB.ValueMember = "Value";
            }
        }
        public static void ComboBoxData(ComboBox CB, string SQL, string IsHaveFirstItem)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                Item li;

                if (IsHaveFirstItem.Length > 0)
                {
                    li = new Item();
                    li.Value = 0;
                    li.Name = IsHaveFirstItem;
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(ComboBox CB, DataTable Source, bool IsHaveFirstItem)
        {
            try
            {

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = Source.Columns.Count;
                Item li;

                if (IsHaveFirstItem)
                {
                    li = new Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                foreach (DataRow zRow in Source.Rows)
                {

                    li = new Item();
                    int nValue = 0;
                    if (int.TryParse(zRow[0].ToString(), out nValue))
                        li.Value = zRow[0];
                    else
                        li.Value = zRow[0].ToString();

                    if (n == 2)
                        li.Name = zRow[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zRow[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(ComboBox CB, Item ItemFirst, string SQL)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                Item li;
                ListItems.Add(ItemFirst);
                while (zReader.Read())
                {

                    li = new Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();

                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxData(ComboBox CB, string SQL, int MaxFirstCol, bool IsHaveFirstItem)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                Item li;

                if (IsHaveFirstItem)
                {
                    li = new Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new Item();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();
                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            if (i == 1)
                                li.Name = zReader[i].ToString().Trim().PadRight(MaxFirstCol, ' ') + ":";
                            else
                                li.Name = li.Name + zReader[i].ToString().Trim();

                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        public static void ComboBoxData(DataGridViewComboBoxColumn CB, string SQL, bool IsHaveFirstItem)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                Item li;

                if (IsHaveFirstItem)
                {
                    li = new Item();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new Item();
                    li.Value = zReader[0];
                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        public static void ComboBoxColumn(DataGridViewComboBoxColumn CB, string SQL)
        {
            string nSQL;
            try
            {
                nSQL = SQL;

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                Item li;
                while (zReader.Read())
                {

                    li = new Item();
                    li.Value = zReader[0];
                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCommand.Dispose();
                zConnect.Close();

                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
        public static void ComboBoxDataTime(DataGridViewComboBoxColumn CB)
        {

            ArrayList ListItems = new ArrayList();

            Item li;

            li = new Item();
            li.Value = "HOUR";
            li.Name = "GIỜ";
            ListItems.Add(li);

            li = new Item();
            li.Value = "DATE";
            li.Name = "NGÀY";
            ListItems.Add(li);


            CB.DataSource = ListItems;
            if (ListItems.Count > 0)
            {
                CB.DisplayMember = "Name";
                CB.ValueMember = "Value";
            }

        }

        public static void AutoCompleteTextBox(TextBox TB, string SQL)
        {
            string nSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBox(DataGridViewTextBoxEditingControl TB, string SQL)
        {
            string nSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBox(DataGridViewTextBoxEditingControl TB, string SQL, int NumberColumns)
        {
            string nSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            while (zReader.Read())
            {
                string strItem = zReader[0].ToString().Trim().PadRight(15, ' ') + " : ";
                for (int i = 1; i < NumberColumns; i++)
                {
                    strItem += zReader[i].ToString().Trim() + " ";

                }
                Items.Add(strItem);
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
        public static void AutoCompleteTextBoxMutiColumn(TextBox TB, string SQL)
        {
            string nSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim() + " : " + zReader[1].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;


        }
        public static void AutoCompleteComboBox(ComboBox CB, string SQL)
        {
            string nSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(nSQL, zConnect);
            SqlDataReader zReader = zCommand.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim());
            }
            zReader.Close();
            zCommand.Dispose();
            zConnect.Close();

            CB.AutoCompleteCustomSource = Items;
            CB.AutoCompleteMode = AutoCompleteMode.Suggest;
            CB.AutoCompleteSource = AutoCompleteSource.ListItems;

        }


    }
}
