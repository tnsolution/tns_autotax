﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class Frm_Loading : Form
    {
        public Action _Worker { get; set; }
        public Frm_Loading(Action Worker)
        {
            InitializeComponent();
            if (Worker != null)
            {
                _Worker = Worker;
            }
        }

        private void Frm_Loading_Load(object sender, EventArgs e)
        {

        }
       
       
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (_Worker != null)
                Task.Factory.StartNew(_Worker).ContinueWith(t => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
