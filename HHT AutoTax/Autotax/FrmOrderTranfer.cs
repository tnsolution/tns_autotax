﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;
namespace HHT_AutoTax
{
    public partial class FrmOrderTranfer : Form
    {
        public FrmOrderTranfer()
        {
            InitializeComponent();
        }

        private void FrmOrderTranfer_Load(object sender, EventArgs e)
        {
            Picker_From.Value = new DateTime(2021, 03, 13);
            //Picker_From.Value = DateTime.Now;
            InitListView(LV_Data);
            LoadDataToListView(SectionImport_Data.ListNew(Picker_From.Value, Picker_From .Value));
        }

        private void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Section No";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "UserName";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "File Name - Sheet";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thời Gian";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Dữ liệu Import";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phân tích";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            
            colHead = new ColumnHeader();
            colHead.Text = "Tỷ lệ";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            
        }
        public void LoadDataToListView(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            double zTotalImport = 0;
            double zTotalSuccess = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SectionNo"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UserName"].ToString().Trim();

                lvi.SubItems.Add(lvsi);
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FileName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                DateTime z_trdate = (DateTime)nRow["CreatedOn"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy HH:mm");
                lvi.SubItems.Add(lvsi);

                double zAmount = double.Parse(nRow["AmountData"].ToString());
                double zAmountFail = double.Parse(nRow["AmountFail"].ToString());
                double zAmountSuccess = zAmount - zAmountFail;


                int zPercent = (int)(zAmountSuccess * 100) / (int)zAmount;
                zTotalImport += zAmount;
                zTotalSuccess += zAmountSuccess;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zAmount.ToString("###,###,###");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zAmountSuccess.ToString("###,###,###");
                lvi.SubItems.Add(lvsi);
                
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zPercent.ToString() + "%";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);

            }
            txt_TotalImport.Text = "Tổng số import : " + zTotalImport.ToString();
            lbl_TotalSuccess.Text = "Tổng số thành công : " + zTotalSuccess.ToString();
            lbl_Total_Error.Text = "Tổng số thất bại : " + (zTotalImport - zTotalSuccess).ToString();
            int zTotalPercent = 0;
            if (zTotalImport > 0)
            {
                zTotalPercent = (int)(zTotalSuccess * 100) / (int)zTotalImport;
            }
            lbl_Total_Percent.Text = zTotalPercent.ToString() + "%";
            this.Cursor = Cursors.Default;

        }

        private void btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_Excel frm = new Frm_Import_Excel();
            frm.ShowDialog();
            LoadDataToListView(SectionImport_Data.ListNew(Picker_From.Value, Picker_To.Value));

        }

        private void btn_Export_Click(object sender, EventArgs e)
        {
            Frm_Export_Excel frm = new Frm_Export_Excel();
            //frm.SectionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[1].Text);
            frm._Status = 1;
            frm.zFromDate = Picker_From.Value;
            frm.zToDate = Picker_To.Value;
            frm.ShowDialog();
        }

        private void LV_Data_ItemActivate(object sender, EventArgs e)
        {
            FrmOrderAnalysis_10 frm = new FrmOrderAnalysis_10();
            frm.SectionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[1].Text);
            frm.ShowDialog();
            LoadDataToListView(SectionImport_Data.ListNew(Picker_From.Value, Picker_To.Value));
        }

        private void btn_View_Click(object sender, EventArgs e)
        {
            LoadDataToListView(SectionImport_Data.ListNew(Picker_From.Value, Picker_To.Value));
        }

        private void Picker_From_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Picker_To_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btn_Analysis_2_Click(object sender, EventArgs e)
        {
           /* FrmOrderAnalysis_Ext frm = new FrmOrderAnalysis_Ext();
            frm.SectionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[1].Text);
            frm.ShowDialog();
            LoadDataToListView(SectionImport_Data.ListNew(Picker_From.Value, Picker_To.Value));
            */
        }

        private void btn_Analysis_1_Click(object sender, EventArgs e)
        {
            FrmOrderAnalysis_10 frm = new FrmOrderAnalysis_10();
            frm.SectionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[1].Text);
            frm.ShowDialog();
            LoadDataToListView(SectionImport_Data.ListNew(Picker_From.Value, Picker_To.Value));
        }

        private void btn_Export_2_Click(object sender, EventArgs e)
        {
            Frm_Export_Excel frm = new Frm_Export_Excel();
            //frm.SectionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[1].Text);
            frm._Status = 2;
            frm.zFromDate = Picker_From.Value;
            frm.zToDate = Picker_To.Value;
            frm.ShowDialog();
        }
    }
}
