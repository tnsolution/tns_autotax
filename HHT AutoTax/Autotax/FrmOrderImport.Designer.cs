﻿namespace HHT_AutoTax
{
    partial class FrmOrderTranfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOrderTranfer));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Export = new System.Windows.Forms.Button();
            this.btn_Import = new System.Windows.Forms.Button();
            this.btn_View = new System.Windows.Forms.Button();
            this.Picker_To = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.Picker_From = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LV_Data = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbl_Total_Percent = new System.Windows.Forms.Label();
            this.lbl_Total_Error = new System.Windows.Forms.Label();
            this.lbl_TotalSuccess = new System.Windows.Forms.Label();
            this.txt_TotalImport = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.btn_Import);
            this.panel1.Controls.Add(this.btn_View);
            this.panel1.Controls.Add(this.Picker_To);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Picker_From);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 456);
            this.panel1.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.Location = new System.Drawing.Point(11, 109);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(241, 1);
            this.panel4.TabIndex = 7;
            // 
            // btn_Export
            // 
            this.btn_Export.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Export.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.ForeColor = System.Drawing.Color.White;
            this.btn_Export.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Image")));
            this.btn_Export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Export.Location = new System.Drawing.Point(140, 112);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.Size = new System.Drawing.Size(112, 33);
            this.btn_Export.TabIndex = 6;
            this.btn_Export.Text = "Export";
            this.btn_Export.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Export.UseVisualStyleBackColor = false;
            this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
            // 
            // btn_Import
            // 
            this.btn_Import.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Import.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Import.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Import.ForeColor = System.Drawing.Color.White;
            this.btn_Import.Image = ((System.Drawing.Image)(resources.GetObject("btn_Import.Image")));
            this.btn_Import.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Import.Location = new System.Drawing.Point(12, 112);
            this.btn_Import.Name = "btn_Import";
            this.btn_Import.Size = new System.Drawing.Size(115, 33);
            this.btn_Import.TabIndex = 6;
            this.btn_Import.Text = "Import";
            this.btn_Import.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Import.UseVisualStyleBackColor = false;
            this.btn_Import.Click += new System.EventHandler(this.btn_Import_Click);
            // 
            // btn_View
            // 
            this.btn_View.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_View.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_View.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_View.ForeColor = System.Drawing.Color.White;
            this.btn_View.Image = ((System.Drawing.Image)(resources.GetObject("btn_View.Image")));
            this.btn_View.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_View.Location = new System.Drawing.Point(174, 54);
            this.btn_View.Name = "btn_View";
            this.btn_View.Size = new System.Drawing.Size(82, 49);
            this.btn_View.TabIndex = 6;
            this.btn_View.Text = "Xem";
            this.btn_View.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_View.UseVisualStyleBackColor = false;
            this.btn_View.Click += new System.EventHandler(this.btn_View_Click);
            // 
            // Picker_To
            // 
            this.Picker_To.CustomFormat = "dd/MM/yyyy";
            this.Picker_To.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Picker_To.Location = new System.Drawing.Point(82, 81);
            this.Picker_To.Name = "Picker_To";
            this.Picker_To.Size = new System.Drawing.Size(86, 21);
            this.Picker_To.TabIndex = 4;
            this.Picker_To.ValueChanged += new System.EventHandler(this.Picker_To_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(12, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Đến Ngày";
            // 
            // Picker_From
            // 
            this.Picker_From.CustomFormat = "dd/MM/yyyy";
            this.Picker_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Picker_From.Location = new System.Drawing.Point(82, 54);
            this.Picker_From.Name = "Picker_From";
            this.Picker_From.Size = new System.Drawing.Size(86, 21);
            this.Picker_From.TabIndex = 4;
            this.Picker_From.ValueChanged += new System.EventHandler(this.Picker_From_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Từ Ngày";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(262, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "DỮ LIỆU";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LV_Data
            // 
            this.LV_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Data.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Data.ForeColor = System.Drawing.Color.Navy;
            this.LV_Data.FullRowSelect = true;
            this.LV_Data.GridLines = true;
            this.LV_Data.HideSelection = false;
            this.LV_Data.Location = new System.Drawing.Point(266, 43);
            this.LV_Data.Name = "LV_Data";
            this.LV_Data.Size = new System.Drawing.Size(639, 413);
            this.LV_Data.TabIndex = 5;
            this.LV_Data.UseCompatibleStateImageBehavior = false;
            this.LV_Data.View = System.Windows.Forms.View.Details;
            this.LV_Data.ItemActivate += new System.EventHandler(this.LV_Data_ItemActivate);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(262, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 456);
            this.panel2.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.panel3.Controls.Add(this.lbl_Total_Percent);
            this.panel3.Controls.Add(this.lbl_Total_Error);
            this.panel3.Controls.Add(this.lbl_TotalSuccess);
            this.panel3.Controls.Add(this.txt_TotalImport);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(266, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(639, 43);
            this.panel3.TabIndex = 8;
            // 
            // lbl_Total_Percent
            // 
            this.lbl_Total_Percent.AutoSize = true;
            this.lbl_Total_Percent.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Total_Percent.ForeColor = System.Drawing.Color.White;
            this.lbl_Total_Percent.Location = new System.Drawing.Point(398, 18);
            this.lbl_Total_Percent.Name = "lbl_Total_Percent";
            this.lbl_Total_Percent.Size = new System.Drawing.Size(16, 15);
            this.lbl_Total_Percent.TabIndex = 3;
            this.lbl_Total_Percent.Text = "%";
            // 
            // lbl_Total_Error
            // 
            this.lbl_Total_Error.AutoSize = true;
            this.lbl_Total_Error.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Total_Error.ForeColor = System.Drawing.Color.White;
            this.lbl_Total_Error.Location = new System.Drawing.Point(449, 18);
            this.lbl_Total_Error.Name = "lbl_Total_Error";
            this.lbl_Total_Error.Size = new System.Drawing.Size(97, 15);
            this.lbl_Total_Error.TabIndex = 3;
            this.lbl_Total_Error.Text = "Tổng số thất bại";
            // 
            // lbl_TotalSuccess
            // 
            this.lbl_TotalSuccess.AutoSize = true;
            this.lbl_TotalSuccess.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TotalSuccess.ForeColor = System.Drawing.Color.White;
            this.lbl_TotalSuccess.Location = new System.Drawing.Point(235, 18);
            this.lbl_TotalSuccess.Name = "lbl_TotalSuccess";
            this.lbl_TotalSuccess.Size = new System.Drawing.Size(121, 15);
            this.lbl_TotalSuccess.TabIndex = 3;
            this.lbl_TotalSuccess.Text = "Tổng số Thành công";
            // 
            // txt_TotalImport
            // 
            this.txt_TotalImport.AutoSize = true;
            this.txt_TotalImport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TotalImport.ForeColor = System.Drawing.Color.White;
            this.txt_TotalImport.Location = new System.Drawing.Point(68, 18);
            this.txt_TotalImport.Name = "txt_TotalImport";
            this.txt_TotalImport.Size = new System.Drawing.Size(77, 15);
            this.txt_TotalImport.TabIndex = 3;
            this.txt_TotalImport.Text = "Tổng số LCT";
            // 
            // FrmOrderTranfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(905, 456);
            this.Controls.Add(this.LV_Data);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FrmOrderTranfer";
            this.Text = "Lệnh chuyển tiền";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmOrderTranfer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Export;
        private System.Windows.Forms.Button btn_Import;
        private System.Windows.Forms.Button btn_View;
        private System.Windows.Forms.DateTimePicker Picker_To;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker Picker_From;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView LV_Data;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbl_Total_Percent;
        private System.Windows.Forms.Label lbl_Total_Error;
        private System.Windows.Forms.Label lbl_TotalSuccess;
        private System.Windows.Forms.Label txt_TotalImport;
        private System.Windows.Forms.Panel panel4;
    }
}