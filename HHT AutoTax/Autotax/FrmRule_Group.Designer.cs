﻿namespace HHT_AutoTax
{
    partial class FrmRule_Group
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GV_Rule_Group = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Rule_Group)).BeginInit();
            this.SuspendLayout();
            // 
            // GV_Rule_Group
            // 
            this.GV_Rule_Group.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Rule_Group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Rule_Group.Location = new System.Drawing.Point(0, 0);
            this.GV_Rule_Group.Name = "GV_Rule_Group";
            this.GV_Rule_Group.Size = new System.Drawing.Size(800, 450);
            this.GV_Rule_Group.TabIndex = 1;
            // 
            // FrmRule_Group
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GV_Rule_Group);
            this.Name = "FrmRule_Group";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRule_Group";
            this.Load += new System.EventHandler(this.FrmRule_Group_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Rule_Group)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView GV_Rule_Group;
    }
}