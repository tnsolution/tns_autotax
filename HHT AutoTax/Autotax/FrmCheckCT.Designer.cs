﻿namespace HHT_AutoTax
{
    partial class FrmCheckCT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GV_ListOrderWeb = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_LHXNK1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txt_DBHC_Name1 = new System.Windows.Forms.TextBox();
            this.txt_SoTien1 = new System.Windows.Forms.TextBox();
            this.txt_NgayDk1 = new System.Windows.Forms.TextBox();
            this.txt_NguoiNT1 = new System.Windows.Forms.TextBox();
            this.txt_ToKhaiSo1 = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue1 = new System.Windows.Forms.TextBox();
            this.txt_DBHC_ID1 = new System.Windows.Forms.TextBox();
            this.txt_MaSoThue1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.GV_DataDetailSQL = new System.Windows.Forms.DataGridView();
            this.label35 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_LHXNK = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue_Name = new System.Windows.Forms.TextBox();
            this.txt_DBHC_Name = new System.Windows.Forms.TextBox();
            this.txt_SoTien = new System.Windows.Forms.TextBox();
            this.txt_NgayDk = new System.Windows.Forms.TextBox();
            this.txt_NguoiNT = new System.Windows.Forms.TextBox();
            this.txt_ToKhaiSo = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue = new System.Windows.Forms.TextBox();
            this.txt_DBHC_ID = new System.Windows.Forms.TextBox();
            this.txt_MaSoThue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_ListOrderWeb)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_DataDetailSQL)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(587, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(659, 632);
            this.panel3.TabIndex = 2;
            // 
            // tabControl2
            // 
            this.tabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(659, 632);
            this.tabControl2.TabIndex = 6;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.GV_ListOrderWeb);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.panel13);
            this.tabPage2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(651, 606);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Lệnh CT";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GV_ListOrderWeb
            // 
            this.GV_ListOrderWeb.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_ListOrderWeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_ListOrderWeb.Location = new System.Drawing.Point(3, 292);
            this.GV_ListOrderWeb.Name = "GV_ListOrderWeb";
            this.GV_ListOrderWeb.Size = new System.Drawing.Size(645, 311);
            this.GV_ListOrderWeb.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.LightGray;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(645, 22);
            this.label5.TabIndex = 7;
            this.label5.Text = "Thông Tin Chi Tiết";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Silver;
            this.panel11.Controls.Add(this.label6);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Controls.Add(this.txt_LHXNK1);
            this.panel11.Controls.Add(this.label18);
            this.panel11.Controls.Add(this.label26);
            this.panel11.Controls.Add(this.label27);
            this.panel11.Controls.Add(this.label29);
            this.panel11.Controls.Add(this.label31);
            this.panel11.Controls.Add(this.textBox2);
            this.panel11.Controls.Add(this.textBox3);
            this.panel11.Controls.Add(this.txt_DBHC_Name1);
            this.panel11.Controls.Add(this.txt_SoTien1);
            this.panel11.Controls.Add(this.txt_NgayDk1);
            this.panel11.Controls.Add(this.txt_NguoiNT1);
            this.panel11.Controls.Add(this.txt_ToKhaiSo1);
            this.panel11.Controls.Add(this.txt_LoaiThue1);
            this.panel11.Controls.Add(this.txt_DBHC_ID1);
            this.panel11.Controls.Add(this.txt_MaSoThue1);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel11.Location = new System.Drawing.Point(3, 64);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(645, 206);
            this.panel11.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Loại Hình";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số Tiền";
            // 
            // txt_LHXNK1
            // 
            this.txt_LHXNK1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LHXNK1.Location = new System.Drawing.Point(110, 141);
            this.txt_LHXNK1.Name = "txt_LHXNK1";
            this.txt_LHXNK1.Size = new System.Drawing.Size(354, 21);
            this.txt_LHXNK1.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 119);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 15);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngày DK";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 93);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 15);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tờ Khai Số";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 15);
            this.label27.TabIndex = 0;
            this.label27.Text = "Loại Thuế";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 40);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 15);
            this.label29.TabIndex = 0;
            this.label29.Text = "Mã DBHC";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(72, 15);
            this.label31.TabIndex = 0;
            this.label31.Text = "Mã Số Thuế";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox2.Location = new System.Drawing.Point(205, 90);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(259, 21);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox3.Location = new System.Drawing.Point(205, 64);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(259, 21);
            this.textBox3.TabIndex = 1;
            // 
            // txt_DBHC_Name1
            // 
            this.txt_DBHC_Name1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_Name1.Location = new System.Drawing.Point(205, 37);
            this.txt_DBHC_Name1.Name = "txt_DBHC_Name1";
            this.txt_DBHC_Name1.Size = new System.Drawing.Size(259, 21);
            this.txt_DBHC_Name1.TabIndex = 1;
            // 
            // txt_SoTien1
            // 
            this.txt_SoTien1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_SoTien1.Location = new System.Drawing.Point(110, 168);
            this.txt_SoTien1.Name = "txt_SoTien1";
            this.txt_SoTien1.Size = new System.Drawing.Size(352, 21);
            this.txt_SoTien1.TabIndex = 1;
            // 
            // txt_NgayDk1
            // 
            this.txt_NgayDk1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NgayDk1.Location = new System.Drawing.Point(110, 116);
            this.txt_NgayDk1.Name = "txt_NgayDk1";
            this.txt_NgayDk1.Size = new System.Drawing.Size(354, 21);
            this.txt_NgayDk1.TabIndex = 1;
            // 
            // txt_NguoiNT1
            // 
            this.txt_NguoiNT1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NguoiNT1.Location = new System.Drawing.Point(205, 13);
            this.txt_NguoiNT1.Name = "txt_NguoiNT1";
            this.txt_NguoiNT1.Size = new System.Drawing.Size(259, 21);
            this.txt_NguoiNT1.TabIndex = 1;
            // 
            // txt_ToKhaiSo1
            // 
            this.txt_ToKhaiSo1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_ToKhaiSo1.Location = new System.Drawing.Point(110, 90);
            this.txt_ToKhaiSo1.Name = "txt_ToKhaiSo1";
            this.txt_ToKhaiSo1.Size = new System.Drawing.Size(98, 21);
            this.txt_ToKhaiSo1.TabIndex = 1;
            // 
            // txt_LoaiThue1
            // 
            this.txt_LoaiThue1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue1.Location = new System.Drawing.Point(110, 64);
            this.txt_LoaiThue1.Name = "txt_LoaiThue1";
            this.txt_LoaiThue1.Size = new System.Drawing.Size(98, 21);
            this.txt_LoaiThue1.TabIndex = 1;
            // 
            // txt_DBHC_ID1
            // 
            this.txt_DBHC_ID1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_ID1.Location = new System.Drawing.Point(110, 37);
            this.txt_DBHC_ID1.Name = "txt_DBHC_ID1";
            this.txt_DBHC_ID1.Size = new System.Drawing.Size(98, 21);
            this.txt_DBHC_ID1.TabIndex = 1;
            // 
            // txt_MaSoThue1
            // 
            this.txt_MaSoThue1.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_MaSoThue1.Location = new System.Drawing.Point(110, 13);
            this.txt_MaSoThue1.Name = "txt_MaSoThue1";
            this.txt_MaSoThue1.Size = new System.Drawing.Size(98, 21);
            this.txt_MaSoThue1.TabIndex = 1;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.LightGray;
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 42);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(645, 22);
            this.label33.TabIndex = 3;
            this.label33.Text = "Thông Tin Về NNT";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label3);
            this.panel13.Controls.Add(this.button1);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(3, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(645, 39);
            this.panel13.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(645, 33);
            this.label3.TabIndex = 7;
            this.label3.Text = "NỘI DUNG EXCEL";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(496, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 30);
            this.button1.TabIndex = 6;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(587, 632);
            this.panel4.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(587, 632);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.GV_DataDetailSQL);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(579, 606);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lệnh CT";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // GV_DataDetailSQL
            // 
            this.GV_DataDetailSQL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_DataDetailSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_DataDetailSQL.Location = new System.Drawing.Point(3, 292);
            this.GV_DataDetailSQL.Name = "GV_DataDetailSQL";
            this.GV_DataDetailSQL.Size = new System.Drawing.Size(573, 311);
            this.GV_DataDetailSQL.TabIndex = 8;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.LightGray;
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(3, 270);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(573, 22);
            this.label35.TabIndex = 7;
            this.label35.Text = "Thông Tin Chi Tiết";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Silver;
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.txt_LHXNK);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.textBox29);
            this.panel6.Controls.Add(this.txt_LoaiThue_Name);
            this.panel6.Controls.Add(this.txt_DBHC_Name);
            this.panel6.Controls.Add(this.txt_SoTien);
            this.panel6.Controls.Add(this.txt_NgayDk);
            this.panel6.Controls.Add(this.txt_NguoiNT);
            this.panel6.Controls.Add(this.txt_ToKhaiSo);
            this.panel6.Controls.Add(this.txt_LoaiThue);
            this.panel6.Controls.Add(this.txt_DBHC_ID);
            this.panel6.Controls.Add(this.txt_MaSoThue);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(3, 64);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(573, 206);
            this.panel6.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Loại Hình";
            // 
            // txt_LHXNK
            // 
            this.txt_LHXNK.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LHXNK.Location = new System.Drawing.Point(109, 146);
            this.txt_LHXNK.Name = "txt_LHXNK";
            this.txt_LHXNK.Size = new System.Drawing.Size(354, 21);
            this.txt_LHXNK.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 174);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 15);
            this.label25.TabIndex = 0;
            this.label25.Text = "Số Tiền";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 122);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 15);
            this.label24.TabIndex = 0;
            this.label24.Text = "Ngày DK";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 96);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 15);
            this.label23.TabIndex = 0;
            this.label23.Text = "Tờ Khai Số";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 70);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 15);
            this.label22.TabIndex = 0;
            this.label22.Text = "Loại Thuế";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "Mã DBHC";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã Số Thuế";
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox29.Location = new System.Drawing.Point(205, 93);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(259, 21);
            this.textBox29.TabIndex = 1;
            // 
            // txt_LoaiThue_Name
            // 
            this.txt_LoaiThue_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue_Name.Location = new System.Drawing.Point(205, 67);
            this.txt_LoaiThue_Name.Name = "txt_LoaiThue_Name";
            this.txt_LoaiThue_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_LoaiThue_Name.TabIndex = 1;
            // 
            // txt_DBHC_Name
            // 
            this.txt_DBHC_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_Name.Location = new System.Drawing.Point(205, 37);
            this.txt_DBHC_Name.Name = "txt_DBHC_Name";
            this.txt_DBHC_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_DBHC_Name.TabIndex = 1;
            // 
            // txt_SoTien
            // 
            this.txt_SoTien.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_SoTien.Location = new System.Drawing.Point(110, 171);
            this.txt_SoTien.Name = "txt_SoTien";
            this.txt_SoTien.Size = new System.Drawing.Size(353, 21);
            this.txt_SoTien.TabIndex = 1;
            // 
            // txt_NgayDk
            // 
            this.txt_NgayDk.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NgayDk.Location = new System.Drawing.Point(110, 119);
            this.txt_NgayDk.Name = "txt_NgayDk";
            this.txt_NgayDk.Size = new System.Drawing.Size(354, 21);
            this.txt_NgayDk.TabIndex = 1;
            // 
            // txt_NguoiNT
            // 
            this.txt_NguoiNT.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NguoiNT.Location = new System.Drawing.Point(205, 10);
            this.txt_NguoiNT.Name = "txt_NguoiNT";
            this.txt_NguoiNT.Size = new System.Drawing.Size(259, 21);
            this.txt_NguoiNT.TabIndex = 1;
            // 
            // txt_ToKhaiSo
            // 
            this.txt_ToKhaiSo.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_ToKhaiSo.Location = new System.Drawing.Point(110, 93);
            this.txt_ToKhaiSo.Name = "txt_ToKhaiSo";
            this.txt_ToKhaiSo.Size = new System.Drawing.Size(98, 21);
            this.txt_ToKhaiSo.TabIndex = 1;
            // 
            // txt_LoaiThue
            // 
            this.txt_LoaiThue.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue.Location = new System.Drawing.Point(110, 67);
            this.txt_LoaiThue.Name = "txt_LoaiThue";
            this.txt_LoaiThue.Size = new System.Drawing.Size(98, 21);
            this.txt_LoaiThue.TabIndex = 1;
            // 
            // txt_DBHC_ID
            // 
            this.txt_DBHC_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_ID.Location = new System.Drawing.Point(110, 37);
            this.txt_DBHC_ID.Name = "txt_DBHC_ID";
            this.txt_DBHC_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_DBHC_ID.TabIndex = 1;
            // 
            // txt_MaSoThue
            // 
            this.txt_MaSoThue.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_MaSoThue.Location = new System.Drawing.Point(110, 10);
            this.txt_MaSoThue.Name = "txt_MaSoThue";
            this.txt_MaSoThue.Size = new System.Drawing.Size(98, 21);
            this.txt_MaSoThue.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.LightGray;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(573, 22);
            this.label10.TabIndex = 3;
            this.label10.Text = "Thông Tin Về NNT";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.btn_Save);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(573, 39);
            this.panel8.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(573, 33);
            this.label2.TabIndex = 7;
            this.label2.Text = "NỘI DUNG DỮ LIỆU";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(496, 3);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(70, 30);
            this.btn_Save.TabIndex = 6;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = false;
            // 
            // FrmCheckCT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 632);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Name = "FrmCheckCT";
            this.Text = "FrmCheckCT";
            this.Load += new System.EventHandler(this.FrmCheckCT_Load);
            this.SizeChanged += new System.EventHandler(this.FrmCheckCT_SizeChanged);
            this.panel3.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_ListOrderWeb)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_DataDetailSQL)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txt_DBHC_Name1;
        private System.Windows.Forms.TextBox txt_SoTien1;
        private System.Windows.Forms.TextBox txt_NgayDk1;
        private System.Windows.Forms.TextBox txt_NguoiNT1;
        private System.Windows.Forms.TextBox txt_ToKhaiSo1;
        private System.Windows.Forms.TextBox txt_LoaiThue1;
        private System.Windows.Forms.TextBox txt_DBHC_ID1;
        private System.Windows.Forms.TextBox txt_MaSoThue1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox txt_LoaiThue_Name;
        private System.Windows.Forms.TextBox txt_DBHC_Name;
        private System.Windows.Forms.TextBox txt_SoTien;
        private System.Windows.Forms.TextBox txt_NgayDk;
        private System.Windows.Forms.TextBox txt_NguoiNT;
        private System.Windows.Forms.TextBox txt_ToKhaiSo;
        private System.Windows.Forms.TextBox txt_LoaiThue;
        private System.Windows.Forms.TextBox txt_DBHC_ID;
        private System.Windows.Forms.TextBox txt_MaSoThue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_LHXNK1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_LHXNK;
        private System.Windows.Forms.DataGridView GV_ListOrderWeb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView GV_DataDetailSQL;
        private System.Windows.Forms.Label label35;
    }
}