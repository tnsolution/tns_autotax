﻿using HHT.Library.Bank;
using HHT.Library.System;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class Frm_ImportFileWeb : Form
    {
        private bool _IsPostback;
        public Frm_ImportFileWeb()
        {
            InitializeComponent();
        }

        private void Frm_ImportFileWeb_Load(object sender, EventArgs e)
        {

        }
        void LoadDataListView_Excel(DataTable InTable)
        {
            this.Cursor = Cursors.WaitCursor;
            list_Name.Items.Clear();
            list_Name.Columns.Clear();

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            list_Name.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Message";
            colHead.Width = 100;
            list_Name.Columns.Add(colHead);

            DataRow rHead = InTable.Rows[0];
            for (int i = 0; i < InTable.Columns.Count; i++)
            {
                colHead = new ColumnHeader();
                colHead.Text = rHead[i].ToString();
                colHead.Width = 100;
                list_Name.Columns.Add(colHead);
            }

            for (int i = 1; i < InTable.Rows.Count; i++)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;
                lvi = new ListViewItem();
                lvi.Text = i.ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                for (int o = 0; o < InTable.Columns.Count; o++)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = InTable.Rows[i][o].ToString().Trim();
                    lvi.SubItems.Add(lvsi);
                }

                list_Name.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }

        private void btn_Often_Click(object sender, EventArgs e)
        {
            _IsPostback = false;
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txt_Excel.Text = zOpf.FileName;
                    System.IO.FileInfo zFileImport = new System.IO.FileInfo(zOpf.FileName);
                    txt_Excel.Tag = zFileImport.Name;
                    ExcelPackage package = new OfficeOpenXml.ExcelPackage(zFileImport);

                    foreach (var sheet in package.Workbook.Worksheets)
                    {
                        cbo_Sheet.Items.Add(sheet.Name);
                    }
                    package.Dispose();

                    cbo_Sheet.SelectedIndex = 0;
                    DataTable zTable = Import_Excel.ToTable_1(txt_Excel.Text, cbo_Sheet.Text);
                    LoadDataListView_Excel(zTable);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            _IsPostback = true;
        }

        private void btn_Up_Click(object sender, EventArgs e)
        {

        }

        private void btn_Up_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string SQL = "";
            for (int i = 0; i < list_Name.Items.Count; i++)
            {
                try
                {
                    ListViewItem item = list_Name.Items[i];
                    string MaNV = item.SubItems[5].Text;
                    string MaKS = item.SubItems[7].Text;
                    string SoCT = item.SubItems[9].Text;
                    string NgayKB = item.SubItems[11].Text;
                    string TKNSNN = item.SubItems[12].Text;
                    string MaCQT = item.SubItems[13].Text;
                    string SoTK = item.SubItems[15].Text;
                    string NgayTK = item.SubItems[16].Text;
                    string LHXNK = item.SubItems[17].Text;
                    string TenLH = item.SubItems[19].Text;
                    string MaSoThue = item.SubItems[20].Text;
                    string TenNguoiNopThue = item.SubItems[21].Text.Replace("'", "''");
                    string NDKT = item.SubItems[23].Text;
                    string SoTienChiTiet = item.SubItems[24].Text;
                    string MaDBHC = item.SubItems[25].Text;
                    string LoaiThue = item.SubItems[26].Text;
                    string TrangThai = item.SubItems[27].Text;
                    if (TrangThai != "Đã hủy")
                    {


                        //insert
                        SQL = "INSERT BNK_Order_Web_Template ";
                        SQL += " (MaNV,MaKS,SoCT,NgayKB,TKNSNN,MaCQT," +
                            "SoTK ," +
                            "NgayTK ," +
                            "LHXNK ," +
                            "TenLH ," +
                            "MaSoThue ," +
                            "TenCTy ," +
                            "NDKT ," +
                            "SoTienChiTiet ," +
                            "MaDBHC ," +
                            "LoaiThue) ";
                        SQL += " VALUES ";
                        SQL += " ('" + MaNV + "','" + MaKS + "','" + SoCT + "','" + NgayKB + "','" + TKNSNN + "','" + MaCQT + "','"
                            + SoTK + "','"
                            + NgayTK + "','"
                            + LHXNK + "',N'"
                            + TenLH + "','"
                            + MaSoThue + "',N'"
                            + TenNguoiNopThue + "','"
                            + NDKT + "','"
                            + SoTienChiTiet + "','"
                            + MaDBHC + "','"
                            + LoaiThue + "') ";
                        string zMessage = Data_Access.InsertToTable(SQL);

                        if (zMessage.Length == 0)
                        {
                            list_Name.Items[i].SubItems[1].Text = "Thành công";
                        }
                        else
                        {
                            list_Name.Items[i].BackColor = Color.Red;
                            if (zMessage.ToLower().Contains("cannot insert duplicate key"))
                                list_Name.Items[i].SubItems[1].Text = "Đã đưa lệnh này vào rồi";
                            else
                                list_Name.Items[i].SubItems[1].Text = zMessage;
                        }
                    }
                }
                catch (Exception ex)
                {
                    list_Name.Items[i].SubItems[1].Text = ex.ToString();
                }


            }
            string SQL1 = @"Insert_Web";
            string _Message2 = OrderTranfer_Data.Insert_Web(SQL1).ToString();
            this.Cursor = Cursors.Default;
        }
    }

}
