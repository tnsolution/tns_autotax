﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;

namespace HHT_AutoTax
{
    public partial class FrmOrder : Form
    {

        public FrmOrder()
        {
            InitializeComponent();
        }

        private void FrmOrder_Load(object sender, EventArgs e)
        {
            InitListView(LV_Data);

            //Picker_From.Value = DateTime.Now;
            Picker_From.Value = new DateTime(2021, 03, 13);
            Picker_To.Value = DateTime.Now;
            LoadDataToListView(SectionImport_Data.ListToWeb(Picker_From.Value, Picker_To.Value));
        }
        private void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Section No";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "File Name - Sheet";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thời Gian";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Dữ liệu CT";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Web Thành Công";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Còn lại";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tỷ Lệ";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }
        public void LoadDataToListView(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SectionNo"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FileName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                DateTime z_trdate = (DateTime)nRow["CreatedOn"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy HH:mm");
                lvi.SubItems.Add(lvsi);

                double zAmount = double.Parse(nRow["AmountToWeb"].ToString());
                if (zAmount > 0)
                {
                    double zAmountSuccess = double.Parse(nRow["AmountSuccess"].ToString());
                    int zPercent = (int)(zAmountSuccess * 100) / (int)zAmount;

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zAmount.ToString("###,###,###");
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zAmountSuccess.ToString("###,###,###");
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = (zAmount - zAmountSuccess).ToString("###,###,###");
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zPercent.ToString() + "%";
                    lvi.SubItems.Add(lvsi);
                }



                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }

        private void btn_View_Click(object sender, EventArgs e)
        {
            LoadDataToListView(SectionImport_Data.ListToWeb(Picker_From.Value, Picker_To.Value));
        }

        private void btn_Delivery_Click(object sender, EventArgs e)
        {
            /*int n = LV_Data.SelectedItems.Count;
            string zUserKey = "";
            string z_trref = "";
            int zStatus = 0;
            if (n > 0)
            {
                FrmEmployeeChoose frm = new FrmEmployeeChoose();
                frm.ShowDialog();
                zUserKey = frm.UserKeyChoose;
                if (zUserKey.Length > 0)
                {
                    for (int i = 0; i < n; i++)
                    {
                        z_trref = LV_Data.SelectedItems[i].SubItems[4].Text.ToString();
                        zStatus = (int)LV_Data.SelectedItems[i].SubItems[1].Tag;

                        if (z_trref.Length > 0)
                        {
                            if (zStatus == 3)
                                MessageBox.Show(z_trref + " Đã hoàn thành, không thể phân công");
                            else
                                OrderTranfer_Data.DeliveryOrder(z_trref, zUserKey);
                        }
                    }
                }
            }
            else
                MessageBox.Show("Xin vui lòng chọn danh sách lệnh");
            btn_View_Click(null, null);*/
        }

        private void btn_Import_Click(object sender, EventArgs e)
        {
           /* Frm_UpExcel frm = new Frm_UpExcel();
            frm.ShowDialog();
            LoadDataToListView(SectionImportData_Data.List(Picker_From.Value, Picker_To.Value));*/
        }

        private void LV_Data_ItemActivate(object sender, EventArgs e)
        {
            FrmOrderUpToWeb frm = new FrmOrderUpToWeb();
            frm.SectionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[1].Text);
            frm.ShowDialog();
            
            LoadDataToListView(SectionImport_Data.ListToWeb(Picker_From.Value, Picker_To.Value));

        }

        private void btn_Group_Click(object sender, EventArgs e)
        {
            //FrmMakeGroupOrder frm = new FrmMakeGroupOrder();
            //frm.ShowDialog();
        }

        private void bnt_Check_Click(object sender, EventArgs e)
        {
            //FrmCheckAfterUpWeb frm = new FrmCheckAfterUpWeb();
            //frm.SecctionNo = int.Parse(LV_Data.SelectedItems[0].SubItems[0].Text);
            //frm.ShowDialog();
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {

        }
    }
}
