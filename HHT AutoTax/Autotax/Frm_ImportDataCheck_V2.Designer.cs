﻿namespace HHT_AutoTax
{
    partial class Frm_ImportDataCheck_V2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ImportDataCheck_V2));
            this.panel2 = new System.Windows.Forms.Panel();
            this.LV_Data_Web = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_BeginCheck = new System.Windows.Forms.Button();
            this.btn_Copy = new System.Windows.Forms.Button();
            this.lbl_AmountCheck = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_ImportAgriTax = new System.Windows.Forms.Button();
            this.btn_ImportIPICAS = new System.Windows.Forms.Button();
            this.DayForOrder = new System.Windows.Forms.MonthCalendar();
            this.btn_FillCheck = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LV_IPCAS = new System.Windows.Forms.ListView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.LV_Report = new System.Windows.Forms.ListView();
            this.LV_ItemMutil = new System.Windows.Forms.ListView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.LV_IPCAS_Fail = new System.Windows.Forms.ListView();
            this.LV_Web_Fail = new System.Windows.Forms.ListView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.LV_IPCAS_Copy = new System.Windows.Forms.ListView();
            this.LV_Data_Web_Copy = new System.Windows.Forms.ListView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.LV_FailConLai = new System.Windows.Forms.ListView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LV_Check_Ver2 = new System.Windows.Forms.ListView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_View = new System.Windows.Forms.Button();
            this.Picker_To = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.Picker_From = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(234, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(5, 486);
            this.panel2.TabIndex = 6;
            // 
            // LV_Data_Web
            // 
            this.LV_Data_Web.Dock = System.Windows.Forms.DockStyle.Left;
            this.LV_Data_Web.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Data_Web.ForeColor = System.Drawing.Color.Navy;
            this.LV_Data_Web.FullRowSelect = true;
            this.LV_Data_Web.GridLines = true;
            this.LV_Data_Web.Location = new System.Drawing.Point(3, 38);
            this.LV_Data_Web.Name = "LV_Data_Web";
            this.LV_Data_Web.Size = new System.Drawing.Size(320, 419);
            this.LV_Data_Web.TabIndex = 5;
            this.LV_Data_Web.UseCompatibleStateImageBehavior = false;
            this.LV_Data_Web.View = System.Windows.Forms.View.Details;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "DỮ LIỆU";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btn_BeginCheck);
            this.panel1.Controls.Add(this.btn_Copy);
            this.panel1.Controls.Add(this.lbl_AmountCheck);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.DayForOrder);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_FillCheck);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 486);
            this.panel1.TabIndex = 4;
            // 
            // btn_BeginCheck
            // 
            this.btn_BeginCheck.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_BeginCheck.Location = new System.Drawing.Point(0, 417);
            this.btn_BeginCheck.Name = "btn_BeginCheck";
            this.btn_BeginCheck.Size = new System.Drawing.Size(234, 23);
            this.btn_BeginCheck.TabIndex = 15;
            this.btn_BeginCheck.Text = "Kiểm Tra";
            this.btn_BeginCheck.UseVisualStyleBackColor = true;
            this.btn_BeginCheck.Click += new System.EventHandler(this.btn_BeginCheck_Click);
            // 
            // btn_Copy
            // 
            this.btn_Copy.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_Copy.Location = new System.Drawing.Point(0, 440);
            this.btn_Copy.Name = "btn_Copy";
            this.btn_Copy.Size = new System.Drawing.Size(234, 23);
            this.btn_Copy.TabIndex = 17;
            this.btn_Copy.Text = "Phân Loại";
            this.btn_Copy.UseVisualStyleBackColor = true;
            // 
            // lbl_AmountCheck
            // 
            this.lbl_AmountCheck.AutoSize = true;
            this.lbl_AmountCheck.Location = new System.Drawing.Point(89, 291);
            this.lbl_AmountCheck.Name = "lbl_AmountCheck";
            this.lbl_AmountCheck.Size = new System.Drawing.Size(14, 15);
            this.lbl_AmountCheck.TabIndex = 16;
            this.lbl_AmountCheck.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 291);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "Số Lượng : ";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_ImportAgriTax);
            this.panel4.Controls.Add(this.btn_ImportIPICAS);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 195);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(234, 83);
            this.panel4.TabIndex = 13;
            // 
            // btn_ImportAgriTax
            // 
            this.btn_ImportAgriTax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_ImportAgriTax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ImportAgriTax.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportAgriTax.ForeColor = System.Drawing.Color.White;
            this.btn_ImportAgriTax.Image = ((System.Drawing.Image)(resources.GetObject("btn_ImportAgriTax.Image")));
            this.btn_ImportAgriTax.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ImportAgriTax.Location = new System.Drawing.Point(8, 38);
            this.btn_ImportAgriTax.Name = "btn_ImportAgriTax";
            this.btn_ImportAgriTax.Size = new System.Drawing.Size(107, 33);
            this.btn_ImportAgriTax.TabIndex = 10;
            this.btn_ImportAgriTax.Text = "Agri Tax";
            this.btn_ImportAgriTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ImportAgriTax.UseVisualStyleBackColor = false;
            this.btn_ImportAgriTax.Click += new System.EventHandler(this.btn_ImportAgriTax_Click);
            // 
            // btn_ImportIPICAS
            // 
            this.btn_ImportIPICAS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_ImportIPICAS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ImportIPICAS.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportIPICAS.ForeColor = System.Drawing.Color.White;
            this.btn_ImportIPICAS.Image = ((System.Drawing.Image)(resources.GetObject("btn_ImportIPICAS.Image")));
            this.btn_ImportIPICAS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ImportIPICAS.Location = new System.Drawing.Point(120, 38);
            this.btn_ImportIPICAS.Name = "btn_ImportIPICAS";
            this.btn_ImportIPICAS.Size = new System.Drawing.Size(100, 33);
            this.btn_ImportIPICAS.TabIndex = 11;
            this.btn_ImportIPICAS.Text = "IPCAS";
            this.btn_ImportIPICAS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ImportIPICAS.UseVisualStyleBackColor = false;
            this.btn_ImportIPICAS.Click += new System.EventHandler(this.btn_ImportIPICAS_Click);
            // 
            // DayForOrder
            // 
            this.DayForOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.DayForOrder.Location = new System.Drawing.Point(0, 33);
            this.DayForOrder.Name = "DayForOrder";
            this.DayForOrder.TabIndex = 3;
            this.DayForOrder.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.DayForOrder_DateSelected);
            // 
            // btn_FillCheck
            // 
            this.btn_FillCheck.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_FillCheck.Location = new System.Drawing.Point(0, 463);
            this.btn_FillCheck.Name = "btn_FillCheck";
            this.btn_FillCheck.Size = new System.Drawing.Size(234, 23);
            this.btn_FillCheck.TabIndex = 18;
            this.btn_FillCheck.Text = "Kiểm Tra Còn Lại";
            this.btn_FillCheck.UseVisualStyleBackColor = true;
            this.btn_FillCheck.Click += new System.EventHandler(this.btn_FillCheck_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(239, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(611, 486);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LV_IPCAS);
            this.tabPage1.Controls.Add(this.LV_Data_Web);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(603, 460);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "AgriTax-IPCAS";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LV_IPCAS
            // 
            this.LV_IPCAS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_IPCAS.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_IPCAS.ForeColor = System.Drawing.Color.Navy;
            this.LV_IPCAS.FullRowSelect = true;
            this.LV_IPCAS.GridLines = true;
            this.LV_IPCAS.Location = new System.Drawing.Point(323, 38);
            this.LV_IPCAS.Name = "LV_IPCAS";
            this.LV_IPCAS.Size = new System.Drawing.Size(277, 419);
            this.LV_IPCAS.TabIndex = 6;
            this.LV_IPCAS.UseCompatibleStateImageBehavior = false;
            this.LV_IPCAS.View = System.Windows.Forms.View.Details;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(597, 35);
            this.panel5.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(451, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 35);
            this.label5.TabIndex = 7;
            this.label5.Text = "IP CAS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 35);
            this.label4.TabIndex = 7;
            this.label4.Text = "AGRI TAX";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.LV_Report);
            this.tabPage2.Controls.Add(this.LV_ItemMutil);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(603, 460);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Report";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // LV_Report
            // 
            this.LV_Report.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Report.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Report.ForeColor = System.Drawing.Color.Navy;
            this.LV_Report.FullRowSelect = true;
            this.LV_Report.GridLines = true;
            this.LV_Report.Location = new System.Drawing.Point(3, 284);
            this.LV_Report.Name = "LV_Report";
            this.LV_Report.Size = new System.Drawing.Size(597, 173);
            this.LV_Report.TabIndex = 15;
            this.LV_Report.UseCompatibleStateImageBehavior = false;
            this.LV_Report.View = System.Windows.Forms.View.Details;
            // 
            // LV_ItemMutil
            // 
            this.LV_ItemMutil.Dock = System.Windows.Forms.DockStyle.Top;
            this.LV_ItemMutil.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_ItemMutil.ForeColor = System.Drawing.Color.Navy;
            this.LV_ItemMutil.FullRowSelect = true;
            this.LV_ItemMutil.GridLines = true;
            this.LV_ItemMutil.Location = new System.Drawing.Point(3, 3);
            this.LV_ItemMutil.Name = "LV_ItemMutil";
            this.LV_ItemMutil.Size = new System.Drawing.Size(597, 281);
            this.LV_ItemMutil.TabIndex = 6;
            this.LV_ItemMutil.UseCompatibleStateImageBehavior = false;
            this.LV_ItemMutil.View = System.Windows.Forms.View.Details;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.LV_IPCAS_Fail);
            this.tabPage5.Controls.Add(this.LV_Web_Fail);
            this.tabPage5.Controls.Add(this.panel7);
            this.tabPage5.Location = new System.Drawing.Point(4, 4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(603, 460);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Kết Quả Kiểm tra lại";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // LV_IPCAS_Fail
            // 
            this.LV_IPCAS_Fail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_IPCAS_Fail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_IPCAS_Fail.ForeColor = System.Drawing.Color.Navy;
            this.LV_IPCAS_Fail.FullRowSelect = true;
            this.LV_IPCAS_Fail.GridLines = true;
            this.LV_IPCAS_Fail.Location = new System.Drawing.Point(323, 38);
            this.LV_IPCAS_Fail.Name = "LV_IPCAS_Fail";
            this.LV_IPCAS_Fail.Size = new System.Drawing.Size(277, 419);
            this.LV_IPCAS_Fail.TabIndex = 13;
            this.LV_IPCAS_Fail.UseCompatibleStateImageBehavior = false;
            this.LV_IPCAS_Fail.View = System.Windows.Forms.View.Details;
            // 
            // LV_Web_Fail
            // 
            this.LV_Web_Fail.Dock = System.Windows.Forms.DockStyle.Left;
            this.LV_Web_Fail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Web_Fail.ForeColor = System.Drawing.Color.Navy;
            this.LV_Web_Fail.FullRowSelect = true;
            this.LV_Web_Fail.GridLines = true;
            this.LV_Web_Fail.Location = new System.Drawing.Point(3, 38);
            this.LV_Web_Fail.Name = "LV_Web_Fail";
            this.LV_Web_Fail.Size = new System.Drawing.Size(320, 419);
            this.LV_Web_Fail.TabIndex = 12;
            this.LV_Web_Fail.UseCompatibleStateImageBehavior = false;
            this.LV_Web_Fail.View = System.Windows.Forms.View.Details;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(597, 35);
            this.panel7.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label9.Dock = System.Windows.Forms.DockStyle.Right;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(451, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(146, 35);
            this.label9.TabIndex = 7;
            this.label9.Text = "IP CAS";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(146, 35);
            this.label10.TabIndex = 7;
            this.label10.Text = "AGRI TAX";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.LV_IPCAS_Copy);
            this.tabPage6.Controls.Add(this.LV_Data_Web_Copy);
            this.tabPage6.Controls.Add(this.panel6);
            this.tabPage6.Location = new System.Drawing.Point(4, 4);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(603, 460);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Phân Loại";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // LV_IPCAS_Copy
            // 
            this.LV_IPCAS_Copy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_IPCAS_Copy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_IPCAS_Copy.ForeColor = System.Drawing.Color.Navy;
            this.LV_IPCAS_Copy.FullRowSelect = true;
            this.LV_IPCAS_Copy.GridLines = true;
            this.LV_IPCAS_Copy.Location = new System.Drawing.Point(323, 38);
            this.LV_IPCAS_Copy.Name = "LV_IPCAS_Copy";
            this.LV_IPCAS_Copy.Size = new System.Drawing.Size(277, 419);
            this.LV_IPCAS_Copy.TabIndex = 10;
            this.LV_IPCAS_Copy.UseCompatibleStateImageBehavior = false;
            this.LV_IPCAS_Copy.View = System.Windows.Forms.View.Details;
            // 
            // LV_Data_Web_Copy
            // 
            this.LV_Data_Web_Copy.Dock = System.Windows.Forms.DockStyle.Left;
            this.LV_Data_Web_Copy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Data_Web_Copy.ForeColor = System.Drawing.Color.Navy;
            this.LV_Data_Web_Copy.FullRowSelect = true;
            this.LV_Data_Web_Copy.GridLines = true;
            this.LV_Data_Web_Copy.Location = new System.Drawing.Point(3, 38);
            this.LV_Data_Web_Copy.Name = "LV_Data_Web_Copy";
            this.LV_Data_Web_Copy.Size = new System.Drawing.Size(320, 419);
            this.LV_Data_Web_Copy.TabIndex = 9;
            this.LV_Data_Web_Copy.UseCompatibleStateImageBehavior = false;
            this.LV_Data_Web_Copy.View = System.Windows.Forms.View.Details;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(597, 35);
            this.panel6.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(451, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 35);
            this.label7.TabIndex = 7;
            this.label7.Text = "IP CAS";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 35);
            this.label8.TabIndex = 7;
            this.label8.Text = "AGRI TAX";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.LV_FailConLai);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(603, 460);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Lệnh Sai";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // LV_FailConLai
            // 
            this.LV_FailConLai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_FailConLai.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_FailConLai.ForeColor = System.Drawing.Color.Navy;
            this.LV_FailConLai.FullRowSelect = true;
            this.LV_FailConLai.GridLines = true;
            this.LV_FailConLai.Location = new System.Drawing.Point(0, 0);
            this.LV_FailConLai.Name = "LV_FailConLai";
            this.LV_FailConLai.Size = new System.Drawing.Size(603, 460);
            this.LV_FailConLai.TabIndex = 11;
            this.LV_FailConLai.UseCompatibleStateImageBehavior = false;
            this.LV_FailConLai.View = System.Windows.Forms.View.Details;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LV_Check_Ver2);
            this.tabPage4.Controls.Add(this.panel3);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(603, 460);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "VUI LÒNG KIỂM TRA LẠI CHẠY LẦN 2 ";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LV_Check_Ver2
            // 
            this.LV_Check_Ver2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Check_Ver2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Check_Ver2.ForeColor = System.Drawing.Color.Navy;
            this.LV_Check_Ver2.FullRowSelect = true;
            this.LV_Check_Ver2.GridLines = true;
            this.LV_Check_Ver2.Location = new System.Drawing.Point(0, 60);
            this.LV_Check_Ver2.Name = "LV_Check_Ver2";
            this.LV_Check_Ver2.Size = new System.Drawing.Size(603, 400);
            this.LV_Check_Ver2.TabIndex = 13;
            this.LV_Check_Ver2.UseCompatibleStateImageBehavior = false;
            this.LV_Check_Ver2.View = System.Windows.Forms.View.Details;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_View);
            this.panel3.Controls.Add(this.Picker_To);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.Picker_From);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(603, 60);
            this.panel3.TabIndex = 0;
            // 
            // btn_View
            // 
            this.btn_View.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_View.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_View.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_View.ForeColor = System.Drawing.Color.White;
            this.btn_View.Image = ((System.Drawing.Image)(resources.GetObject("btn_View.Image")));
            this.btn_View.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_View.Location = new System.Drawing.Point(155, 4);
            this.btn_View.Name = "btn_View";
            this.btn_View.Size = new System.Drawing.Size(82, 49);
            this.btn_View.TabIndex = 11;
            this.btn_View.Text = "Xem";
            this.btn_View.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_View.UseVisualStyleBackColor = false;
            this.btn_View.Click += new System.EventHandler(this.btn_View_Click);
            // 
            // Picker_To
            // 
            this.Picker_To.CustomFormat = "dd/MM/yyyy";
            this.Picker_To.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Picker_To.Location = new System.Drawing.Point(63, 32);
            this.Picker_To.Name = "Picker_To";
            this.Picker_To.Size = new System.Drawing.Size(86, 20);
            this.Picker_To.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(4, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Từ Ngày";
            // 
            // Picker_From
            // 
            this.Picker_From.CustomFormat = "dd/MM/yyyy";
            this.Picker_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Picker_From.Location = new System.Drawing.Point(63, 6);
            this.Picker_From.Name = "Picker_From";
            this.Picker_From.Size = new System.Drawing.Size(86, 20);
            this.Picker_From.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(4, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Từ Ngày";
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Frm_ImportDataCheck_V2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 486);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Frm_ImportDataCheck_V2";
            this.Text = "Frm_ImportDataCheck_V2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_ImportDataCheck_V2_Load);
            this.SizeChanged += new System.EventHandler(this.Frm_ImportDataCheck_V2_SizeChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView LV_Data_Web;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MonthCalendar DayForOrder;
        private System.Windows.Forms.Button btn_ImportIPICAS;
        private System.Windows.Forms.Button btn_ImportAgriTax;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn_BeginCheck;
        private System.Windows.Forms.ListView LV_ItemMutil;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView LV_IPCAS;
        private System.Windows.Forms.ListView LV_Report;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_AmountCheck;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btn_Copy;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ListView LV_IPCAS_Copy;
        private System.Windows.Forms.ListView LV_Data_Web_Copy;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_FillCheck;
        private System.Windows.Forms.ListView LV_IPCAS_Fail;
        private System.Windows.Forms.ListView LV_Web_Fail;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ListView LV_FailConLai;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView LV_Check_Ver2;
        private System.Windows.Forms.Button btn_View;
        private System.Windows.Forms.DateTimePicker Picker_To;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker Picker_From;
        private System.Windows.Forms.Label label2;
    }
}