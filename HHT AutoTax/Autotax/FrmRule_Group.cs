﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class FrmRule_Group : Form
    {
        public FrmRule_Group()
        {
            InitializeComponent();
        }

        private void FrmRule_Group_Load(object sender, EventArgs e)
        {
            InitGridView_Rule_Group(GV_Rule_Group);
        }
        public void InitGridView_Rule_Group(DataGridView GV)
        {
            // Setup Column 
            for (int i = 0; i < 20; i++)
            {
                GV.Columns.Add("Item_" + i.ToString(), "");
            }
            for (int i = 0; i < 20; i++)
            {
                GV.Columns[i].Width = 70;
                GV.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 10; i++)
            {
                GV.Rows.Add();
            }
        }
    }
}
