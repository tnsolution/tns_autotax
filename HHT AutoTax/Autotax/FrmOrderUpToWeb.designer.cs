﻿namespace HHT_AutoTax
{
    partial class FrmOrderUpToWeb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOrderUpToWeb));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.GV_DataDetail = new System.Windows.Forms.DataGridView();
            this.label35 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_LHXNK_Name = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue_Name = new System.Windows.Forms.TextBox();
            this.txt_CQQLThu_Name = new System.Windows.Forms.TextBox();
            this.txt_DBHC_Name = new System.Windows.Forms.TextBox();
            this.txt_LHXNK = new System.Windows.Forms.TextBox();
            this.txt_TK_Name = new System.Windows.Forms.TextBox();
            this.txt_NgayDk = new System.Windows.Forms.TextBox();
            this.txt_NguoiNT = new System.Windows.Forms.TextBox();
            this.txt_ToKhaiSo = new System.Windows.Forms.TextBox();
            this.txt_NH_PhucVu_Name = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue = new System.Windows.Forms.TextBox();
            this.txt_CQQLThu_ID = new System.Windows.Forms.TextBox();
            this.txt_DBHC_ID = new System.Windows.Forms.TextBox();
            this.txt_TK_Thu_NS = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.txt_MaSoThue = new System.Windows.Forms.TextBox();
            this.txt_NgayNT = new System.Windows.Forms.TextBox();
            this.txt_NH_PhucVu = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbo_KieuThu = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_NHNN_Name = new System.Windows.Forms.TextBox();
            this.txt_NHNN_ID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_So_CT_Web = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.txt_SoChungTu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LB_Web_Log = new System.Windows.Forms.ListBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_SoBT = new System.Windows.Forms.TextBox();
            this.btn_Get_Amount = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_ID = new System.Windows.Forms.TextBox();
            this.txt_Amount_On_Web = new System.Windows.Forms.TextBox();
            this.cbo_ListWeb = new System.Windows.Forms.ComboBox();
            this.Panel_Hide = new System.Windows.Forms.Panel();
            this.btn_Hide = new System.Windows.Forms.Button();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.GV_ListOrder = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_Export = new System.Windows.Forms.Button();
            this.btn_Run_One = new System.Windows.Forms.Button();
            this.btn_Apply = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_View = new System.Windows.Forms.Button();
            this.rdo_NotYet = new System.Windows.Forms.RadioButton();
            this.rdo_All = new System.Windows.Forms.RadioButton();
            this.rdo_Error = new System.Windows.Forms.RadioButton();
            this.rdo_Success = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Record_Error = new System.Windows.Forms.Label();
            this.lbl_Total_Record = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_Record_Success = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.txt_DVSDNS_Name = new System.Windows.Forms.TextBox();
            this.txt_DVSDNS_ID = new System.Windows.Forms.TextBox();
            this.Panel_Right.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_DataDetail)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.Panel_Hide.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_ListOrder)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Panel_Right
            // 
            this.Panel_Right.Controls.Add(this.tabControl1);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(657, 0);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(600, 504);
            this.Panel_Right.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(600, 504);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(592, 478);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lệnh CT";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.GV_DataDetail);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 488);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(569, 146);
            this.panel1.TabIndex = 7;
            // 
            // GV_DataDetail
            // 
            this.GV_DataDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_DataDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_DataDetail.Location = new System.Drawing.Point(0, 22);
            this.GV_DataDetail.Name = "GV_DataDetail";
            this.GV_DataDetail.Size = new System.Drawing.Size(569, 124);
            this.GV_DataDetail.TabIndex = 5;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.LightGray;
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(569, 22);
            this.label35.TabIndex = 4;
            this.label35.Text = "Thông Tin Chi Tiết";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.txt_DVSDNS_Name);
            this.panel4.Controls.Add(this.txt_DVSDNS_ID);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.txt_LHXNK_Name);
            this.panel4.Controls.Add(this.textBox29);
            this.panel4.Controls.Add(this.txt_LoaiThue_Name);
            this.panel4.Controls.Add(this.txt_CQQLThu_Name);
            this.panel4.Controls.Add(this.txt_DBHC_Name);
            this.panel4.Controls.Add(this.txt_LHXNK);
            this.panel4.Controls.Add(this.txt_TK_Name);
            this.panel4.Controls.Add(this.txt_NgayDk);
            this.panel4.Controls.Add(this.txt_NguoiNT);
            this.panel4.Controls.Add(this.txt_ToKhaiSo);
            this.panel4.Controls.Add(this.txt_NH_PhucVu_Name);
            this.panel4.Controls.Add(this.txt_LoaiThue);
            this.panel4.Controls.Add(this.txt_CQQLThu_ID);
            this.panel4.Controls.Add(this.txt_DBHC_ID);
            this.panel4.Controls.Add(this.txt_TK_Thu_NS);
            this.panel4.Controls.Add(this.textBox14);
            this.panel4.Controls.Add(this.txt_MaSoThue);
            this.panel4.Controls.Add(this.txt_NgayNT);
            this.panel4.Controls.Add(this.txt_NH_PhucVu);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(3, 155);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(569, 333);
            this.panel4.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ngày Nộp Thuế";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 95);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Địa chỉ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 300);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 15);
            this.label25.TabIndex = 0;
            this.label25.Text = "Loại Hình XNK";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 274);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 15);
            this.label24.TabIndex = 0;
            this.label24.Text = "Ngày DK";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 248);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 15);
            this.label23.TabIndex = 0;
            this.label23.Text = "Tờ Khai Số";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 222);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 15);
            this.label22.TabIndex = 0;
            this.label22.Text = "Loại Thuế";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 196);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 15);
            this.label21.TabIndex = 0;
            this.label21.Text = "CQ QL Thu";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 169);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "Mã DBHC";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 15);
            this.label19.TabIndex = 0;
            this.label19.Text = "TK Thu NS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã Số Thuế";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "NH Phục Vụ NNT";
            // 
            // txt_LHXNK_Name
            // 
            this.txt_LHXNK_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LHXNK_Name.Location = new System.Drawing.Point(205, 297);
            this.txt_LHXNK_Name.Name = "txt_LHXNK_Name";
            this.txt_LHXNK_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_LHXNK_Name.TabIndex = 1;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox29.Location = new System.Drawing.Point(205, 245);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(259, 21);
            this.textBox29.TabIndex = 1;
            // 
            // txt_LoaiThue_Name
            // 
            this.txt_LoaiThue_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue_Name.Location = new System.Drawing.Point(205, 219);
            this.txt_LoaiThue_Name.Name = "txt_LoaiThue_Name";
            this.txt_LoaiThue_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_LoaiThue_Name.TabIndex = 1;
            // 
            // txt_CQQLThu_Name
            // 
            this.txt_CQQLThu_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_CQQLThu_Name.Location = new System.Drawing.Point(205, 193);
            this.txt_CQQLThu_Name.Name = "txt_CQQLThu_Name";
            this.txt_CQQLThu_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_CQQLThu_Name.TabIndex = 1;
            // 
            // txt_DBHC_Name
            // 
            this.txt_DBHC_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_Name.Location = new System.Drawing.Point(205, 166);
            this.txt_DBHC_Name.Name = "txt_DBHC_Name";
            this.txt_DBHC_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_DBHC_Name.TabIndex = 1;
            // 
            // txt_LHXNK
            // 
            this.txt_LHXNK.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LHXNK.Location = new System.Drawing.Point(110, 297);
            this.txt_LHXNK.Name = "txt_LHXNK";
            this.txt_LHXNK.Size = new System.Drawing.Size(98, 21);
            this.txt_LHXNK.TabIndex = 1;
            // 
            // txt_TK_Name
            // 
            this.txt_TK_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_TK_Name.Location = new System.Drawing.Point(205, 116);
            this.txt_TK_Name.Name = "txt_TK_Name";
            this.txt_TK_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_TK_Name.TabIndex = 1;
            // 
            // txt_NgayDk
            // 
            this.txt_NgayDk.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NgayDk.Location = new System.Drawing.Point(110, 271);
            this.txt_NgayDk.Name = "txt_NgayDk";
            this.txt_NgayDk.Size = new System.Drawing.Size(354, 21);
            this.txt_NgayDk.TabIndex = 1;
            // 
            // txt_NguoiNT
            // 
            this.txt_NguoiNT.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NguoiNT.Location = new System.Drawing.Point(205, 62);
            this.txt_NguoiNT.Name = "txt_NguoiNT";
            this.txt_NguoiNT.Size = new System.Drawing.Size(259, 21);
            this.txt_NguoiNT.TabIndex = 1;
            // 
            // txt_ToKhaiSo
            // 
            this.txt_ToKhaiSo.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_ToKhaiSo.Location = new System.Drawing.Point(110, 245);
            this.txt_ToKhaiSo.Name = "txt_ToKhaiSo";
            this.txt_ToKhaiSo.Size = new System.Drawing.Size(98, 21);
            this.txt_ToKhaiSo.TabIndex = 1;
            // 
            // txt_NH_PhucVu_Name
            // 
            this.txt_NH_PhucVu_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NH_PhucVu_Name.Location = new System.Drawing.Point(205, 8);
            this.txt_NH_PhucVu_Name.Name = "txt_NH_PhucVu_Name";
            this.txt_NH_PhucVu_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_NH_PhucVu_Name.TabIndex = 1;
            // 
            // txt_LoaiThue
            // 
            this.txt_LoaiThue.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue.Location = new System.Drawing.Point(110, 219);
            this.txt_LoaiThue.Name = "txt_LoaiThue";
            this.txt_LoaiThue.Size = new System.Drawing.Size(98, 21);
            this.txt_LoaiThue.TabIndex = 1;
            // 
            // txt_CQQLThu_ID
            // 
            this.txt_CQQLThu_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_CQQLThu_ID.Location = new System.Drawing.Point(110, 193);
            this.txt_CQQLThu_ID.Name = "txt_CQQLThu_ID";
            this.txt_CQQLThu_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_CQQLThu_ID.TabIndex = 1;
            // 
            // txt_DBHC_ID
            // 
            this.txt_DBHC_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_ID.Location = new System.Drawing.Point(110, 166);
            this.txt_DBHC_ID.Name = "txt_DBHC_ID";
            this.txt_DBHC_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_DBHC_ID.TabIndex = 1;
            // 
            // txt_TK_Thu_NS
            // 
            this.txt_TK_Thu_NS.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_TK_Thu_NS.Location = new System.Drawing.Point(110, 116);
            this.txt_TK_Thu_NS.Name = "txt_TK_Thu_NS";
            this.txt_TK_Thu_NS.Size = new System.Drawing.Size(98, 21);
            this.txt_TK_Thu_NS.TabIndex = 1;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox14.Location = new System.Drawing.Point(110, 89);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(352, 21);
            this.textBox14.TabIndex = 1;
            // 
            // txt_MaSoThue
            // 
            this.txt_MaSoThue.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_MaSoThue.Location = new System.Drawing.Point(110, 62);
            this.txt_MaSoThue.Name = "txt_MaSoThue";
            this.txt_MaSoThue.Size = new System.Drawing.Size(98, 21);
            this.txt_MaSoThue.TabIndex = 1;
            // 
            // txt_NgayNT
            // 
            this.txt_NgayNT.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NgayNT.Location = new System.Drawing.Point(110, 35);
            this.txt_NgayNT.Name = "txt_NgayNT";
            this.txt_NgayNT.Size = new System.Drawing.Size(354, 21);
            this.txt_NgayNT.TabIndex = 1;
            // 
            // txt_NH_PhucVu
            // 
            this.txt_NH_PhucVu.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NH_PhucVu.Location = new System.Drawing.Point(110, 8);
            this.txt_NH_PhucVu.Name = "txt_NH_PhucVu";
            this.txt_NH_PhucVu.Size = new System.Drawing.Size(98, 21);
            this.txt_NH_PhucVu.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.LightGray;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(569, 22);
            this.label10.TabIndex = 3;
            this.label10.Text = "Thông Tin Về NNT";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbo_KieuThu);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.txt_NHNN_Name);
            this.panel3.Controls.Add(this.txt_NHNN_ID);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(569, 69);
            this.panel3.TabIndex = 2;
            // 
            // cbo_KieuThu
            // 
            this.cbo_KieuThu.FormattingEnabled = true;
            this.cbo_KieuThu.Items.AddRange(new object[] {
            "Tiền mặt",
            "Chuyển khoản",
            "Chuyển liên ngân hàng"});
            this.cbo_KieuThu.Location = new System.Drawing.Point(101, 35);
            this.cbo_KieuThu.Name = "cbo_KieuThu";
            this.cbo_KieuThu.Size = new System.Drawing.Size(363, 23);
            this.cbo_KieuThu.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "Hình Thức Thu";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "NHNN Thu";
            // 
            // txt_NHNN_Name
            // 
            this.txt_NHNN_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NHNN_Name.Location = new System.Drawing.Point(205, 8);
            this.txt_NHNN_Name.Name = "txt_NHNN_Name";
            this.txt_NHNN_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_NHNN_Name.TabIndex = 1;
            // 
            // txt_NHNN_ID
            // 
            this.txt_NHNN_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NHNN_ID.Location = new System.Drawing.Point(101, 8);
            this.txt_NHNN_ID.Name = "txt_NHNN_ID";
            this.txt_NHNN_ID.Size = new System.Drawing.Size(107, 21);
            this.txt_NHNN_ID.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.LightGray;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(569, 22);
            this.label7.TabIndex = 1;
            this.label7.Text = "Chọn Kênh Thanh Toán";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txt_So_CT_Web);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btn_Save);
            this.panel2.Controls.Add(this.txt_SoChungTu);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(569, 39);
            this.panel2.TabIndex = 0;
            // 
            // txt_So_CT_Web
            // 
            this.txt_So_CT_Web.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_So_CT_Web.Location = new System.Drawing.Point(330, 9);
            this.txt_So_CT_Web.Name = "txt_So_CT_Web";
            this.txt_So_CT_Web.Size = new System.Drawing.Size(134, 21);
            this.txt_So_CT_Web.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(256, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "Mã Lệnh";
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(496, 3);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(70, 30);
            this.btn_Save.TabIndex = 6;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = false;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // txt_SoChungTu
            // 
            this.txt_SoChungTu.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_SoChungTu.Location = new System.Drawing.Point(101, 8);
            this.txt_SoChungTu.Name = "txt_SoChungTu";
            this.txt_SoChungTu.Size = new System.Drawing.Size(147, 21);
            this.txt_SoChungTu.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số CT";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LB_Web_Log);
            this.tabPage4.Controls.Add(this.panel7);
            this.tabPage4.Controls.Add(this.cbo_ListWeb);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(592, 478);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Access Web";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LB_Web_Log
            // 
            this.LB_Web_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LB_Web_Log.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_Web_Log.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LB_Web_Log.FormattingEnabled = true;
            this.LB_Web_Log.ItemHeight = 16;
            this.LB_Web_Log.Location = new System.Drawing.Point(0, 21);
            this.LB_Web_Log.Name = "LB_Web_Log";
            this.LB_Web_Log.Size = new System.Drawing.Size(592, 394);
            this.LB_Web_Log.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.txt_SoBT);
            this.panel7.Controls.Add(this.btn_Get_Amount);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.txt_ID);
            this.panel7.Controls.Add(this.txt_Amount_On_Web);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 415);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(592, 63);
            this.panel7.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(224, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số BT";
            // 
            // txt_SoBT
            // 
            this.txt_SoBT.Location = new System.Drawing.Point(305, 7);
            this.txt_SoBT.Name = "txt_SoBT";
            this.txt_SoBT.Size = new System.Drawing.Size(100, 20);
            this.txt_SoBT.TabIndex = 5;
            // 
            // btn_Get_Amount
            // 
            this.btn_Get_Amount.Location = new System.Drawing.Point(305, 33);
            this.btn_Get_Amount.Name = "btn_Get_Amount";
            this.btn_Get_Amount.Size = new System.Drawing.Size(104, 23);
            this.btn_Get_Amount.TabIndex = 4;
            this.btn_Get_Amount.Text = "Lấy Số Lượng";
            this.btn_Get_Amount.UseVisualStyleBackColor = true;
            this.btn_Get_Amount.Click += new System.EventHandler(this.btn_Get_Amount_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(18, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "ID";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Số Lượng CT";
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(108, 33);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Size = new System.Drawing.Size(100, 20);
            this.txt_ID.TabIndex = 1;
            // 
            // txt_Amount_On_Web
            // 
            this.txt_Amount_On_Web.Location = new System.Drawing.Point(108, 7);
            this.txt_Amount_On_Web.Name = "txt_Amount_On_Web";
            this.txt_Amount_On_Web.Size = new System.Drawing.Size(100, 20);
            this.txt_Amount_On_Web.TabIndex = 0;
            // 
            // cbo_ListWeb
            // 
            this.cbo_ListWeb.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbo_ListWeb.FormattingEnabled = true;
            this.cbo_ListWeb.Location = new System.Drawing.Point(0, 0);
            this.cbo_ListWeb.Name = "cbo_ListWeb";
            this.cbo_ListWeb.Size = new System.Drawing.Size(592, 21);
            this.cbo_ListWeb.TabIndex = 1;
            this.cbo_ListWeb.SelectedIndexChanged += new System.EventHandler(this.cbo_ListWeb_SelectedIndexChanged);
            // 
            // Panel_Hide
            // 
            this.Panel_Hide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.Panel_Hide.Controls.Add(this.btn_Hide);
            this.Panel_Hide.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Hide.Location = new System.Drawing.Point(620, 0);
            this.Panel_Hide.Name = "Panel_Hide";
            this.Panel_Hide.Size = new System.Drawing.Size(37, 504);
            this.Panel_Hide.TabIndex = 3;
            // 
            // btn_Hide
            // 
            this.btn_Hide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Hide.ForeColor = System.Drawing.Color.White;
            this.btn_Hide.Location = new System.Drawing.Point(4, 4);
            this.btn_Hide.Name = "btn_Hide";
            this.btn_Hide.Size = new System.Drawing.Size(31, 29);
            this.btn_Hide.TabIndex = 0;
            this.btn_Hide.Text = ">>";
            this.btn_Hide.UseVisualStyleBackColor = false;
            this.btn_Hide.Click += new System.EventHandler(this.bnt_Hide_Click);
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.GV_ListOrder);
            this.Panel_Left.Controls.Add(this.panel5);
            this.Panel_Left.Controls.Add(this.panel6);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Left.Location = new System.Drawing.Point(0, 0);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(620, 504);
            this.Panel_Left.TabIndex = 4;
            // 
            // GV_ListOrder
            // 
            this.GV_ListOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_ListOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_ListOrder.Location = new System.Drawing.Point(0, 119);
            this.GV_ListOrder.Name = "GV_ListOrder";
            this.GV_ListOrder.Size = new System.Drawing.Size(620, 340);
            this.GV_ListOrder.TabIndex = 11;
            this.GV_ListOrder.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GV_ListOrder_RowEnter);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btn_Export);
            this.panel5.Controls.Add(this.btn_Run_One);
            this.panel5.Controls.Add(this.btn_Apply);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 459);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(620, 45);
            this.panel5.TabIndex = 10;
            this.panel5.SizeChanged += new System.EventHandler(this.panel5_SizeChanged);
            // 
            // btn_Export
            // 
            this.btn_Export.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Export.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.ForeColor = System.Drawing.Color.White;
            this.btn_Export.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Image")));
            this.btn_Export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Export.Location = new System.Drawing.Point(285, 6);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.Size = new System.Drawing.Size(97, 33);
            this.btn_Export.TabIndex = 8;
            this.btn_Export.Text = "Export";
            this.btn_Export.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Export.UseVisualStyleBackColor = false;
            this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
            // 
            // btn_Run_One
            // 
            this.btn_Run_One.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Run_One.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Run_One.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Run_One.ForeColor = System.Drawing.Color.White;
            this.btn_Run_One.Location = new System.Drawing.Point(132, 6);
            this.btn_Run_One.Name = "btn_Run_One";
            this.btn_Run_One.Size = new System.Drawing.Size(100, 32);
            this.btn_Run_One.TabIndex = 4;
            this.btn_Run_One.Text = "Run One";
            this.btn_Run_One.UseVisualStyleBackColor = false;
            this.btn_Run_One.Click += new System.EventHandler(this.btn_Run_One_Click);
            // 
            // btn_Apply
            // 
            this.btn_Apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Apply.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Apply.ForeColor = System.Drawing.Color.White;
            this.btn_Apply.Location = new System.Drawing.Point(12, 6);
            this.btn_Apply.Name = "btn_Apply";
            this.btn_Apply.Size = new System.Drawing.Size(100, 32);
            this.btn_Apply.TabIndex = 4;
            this.btn_Apply.Text = "Run All";
            this.btn_Apply.UseVisualStyleBackColor = false;
            this.btn_Apply.Click += new System.EventHandler(this.btn_Apply_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.groupBox2);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(620, 119);
            this.panel6.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_Update);
            this.groupBox2.Controls.Add(this.btn_View);
            this.groupBox2.Controls.Add(this.rdo_NotYet);
            this.groupBox2.Controls.Add(this.rdo_All);
            this.groupBox2.Controls.Add(this.rdo_Error);
            this.groupBox2.Controls.Add(this.rdo_Success);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(241, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(373, 102);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tìm kiếm";
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(121, 51);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(77, 29);
            this.btn_Update.TabIndex = 2;
            this.btn_Update.Text = "Save";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_View
            // 
            this.btn_View.Location = new System.Drawing.Point(23, 51);
            this.btn_View.Name = "btn_View";
            this.btn_View.Size = new System.Drawing.Size(77, 29);
            this.btn_View.TabIndex = 0;
            this.btn_View.Text = "View";
            this.btn_View.UseVisualStyleBackColor = true;
            this.btn_View.Click += new System.EventHandler(this.btn_View_Click);
            // 
            // rdo_NotYet
            // 
            this.rdo_NotYet.AutoSize = true;
            this.rdo_NotYet.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_NotYet.Location = new System.Drawing.Point(250, 20);
            this.rdo_NotYet.Name = "rdo_NotYet";
            this.rdo_NotYet.Size = new System.Drawing.Size(56, 19);
            this.rdo_NotYet.TabIndex = 0;
            this.rdo_NotYet.Text = "Chưa";
            this.rdo_NotYet.UseVisualStyleBackColor = true;
            // 
            // rdo_All
            // 
            this.rdo_All.AutoSize = true;
            this.rdo_All.Checked = true;
            this.rdo_All.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_All.Location = new System.Drawing.Point(23, 21);
            this.rdo_All.Name = "rdo_All";
            this.rdo_All.Size = new System.Drawing.Size(58, 19);
            this.rdo_All.TabIndex = 0;
            this.rdo_All.TabStop = true;
            this.rdo_All.Text = "Tất cả";
            this.rdo_All.UseVisualStyleBackColor = true;
            // 
            // rdo_Error
            // 
            this.rdo_Error.AutoSize = true;
            this.rdo_Error.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_Error.Location = new System.Drawing.Point(192, 20);
            this.rdo_Error.Name = "rdo_Error";
            this.rdo_Error.Size = new System.Drawing.Size(42, 19);
            this.rdo_Error.TabIndex = 0;
            this.rdo_Error.Text = "Lỗi";
            this.rdo_Error.UseVisualStyleBackColor = true;
            // 
            // rdo_Success
            // 
            this.rdo_Success.AutoSize = true;
            this.rdo_Success.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_Success.Location = new System.Drawing.Point(87, 20);
            this.rdo_Success.Name = "rdo_Success";
            this.rdo_Success.Size = new System.Drawing.Size(87, 19);
            this.rdo_Success.TabIndex = 0;
            this.rdo_Success.Text = "Đã lên web";
            this.rdo_Success.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbl_Record_Error);
            this.groupBox1.Controls.Add(this.lbl_Total_Record);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbl_Record_Success);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 103);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.Location = new System.Drawing.Point(15, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tổng Record";
            // 
            // lbl_Record_Error
            // 
            this.lbl_Record_Error.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Record_Error.Location = new System.Drawing.Point(148, 60);
            this.lbl_Record_Error.Name = "lbl_Record_Error";
            this.lbl_Record_Error.Size = new System.Drawing.Size(40, 13);
            this.lbl_Record_Error.TabIndex = 0;
            this.lbl_Record_Error.Text = "0";
            this.lbl_Record_Error.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_Total_Record
            // 
            this.lbl_Total_Record.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Total_Record.Location = new System.Drawing.Point(148, 21);
            this.lbl_Total_Record.Name = "lbl_Total_Record";
            this.lbl_Total_Record.Size = new System.Drawing.Size(40, 13);
            this.lbl_Total_Record.TabIndex = 0;
            this.lbl_Total_Record.Text = "0";
            this.lbl_Total_Record.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.Location = new System.Drawing.Point(15, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số Lượng Thành Công";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.Location = new System.Drawing.Point(15, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số Lượng Lên Web";
            // 
            // lbl_Record_Success
            // 
            this.lbl_Record_Success.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Record_Success.Location = new System.Drawing.Point(148, 41);
            this.lbl_Record_Success.Name = "lbl_Record_Success";
            this.lbl_Record_Success.Size = new System.Drawing.Size(40, 13);
            this.lbl_Record_Success.TabIndex = 0;
            this.lbl_Record_Success.Text = "0";
            this.lbl_Record_Success.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 144);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 15);
            this.label17.TabIndex = 2;
            this.label17.Text = "DVSDNS";
            // 
            // txt_DVSDNS_Name
            // 
            this.txt_DVSDNS_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DVSDNS_Name.Location = new System.Drawing.Point(205, 141);
            this.txt_DVSDNS_Name.Name = "txt_DVSDNS_Name";
            this.txt_DVSDNS_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_DVSDNS_Name.TabIndex = 3;
            // 
            // txt_DVSDNS_ID
            // 
            this.txt_DVSDNS_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DVSDNS_ID.Location = new System.Drawing.Point(110, 141);
            this.txt_DVSDNS_ID.Name = "txt_DVSDNS_ID";
            this.txt_DVSDNS_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_DVSDNS_ID.TabIndex = 4;
            // 
            // FrmOrderUpToWeb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 504);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Panel_Hide);
            this.Controls.Add(this.Panel_Right);
            this.Name = "FrmOrderUpToWeb";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Upload CT To Web";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmOrderUpToWeb_Load);
            this.Panel_Right.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_DataDetail)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.Panel_Hide.ResumeLayout(false);
            this.Panel_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_ListOrder)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListBox LB_Web_Log;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox cbo_ListWeb;
        private System.Windows.Forms.Panel Panel_Hide;
        private System.Windows.Forms.Button btn_Hide;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.DataGridView GV_ListOrder;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btn_Run_One;
        private System.Windows.Forms.Button btn_Apply;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_View;
        private System.Windows.Forms.RadioButton rdo_NotYet;
        private System.Windows.Forms.RadioButton rdo_All;
        private System.Windows.Forms.RadioButton rdo_Success;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Record_Error;
        private System.Windows.Forms.Label lbl_Total_Record;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Record_Success;
        private System.Windows.Forms.RadioButton rdo_Error;
        private System.Windows.Forms.Button btn_Get_Amount;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_ID;
        private System.Windows.Forms.TextBox txt_Amount_On_Web;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_SoBT;
        private System.Windows.Forms.Button btn_Export;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.DataGridView GV_DataDetail;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_LHXNK_Name;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox txt_LoaiThue_Name;
        private System.Windows.Forms.TextBox txt_CQQLThu_Name;
        private System.Windows.Forms.TextBox txt_DBHC_Name;
        private System.Windows.Forms.TextBox txt_LHXNK;
        private System.Windows.Forms.TextBox txt_TK_Name;
        private System.Windows.Forms.TextBox txt_NgayDk;
        private System.Windows.Forms.TextBox txt_NguoiNT;
        private System.Windows.Forms.TextBox txt_ToKhaiSo;
        private System.Windows.Forms.TextBox txt_NH_PhucVu_Name;
        private System.Windows.Forms.TextBox txt_LoaiThue;
        private System.Windows.Forms.TextBox txt_CQQLThu_ID;
        private System.Windows.Forms.TextBox txt_DBHC_ID;
        private System.Windows.Forms.TextBox txt_TK_Thu_NS;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox txt_MaSoThue;
        private System.Windows.Forms.TextBox txt_NgayNT;
        private System.Windows.Forms.TextBox txt_NH_PhucVu;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbo_KieuThu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_NHNN_Name;
        private System.Windows.Forms.TextBox txt_NHNN_ID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txt_So_CT_Web;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TextBox txt_SoChungTu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_DVSDNS_Name;
        private System.Windows.Forms.TextBox txt_DVSDNS_ID;
    }
}