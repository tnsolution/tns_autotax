﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;

namespace HHT_AutoTax
{
    public partial class Frm_ImportDataCheck : Form
    {
        public Frm_ImportDataCheck()
        {
            InitializeComponent();
        }

        private void Frm_ImportDataCheck_Load(object sender, EventArgs e)
        {
            InitListView_Excel(LV_Data_Web);
            InitListView_ItemMutil(LV_ItemMutil);
            InitLV_Report(LV_Report);
            InitListView_IPCAS_(LV_IPCAS);
            InitListView_Excel_Copy(LV_Data_Web_Copy);
            InitListView_IPCAS_Copy(LV_IPCAS_Copy);
            InitListView_Excel_Fail(LV_FailWeb);
            InitListView_IPCAS_Fal(LV_FailIPCAS);
            InitListView_Fail(LV_FailConLai);
        }
        private void btn_ImportAgriTax_Click(object sender, EventArgs e)
        {
            Frm_ImportFileWeb frm = new Frm_ImportFileWeb();
            frm.ShowDialog();
        }
        private void InitLV_Report(ListView LV)
        {
            ColumnHeader colHead;

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "IPCAS";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);



            colHead = new ColumnHeader();
            colHead.Text = "AgriTax";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Kết Quả";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phân Tích";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Số Lượng AuTo";
            //colHead.Width = 120;
            //colHead.TextAlign = HorizontalAlignment.Center;
            //LV.Columns.Add(colHead);

            for (int i = 0; i < 50; i++)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Text = "...";

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);
            }
        }
        private int _LePhiHQ_Excel = 0;
        private int _LePhiHQ_SQL = 0;
        private int _LePhiHQ_IPCAS = 0;
        private int _LoaiThue_01_SQL = 0;
        private int _LoaiThue_04_SQL = 0;
        private int _AmountAuto = 0;
        private int _AmountAuto_IPICAS = 0;
        private int _AmountIPCAS = 0;
        private int _Amount_zSCT = 0;
        private double _MoneyIPCAS = 0;

        private void InitListView_Excel(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadDataToListView_Excel(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data_Web;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            _LePhiHQ_Excel = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MaSoThue"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoCT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zMoney = double.Parse(nRow["TongTien"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NgayKB"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                string zTNNSNN = nRow["TKNSNN"].ToString().Trim();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTNNSNN;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MaCQT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LoaiThue"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoTK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NgayTK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LHXNK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TenCTy"].ToString().Trim().ToUpper();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MANV"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);

                if (zMoney % 20000 == 0 && zTNNSNN == "3511")
                {
                    _LePhiHQ_Excel++;
                }

            }



            this.Cursor = Cursors.Default;
        }

        private void InitListView_Excel_Copy(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void AddToListView(ListView LV, ListViewItem Item_Copy)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Text = (LV.Items.Count + 1).ToString();
            for (int i = 1; i < Item_Copy.SubItems.Count; i++)
            {
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = Item_Copy.SubItems[i].Text;
                lvi.SubItems.Add(lvsi);
            }

            LV.Items.Add(lvi);

        }

        private void InitListView_Excel_Fail(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitListView_IPCAS_Fal(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "tomgntno";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "cnclflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "cncltrflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Status";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }

        private void InitListView_Fail(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void InitListView_ItemMutil(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadDataToListView_ItemMutil(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_ItemMutil;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRowParent = In_Table.Rows[i];
                DataTable zListOrder = CheckOrder_Data.OrderOnWeb_ItemMutil_Detail(
                    nRowParent["NgayKB"].ToString(),
                    nRowParent["SoTK"].ToString(),
                    nRowParent["MaSoThue"].ToString(),
                    double.Parse(nRowParent["TongTien"].ToString())
                    );
                foreach (DataRow nRow in zListOrder.Rows)
                {
                    lvi = new ListViewItem();
                    lvi.Text = (i + 1).ToString();

                    lvi.ForeColor = Color.DarkBlue;
                    lvi.BackColor = Color.White;

                    lvi.ImageIndex = 0;

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["MANV"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["SoCT"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    double zMoney = double.Parse(nRow["TongTien"].ToString().Trim());
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zMoney.ToString("#,###,###");
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["NgayKB"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    string zTNNSNN = nRow["TKNSNN"].ToString().Trim();
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zTNNSNN;
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["MaCQT"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["LoaiThue"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["SoTK"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["NgayTK"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["LHXNK"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["MaSoThue"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["TenCTy"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    LV.Items.Add(lvi);
                }
                lvi = new ListViewItem();
                lvi.Text = "";

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }



            this.Cursor = Cursors.Default;
        }

        private void InitListView_IPCAS(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void LoadDataToListView_IPCAS(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_IPCAS;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            _AmountIPCAS = n;
            _LePhiHQ_IPCAS = 0;
            _MoneyIPCAS = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["So_CT"].ToString().Trim();
                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Ma_ST"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["So_CT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zMoney = double.Parse(nRow["trccyamt"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                //  DateTime zTrdate = (DateTime)nRow["trdt"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["trdt"].ToString();
                lvi.SubItems.Add(lvsi);

                string zTNNSNN = nRow["TK_ThuNS"].ToString().Trim();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTNNSNN;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CQThuID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                string zLoaiThue = nRow["LoaiThueID"].ToString().Trim();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zLoaiThue;
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ToKhai"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NgayDK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LH_ID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                string zTenCty = nRow["TenCongTy"].ToString().Trim().ToUpper();
                if (zTenCty.Length == 0)
                {
                    zTenCty = nRow["ordcust"].ToString().Trim().ToUpper();
                }
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTenCty;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["So_BT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

                if (zMoney % 20000 == 0)
                {
                    _LePhiHQ_IPCAS++;
                }
                _MoneyIPCAS += zMoney;

                if (nRow["So_CT"].ToString().Trim().Length > 0)
                    _AmountAuto_IPICAS++;
            }



            this.Cursor = Cursors.Default;
        }
        private void InitListView_IPCAS_(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "tomgntno";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "cnclflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "cncltrflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Status";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }
        private void InitListView_IPCAS_Copy(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "tomgntno";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "cnclflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "cncltrflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Status";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }

        private void LoadDataToListView_IPCAS_(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_IPCAS;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            _AmountIPCAS = n;
            _LePhiHQ_IPCAS = 0;
            _MoneyIPCAS = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["Rem"].ToString().Trim();
                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["tomgntno"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zMoney = double.Parse(nRow["trccyamt"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                //  DateTime zTrdate = (DateTime)nRow["trdt"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["trdt"].ToString();
                lvi.SubItems.Add(lvsi);

                string zTNNSNN = nRow["trcdnm"].ToString().Trim();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTNNSNN;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["cnclflg"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["cncltrflg"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);

                if (zMoney % 20000 == 0)
                {
                    _LePhiHQ_IPCAS++;
                }
                _MoneyIPCAS += zMoney;

                //if (nRow["So_CT"].ToString().Trim().Length > 0)
                //    _AmountAuto_IPICAS++;
            }



            this.Cursor = Cursors.Default;
        }
        private void InnitListView_Message(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Thông Báo";
            colHead.Width = 480;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhân Viên";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }

        private void DayForOrder_DateSelected(object sender, DateRangeEventArgs e)
        {
            DateTime zDate = DayForOrder.SelectionStart;
            DataTable zTable = CheckOrder_Data.OrderOnWeb(zDate);
            LoadDataToListView_Excel(zTable);

            //zTable = CheckOrder_Data.IPCAS(zDate);
            //LoadDataToListView_IPCAS(zTable);

            zTable = CheckOrder_Data.IPCAS_(zDate);
            LoadDataToListView_IPCAS_(zTable);

            zTable = CheckOrder_Data.OrderOnWeb_ItemMutil(zDate);
            LoadDataToListView_ItemMutil(zTable);


            FillDataCash();
            FillData_6280ITL();
            FillRule();
            Fill_LCT();
            //-------------------------
            double zOrderOnSQL_TotalRecord = CheckOrder_Data.OrderOnSQL_TotalRecord(zDate);
            double zOrderOnWeb_ToTalReport = CheckOrder_Data.OrderOnWeb_TotalRecord(zDate);
            double zOrderOnSQL_TotablMoney = CheckOrder_Data.OrderOnWeb_TotalMoney(zDate);

            LV_Report.Items[0].SubItems[0].Text = "LỆNH CASH";
            LV_Report.Items[1].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[2].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[3].SubItems[0].Text = "LỆNH CT";
            LV_Report.Items[4].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[5].SubItems[0].Text = "      Tổng Tiền";


            LV_Report.Items[6].SubItems[0].Text = "LỆNH KHÁC";
            LV_Report.Items[7].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[8].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[9].SubItems[0].Text = "TỔNG RECORD";
            LV_Report.Items[10].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[11].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[12].SubItems[0].Text = "LỆNH KHÔNG SỬ DỤNG";
            LV_Report.Items[13].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[14].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[15].SubItems[0].Text = "LỆNH KHÔNG XÁC ĐỊNH";
            LV_Report.Items[16].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[17].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[18].SubItems[0].Text = "TỔNG";
            LV_Report.Items[19].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[20].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[21].SubItems[0].Text = "CHÊNH LỆCH";
            LV_Report.Items[22].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[23].SubItems[0].Text = "      Tổng Tiền";

            LV_Report.Items[24].SubItems[0].Text = "PHÂN TÍCH";
            LV_Report.Items[25].SubItems[0].Text = "      Số lệnh";
            LV_Report.Items[26].SubItems[0].Text = "      Tổng Tiền";
            //--Columns IPCAS
            //----CASH
            LV_Report.Items[1].SubItems[1].Text = _CashAmount.ToString("#,###,###");
            LV_Report.Items[2].SubItems[1].Text = _CashToTal.ToString("#,###,###");
            //----LỆNH CT
            LV_Report.Items[4].SubItems[1].Text = _CashAmount_63280ITL.ToString("#,###,###");
            LV_Report.Items[5].SubItems[1].Text = _CashToTal_6280ITL.ToString("#,###,###");
            //----LỆNH KHÁC
            LV_Report.Items[7].SubItems[1].Text = "";
            LV_Report.Items[8].SubItems[1].Text = "";
            //----TỔNG
            LV_Report.Items[10].SubItems[1].Text = (_CashAmount + _CashAmount_63280ITL).ToString("#,###,###");
            LV_Report.Items[11].SubItems[1].Text = (_CashToTal + _CashToTal_6280ITL).ToString("#,###,###");
            //----LỆNH 99
            LV_Report.Items[13].SubItems[1].Text = _99Amount.ToString("#,###,###");
            LV_Report.Items[14].SubItems[1].Text = _99Money.ToString("#,###,###");
            //----LỆNH KHÔNG XÁC ĐỊNH
            LV_Report.Items[16].SubItems[1].Text = _FailAmount.ToString("#,###,###");
            LV_Report.Items[17].SubItems[1].Text = _FailMoney.ToString("#,###,###");
            //----TỔNG COLUMNS 
            LV_Report.Items[19].SubItems[1].Text = _AmountIPCAS.ToString("#,###,###");
            LV_Report.Items[20].SubItems[1].Text = _MoneyIPCAS.ToString("#,###,###");
            //----CHÊNH LỆCH
            int zAmountChenhLech = _AmountIPCAS - (_CashAmount + _CashAmount_63280ITL + _FailAmount + _99Amount);
            double zMoneyChechLech = _MoneyIPCAS - (_CashToTal + _CashToTal_6280ITL + _99Money + _FailMoney);
            LV_Report.Items[22].SubItems[1].Text = zAmountChenhLech.ToString("#,###,##0");
            LV_Report.Items[23].SubItems[1].Text = zMoneyChechLech.ToString("#,###,##0");

            //----WEB ARGAX
            //----TỔNG 
            LV_Report.Items[10].SubItems[2].Text = zOrderOnWeb_ToTalReport.ToString("#,###,###");
            LV_Report.Items[11].SubItems[2].Text = zOrderOnSQL_TotablMoney.ToString("#,###,###");
            //----KẾT QUẢ 
            LV_Report.Items[10].SubItems[3].Text = ((_CashAmount + _CashAmount_63280ITL) - zOrderOnWeb_ToTalReport).ToString("#,###,###");
            LV_Report.Items[11].SubItems[3].Text = ((_CashToTal + _CashToTal_6280ITL) - zOrderOnSQL_TotablMoney).ToString("#,###,###");

            //
            //
            //
            //     
            //
            //LV_Report.Items[7].SubItems[4].Text = (zOrderOnWeb_ToTalReport - (_CashAmount + _CashAmount_63280ITL)).ToString("#,###,###");
            //
            //LV_Report.Items[8].SubItems[2].Text = zOrderOnSQL_TotablMoney.ToString("#,###,###");
            //LV_Report.Items[8].SubItems[3].Text = _99Money.ToString("#,###,###");
            //LV_Report.Items[8].SubItems[4].Text = (zOrderOnSQL_TotablMoney - (_CashToTal + _CashToTal_6280ITL)).ToString("#,###,###");
            //LV_Report.Items[13].SubItems[3].Text = _FailAmount.ToString("#,###,###");
            //
            //

        }

        private void Frm_ImportDataCheck_SizeChanged(object sender, EventArgs e)
        {
            LV_Data_Web.Width = this.Width / 2 - panel1.Width / 2;
            LV_Data_Web_Copy.Width = this.Width / 2 - panel1.Width / 2;
            LV_FailWeb.Width = this.Width / 2 - panel1.Width / 2;
        }

        private int _IndexFound = 0;
        private void LV_Data_Web_ItemActivate(object sender, EventArgs e)
        {
            string zSoCT_Web = LV_Data_Web.SelectedItems[0].SubItems[2].Text.Trim();
            string zSoCT_SQL = "";
            LV_IPCAS.Items[_IndexFound].BackColor = Color.White;
            LV_IPCAS.Items[_IndexFound].ForeColor = Color.DarkBlue;

            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                zSoCT_SQL = LV_IPCAS.Items[i].SubItems[2].Text.Trim();

                if (zSoCT_SQL == zSoCT_Web)
                {
                    LV_IPCAS.Items[i].Selected = true;
                    LV_IPCAS.Items[i].BackColor = Color.FromArgb(0, 120, 215);
                    LV_IPCAS.Items[i].ForeColor = Color.White;
                    LV_IPCAS.EnsureVisible(i);
                    _IndexFound = i;
                    break;
                }
            }

        }
        //-------------------------------------------
        private int _IndexBegin = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            string zSoCT_Web = LV_Data_Web.Items[_IndexBegin].SubItems[2].Text.Trim();
            string zNV = "";
            string zSoCT_SQL = "";
            bool zIsFound = false;
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                zSoCT_SQL = LV_IPCAS.Items[i].SubItems[2].Text.Trim();
                if (zSoCT_SQL == zSoCT_Web)
                {
                    LV_IPCAS.Items[i].SubItems[0].Text = _IndexBegin.ToString();
                    string zMessage = "";
                    LV_Data_Web.Items[_IndexBegin].SubItems[13].Text = "1";
                    LV_IPCAS.Items[i].SubItems[10].Text = "1";
                    bool zCheckFail = false;
                    Order_Check zOrderCheck = new Order_Check(LV_IPCAS.Items[i].SubItems[2].Text.Trim());

                    #region [Kiem tra 2]
                    if (LV_Data_Web.Items[_IndexBegin].SubItems[1].Text != zOrderCheck.Ma_ST)
                    {
                        if (zOrderCheck.Ma_ST.Length == 13)
                        {
                            string zMST = zOrderCheck.Ma_ST.Substring(0, 10) + "-" + zOrderCheck.Ma_ST.Substring(10, 3);
                            if (zMST != LV_Data_Web.Items[_IndexBegin].SubItems[1].Text)
                            {
                                zCheckFail = true;
                            }
                        }
                        else
                        {
                            if (LV_Data_Web.Items[_IndexBegin].SubItems[1].Text != zOrderCheck.Ma_ST)
                            {
                                zCheckFail = true;
                            }
                        }
                    }
                    if (LV_Data_Web.Items[_IndexBegin].SubItems[3].Text != zOrderCheck.SoTien.ToString("#,###,###"))
                    {
                        zCheckFail = true;
                    }
                    if (LV_Data_Web.Items[_IndexBegin].SubItems[5].Text != zOrderCheck.TK_ThuNS)
                    {
                        zCheckFail = true;
                    }
                    if (LV_Data_Web.Items[_IndexBegin].SubItems[6].Text != zOrderCheck.CQThuID)
                    {
                        zCheckFail = true;
                    }
                    if (LV_Data_Web.Items[_IndexBegin].SubItems[8].Text != zOrderCheck.ToKhai)
                    {
                        zCheckFail = true;
                    }
                    if (LV_Data_Web.Items[_IndexBegin].SubItems[10].Text != zOrderCheck.LH_ID)
                    {
                        zCheckFail = true;
                    }
                    if (zCheckFail == true)
                    {
                        Add_Items_Fail(LV_FailWeb, LV_Data_Web.Items[_IndexBegin]);
                        Add_Items_Fail(LV_FailIPCAS, LV_IPCAS.Items[i]);
                    }
                    #endregion

                    #region [1] - NHẬP SẴN
                    //else if (LV_IPCAS.Items[i].SubItems[9].Text == "1")
                    //{
                    //    if (LV_Data_Web.Items[_IndexBegin].SubItems[1].Text != LV_IPCAS.Items[i].SubItems[1].Text)
                    //    {
                    //        if (LV_IPCAS.Items[i].SubItems[1].Text.Length == 13)
                    //        {
                    //            string zMST1 = LV_IPCAS.Items[i].SubItems[1].Text.Substring(0, 10) + "-" + LV_IPCAS.Items[i].SubItems[1].Text.Substring(10, 3);
                    //            if (zMST1 != LV_Data_Web.Items[_IndexBegin].SubItems[1].Text)
                    //            {
                    //                zCheckFail = true;
                    //            }
                    //        }
                    //    }

                    //    if (LV_Data_Web.Items[_IndexBegin].SubItems[3].Text != LV_IPCAS.Items[i].SubItems[3].Text)
                    //    {
                    //        zCheckFail = true;
                    //    }


                    //    if (zCheckFail == true)
                    //    {
                    //        Add_Items_Fail(LV_FailWeb, LV_Data_Web.Items[_IndexBegin]);
                    //        Add_Items_Fail(LV_FailIPCAS, LV_IPCAS.Items[i]);
                    //    }
                    //}
                    #endregion
                    int zAmount = int.Parse(lbl_AmountCheck.Text);
                    zAmount++;

                    lbl_AmountCheck.Text = zAmount.ToString("#,###");
                    zIsFound = true;
                    break;
                }
            }
            if (!zIsFound)
            {
                //string zMaST_Web = LV_Data_Web.Items[_IndexBegin].SubItems[1].Text.Trim();
                //string zMoney_Web = LV_Data_Web.Items[_IndexBegin].SubItems[3].Text.Trim();
                //for (int i = 0; i < LV_IPCAS.Items.Count; i++)
                //{
                //    string zMaST = LV_IPCAS.Items[i].SubItems[1].Text.Trim();
                //    string zMoney = LV_IPCAS.Items[i].SubItems[3].Text.Trim();

                //    if (zMaST_Web == zMaST && zMoney_Web == zMoney)
                //    {
                //        LV_IPCAS.Items[i].BackColor = Color.Green;
                //        LV_IPCAS.Items[i].ForeColor = Color.White;
                //        LV_IPCAS.Items[i].SubItems[0].Text = _IndexBegin.ToString();
                //        LV_Data_Web.Items[_IndexBegin].BackColor = Color.Green;
                //        LV_Data_Web.Items[_IndexBegin].ForeColor = Color.White;

                //        int zAmount = int.Parse(lbl_AmountCheck.Text);
                //        zAmount++;
                //        lbl_AmountCheck.Text = zAmount.ToString("#,###");
                //        zIsFound = true;
                //        break;
                //    }
                //}
            }

            _IndexBegin++;
            if (_IndexBegin < LV_Data_Web.Items.Count)
                timer1.Start();
            else
                MessageBox.Show("Done");

        }

        private void Add_Items_Fail(ListView LV, ListViewItem Item_Fail)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Text = (LV.Items.Count + 1).ToString();
            for (int i = 1; i < Item_Fail.SubItems.Count; i++)
            {
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = Item_Fail.SubItems[i].Text;
                lvi.SubItems.Add(lvsi);
            }

            LV.Items.Add(lvi);
        }

        private void btn_BeginCheck_Click(object sender, EventArgs e)
        {
            lbl_AmountCheck.Text = "0";
            timer1.Start();


        }

        private void btn_ImportIPICAS_Click(object sender, EventArgs e)
        {
            Frm_ImportIPICAS frm = new Frm_ImportIPICAS();
            frm.ShowDialog();
        }
        private int _CashAmount = 0;
        private double _CashToTal = 0;
        private int _CashAmount_63280ITL = 0;
        private double _CashToTal_6280ITL = 0;

        #region [FillData]
        private void FillDataCash()
        {
            _CashAmount = 0;
            _CashToTal = 0;
            string zSoCT_IPCAST;
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                zSoCT_IPCAST = LV_IPCAS.Items[i].SubItems[2].Text.Trim();
                if (zSoCT_IPCAST.Length == 0)
                {
                    ArrayList zList = MST_SoCT(LV_IPCAS.Items[i].Tag.ToString());
                    if (zList.Count == 2)
                    {

                        LV_IPCAS.Items[i].SubItems[1].Text = zList[0].ToString();
                        LV_IPCAS.Items[i].SubItems[2].Text = zList[1].ToString();
                        LV_IPCAS.Items[i].SubItems[9].Text = "1";
                        LV_IPCAS.Items[i].ForeColor = Color.Green;
                        _CashAmount++;
                        _CashToTal += double.Parse(LV_IPCAS.Items[i].SubItems[4].Text);
                    }
                }

            }
        }
        private void FillData_6280ITL()
        {
            _CashAmount_63280ITL = 0;
            _CashToTal_6280ITL = 0;
            string z6280ITL;
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                if (LV_IPCAS.Items[i].SubItems[3].Text.Length >= 7)
                {
                    z6280ITL = LV_IPCAS.Items[i].SubItems[3].Text.Trim().Substring(0, 7);

                    if (z6280ITL == "6280ITL")
                    {
                        LV_IPCAS.Items[i].SubItems[9].Text = "2";
                        _CashAmount_63280ITL++;
                        _CashToTal_6280ITL += double.Parse(LV_IPCAS.Items[i].SubItems[4].Text);
                    }
                }

            }
        }
        private int _FailAmount = 0;
        private double _FailMoney = 0;
        private int _99Amount = 0;
        private double _99Money = 0;
        private void FillRule()
        {
            _99Amount = 0;
            _99Money = 0;
            _FailAmount = 0;
            _FailMoney = 0;
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                if (LV_IPCAS.Items[i].SubItems[7].Text == "1" || LV_IPCAS.Items[i].SubItems[8].Text == "1")
                {
                    LV_IPCAS.Items[i].SubItems[9].Text = "99";
                }
                else
                {
                    if (LV_IPCAS.Items[i].SubItems[6].Text.Trim().ToUpper() == "THANH TOÁN SONG PHƯƠNG KBNN")
                    {
                        LV_IPCAS.Items[i].SubItems[9].Text = "99";
                    }
                    else
                    {
                        if (LV_IPCAS.Items[i].SubItems[6].Text.Trim().ToUpper() == "INWARD CREDIT CUSTOMER A/C")
                        {
                            if (LV_IPCAS.Items[i].SubItems[3].Text.Trim().Substring(0, 7) == "6280ITT")
                            {
                                LV_IPCAS.Items[i].SubItems[9].Text = "99";
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                string aa = LV_IPCAS.Items[i].SubItems[9].Text;
                if (LV_IPCAS.Items[i].SubItems[9].Text.Trim().Length == 0)
                {
                    _FailAmount++;
                    _FailMoney += double.Parse(LV_IPCAS.Items[i].SubItems[4].Text);
                }
                if (LV_IPCAS.Items[i].SubItems[9].Text == "99")
                {
                    _99Amount++;
                    _99Money += double.Parse(LV_IPCAS.Items[i].SubItems[4].Text);

                }
            }

        }

        private void Fill_LCT()
        {
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                string ztrref = LV_IPCAS.Items[i].SubItems[3].Text;
                string zstatus = LV_IPCAS.Items[i].SubItems[9].Text;
                if (zstatus == "2")
                {
                    Order_Info zOrderInfo = new Order_Info(ztrref);
                    LV_IPCAS.Items[i].SubItems[1].Text = zOrderInfo.MST;
                    LV_IPCAS.Items[i].SubItems[2].Text = zOrderInfo.So_CT;
                    LV_IPCAS.Items[i].Tag = zOrderInfo;
                }

            }
        }
        private int _AmountCheck = 0;
        private double _MoneyCheck = 0;
        private int _AmountWebFinish = 0;
        private double _MoneyWebFinish = 0;
        private void FillCheck()
        {
            _AmountWebFinish = 0;
            _MoneyWebFinish = 0;
            _AmountCheck = 0;
            int j = 0;
            bool zFound = false;
            for (int i = 0; i < LV_Data_Web_Copy.Items.Count; i++)
            {
                if (j < LV_IPCAS_Copy.Items.Count)
                {
                    string zMoneyWeb = LV_Data_Web_Copy.Items[i].SubItems[3].Text.ToString();
                    string zMoneyIPCAS = LV_IPCAS_Copy.Items[j].SubItems[4].Text.ToString();
                    if (zMoneyWeb == zMoneyIPCAS)
                    {
                        j++;
                    }
                    else
                    {
                        zFound = false;

                        while (!zFound && j < LV_IPCAS_Copy.Items.Count)
                        {
                            LV_IPCAS_Copy.Items[j].BackColor = Color.Red;
                            LV_IPCAS_Copy.Items[j].ForeColor = Color.White;
                            AddToListView(LV_FailConLai, LV_IPCAS_Copy.Items[j]);
                            _AmountCheck++;
                            _MoneyCheck += double.Parse(LV_IPCAS_Copy.Items[j].SubItems[4].Text);
                            j++;
                            zMoneyIPCAS = LV_IPCAS_Copy.Items[j].SubItems[4].Text.ToString();
                            if (zMoneyWeb == zMoneyIPCAS)
                            {
                                zFound = true;
                                j++;
                            }
                        }
                    }
                }
                else
                {
                    LV_Data_Web_Copy.Items[i].BackColor = Color.Red;
                    LV_Data_Web_Copy.Items[i].ForeColor = Color.White;
                    _AmountWebFinish++;
                    _MoneyWebFinish += double.Parse(LV_Data_Web_Copy.Items[i].SubItems[3].Text);
                }
            }

            for (int k = j; k < LV_IPCAS_Copy.Items.Count; k++)
            {
                LV_IPCAS_Copy.Items[j].BackColor = Color.Red;
                LV_IPCAS_Copy.Items[j].ForeColor = Color.White;
                AddToListView(LV_FailConLai, LV_IPCAS_Copy.Items[k]);
                _AmountCheck++;
                _MoneyCheck += double.Parse(LV_IPCAS_Copy.Items[k].SubItems[4].Text);
            }

            LV_Report.Items[25].SubItems[3].Text = _AmountWebFinish.ToString("#,###,###");
            LV_Report.Items[26].SubItems[3].Text = _MoneyWebFinish.ToString("#,###,###");

            LV_Report.Items[25].SubItems[4].Text = _AmountCheck.ToString("#,###,###");
            LV_Report.Items[26].SubItems[4].Text = _MoneyCheck.ToString("#,###,###");
        }
        #endregion

        private ArrayList MST_SoCT(string Rem)
        {
            ArrayList zResult = new ArrayList();
            string zCode1 = "MST:";
            string zCode2 = "-SO CT:";
            int zIndexMST = Rem.IndexOf(zCode1);
            int zIndexSoCT = Rem.IndexOf(zCode2);
            if (zIndexMST > 0 && zIndexSoCT > 0)
            {
                string zMST = Rem.Substring(zIndexMST + zCode1.Length, zIndexSoCT - (zIndexMST + zCode1.Length)).Trim();
                string zSoCT = Rem.Substring(zIndexSoCT + zCode2.Length, Rem.Length - (zIndexSoCT + zCode2.Length + 1)).Trim();

                if (zMST.Length == 10 || zMST.Length == 13)
                    zResult.Add(zMST);
                if (zSoCT.Length == 7)
                    zResult.Add(zSoCT);
            }
            return zResult;
        }
        private int _PhanTichPCASAmount = 0;
        private double _PhanTichPCASMoney = 0;
        private int _PhanTichWEBAmount = 0;
        private double _PhanTichWEBMoney = 0;
        private void btn_Copy_Click(object sender, EventArgs e)
        {
            _PhanTichPCASAmount = 0;
            _PhanTichPCASMoney = 0;
            _PhanTichWEBAmount = 0;
            _PhanTichWEBMoney = 0;
            for (int i = 0; i < LV_Data_Web.Items.Count; i++)
            {
                if (LV_Data_Web.Items[i].SubItems[13].Text.Trim().Length == 0)
                {
                    AddToListView(LV_Data_Web_Copy, LV_Data_Web.Items[i]);
                    _PhanTichPCASAmount++;
                    _PhanTichPCASMoney += double.Parse(LV_Data_Web.Items[i].SubItems[3].Text);
                }
            }
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                if (LV_IPCAS.Items[i].SubItems[10].Text.Trim().Length == 0 && LV_IPCAS.Items[i].SubItems[9].Text != "99")
                {
                    AddToListView(LV_IPCAS_Copy, LV_IPCAS.Items[i]);
                    _PhanTichWEBAmount++;
                    _PhanTichWEBMoney += double.Parse(LV_IPCAS.Items[i].SubItems[4].Text);
                }
            }
            //----PHÂN TÍCH IPCAS
            LV_Report.Items[25].SubItems[2].Text = _PhanTichPCASAmount.ToString("#,###,###");
            LV_Report.Items[26].SubItems[2].Text = _PhanTichPCASMoney.ToString("#,###,###");
            //----PHÂN TÍCH WEB
            LV_Report.Items[25].SubItems[1].Text = _PhanTichWEBAmount.ToString("#,###,###");
            LV_Report.Items[26].SubItems[1].Text = _PhanTichWEBMoney.ToString("#,###,###");
            MessageBox.Show("Done");

        }

        private void btn_FillCheck_Click(object sender, EventArgs e)
        {
            FillCheck();

            MessageBox.Show("Done");
        }
    }
}
