﻿namespace HHT_AutoTax
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.btn_OrderAnalysis = new System.Windows.Forms.Button();
            this.lbl_version = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_TKThuNS = new System.Windows.Forms.Button();
            this.btn_Rule_Group = new System.Windows.Forms.Button();
            this.btn_Config = new System.Windows.Forms.Button();
            this.btn_Audit = new System.Windows.Forms.Button();
            this.btn_ImportDataWeb = new System.Windows.Forms.Button();
            this.btn_SetTimer = new System.Windows.Forms.Button();
            this.btn_CheckCT = new System.Windows.Forms.Button();
            this.btn_OrderTranfer = new System.Windows.Forms.Button();
            this.Panel_Left.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_Left
            // 
            this.Panel_Left.BackColor = System.Drawing.Color.White;
            this.Panel_Left.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Panel_Left.BackgroundImage")));
            this.Panel_Left.Controls.Add(this.btn_OrderAnalysis);
            this.Panel_Left.Controls.Add(this.lbl_version);
            this.Panel_Left.Controls.Add(this.label1);
            this.Panel_Left.Controls.Add(this.btn_TKThuNS);
            this.Panel_Left.Controls.Add(this.btn_Rule_Group);
            this.Panel_Left.Controls.Add(this.btn_Config);
            this.Panel_Left.Controls.Add(this.btn_Audit);
            this.Panel_Left.Controls.Add(this.btn_ImportDataWeb);
            this.Panel_Left.Controls.Add(this.btn_SetTimer);
            this.Panel_Left.Controls.Add(this.btn_CheckCT);
            this.Panel_Left.Controls.Add(this.btn_OrderTranfer);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 0);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(293, 469);
            this.Panel_Left.TabIndex = 1;
            this.Panel_Left.SizeChanged += new System.EventHandler(this.Panel_Left_SizeChanged);
            // 
            // btn_OrderAnalysis
            // 
            this.btn_OrderAnalysis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_OrderAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_OrderAnalysis.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OrderAnalysis.ForeColor = System.Drawing.Color.White;
            this.btn_OrderAnalysis.Image = ((System.Drawing.Image)(resources.GetObject("btn_OrderAnalysis.Image")));
            this.btn_OrderAnalysis.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_OrderAnalysis.Location = new System.Drawing.Point(12, 50);
            this.btn_OrderAnalysis.Name = "btn_OrderAnalysis";
            this.btn_OrderAnalysis.Size = new System.Drawing.Size(83, 86);
            this.btn_OrderAnalysis.TabIndex = 3;
            this.btn_OrderAnalysis.Text = "Phân tích";
            this.btn_OrderAnalysis.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_OrderAnalysis.UseVisualStyleBackColor = false;
            this.btn_OrderAnalysis.Click += new System.EventHandler(this.btn_OrderAnalysis_Click);
            // 
            // lbl_version
            // 
            this.lbl_version.AutoSize = true;
            this.lbl_version.BackColor = System.Drawing.Color.Transparent;
            this.lbl_version.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_version.ForeColor = System.Drawing.Color.Red;
            this.lbl_version.Location = new System.Drawing.Point(6, 429);
            this.lbl_version.Name = "lbl_version";
            this.lbl_version.Size = new System.Drawing.Size(99, 32);
            this.lbl_version.TabIndex = 2;
            this.lbl_version.Text = "5.0.2.6";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(293, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "MENU";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_TKThuNS
            // 
            this.btn_TKThuNS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_TKThuNS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TKThuNS.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TKThuNS.ForeColor = System.Drawing.Color.White;
            this.btn_TKThuNS.Image = ((System.Drawing.Image)(resources.GetObject("btn_TKThuNS.Image")));
            this.btn_TKThuNS.Location = new System.Drawing.Point(12, 145);
            this.btn_TKThuNS.Name = "btn_TKThuNS";
            this.btn_TKThuNS.Size = new System.Drawing.Size(83, 86);
            this.btn_TKThuNS.TabIndex = 0;
            this.btn_TKThuNS.UseVisualStyleBackColor = false;
            this.btn_TKThuNS.Click += new System.EventHandler(this.btn_TKThuNS_Click);
            // 
            // btn_Rule_Group
            // 
            this.btn_Rule_Group.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_Rule_Group.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Rule_Group.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Rule_Group.ForeColor = System.Drawing.Color.White;
            this.btn_Rule_Group.Location = new System.Drawing.Point(198, 239);
            this.btn_Rule_Group.Name = "btn_Rule_Group";
            this.btn_Rule_Group.Size = new System.Drawing.Size(83, 86);
            this.btn_Rule_Group.TabIndex = 0;
            this.btn_Rule_Group.UseVisualStyleBackColor = false;
            this.btn_Rule_Group.Click += new System.EventHandler(this.btn_Rule_Group_Click);
            // 
            // btn_Config
            // 
            this.btn_Config.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_Config.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Config.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Config.ForeColor = System.Drawing.Color.White;
            this.btn_Config.Image = ((System.Drawing.Image)(resources.GetObject("btn_Config.Image")));
            this.btn_Config.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Config.Location = new System.Drawing.Point(12, 239);
            this.btn_Config.Name = "btn_Config";
            this.btn_Config.Size = new System.Drawing.Size(83, 86);
            this.btn_Config.TabIndex = 0;
            this.btn_Config.Text = "Tập Luật";
            this.btn_Config.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Config.UseVisualStyleBackColor = false;
            this.btn_Config.Click += new System.EventHandler(this.btn_Config_Click);
            // 
            // btn_Audit
            // 
            this.btn_Audit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_Audit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Audit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Audit.ForeColor = System.Drawing.Color.White;
            this.btn_Audit.Location = new System.Drawing.Point(198, 145);
            this.btn_Audit.Name = "btn_Audit";
            this.btn_Audit.Size = new System.Drawing.Size(83, 86);
            this.btn_Audit.TabIndex = 0;
            this.btn_Audit.UseVisualStyleBackColor = false;
            this.btn_Audit.Click += new System.EventHandler(this.btn_Audit_Click);
            // 
            // btn_ImportDataWeb
            // 
            this.btn_ImportDataWeb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_ImportDataWeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ImportDataWeb.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportDataWeb.ForeColor = System.Drawing.Color.White;
            this.btn_ImportDataWeb.Image = ((System.Drawing.Image)(resources.GetObject("btn_ImportDataWeb.Image")));
            this.btn_ImportDataWeb.Location = new System.Drawing.Point(105, 145);
            this.btn_ImportDataWeb.Name = "btn_ImportDataWeb";
            this.btn_ImportDataWeb.Size = new System.Drawing.Size(83, 86);
            this.btn_ImportDataWeb.TabIndex = 0;
            this.btn_ImportDataWeb.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_ImportDataWeb.UseVisualStyleBackColor = false;
            this.btn_ImportDataWeb.Click += new System.EventHandler(this.btn_ImportDataWeb_Click);
            // 
            // btn_SetTimer
            // 
            this.btn_SetTimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_SetTimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SetTimer.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SetTimer.ForeColor = System.Drawing.Color.White;
            this.btn_SetTimer.Image = ((System.Drawing.Image)(resources.GetObject("btn_SetTimer.Image")));
            this.btn_SetTimer.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SetTimer.Location = new System.Drawing.Point(105, 239);
            this.btn_SetTimer.Name = "btn_SetTimer";
            this.btn_SetTimer.Size = new System.Drawing.Size(83, 86);
            this.btn_SetTimer.TabIndex = 0;
            this.btn_SetTimer.Text = "Thời Gian";
            this.btn_SetTimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_SetTimer.UseVisualStyleBackColor = false;
            this.btn_SetTimer.Click += new System.EventHandler(this.btn_SetTimer_Click);
            // 
            // btn_CheckCT
            // 
            this.btn_CheckCT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_CheckCT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CheckCT.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CheckCT.ForeColor = System.Drawing.Color.White;
            this.btn_CheckCT.Image = ((System.Drawing.Image)(resources.GetObject("btn_CheckCT.Image")));
            this.btn_CheckCT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_CheckCT.Location = new System.Drawing.Point(198, 50);
            this.btn_CheckCT.Name = "btn_CheckCT";
            this.btn_CheckCT.Size = new System.Drawing.Size(83, 86);
            this.btn_CheckCT.TabIndex = 0;
            this.btn_CheckCT.Text = "Kiểm Duyệt";
            this.btn_CheckCT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_CheckCT.UseVisualStyleBackColor = false;
            this.btn_CheckCT.Click += new System.EventHandler(this.btn_CheckCT_Click);
            // 
            // btn_OrderTranfer
            // 
            this.btn_OrderTranfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.btn_OrderTranfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_OrderTranfer.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OrderTranfer.ForeColor = System.Drawing.Color.White;
            this.btn_OrderTranfer.Image = ((System.Drawing.Image)(resources.GetObject("btn_OrderTranfer.Image")));
            this.btn_OrderTranfer.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_OrderTranfer.Location = new System.Drawing.Point(105, 50);
            this.btn_OrderTranfer.Name = "btn_OrderTranfer";
            this.btn_OrderTranfer.Size = new System.Drawing.Size(83, 86);
            this.btn_OrderTranfer.TabIndex = 0;
            this.btn_OrderTranfer.Text = "Lệnh CT";
            this.btn_OrderTranfer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_OrderTranfer.UseVisualStyleBackColor = false;
            this.btn_OrderTranfer.Click += new System.EventHandler(this.btn_OrderTranfer_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(799, 469);
            this.Controls.Add(this.Panel_Left);
            this.Name = "FrmMain";
            this.Text = "Auto Tax";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Left.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Rule_Group;
        private System.Windows.Forms.Button btn_Config;
        private System.Windows.Forms.Button btn_Audit;
        private System.Windows.Forms.Button btn_ImportDataWeb;
        private System.Windows.Forms.Button btn_SetTimer;
        private System.Windows.Forms.Button btn_CheckCT;
        private System.Windows.Forms.Button btn_OrderTranfer;
        private System.Windows.Forms.Button btn_OrderAnalysis;
        private System.Windows.Forms.Label lbl_version;
        private System.Windows.Forms.Button btn_TKThuNS;
    }
}

