﻿namespace HHT_AutoTax
{
    partial class Frm_Check
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Check));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_loai = new System.Windows.Forms.ComboBox();
            this.txt_MoneyTo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_MoneyFrom = new System.Windows.Forms.TextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.lbl_s3 = new System.Windows.Forms.Label();
            this.rd_auto = new System.Windows.Forms.RadioButton();
            this.btn_View = new System.Windows.Forms.Button();
            this.lbl_End = new System.Windows.Forms.Label();
            this.btn_Apply = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.Picker_From = new System.Windows.Forms.DateTimePicker();
            this.lbl_s2 = new System.Windows.Forms.Label();
            this.lbl_s1 = new System.Windows.Forms.Label();
            this.lbl_Web = new System.Windows.Forms.Label();
            this.lbl_SQL = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LV_Message = new System.Windows.Forms.ListView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PN_Left = new System.Windows.Forms.Panel();
            this.LV_DataSQL = new System.Windows.Forms.ListView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.PN_right = new System.Windows.Forms.Panel();
            this.LV_DataExcel = new System.Windows.Forms.ListView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.PN_Left.SuspendLayout();
            this.panel6.SuspendLayout();
            this.PN_right.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cb_loai);
            this.panel1.Controls.Add(this.txt_MoneyTo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txt_MoneyFrom);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.lbl_s3);
            this.panel1.Controls.Add(this.rd_auto);
            this.panel1.Controls.Add(this.btn_View);
            this.panel1.Controls.Add(this.lbl_End);
            this.panel1.Controls.Add(this.btn_Apply);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.Picker_From);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1099, 62);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(477, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 15);
            this.label6.TabIndex = 32;
            this.label6.Text = "Nội Dung";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(540, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(179, 20);
            this.textBox1.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(478, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Loại";
            // 
            // cb_loai
            // 
            this.cb_loai.FormattingEnabled = true;
            this.cb_loai.Items.AddRange(new object[] {
            "Tất Cả",
            "Tờ khai",
            "Thuế Nội Địa",
            "Thuế Hải Quan"});
            this.cb_loai.Location = new System.Drawing.Point(540, 8);
            this.cb_loai.Name = "cb_loai";
            this.cb_loai.Size = new System.Drawing.Size(179, 21);
            this.cb_loai.TabIndex = 29;
            // 
            // txt_MoneyTo
            // 
            this.txt_MoneyTo.Location = new System.Drawing.Point(359, 35);
            this.txt_MoneyTo.Name = "txt_MoneyTo";
            this.txt_MoneyTo.Size = new System.Drawing.Size(100, 20);
            this.txt_MoneyTo.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(272, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 15);
            this.label4.TabIndex = 27;
            this.label4.Text = "Số Tiền Đến";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(272, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 26;
            this.label3.Text = "Số Tiền Từ";
            // 
            // txt_MoneyFrom
            // 
            this.txt_MoneyFrom.Location = new System.Drawing.Point(359, 9);
            this.txt_MoneyFrom.Name = "txt_MoneyFrom";
            this.txt_MoneyFrom.Size = new System.Drawing.Size(100, 20);
            this.txt_MoneyFrom.TabIndex = 25;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(143, 38);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(124, 20);
            this.radioButton1.TabIndex = 24;
            this.radioButton1.Text = "Nhập Thủ Công";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // lbl_s3
            // 
            this.lbl_s3.AutoSize = true;
            this.lbl_s3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_s3.ForeColor = System.Drawing.Color.Black;
            this.lbl_s3.Location = new System.Drawing.Point(871, 24);
            this.lbl_s3.Name = "lbl_s3";
            this.lbl_s3.Size = new System.Drawing.Size(14, 15);
            this.lbl_s3.TabIndex = 22;
            this.lbl_s3.Text = "0";
            // 
            // rd_auto
            // 
            this.rd_auto.AutoSize = true;
            this.rd_auto.Checked = true;
            this.rd_auto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd_auto.Location = new System.Drawing.Point(10, 38);
            this.rd_auto.Name = "rd_auto";
            this.rd_auto.Size = new System.Drawing.Size(116, 20);
            this.rd_auto.TabIndex = 23;
            this.rd_auto.TabStop = true;
            this.rd_auto.Text = "Nhập Tự động";
            this.rd_auto.UseVisualStyleBackColor = true;
            // 
            // btn_View
            // 
            this.btn_View.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_View.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_View.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_View.ForeColor = System.Drawing.Color.White;
            this.btn_View.Image = ((System.Drawing.Image)(resources.GetObject("btn_View.Image")));
            this.btn_View.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_View.Location = new System.Drawing.Point(892, 8);
            this.btn_View.Name = "btn_View";
            this.btn_View.Size = new System.Drawing.Size(89, 47);
            this.btn_View.TabIndex = 16;
            this.btn_View.Text = "Xem";
            this.btn_View.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_View.UseVisualStyleBackColor = false;
            this.btn_View.Click += new System.EventHandler(this.btn_View_Click);
            // 
            // lbl_End
            // 
            this.lbl_End.AutoSize = true;
            this.lbl_End.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_End.ForeColor = System.Drawing.Color.Black;
            this.lbl_End.Location = new System.Drawing.Point(822, 24);
            this.lbl_End.Name = "lbl_End";
            this.lbl_End.Size = new System.Drawing.Size(49, 15);
            this.lbl_End.TabIndex = 19;
            this.lbl_End.Text = "Còn Lại";
            // 
            // btn_Apply
            // 
            this.btn_Apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Apply.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Apply.ForeColor = System.Drawing.Color.White;
            this.btn_Apply.Location = new System.Drawing.Point(987, 8);
            this.btn_Apply.Name = "btn_Apply";
            this.btn_Apply.Size = new System.Drawing.Size(100, 47);
            this.btn_Apply.TabIndex = 8;
            this.btn_Apply.Text = "Bắt Đầu";
            this.btn_Apply.UseVisualStyleBackColor = false;
            this.btn_Apply.Click += new System.EventHandler(this.btn_Apply_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(2, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 15);
            this.label15.TabIndex = 13;
            this.label15.Text = "Từ Ngày";
            // 
            // Picker_From
            // 
            this.Picker_From.CustomFormat = "dd/MM/yyyy";
            this.Picker_From.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Picker_From.Location = new System.Drawing.Point(60, 11);
            this.Picker_From.Name = "Picker_From";
            this.Picker_From.Size = new System.Drawing.Size(199, 20);
            this.Picker_From.TabIndex = 15;
            // 
            // lbl_s2
            // 
            this.lbl_s2.AutoSize = true;
            this.lbl_s2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_s2.ForeColor = System.Drawing.Color.Black;
            this.lbl_s2.Location = new System.Drawing.Point(96, 38);
            this.lbl_s2.Name = "lbl_s2";
            this.lbl_s2.Size = new System.Drawing.Size(14, 15);
            this.lbl_s2.TabIndex = 21;
            this.lbl_s2.Text = "0";
            // 
            // lbl_s1
            // 
            this.lbl_s1.AutoSize = true;
            this.lbl_s1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_s1.ForeColor = System.Drawing.Color.Black;
            this.lbl_s1.Location = new System.Drawing.Point(103, 37);
            this.lbl_s1.Name = "lbl_s1";
            this.lbl_s1.Size = new System.Drawing.Size(14, 15);
            this.lbl_s1.TabIndex = 20;
            this.lbl_s1.Text = "0";
            // 
            // lbl_Web
            // 
            this.lbl_Web.AutoSize = true;
            this.lbl_Web.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Web.ForeColor = System.Drawing.Color.Black;
            this.lbl_Web.Location = new System.Drawing.Point(5, 38);
            this.lbl_Web.Name = "lbl_Web";
            this.lbl_Web.Size = new System.Drawing.Size(84, 15);
            this.lbl_Web.TabIndex = 17;
            this.lbl_Web.Text = "Tổng Số Lệnh\r\n";
            // 
            // lbl_SQL
            // 
            this.lbl_SQL.AutoSize = true;
            this.lbl_SQL.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SQL.ForeColor = System.Drawing.Color.Black;
            this.lbl_SQL.Location = new System.Drawing.Point(2, 37);
            this.lbl_SQL.Name = "lbl_SQL";
            this.lbl_SQL.Size = new System.Drawing.Size(84, 15);
            this.lbl_SQL.TabIndex = 18;
            this.lbl_SQL.Text = "Tổng Số Lệnh";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LV_Message);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 330);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1099, 166);
            this.panel2.TabIndex = 2;
            // 
            // LV_Message
            // 
            this.LV_Message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Message.FullRowSelect = true;
            this.LV_Message.GridLines = true;
            this.LV_Message.Location = new System.Drawing.Point(0, 0);
            this.LV_Message.Name = "LV_Message";
            this.LV_Message.Size = new System.Drawing.Size(1099, 166);
            this.LV_Message.TabIndex = 0;
            this.LV_Message.UseCompatibleStateImageBehavior = false;
            this.LV_Message.View = System.Windows.Forms.View.Details;
            this.LV_Message.ItemActivate += new System.EventHandler(this.LV_Message_ItemActivate);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.PN_Left);
            this.panel3.Controls.Add(this.PN_right);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 62);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1099, 268);
            this.panel3.TabIndex = 3;
            // 
            // PN_Left
            // 
            this.PN_Left.Controls.Add(this.LV_DataSQL);
            this.PN_Left.Controls.Add(this.panel6);
            this.PN_Left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Left.Location = new System.Drawing.Point(0, 0);
            this.PN_Left.Name = "PN_Left";
            this.PN_Left.Size = new System.Drawing.Size(630, 268);
            this.PN_Left.TabIndex = 1;
            // 
            // LV_DataSQL
            // 
            this.LV_DataSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_DataSQL.FullRowSelect = true;
            this.LV_DataSQL.GridLines = true;
            this.LV_DataSQL.Location = new System.Drawing.Point(0, 63);
            this.LV_DataSQL.Name = "LV_DataSQL";
            this.LV_DataSQL.Size = new System.Drawing.Size(630, 205);
            this.LV_DataSQL.TabIndex = 0;
            this.LV_DataSQL.UseCompatibleStateImageBehavior = false;
            this.LV_DataSQL.View = System.Windows.Forms.View.Details;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.lbl_SQL);
            this.panel6.Controls.Add(this.lbl_s1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(630, 63);
            this.panel6.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(628, 33);
            this.label1.TabIndex = 21;
            this.label1.Text = "DỮ LIỆU PHÂN TÍCH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PN_right
            // 
            this.PN_right.Controls.Add(this.LV_DataExcel);
            this.PN_right.Controls.Add(this.panel7);
            this.PN_right.Dock = System.Windows.Forms.DockStyle.Right;
            this.PN_right.Location = new System.Drawing.Point(630, 0);
            this.PN_right.Name = "PN_right";
            this.PN_right.Size = new System.Drawing.Size(469, 268);
            this.PN_right.TabIndex = 2;
            // 
            // LV_DataExcel
            // 
            this.LV_DataExcel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_DataExcel.FullRowSelect = true;
            this.LV_DataExcel.GridLines = true;
            this.LV_DataExcel.Location = new System.Drawing.Point(0, 63);
            this.LV_DataExcel.Name = "LV_DataExcel";
            this.LV_DataExcel.Size = new System.Drawing.Size(469, 205);
            this.LV_DataExcel.TabIndex = 0;
            this.LV_DataExcel.UseCompatibleStateImageBehavior = false;
            this.LV_DataExcel.View = System.Windows.Forms.View.Details;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.lbl_Web);
            this.panel7.Controls.Add(this.lbl_s2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(469, 63);
            this.panel7.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(467, 33);
            this.label2.TabIndex = 22;
            this.label2.Text = "DỮ LIỆU AGRITAX";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_Check
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 496);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Frm_Check";
            this.Text = "Frm_Check";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Check_Load);
            this.SizeChanged += new System.EventHandler(this.Frm_Check_SizeChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.PN_Left.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.PN_right.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Apply;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView LV_Message;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView LV_DataSQL;
        private System.Windows.Forms.Button btn_View;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker Picker_From;
        private System.Windows.Forms.ListView LV_DataExcel;
        private System.Windows.Forms.Label lbl_Web;
        private System.Windows.Forms.Label lbl_SQL;
        private System.Windows.Forms.Label lbl_End;
        private System.Windows.Forms.Label lbl_s3;
        private System.Windows.Forms.Label lbl_s2;
        private System.Windows.Forms.Label lbl_s1;
        private System.Windows.Forms.Panel PN_right;
        private System.Windows.Forms.Panel PN_Left;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cb_loai;
        private System.Windows.Forms.TextBox txt_MoneyTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_MoneyFrom;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton rd_auto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
    }
}