﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;
using HHT.Library.System;

namespace HHT_AutoTax
{
    public partial class Frm_SetupRule : Form
    {
        public Frm_SetupRule()
        {
            InitializeComponent();
        }

        private void Frm_SetupRule_Load(object sender, EventArgs e)
        {
            InitListView(LV_Data);
            LoadDataToListView(RuleFormat_Data.List());
            LoadDataToToolbox.ComboBoxData(cbo_Items, "SELECT ItemKey,ItemName FROM Rule_Items", "---- Choose ----");
        }
        private void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ký Hiệu Nhận Dạng";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thứ Tự";
            colHead.Width = 90;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Định Dạng";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            
        }
        public void LoadDataToListView(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["RuleID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Signal"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Rank"].ToString().Trim();
                lvi.SubItems.Add(lvsi);
                
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Format"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
      
        private void Load_Info(string ID)
        {
            RuleFormat_Info zRule = new RuleFormat_Info(ID);
            txt_ID.Text = zRule.RuleID;
            txt_Signal.Text = zRule.Signal;
            txt_CategoryID.Text = zRule.CategoryID.ToString();
            txt_Rank.Text = zRule.Rank.ToString();
            txt_Format.Text = zRule.Format;
           
        }
        private void LV_Data_ItemActivate(object sender, EventArgs e)
        {
            string ID = LV_Data.SelectedItems[0].SubItems[1].Text;
            Load_Info(ID);
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            string zFormat = "";
            zFormat = txt_WordCode.Text + "#" + cbo_Items.SelectedValue.ToString();
            if (txt_Format.Text.Trim().Length == 0)
                txt_Format.Text = zFormat;
            else
                txt_Format.Text += "|"+zFormat;
            txt_Format.Text = txt_Format.Text.Trim();

        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            RuleFormat_Info zRule = new RuleFormat_Info();
            zRule.RuleID = txt_ID.Text ;
            zRule.Signal = txt_Signal.Text ;
            zRule.CategoryID = int.Parse(txt_CategoryID.Text);
            zRule.Rank = int.Parse(txt_Rank.Text);
            zRule.Format = txt_Format.Text ;
            zRule.Update();
            LoadDataToListView(RuleFormat_Data.List());
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            RuleFormat_Info zRule = new RuleFormat_Info();
            zRule.RuleID = txt_ID.Text;
            zRule.Signal = txt_Signal.Text;
            zRule.CategoryID = int.Parse(txt_CategoryID.Text);
            zRule.Rank = int.Parse(txt_Rank.Text);
            zRule.Format = txt_Format.Text;
            zRule.Create();
            LoadDataToListView(RuleFormat_Data.List());
        }

        private void btn_Del_Click(object sender, EventArgs e)
        {
            RuleFormat_Info zRule = new RuleFormat_Info();
            zRule.RuleID = txt_ID.Text;
            zRule.Delete();
            LoadDataToListView(RuleFormat_Data.List());
        }

        
    }
}
