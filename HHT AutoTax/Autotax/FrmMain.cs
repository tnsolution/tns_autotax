﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class FrmMain : Form
    {
        //private string NameBank = "AutoTax-Đông Sài Gòn";
        private string NameBank = "AutoTax-Hải An";  // hải phòng
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FrmMain.ActiveForm.Text = NameBank;
        }

        private void Panel_Left_SizeChanged(object sender, EventArgs e)
        {
            lbl_version.Top = Panel_Left.Height - lbl_version.Height - 5;
        }


        private void btn_OrderAnalysis_Click(object sender, EventArgs e)
        {
            FrmOrderTranfer frm = new FrmOrderTranfer();
            frm.ShowDialog();
        }

        private void btn_Config_Click(object sender, EventArgs e)
        {
            //FrmConfig frm = new FrmConfig();
            Frm_SetupRule frm = new Frm_SetupRule();
            frm.ShowDialog();

        }

        private void btn_Rule_Group_Click(object sender, EventArgs e)
        {
            FrmRule_Group frm = new FrmRule_Group();
            frm.ShowDialog();
        }

        private void btn_OrderTranfer_Click(object sender, EventArgs e)
        {
            FrmOrder frm = new FrmOrder();
            frm.ShowDialog();
        }

        private void btn_CheckCT_Click(object sender, EventArgs e)
        {
            Frm_ImportDataCheck_V2 frm = new Frm_ImportDataCheck_V2();
            frm.ShowDialog();
        }

        private void btn_SetTimer_Click(object sender, EventArgs e)
        {
            Frm_SetUpTimer frm = new Frm_SetUpTimer();
            frm.ShowDialog();
        }

        private void btn_ImportDataWeb_Click(object sender, EventArgs e)
        {
            Frm_Check frm = new Frm_Check();
            frm.ShowDialog();
        }

        private void btn_OrderAnalysis_2_Click(object sender, EventArgs e)
        {
            //FrmOrderAnalysis_Ext frm = new FrmOrderAnalysis_Ext();
           // frm.ShowDialog();

        }

        private void btn_Audit_Click(object sender, EventArgs e)
        {
            Frm_ImportDataCheck frm = new Frm_ImportDataCheck();
            frm.ShowDialog();

        }

        private void btn_TKThuNS_Click(object sender, EventArgs e)
        {
            FrmAccount frm = new FrmAccount();
            frm.ShowDialog();
        }
    }
}
