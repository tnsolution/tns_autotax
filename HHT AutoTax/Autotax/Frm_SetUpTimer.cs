﻿using HHT.Library.Bank;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class Frm_SetUpTimer : Form
    {
        public Frm_SetUpTimer()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TimerDelay_Info timer = new TimerDelay_Info();
            timer.TimerWaitMST = int.Parse(txt_TimerMST.Text);
            timer.TimerLoad = int.Parse(txt_TimerLoad.Text);
            timer.Update_Load();
        }
    }
}
