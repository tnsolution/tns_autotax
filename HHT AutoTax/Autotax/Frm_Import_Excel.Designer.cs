﻿namespace HHT_AutoTax
{
    partial class Frm_Import_Excel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Import_Excel));
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbo_Sheet = new System.Windows.Forms.ComboBox();
            this.CoUsers = new System.Windows.Forms.ComboBox();
            this.btn_Up = new System.Windows.Forms.Button();
            this.btn_Often = new System.Windows.Forms.Button();
            this.txt_Excel = new System.Windows.Forms.TextBox();
            this.txt_MaxSesion = new System.Windows.Forms.TextBox();
            this.txt_Amount_Excel = new System.Windows.Forms.TextBox();
            this.txt_Amout_SQL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.list_Name = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label2.Location = new System.Drawing.Point(100, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "IMPORT DỮ LIỆU";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 55);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(928, 58);
            this.panel2.TabIndex = 33;
            // 
            // cbo_Sheet
            // 
            this.cbo_Sheet.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_Sheet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Sheet.FormattingEnabled = true;
            this.cbo_Sheet.Location = new System.Drawing.Point(15, 38);
            this.cbo_Sheet.Name = "cbo_Sheet";
            this.cbo_Sheet.Size = new System.Drawing.Size(223, 22);
            this.cbo_Sheet.TabIndex = 4;
            this.cbo_Sheet.SelectedIndexChanged += new System.EventHandler(this.cbo_Sheet_SelectedIndexChanged);
            // 
            // CoUsers
            // 
            this.CoUsers.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CoUsers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CoUsers.FormattingEnabled = true;
            this.CoUsers.Location = new System.Drawing.Point(732, 13);
            this.CoUsers.Name = "CoUsers";
            this.CoUsers.Size = new System.Drawing.Size(193, 23);
            this.CoUsers.TabIndex = 4;
            this.CoUsers.SelectedIndexChanged += new System.EventHandler(this.cbo_Sheet_SelectedIndexChanged);
            // 
            // btn_Up
            // 
            this.btn_Up.Location = new System.Drawing.Point(850, 38);
            this.btn_Up.Name = "btn_Up";
            this.btn_Up.Size = new System.Drawing.Size(75, 23);
            this.btn_Up.TabIndex = 3;
            this.btn_Up.Text = "Upload";
            this.btn_Up.UseVisualStyleBackColor = true;
            this.btn_Up.Click += new System.EventHandler(this.btn_Up_Click);
            // 
            // btn_Often
            // 
            this.btn_Often.Location = new System.Drawing.Point(244, 10);
            this.btn_Often.Name = "btn_Often";
            this.btn_Often.Size = new System.Drawing.Size(75, 23);
            this.btn_Often.TabIndex = 1;
            this.btn_Often.Text = "Browse...";
            this.btn_Often.UseVisualStyleBackColor = true;
            this.btn_Often.Click += new System.EventHandler(this.btn_Often_Click);
            // 
            // txt_Excel
            // 
            this.txt_Excel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Excel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Excel.Location = new System.Drawing.Point(15, 12);
            this.txt_Excel.Name = "txt_Excel";
            this.txt_Excel.Size = new System.Drawing.Size(223, 20);
            this.txt_Excel.TabIndex = 0;
            // 
            // txt_MaxSesion
            // 
            this.txt_MaxSesion.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MaxSesion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_MaxSesion.Location = new System.Drawing.Point(244, 39);
            this.txt_MaxSesion.Name = "txt_MaxSesion";
            this.txt_MaxSesion.ReadOnly = true;
            this.txt_MaxSesion.Size = new System.Drawing.Size(75, 20);
            this.txt_MaxSesion.TabIndex = 5;
            // 
            // txt_Amount_Excel
            // 
            this.txt_Amount_Excel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Amount_Excel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Amount_Excel.Location = new System.Drawing.Point(651, 14);
            this.txt_Amount_Excel.Name = "txt_Amount_Excel";
            this.txt_Amount_Excel.ReadOnly = true;
            this.txt_Amount_Excel.Size = new System.Drawing.Size(75, 20);
            this.txt_Amount_Excel.TabIndex = 5;
            // 
            // txt_Amout_SQL
            // 
            this.txt_Amout_SQL.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Amout_SQL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Amout_SQL.Location = new System.Drawing.Point(651, 38);
            this.txt_Amout_SQL.Name = "txt_Amout_SQL";
            this.txt_Amout_SQL.ReadOnly = true;
            this.txt_Amout_SQL.Size = new System.Drawing.Size(75, 20);
            this.txt_Amout_SQL.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(551, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số Lượng Excel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(551, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Số Lượng SQL";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txt_Amout_SQL);
            this.panel3.Controls.Add(this.txt_Amount_Excel);
            this.panel3.Controls.Add(this.txt_MaxSesion);
            this.panel3.Controls.Add(this.txt_Excel);
            this.panel3.Controls.Add(this.btn_Often);
            this.panel3.Controls.Add(this.btn_Up);
            this.panel3.Controls.Add(this.CoUsers);
            this.panel3.Controls.Add(this.cbo_Sheet);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(928, 71);
            this.panel3.TabIndex = 34;
            // 
            // list_Name
            // 
            this.list_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_Name.FullRowSelect = true;
            this.list_Name.GridLines = true;
            this.list_Name.Location = new System.Drawing.Point(0, 129);
            this.list_Name.Name = "list_Name";
            this.list_Name.Size = new System.Drawing.Size(928, 321);
            this.list_Name.TabIndex = 35;
            this.list_Name.UseCompatibleStateImageBehavior = false;
            this.list_Name.View = System.Windows.Forms.View.Details;
            // 
            // Frm_Import_Excel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 450);
            this.Controls.Add(this.list_Name);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "Frm_Import_Excel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import từ file Excel";
            this.Load += new System.EventHandler(this.Frm_Import_Excel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbo_Sheet;
        private System.Windows.Forms.ComboBox CoUsers;
        private System.Windows.Forms.Button btn_Up;
        private System.Windows.Forms.Button btn_Often;
        private System.Windows.Forms.TextBox txt_Excel;
        private System.Windows.Forms.TextBox txt_MaxSesion;
        private System.Windows.Forms.TextBox txt_Amount_Excel;
        private System.Windows.Forms.TextBox txt_Amout_SQL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView list_Name;
    }
}