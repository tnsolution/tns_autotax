﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using mshtml;
using SHDocVw;
using System.IO;
using System.Threading;
using Excel1 = Microsoft.Office.Interop.Excel;
using HHT.Library.Bank;

namespace HHT_AutoTax
{
    public partial class FrmOrderUpToWeb : Form
    {
        public int SectionNo = 0;
        private bool _IsPostback;
        private Order_Info _Order;
        private ArrayList _OrderDetail;
        private DataTable _ListOrder;
        private TimerDelay_Info _TimerDelay;
        int _RowAll = 0;// tat ca dong GV
        string _NganHang = "79204014";//  Đong sai gòn
        //string _NganHang = "31204015";//  hải phòng
        public FrmOrderUpToWeb()
        {
            InitializeComponent();
        }


        private void FrmOrderUpToWeb_Load(object sender, EventArgs e)
        {
            _IsPostback = false;
            InitGridView_Order(GV_ListOrder);
            InitGridView(GV_DataDetail);

           // DataTable zGroup = Order_Data.List(SectionNo, 0);

            LoadDataOfOrder();
            LoadIE();
            _TimerDelay = new TimerDelay_Info(1);
            _IsPostback = true;

            _RowAll = GV_ListOrder.Rows.Count;
        }

        #region [ Design Layout ]
        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("Message", "Message");
            GV.Columns.Add("So_BT", "Số BT");
            GV.Columns.Add("StatusOrder", "Status");
            GV.Columns.Add("trdate", "Ngày");
            GV.Columns.Add("trref", "Số Chứng Từ");
            GV.Columns.Add("traamt", "Số Tiền");
            GV.Columns.Add("TKTNS", "TK Thu NS");
            GV.Columns.Add("bencust", "Hải Quan");
            GV.Columns.Add("ordcust", "Người Nộp Thuế");

            GV.Columns[0].Width = 40;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 120;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 40;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[4].Width = 70;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[5].Width = 120;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 80;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 50;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[8].Width = 180;
            GV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[9].Width = 100;
            GV.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

         
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        public void InitGridView(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Chuong", "Chương");
            GV.Columns.Add("NDKT", "NDKT");
            GV.Columns.Add("Content", "Nội Dung");
            GV.Columns.Add("TienNT", "Tiền NT");
            GV.Columns.Add("KyThue", "Kỳ Thuế");

            GV.Columns[0].Width = 80;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 80;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 220;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 100;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
        }
        private void Form_Clear()
        {
            txt_CQQLThu_ID.Text = "";
            txt_CQQLThu_Name.Text = "";
            txt_DBHC_ID.Text = "";
            txt_DBHC_Name.Text = "";
            txt_LHXNK.Text = "";
            txt_LHXNK_Name.Text = "";
            txt_MaSoThue.Text = "";
            txt_NgayDk.Text = "";
            txt_NgayNT.Text = "";
            txt_NguoiNT.Text = "";
            txt_NHNN_ID.Text = "";
            txt_NHNN_Name.Text = "";
            txt_NH_PhucVu.Text = "";
            txt_SoChungTu.Text = "";
            txt_TK_Name.Text = "";
            txt_TK_Thu_NS.Text = "";
            txt_ToKhaiSo.Text = "";
        }
        private void panel5_SizeChanged(object sender, EventArgs e)
        {
            btn_Run_One.Left = panel5.Right - btn_Run_One.Width;
        }
        private void bnt_Hide_Click(object sender, EventArgs e)
        {
            if (btn_Hide.Text == ">>")
            {
                Panel_Right.Visible = false;
                btn_Hide.Text = "<<";
            }
            else
            {
                Panel_Right.Visible = true;
                btn_Hide.Text = ">>";
            }
        }
        #endregion

        #region [ Action ]

        private int _IndexRun;
        private bool _IsRunOnAllRecord = false;
        private void btn_Apply_Click(object sender, EventArgs e)
        {
            if (btn_Apply.Text == "Stop")
            {
                btn_Apply.Text = "Run All";
                _IsRunOnAllRecord = false;
                timer1.Stop();
            }
            else
            {
                btn_Apply.Text = "Stop";
                _IsRunOnAllRecord = true;
                _IndexRun = -1;
                timer1.Start();
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            #region Code Cũ
            timer1.Stop();
            if (btn_Apply.Text == "Run All")
                return;

            _IndexRun++;
            if (_IndexRun > GV_ListOrder.Rows.Count - 1)
            {
                MessageBox.Show("Finish");
                btn_Apply.Text = "Run All";
            }
            else
            {
                if (GV_ListOrder.Rows[_IndexRun].Cells["trref"].Value != null)
                {
                    Form_Clear();
                    GV_DataDetail.Rows.Clear();

                    string zID = GV_ListOrder.Rows[_IndexRun].Cells["trref"].Value.ToString();
                    _Order = new Order_Info(zID);

                    DataTable zOrderDetail = Order_Data.ListOrderDetail(zID);
                    _OrderDetail = new ArrayList();
                    Order_Detail_Info zDetail;
                    foreach (DataRow zRow in zOrderDetail.Rows)
                    {
                        zDetail = new Order_Detail_Info(zRow);
                        _OrderDetail.Add(zDetail);
                    }

                    LoadToToolBox();

                    if (_Order.OrderStatus == 2)
                    {
                        Message_System("Chứng từ [" + _Order.trref + "] đã đưa lên web rồi");
                        timer1.Start();
                    }
                    else
                    {
                        GV_ListOrder.Rows[_IndexRun].Cells["Message"].Value = "Processing....";
                        bool zIsCorrect = false;
                        if (_Order.LoaiThue_ID == "04")
                            zIsCorrect = CheckFinishOrder_HaiQuan();
                        else
                            zIsCorrect = CheckFinishOrder_NoiDia();
                        if (zIsCorrect)
                        {
                            //Message_System("RunUpToWeb");
                            RunUpToWeb();
                            btn_Get_Amount_Click(null, null);
                            timer1.Start();
                            ////GV_ListOrder.Rows[_IndexRun].Cells["Message"].Value = "Done";

                        }
                    }
                }
            }
            #endregion

        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_So_CT_Web.Text.Length > 0)
                _Order.UpdateMessage(2, txt_So_CT_Web.Text, "", txt_SoBT.Text);
            else
                _Order.UpdateMessage(0, txt_So_CT_Web.Text, "", txt_SoBT.Text);

            GV_ListOrder.Rows[_IndexRun].Cells["StatusOrder"].Value = 2;
            GV_ListOrder.Rows[_IndexRun].Cells["Message"].Value = txt_So_CT_Web.Text;
            _Order.NgayNT = DateTime.Parse(txt_NgayNT.Text);
            _Order.Update();
        }
        #endregion

        #region [ Process Data ]
        private void btn_View_Click(object sender, EventArgs e)
        {
            GV_ListOrder.Rows.Clear();
            LoadDataOfOrder();
        }
        public void LoadDataOfOrder()
        {
            int zStatusOrder = -1;
            if (rdo_NotYet.Checked)
                zStatusOrder = 0;
            if (rdo_Error.Checked)
                zStatusOrder = 1;
            if (rdo_Success.Checked)
                zStatusOrder = 2;

            _ListOrder = Order_Data.List(SectionNo, zStatusOrder);

            int i = 0;
            foreach (DataRow nRow in _ListOrder.Rows)
            {
                GV_ListOrder.Rows.Add();
                DataGridViewRow nRowView = GV_ListOrder.Rows[i];
                DateTime z_trdate = (DateTime)nRow["NgayNT"];
                double zMoney = double.Parse(nRow["SoTien"].ToString());

                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StatusOrder"].Value = nRow["OrderStatus"].ToString().Trim();
                nRowView.Cells["trdate"].Value = z_trdate.ToString("dd/MM/yyyy");
                nRowView.Cells["trref"].Value = nRow["trref"].ToString();
                nRowView.Cells["traamt"].Value = zMoney.ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR"));
                nRowView.Cells["TKTNS"].Value = nRow["TKThuNS_ID"].ToString().Trim();
                nRowView.Cells["bencust"].Value = nRow["CQThu_Name"].ToString().Trim();
                nRowView.Cells["ordcust"].Value = nRow["MST"].ToString().Trim();
                if (nRow["So_CT"].ToString().Trim().Length > 0)
                    nRowView.Cells["Message"].Value = nRow["So_CT"].ToString().Trim();
                else
                    nRowView.Cells["Message"].Value = nRow["MessageReturn"].ToString().Trim();
                //nRowView.Cells["remark"].Value = nRow["remark"].ToString().Trim();
                nRowView.Cells["So_BT"].Value = nRow["So_BT"].ToString().Trim();
                i++;
            }
            lbl_Total_Record.Text = _ListOrder.Rows.Count.ToString();
        }
        private void LoadToToolBox()
        {
            txt_So_CT_Web.Text = _Order.So_CT;
            txt_SoChungTu.Text = _Order.trref;
            txt_NHNN_ID.Text = _Order.NHNN_ID;
            txt_NHNN_Name.Text = _Order.NHNN_Name;
            cbo_KieuThu.SelectedIndex = _Order.HinhThucThu;

            DateTime zDay = (DateTime)_Order.NgayNT;
            txt_NH_PhucVu.Text = _Order.NHPV_ID;
            txt_NH_PhucVu_Name.Text = _Order.NHPV_Name;
            txt_NgayNT.Text = zDay.ToString("dd/MM/yyyy");

            txt_MaSoThue.Text = _Order.MST;
            txt_NguoiNT.Text = _Order.TenCty;

            txt_TK_Thu_NS.Text = _Order.TKThuNS_ID;
            txt_TK_Name.Text = _Order.TKThuNS_Name;

            txt_DVSDNS_ID.Text = _Order.DVSDNS_ID;
            txt_DVSDNS_Name.Text = _Order.DVSDNS_Name;

            txt_DBHC_ID.Text = _Order.DBHC_ID + "HH";
            txt_DBHC_Name.Text = _Order.DBHC_Name;

            txt_CQQLThu_ID.Text = _Order.CQThu_ID;
            txt_CQQLThu_Name.Text = _Order.CQThu_Name;

            txt_LHXNK.Text = _Order.LH_ID;
            txt_LHXNK_Name.Text = _Order.LH_Name;

            txt_ToKhaiSo.Text = _Order.ToKhai;
            txt_LoaiThue.Text = _Order.LoaiThue_ID;
            
            //------------------------------

            int n = _OrderDetail.Count;
            for (int i = 0; i < n; i++)
            {
                GV_DataDetail.Rows.Add();
                Order_Detail_Info zDetail = (Order_Detail_Info)_OrderDetail[i];
               
                GV_DataDetail.Rows[i].Cells[0].Value = zDetail.ChuongID;
                GV_DataDetail.Rows[i].Cells[1].Value = zDetail.NDKT_ID;
                GV_DataDetail.Rows[i].Cells[2].Value = zDetail.NDKT_Name;
                GV_DataDetail.Rows[i].Cells[3].Value = zDetail.SoTien.ToString("#,###,###");
                GV_DataDetail.Rows[i].Cells[4].Value = zDetail.KyThue;
            }

        }

        private int CheckSo_CT(string So_CT)
        {
            int zResult = 0;

            foreach (DataRow zRow in _ListOrder.Rows)
            {
                if (zRow["So_CT"].ToString() == So_CT)
                    zResult++;
            }

            return zResult;
        }

        private bool CheckFinishOrder_HaiQuan()
        {
            if (_Order.MST.Length == 0)
                return false;
            if (_Order.MST.Length == 13)
            {
                _Order.MST = _Order.MST.Substring(0, 10) + "-" + _Order.MST.Substring(10, 3);
            }
            if (_Order.ToKhai.Length == 0)
                return false;

            if (_Order.NgayDK == null)
                return false;
            if (_Order.TKThuNS_ID.Length == 0)
                return false;
            if (_Order.DBHC_ID.Length == 0 || _Order.DBHC_ID.Length > 4)
                return false;
            if (_OrderDetail.Count == 0)
                return false;

            double zSumMoney = 0;
            for (int i = 0; i < _OrderDetail.Count; i++)
            {
                Order_Detail_Info zDetail = (Order_Detail_Info)_OrderDetail[i];
                zSumMoney += zDetail.SoTien;
                if (zDetail.ChuongID.Length == 0)
                {
                    return false;
                }
                if (zDetail.NDKT_ID.Length == 0)
                {
                    return false;
                }
                if (zDetail.NDKT_Name.Length == 0)
                {
                    return false;
                }
                if (zDetail.SoTien == 0)
                {
                    return false;
                }
            }

            if (zSumMoney != _Order.SoTien)
                return false;

            return true;
        }
        private bool CheckFinishOrder_NoiDia()
        {
            if (_Order.MST.Length == 0)
                return false;
            if (_Order.MST.Length == 13)
            {
                _Order.MST = _Order.MST.Substring(0, 10) + "-" + _Order.MST.Substring(10, 3);
            }
            if (_Order.TKThuNS_ID.Length == 0)
                return false;
            if (_Order.DBHC_ID.Length == 0 || _Order.DBHC_ID.Length > 4)
                return false;
            if (_OrderDetail.Count == 0)
                return false;
            double zSumMoney = 0;
            for (int i = 0; i < _OrderDetail.Count; i++)
            {
                Order_Detail_Info zDetail = (Order_Detail_Info)_OrderDetail[i];
                zSumMoney += zDetail.SoTien;
                if (zDetail.ChuongID.Length == 0)
                {
                    return false;
                }
                if (zDetail.NDKT_ID.Length == 0)
                {
                    return false;
                }
                if (zDetail.NDKT_Name.Length == 0)
                {
                    return false;
                }
                if (zDetail.SoTien == 0)
                {
                    return false;
                }
            }

            if (zSumMoney != _Order.SoTien)
                return false;

            return true;
        }
        #endregion

        #region [ Access Web ]
        private ArrayList _ListIE;
        private InternetExplorer IEBrowse;
        private AutoResetEvent IEBrowse_NavigateCompleteAutoReset;
        private void IEBrowse_NavigateComplete2(object pDisp, ref object URL)
        {
            IEBrowse_NavigateCompleteAutoReset.Set();
        }

        private void LoadIE()
        {
            ShellWindows m_IEFoundBrowsers = new ShellWindows();
            _ListIE = new ArrayList();
            int i = 0, k = -1;

            foreach (InternetExplorer zBrowser in m_IEFoundBrowsers)
            {
                cbo_ListWeb.Items.Add("[" + zBrowser.Name + "][Url name: " + zBrowser.LocationURL);
                _ListIE.Add(zBrowser);
                if (zBrowser.LocationURL.Contains("http://agritax.agribank.com.vn/Load_ChungTu.do?function_id=030100"))
                {
                    IEBrowse = zBrowser;
                    k = i;
                }
                i++;
            }
            if (k >= 0)
                cbo_ListWeb.SelectedIndex = k;
            //IEBrowse = (InternetExplorer)_ListIE[0];
        }
        private void LoadIEAfterSave()
        {
            ShellWindows m_IEFoundBrowsers = new ShellWindows();
            foreach (InternetExplorer zBrowser in m_IEFoundBrowsers)
            {
                LB_Web_Log.Items.Add("[" + zBrowser.Name + "][Url name: " + zBrowser.LocationURL);
                if (zBrowser.LocationURL.Contains("http://agritax.agribank.com.vn/print_GNT_ByKeyCTU_HQ.do?ngay_kb") == true)
                {
                    zBrowser.Quit();
                }
            }
        }
        private void cbo_ListWeb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_IsPostback)
            {
                IEBrowse = (InternetExplorer)_ListIE[cbo_ListWeb.SelectedIndex];
                btn_Get_Amount_Click(null, null);
            }
        }
        private string ReadFile(string strFileName)
        {
            string zListSetting = "";
            if (File.Exists(strFileName))
            {
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                StreamReader r = new StreamReader(fs, Encoding.UTF8);

                zListSetting = r.ReadToEnd();
                r.Close();
                fs.Close();

            }
            return zListSetting;
        }
        #endregion

        private string _MessageLog = "";
        private int _AmountWaitingNewID = 0;

        #region [ Up data to web ]
        private void RunUpToWeb()
        {
            _MessageLog = "";
            _AmountWaitingNewID = 0;
            bool zFindMST = false;
            int Count = 5;
            while (!zFindMST && Count > 0)
                if (JavaScrip_CheckMST())
                {
                    Thread.Sleep(_TimerDelay.TimerSleepAfterMST);
                    if (CheckCompanyName())
                    {
                        zFindMST = true;
                        Count = 0;
                        Message_System("Bắt đầu đưa dữ liệu vào field");

                        if (!JavaScripFill())
                            return;
                        Thread.Sleep(_TimerDelay.TimerSleepAfterFill);

                        if (_Order.OrderStatus != 1)
                        {

                            Message_System("Bắt đầu lệnh save");
                            if (!JavaScrip_Save())
                                return;
                            Thread.Sleep(_TimerDelay.TimerSleepAfterSave);

                            Message_System("Bắt đầu lệnh kiểm tra");

                            Thread.Sleep(6000);
                            bool zFindOut = false;
                            while (!zFindOut)
                            {
                                if (CheckAfterSave())
                                {
                                    if (_IsRunOnAllRecord)
                                    {
                                        zFindOut = true;
                                        timer1.Start();
                                    }
                                }
                                else
                                    Thread.Sleep(6000);
                            }

                        }
                    }
                }
                else
                {
                    Count--;
                    if (Count == 0)
                        MessageBox.Show("Vui lòng kiểm tra lại mạng");
                    timer1.Stop();
                }
        }
        private bool JavaScrip_CheckMST()
        {
            bool zResult = false;
            if (IEBrowse.ReadyState == SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
            {
                try
                {
                    IEBrowse.NavigateComplete2 += new SHDocVw.DWebBrowserEvents2_NavigateComplete2EventHandler(IEBrowse_NavigateComplete2);
                    IEBrowse_NavigateCompleteAutoReset = new AutoResetEvent(false);

                    Message_System("   -- Gọi lệnh kiểm tra MST ");
                    //-----------Check Ma So Thue--------------
                    string zFindMST = ReadFile("JavaScripFill.txt");
                    StringBuilder zJavaScrip = new StringBuilder();
                    zJavaScrip.Append(zFindMST);
                    zJavaScrip.AppendLine("HHT_LoadMST('" + _Order.MST + "');");
                    IHTMLDocument2 doc = (IHTMLDocument2)IEBrowse.Document;
                    IHTMLWindow2 parentWindow = null;
                    if (doc != null)
                    {
                        parentWindow = doc.parentWindow;
                        if (parentWindow != null)
                        {
                            parentWindow.execScript(zJavaScrip.ToString(), "javascript");

                            Message_System("   -- Gởi xong lệnh kiểm tra MST (Chờ......)");
                            IEBrowse_NavigateCompleteAutoReset.WaitOne(_TimerDelay.TimerWaitMST);
                            Message_System("   -- Gởi xong lệnh kiểm tra MST (Chờ xong)");
                            zResult = true;
                        }
                        else
                        {
                            zResult = false;
                        }
                    }
                }
                catch (Exception err)
                {
                    _MessageLog += err.ToString();
                    Message_System("   --ERROR [" + err.ToString() + "]");
                    zResult = false;
                    return false;
                }
                finally
                {
                    /*Remove the event handler*/
                    IEBrowse.NavigateComplete2 -= IEBrowse_NavigateComplete2;
                    Message_System("   -- Xong hàm kiểm tra MST");

                }
            }
            return zResult;
        }

        private bool JavaScrip_New()
        {
            bool zResult = false;
            if (IEBrowse.ReadyState == SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
            {
                try
                {
                    IEBrowse.NavigateComplete2 += new SHDocVw.DWebBrowserEvents2_NavigateComplete2EventHandler(IEBrowse_NavigateComplete2);
                    IEBrowse_NavigateCompleteAutoReset = new AutoResetEvent(false);

                    Message_System("   -- Lệnh tạo mới ");
                    //-----------Check Ma So Thue--------------
                    StringBuilder zJavaScrip = new StringBuilder();
                    zJavaScrip.AppendLine("sbNew_view() ;");
                    IHTMLDocument2 doc = (IHTMLDocument2)IEBrowse.Document;
                    IHTMLWindow2 parentWindow = null;
                    if (doc != null)
                    {
                        parentWindow = doc.parentWindow;
                        if (parentWindow != null)
                        {
                            parentWindow.execScript(zJavaScrip.ToString(), "javascript");
                            zResult = true;
                        }
                        else
                        {
                            zResult = false;
                        }
                    }
                }
                catch (Exception err)
                {
                    _MessageLog += err.ToString();
                    Message_System("   --ERROR [" + err.ToString() + "]");
                    zResult = false;
                    return false;
                }
                finally
                {
                    /*Remove the event handler*/
                    IEBrowse.NavigateComplete2 -= IEBrowse_NavigateComplete2;
                    Message_System("   -- Tạo mới xong");

                }
            }
            return zResult;
        }
        private bool CheckCompanyName()
        {
            bool zResult = false;
            mshtml.HTMLDocument zDoc = (mshtml.HTMLDocument)IEBrowse.Document;
            mshtml.IHTMLElement element = (mshtml.IHTMLElement)zDoc.getElementById("id_ten_thue");
            string zName = element.getAttribute("value");
            if (zName != null)
            {
                Message_System("   -- MST " + _Order.MST + ": " + zName);
                _Order.TenCty = zName;
                zResult = true;
            }
            else
            {
                Message_System("   -- MST " + _Order.MST + ": Không tìm thấy khách hàng này");
                GV_ListOrder.Rows[_IndexRun].Cells["Message"].Value = "Không có khách hàng này";
                zResult = false;
            }
            return zResult;
        }
        private bool JavaScripFill()
        {
            bool zResult = false;
            if (IEBrowse.ReadyState == SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
            {
                try
                {
                    Message_System("   -- Đưa lệnh Fill lên web ");
                    IEBrowse.NavigateComplete2 += new SHDocVw.DWebBrowserEvents2_NavigateComplete2EventHandler(IEBrowse_NavigateComplete2);
                    IEBrowse_NavigateCompleteAutoReset = new AutoResetEvent(false);
                    StringBuilder zJavaScrip = new StringBuilder();
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.check_hthuc_thu.value = 2;");
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.hthuc_thu.value = '2';");
                    zJavaScrip.AppendLine("document.getElementById('id_tr_ma_nh_b').style.display = \"none\";");
                    zJavaScrip.AppendLine("document.getElementById('id_tr_tk_co').style.display = \"none\"; ");
                    zJavaScrip.AppendLine("document.getElementById('id_tr_tk_no').style.display = \"none\";");
                    zJavaScrip.AppendLine("document.getElementById('id_title_ttnh_ht').style.display = \"none\";");
                    zJavaScrip.AppendLine("document.getElementById('id_nh_nnt_ngaynt').style.display = \"\";");
                    zJavaScrip.AppendLine("document.getElementById('id_nh_nnt').style.display = \"\";");

                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.loai_thue.value = '"+ _Order.LoaiThue_ID+ "';");
                    zJavaScrip.AppendLine("ChangeLoaiThue(document.TcsLapChungTuForm.loai_thue);");
                   /* if (_Order.CQThu_ID == "1056286" || _Order.CQThu_ID == "1054219" || _Order.CQThu_ID == "1056439")
                    {
                        zJavaScrip.AppendLine("document.TcsLapChungTuForm.loai_thue.value = '01';");
                        zJavaScrip.AppendLine("ChangeLoaiThue(document.TcsLapChungTuForm.loai_thue);");
                    }
                    */
                    //// ngan hang tu phuc vu ----------------------------------------------------------------------------------------------------------
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.ma_nh_nnt.value = '" + _NganHang + "';");
                    zJavaScrip.AppendLine("autoGetKB();");

                    string NDK = "";
                    if (_Order.NgayDK != null)
                    {
                        DateTime zNgayDK = (DateTime)_Order.NgayDK;
                        NDK = zNgayDK.ToString("dd/MM/yyyy");
                    }
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.ngay_dk.value = '" + NDK + "';");
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.ngaynt.value = '" + _Order.NgayNT.Value.ToString("dd/MM/yyyy") + "';");
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.tkhai_so.value = '" + _Order.ToKhai + "';");
                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.ma_dbhc.value = '" + _Order.DBHC_ID + "HH';");

                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.tk_tnsach.value = '" + _Order.TKThuNS_ID + "';");//-------------------------------------------------
                    if (_Order.TKThuNS_ID == "3511")
                    {
                        zJavaScrip.AppendLine("document.TcsLapChungTuForm.ma_dvqhns.value = '" + _Order.DVSDNS_ID + "';");//-------------------------------------------------
                        zJavaScrip.AppendLine("document.getElementById('id_dvsdns').style.display = \"block\";");
                    }

                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.cq_thu.value = '" + _Order.DVSDNS_ID + "';");

                    if (_Order.ToKhai == "9999999999" || CheckLePhiToKhai())
                    {
                        zJavaScrip.AppendLine("document.TcsLapChungTuForm.ma_loaitien.value = '11';");
                    }
                    else
                        zJavaScrip.AppendLine("document.TcsLapChungTuForm.ma_loaitien.value = '1';");

                    zJavaScrip.AppendLine("document.TcsLapChungTuForm.lh_xnk.value = '" + _Order.LH_ID + "';");


                    zJavaScrip.AppendLine("LoadMa_Ten_LHXNK();");

                    //Load ma ten--------------------------------------------------------------------------------------------------------------------------

                    zJavaScrip.AppendLine("Load_DVQDNS(" + _Order.DVSDNS_ID + ");");

                    int n = _OrderDetail.Count;

                    zJavaScrip.AppendLine("var tbl = document.getElementById('tblGridDetailTable');");
                    zJavaScrip.AppendLine("var lastRow = tbl.rows.length;");
                    if (n >= 1)
                    {
                        Order_Detail_Info zDetail = (Order_Detail_Info)_OrderDetail[0];
                        zJavaScrip.AppendLine("$('id_chuong_row1').value = '" + zDetail.ChuongID + "';");
                        zJavaScrip.AppendLine("$('id_noidungkt_row1').value =" + zDetail.NDKT_ID + ";");
                        zJavaScrip.AppendLine("$('id_noidung_row1').value ='" + zDetail.NDKT_Name + "';");
                        zJavaScrip.AppendLine("$('id_tien_nt_row1').value =" + zDetail.SoTien.ToString() + ";");
                        zJavaScrip.AppendLine("$('id_tien_row1').value =" + zDetail.SoTien.ToString() + ";");
                        if (_Order.LoaiThue_ID == "01")
                        {
                            zJavaScrip.AppendLine("$('id_ky_thue_row1').value ='" + zDetail.KyThue.ToString() + "';");
                        }
                        zJavaScrip.AppendLine("toFormatNumberVN_TCS($('id_tien_nt_row1'));");
                        zJavaScrip.AppendLine("toFormatNumberVN_TCS($('id_tien_row1'));");

                        zJavaScrip.AppendLine("if (lastRow > 2){");
                        zJavaScrip.AppendLine("for (rowdelete = lastRow - 1; rowdelete >= 2; rowdelete--) {");
                        zJavaScrip.AppendLine("document.all(\"tblGridDetailTable\").deleteRow(rowdelete); }}");

                        for (int i = 1; i < n; i++)
                        {
                            zDetail = (Order_Detail_Info)_OrderDetail[i];
                            zJavaScrip.AppendLine("addDetailRowForQuery('" + zDetail.ChuongID + "', '', ");
                            zJavaScrip.Append(zDetail.NDKT_ID + ", ");
                            zJavaScrip.Append("'" + zDetail.NDKT_Name + "',");
                            zJavaScrip.Append("'" + zDetail.SoTien.ToString() + "',"); // chinh da thay doi
                            zJavaScrip.Append("'" + zDetail.SoTien.ToString() + "');"); // da thay doi
                        }
                    }

                    zJavaScrip.AppendLine("cal_tongtien(type_nganhang); cal_tongtien_nt(); ");



                    IHTMLDocument2 doc = (IHTMLDocument2)IEBrowse.Document;
                    IHTMLWindow2 parentWindow = null;
                    if (doc != null)
                    {
                        parentWindow = doc.parentWindow;
                        if (parentWindow != null)
                        {
                            parentWindow.execScript(zJavaScrip.ToString(), "javascript");
                            Message_System("   -- Gởi xong lệnh Fill  (Chờ......)");
                            IEBrowse_NavigateCompleteAutoReset.WaitOne(_TimerDelay.TimerWaitFill);
                            Message_System("   -- Gởi xong lệnh Fill (Chờ xong)");
                            zResult = true;
                        }
                    }
                    return zResult;
                }
                catch (Exception err)
                {
                    Message_System(err.ToString());
                    zResult = false;
                    return zResult;
                }
                finally
                {
                    /*Remove the event handler*/
                    IEBrowse.NavigateComplete2 -= IEBrowse_NavigateComplete2;
                    Message_System("   -- Xong hàm Fill");
                }
            }
            return zResult;
        }
        private bool CheckLePhiToKhai()
        {
            bool zResult = false;
            if (_Order.SoTien == 20000 && _OrderDetail.Count == 1)
            {
                Order_Detail_Info zDetail = (Order_Detail_Info)_OrderDetail[0];
                if (zDetail.NDKT_ID == "2663")
                {
                    zResult = true;
                }
            }
            return zResult;
        }

        private bool JavaScrip_Save()
        {
            bool zResult = false;
            if (IEBrowse.ReadyState == SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
            {
                try
                {
                    Message_System("   -- Đưa lệnh save lên web ");
                    IEBrowse.NavigateComplete2 += new SHDocVw.DWebBrowserEvents2_NavigateComplete2EventHandler(IEBrowse_NavigateComplete2);
                    IEBrowse_NavigateCompleteAutoReset = new AutoResetEvent(false);

                    //-----------Check Ma So Thue--------------
                    StringBuilder zJavaScrip = new StringBuilder();
                    zJavaScrip.AppendLine("document.getElementById('msgText').style.display = '';");
                    zJavaScrip.AppendLine("HHT_sbSave();");
                    //zJavaScrip.AppendLine("setTimeout(function(){window.close();}, 500);");
                    IHTMLDocument2 doc = (IHTMLDocument2)IEBrowse.Document;
                    IHTMLWindow2 parentWindow = null;
                    if (doc != null)
                    {
                        parentWindow = doc.parentWindow;
                        if (parentWindow != null)
                        {
                            parentWindow.execScript(zJavaScrip.ToString(), "javascript");

                            Message_System("   -- xong lệnh save lên web (chờ.....) ");
                            IEBrowse_NavigateCompleteAutoReset.WaitOne(_TimerDelay.TimerWaitSave);
                            Message_System("   -- xong lệnh save lên web (chờ xong) ");
                            zResult = true;
                        }
                    }
                    return zResult;
                }
                catch (Exception err)
                {
                    _MessageLog += err.ToString();
                    Message_System("   -- ERROR " + err.ToString());
                    zResult = false;
                    return zResult;
                }
                finally
                {
                    /*Remove the event handler*/
                    IEBrowse.NavigateComplete2 -= IEBrowse_NavigateComplete2;
                    Message_System("   -- Xong hàm Save");
                }
            }
            return zResult;
        }
        private bool CheckAfterSave()
        {
            bool zResult = false;
            try
            {
                Message_System("Bat dau vao ham check after save ");
                IEBrowse.NavigateComplete2 += new SHDocVw.DWebBrowserEvents2_NavigateComplete2EventHandler(IEBrowse_NavigateComplete2);
                IEBrowse_NavigateCompleteAutoReset = new AutoResetEvent(false);

                mshtml.HTMLDocument zDoc = (mshtml.HTMLDocument)IEBrowse.Document;
                mshtml.IHTMLElementCollection elements = (mshtml.IHTMLElementCollection)zDoc.getElementsByName("so_ct");
                mshtml.IHTMLElement AmountCT = (mshtml.IHTMLElement)zDoc.getElementById("so_bt_hidden");
                mshtml.IHTMLElementCollection elements_sbt = (mshtml.IHTMLElementCollection)zDoc.getElementsByName("sbt");
                int n = 0;
                int.TryParse(AmountCT.getAttribute("value"), out n);

                Message_System("Số lượng mới cập nhật là " + n);
                if (n > int.Parse(txt_Amount_On_Web.Text))
                {
                    string zID = "";
                    string zSBT = "";
                    int i = 0;
                    foreach (IHTMLElement item in elements)
                    {
                        zID = item.getAttribute("value");
                        if (i == 0)
                            break;
                    }
                    foreach (IHTMLElement item in elements_sbt)
                    {
                        zSBT = item.getAttribute("value");
                        if (i == 0)
                            break;
                    }
                    txt_ID.Text = zID;
                    txt_SoBT.Text = zSBT;

                    _Order.UpdateMessage(2, zID, "", zSBT);
                    _ListOrder.Rows[_IndexRun]["OrderStatus"] = 2;
                    _ListOrder.Rows[_IndexRun]["So_CT"] = zID;
                    _ListOrder.Rows[_IndexRun]["So_BT"] = zSBT;

                    GV_ListOrder.Rows[_IndexRun].Cells["StatusOrder"].Value = 2;
                    GV_ListOrder.Rows[_IndexRun].Cells["Message"].Value = zID;
                    GV_ListOrder.Rows[_IndexRun].Cells["So_BT"].Value = zSBT;
                    zResult = true;
                }
                return zResult;
            }
            catch (Exception err)
            {
                Message_System(err.ToString());
                return zResult;
            }
            finally
            {
                /*Remove the event handler*/
                IEBrowse.NavigateComplete2 -= IEBrowse_NavigateComplete2;
                Message_System("Hoàn thành hàm check sau khi lưu");
            }
        }
        #endregion

        private void Message_System(string Message)
        {
            Invoke(new MethodInvoker(delegate
            {
                if (LB_Web_Log.Items.Count > 1000)
                    LB_Web_Log.Items.RemoveAt(0);
                LB_Web_Log.Items.Add(DateTime.Now.ToString("dd-MM-yy hh:mm:ss.fff") + " : " + Message);
                LB_Web_Log.SelectedIndex = LB_Web_Log.Items.Count - 1;
            }));
        }

        private void btn_Get_Amount_Click(object sender, EventArgs e)
        {
            mshtml.HTMLDocument zDoc = (mshtml.HTMLDocument)IEBrowse.Document;
            mshtml.IHTMLElementCollection elements = (mshtml.IHTMLElementCollection)zDoc.getElementsByName("so_ct");
            mshtml.IHTMLElement AmountCT = (mshtml.IHTMLElement)zDoc.getElementById("so_bt_hidden");
            int n = 0;
            int.TryParse(AmountCT.getAttribute("value"), out n);
            if (n == 0 && elements.length > 0)
            {
                mshtml.IHTMLElement sbt = (mshtml.IHTMLElement)zDoc.getElementById("sbt");
                if (sbt.getAttribute("value") != null)
                {
                    string strSbt = sbt.getAttribute("value");
                    strSbt = strSbt.Substring(strSbt.Length - 5, 5);
                    int.TryParse(strSbt, out n);
                }
            }
            txt_Amount_On_Web.Text = n.ToString();
            int i = 0;
            foreach (IHTMLElement item in elements)
            {
                txt_ID.Text = item.getAttribute("value");
                if (i == 0)
                    break;
                i++;

            }
        }

        #region RunOne
        private void btn_Run_One_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            GV_ListOrder.Enabled = false;
            _IsRunOnAllRecord = false;
            bool zIsCorrect = false;
            if (_Order.OrderStatus == 2)
            {
                MessageBox.Show("Chứng từ này đã đưa lên web rồi");
            }
            else
            {
                if (_Order == null)
                    return;
                if (_Order.LoaiThue_ID == "04")
                    zIsCorrect = CheckFinishOrder_HaiQuan();
                else
                    zIsCorrect = CheckFinishOrder_NoiDia();
                if (zIsCorrect)
                {
                    RunUpToWeb();
                    btn_Get_Amount_Click(null, null);
                }
            }
            GV_ListOrder.Enabled = true;
            this.Cursor = Cursors.Default;
        }
        private void GV_ListOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_IsPostback)
            {
                if (!_IsRunOnAllRecord)
                {
                    if (GV_ListOrder.Rows[e.RowIndex].Cells["trref"].Value != null)
                    {
                        string zID = GV_ListOrder.Rows[e.RowIndex].Cells["trref"].Value.ToString();
                        Form_Clear();
                        GV_DataDetail.Rows.Clear();
                        _IndexRun = e.RowIndex;
                        _Order = new Order_Info(zID);

                        DataTable zOrderDetail = Order_Data.ListOrderDetail(zID);
                        _OrderDetail = new ArrayList();
                        Order_Detail_Info zDetail;
                        foreach (DataRow zRow in zOrderDetail.Rows)
                        {
                            zDetail = new Order_Detail_Info(zRow);
                            _OrderDetail.Add(zDetail);
                        }

                        LoadToToolBox();
                    }
                }
            }
        }
        #endregion

        private void btn_Export_Click(object sender, EventArgs e)
        {
            SaveFileDialog fsave = new SaveFileDialog();
            fsave.Filter = "(Tất cả các tệp)|*.*|(Các tệp Excel)|*.xlsx";
            fsave.ShowDialog();
            if (fsave.FileName != "")
            {
                Excel1.Application app = new Excel1.Application();
                Excel1.Workbook wb = app.Workbooks.Add(Type.Missing);
                Excel1.Worksheet sheet = null;
                try
                {
                    sheet = wb.ActiveSheet;
                    for (int i = 1; i <= GV_ListOrder.Columns.Count; i++)
                    {
                        sheet.Cells[1, i] = GV_ListOrder.Columns[i - 1].HeaderText;
                    }
                    for (int i = 0; i <= GV_ListOrder.Rows.Count; i++)
                    {
                        for (int j = 0; j <= GV_ListOrder.Columns.Count; j++)
                        {
                            sheet.Cells[i + 2, j + 1] = GV_ListOrder.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    wb.SaveAs(fsave.FileName);
                    MessageBox.Show("Export file Excel Success ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                finally
                {
                    app.Quit();
                    wb = null;
                }
            }
            else
            {
                MessageBox.Show("Bạn không chọn tệp tin nào", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            _Order.NgayNT = DateTime.Parse(txt_NgayNT.Text);
            _Order.Update();
        }
    }
}


