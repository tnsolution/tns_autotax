﻿using HHT.BNK;
using HHT.Library.Bank;
using HHT.Library.System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class FrmCheckCT : Form
    {
        public string _SCT = "";
        public FrmCheckCT()
        {
            InitializeComponent();
        }

        private void FrmCheckCT_Load(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thông Báo";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


        }

        private void Load_Data()
        {
            BNK_Order order = new BNK_Order(_SCT);
            txt_MaSoThue.Text = order.Ma_ST;
            txt_NguoiNT.Text = order.TenCongTy;
            txt_DBHC_ID.Text = order.DBHC_ID;
            txt_ToKhaiSo.Text = order.ToKhai;
            txt_LHXNK.Text = order.LH_ID;
            txt_NgayDk.Text = order.NgayDK.ToString();
            txt_SoTien.Text = order.SoTien.ToString();
            DataTable zTable1 = OrderTranfer_Data.List_SQL_1(_SCT);
            GV_DataDetailSQL.DataSource = zTable1;
            //for (int i = 0; i < zTable1.Rows.Count; i++)
            //{
            //    Check zCheck = new Check(_SCT);
            //    GV_DataDetailSQL.Rows[i].Cells[0].Value = zCheck.NDKT;
            //    GV_DataDetailSQL.Rows[i].Cells[1].Value = zCheck.Sotien.ToString("#,###,###");
            //    GV_DataDetailSQL.Rows.Add();
            //}


            OrderWeb_Info web = new OrderWeb_Info(_SCT);
            txt_MaSoThue1.Text = web.MaSoThue;
            txt_NguoiNT1.Text = web.TenCTy;
            txt_ToKhaiSo1.Text = web.SoTK;
            txt_LHXNK1.Text = web.LHXNK;
            txt_NgayDk1.Text = web.Ngay.ToString();
            txt_SoTien1.Text = web.TongTien.ToString();
            txt_DBHC_ID1.Text = order.DBHC_ID;
            DataTable zTable2 = OrderTranfer_Data.List_Excel_2(_SCT);
            GV_ListOrderWeb.DataSource = zTable2;
        }

        private void btn_Import_Click(object sender, EventArgs e)
        {
            Frm_ImportFileWeb im = new Frm_ImportFileWeb();
            im.ShowDialog();
        }

        private void LV_Data_ItemActivate(object sender, EventArgs e)
        {
            Load_Data();

        }

        private void btn_Apply_Click(object sender, EventArgs e)
        {

        }

        private void FrmCheckCT_SizeChanged(object sender, EventArgs e)
        {
            tabControl2.Width = this.Width / 2;
        }
    }
}
