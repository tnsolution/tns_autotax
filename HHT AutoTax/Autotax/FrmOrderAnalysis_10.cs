﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;
using HHT.Library.System;

namespace HHT_AutoTax
{
    public partial class FrmOrderAnalysis_10 : Form
    {
        public int SectionNo = 0;
        private bool _IsPostback;
        CultureInfo enUS = new CultureInfo("en-US"); // is up to you

        private ArrayList _ListItemInBill;
        private OrderTranfer_Info _OrderOrigin;
        private List<string> _ListNeedRemove = new List<string>();
        // private Order_Info _OrderHeader;
        // private ArrayList _OrderDetail;

        public FrmOrderAnalysis_10()
        {
            InitializeComponent();
            //  Panel_Right.Visible = false;
            //  btn_Hide.Text = "<<";
        }
        private void FrmOrderAnalysis_10_Load(object sender, EventArgs e)
        {
            _IsPostback = false;
            InitGridView_Order(GV_ListOrder);
            InitGridView_Analysis(GV_Analysis);
            InitGridView_Order_Detail(GV_DataDetail);

            //txt_Search.Text = "CARRIER VIETNAM AIR CONDITIONING CO.LTD/CT";
            //DataTable zListOrder = OrderTranfer_Data.ListAnalysis(0, txt_Search.Text);
            DataTable zListOrder = OrderTranfer_Data.ListAnalysis(SectionNo, 0);
            LoadDataOfOrder(zListOrder);

            _ListNeedRemove = Rule_Data.ListStringNeedRomove();
            _IsPostback = true;
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            _IsPostback = false;
            DataTable zListOrder = OrderTranfer_Data.ListAnalysis(0, txt_Search.Text);
            LoadDataOfOrder(zListOrder);
            _IsPostback = true;
        }
        #region [ Tracking ]
        private void Message_System(string Message)
        {
            Invoke(new MethodInvoker(delegate
            {
                LB_Log.Items.Add(DateTime.Now.ToString("dd-MM-yy hh:mm:ss.fff") + " : " + Message);
            }));
        }
        private void Process_Log(string Info)
        {
            Invoke(new MethodInvoker(delegate
            {
                //LB_Process.Items.Add(DateTime.Now.ToString("mm:ss.fff") + " : " + Info);
            }));
        }
        #endregion

        #region [ Design Layout ]
        private void btn_Hide_Click(object sender, EventArgs e)
        {

            if (btn_Hide.Text == ">>")
            {
                Panel_Right.Visible = false;
                btn_Hide.Text = "<<";
            }
            else
            {
                Panel_Right.Visible = true;
                btn_Hide.Text = ">>";
            }
        }
        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("Message", "Message");
            GV.Columns.Add("StatusAnalysis", "Status");
            GV.Columns.Add("trdate", "Ngày CT");
            GV.Columns.Add("remark", "Nội Dung");
            GV.Columns.Add("traamt", "Số Tiền");
            GV.Columns.Add("ordcust", "Người Nộp Thuế");
            GV.Columns.Add("bencust", "cust");
            GV.Columns.Add("remtbk", "MÃ NH");
            GV.Columns.Add("remntbk", "Ngân Hàng ");

            GV.Columns.Add("name_16", "name_16");
            GV.Columns.Add("trref", "Số Chứng Từ");

            GV.Columns[0].Width = 40;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 90;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 50;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 80;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["trdate"].Width = 80;
            GV.Columns["trdate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["remark"].Width = 200;
            GV.Columns["remark"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["remark"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["traamt"].Width = 120;
            GV.Columns["traamt"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ordcust"].Width = 240;
            GV.Columns["ordcust"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ordcust"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            GV.Columns["bencust"].Width = 200;
            GV.Columns["bencust"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["bencust"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            GV.Columns["remtbk"].Width = 100;
            GV.Columns["remtbk"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["remntbk"].Width = 180;
            GV.Columns["remntbk"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //""
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        public void InitGridView_Analysis(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Field_1", "");
            GV.Columns.Add("Field_2", "");
            GV.Columns.Add("Field_3", "");
            GV.Columns.Add("Field_4", "");

            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 120;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 140;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 250;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;


            for (int i = 1; i <= 50; i++)
            {
                GV.Rows.Add();
            }
        }
        public void InitGridView_Order_Detail(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Chuong", "Chương");
            GV.Columns.Add("NDKT", "NDKT");
            GV.Columns.Add("Content", "Nội Dung");
            GV.Columns.Add("TienNT", "Tiền NT");
            GV.Columns.Add("KyThue", "Kỳ Thuế");

            GV.Columns[0].Width = 80;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 80;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 220;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 100;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        private void PanelLeftBottom_SizeChanged(object sender, EventArgs e)
        {
            btn_SaveFail.Left = PanelLeftBottom.Width - btn_SaveFail.Width - 10;
        }
        private void LB_Log_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            LB_Log.Items.Clear();
        }
        private void LB_Process_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }
        #endregion

        #region [Get Data ]

        public void LoadDataOfOrder(DataTable zListOrder)
        {
            this.Cursor = Cursors.WaitCursor;
            GV_ListOrder.Rows.Clear();


            int i = 0;
            int j = 0;
            foreach (DataRow nRow in zListOrder.Rows)
            {
                GV_ListOrder.Rows.Add();
                DataGridViewRow nRowView = GV_ListOrder.Rows[i];
                DateTime z_trdate = (DateTime)nRow["trdate"];
                double zMoney = double.Parse(nRow["traamt"].ToString());

                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StatusAnalysis"].Value = nRow["StatusAnalysis"].ToString();
                nRowView.Cells["trdate"].Value = z_trdate.ToString("dd/MM/yyyy");
                nRowView.Cells["trref"].Value = nRow["trref"].ToString();
                nRowView.Cells["traamt"].Value = zMoney.ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR"));

                nRowView.Cells["bencust"].Value = nRow["bencust"].ToString().Trim();
                nRowView.Cells["remtbk"].Value = nRow["remtbk"].ToString().Trim();
                nRowView.Cells["remntbk"].Value = nRow["remntbk"].ToString().Trim();
                nRowView.Cells["remark"].Value = nRow["remark"].ToString().Trim();
                nRowView.Cells["ordcust"].Value = nRow["ordcust"].ToString().Trim();
                nRowView.Cells["name_16"].Value = nRow["name_16"].ToString().Trim();

                i++;
                if (nRow["StatusAnalysis"].ToString() != "0")
                    j++;
            }
            lbl_Total_Record.Text = i.ToString();
            lbl_Record_Success.Text = j.ToString();
            lbl_Record_Error.Text = (i - j).ToString();

            this.Cursor = Cursors.Default;
        }
        private void LoadToToolBox(Order_Object Order)
        {
            GV_DataDetail.Rows.Clear();

            txt_SoChungTu.Text = Order.Header.trref;
            txt_NHNN_ID.Text = Order.Header.NHNN_ID;
            txt_NHNN_Name.Text = Order.Header.NHNN_Name;
            cbo_HinhThucThu.SelectedIndex = Order.Header.HinhThucThu;

            txt_NH_PhucVu.Text = Order.Header.NHPV_ID;
            txt_NH_PhucVu_Name.Text = Order.Header.NHPV_Name;
            if (Order.Header.NgayNT != null)
                txt_NgayNT.Text = Order.Header.NgayNT.Value.ToString("dd/MM/yyyy");

            txt_MaSoThue.Text = Order.Header.MST;
            txt_NguoiNT.Text = Order.Header.TenCty;

            txt_TK_Thu_NS.Text = Order.Header.TKThuNS_ID;
            txt_TK_Name.Text = Order.Header.TKThuNS_Name;

            txt_DVSDNS_ID.Text = Order.Header.DVSDNS_ID;
            txt_DVSDNS_Name.Text = Order.Header.DVSDNS_Name;

            txt_DBHC_ID.Text = Order.Header.DBHC_ID + "HH";
            txt_DBHC_Name.Text = Order.Header.DBHC_Name;

            txt_CQQLThu_ID.Text = Order.Header.CQThu_ID;
            txt_CQQLThu_Name.Text = Order.Header.CQThu_Name;
            txt_LoaiThue.Text = Order.Header.LoaiThue_ID;

            txt_LHXNK.Text = Order.Header.LH_ID;
            txt_LHXNK_Name.Text = Order.Header.LH_Name;

            txt_ToKhaiSo.Text = Order.Header.ToKhai;
            if (Order.Header.NgayDK != null)
            {
                txt_NgayDk.Text = Order.Header.NgayDK.Value.ToString("dd/MM/yyyy");
            }
            else
                txt_NgayDk.Text = "";

            //------------------------------

            int n = Order.Items.Count;
            Order_Detail_Info zDetail;
            double zTotal = 0;
            int i = 0;
            for (i = 0; i < n; i++)
            {
                GV_DataDetail.Rows.Add();
                zDetail = Order.Items[i];
                GV_DataDetail.Rows[i].Cells[0].Value = zDetail.ChuongID;
                GV_DataDetail.Rows[i].Cells[1].Value = zDetail.NDKT_ID;
                GV_DataDetail.Rows[i].Cells[2].Value = zDetail.NDKT_Name;
                GV_DataDetail.Rows[i].Cells[3].Value = zDetail.SoTien.ToString("#,###,###");
                zTotal += zDetail.SoTien;
                GV_DataDetail.Rows[i].Cells[4].Value = zDetail.KyThue;
            }
            GV_DataDetail.Rows.Add();
            GV_DataDetail.Rows[i].Cells[0].Value = "";
            GV_DataDetail.Rows[i].Cells[1].Value = "";
            GV_DataDetail.Rows[i].Cells[2].Value = "TỔNG CỘNG";
            GV_DataDetail.Rows[i].Cells[3].Value = zTotal.ToString("#,###,###");

        }
        #endregion

        private void GV_ListOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_IsPostback)
            {
                if (_Index_Selected != e.RowIndex)
                {
                    _Index_Selected = e.RowIndex;
                    AnalysicOrder(e.RowIndex, "Normal");

                }

            }
        }
        int _AmountSuccess;
        int _Index_Selected;
        private void btn_Apply_Click(object sender, EventArgs e)
        {
            _AmountSuccess = 0;
            _Index_Selected = 0;
            GV_ListOrder.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            Timer_Auto.Start();
        }
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            Timer_Auto.Stop();
            //==========================================================


            AnalysicOrder(_Index_Selected, "Auto");
            lbl_Record_Success.Text = _AmountSuccess.ToString();

            //===============================================================
            _Index_Selected++;
            if (_Index_Selected < GV_ListOrder.Rows.Count)
                Timer_Auto.Start();
            else
            {
                GV_ListOrder.Enabled = true;
                int zTotal = int.Parse(lbl_Total_Record.Text);
                lbl_Record_Error.Text = (zTotal - _AmountSuccess).ToString();
                this.Cursor = Cursors.Default;
            }
        }

        #region [ Find Rule ]
        private void AnalysicOrder(int RowIndex, string Style)
        {

            string trref = GV_ListOrder.Rows[RowIndex].Cells["trref"].Value.ToString();
            OrderTranfer_Info zOrderOrigin = new OrderTranfer_Info(trref);
            if (zOrderOrigin.StatusAnalysis > 0)
                return;

            GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Processing";

            //Phân tích trên file excel
            Order_Object zOrder = AnalysicOnFields(zOrderOrigin);
            DataTable zListRule = Rule_Data.List_Rule(1);
            bool zStatusDone = false;
            string zRuleID = "";
            zOrder.RuleID = "0";
            if (chkRule1000.Checked)
            {
                foreach (DataRow zRow in zListRule.Rows)
                {
                    string zSignal = zRow["Signal"].ToString();
                    zRuleID = "";
                    if (zSignal == "+")
                    {
                        bool zCorrectFormat = false;
                        if (zSignal.Length == 1)
                            if (CountCharSpecial(zOrder.Remark, zSignal[0]) > 3)
                                zCorrectFormat = true;

                        if (zCorrectFormat)
                        {
                            zRuleID = zRow["RuleID"].ToString();
                            Rule_Data.AnalysisRule(zRuleID, zSignal, zOrder);
                            zStatusDone = CheckOrder(zOrder);
                        }
                    }

                }
            }

            if (chkRule8000.Checked)
            {
                if (!zStatusDone) // Theo luật chữ và số liên tiếp
                {
                    zRuleID = "80001";
                    zOrder = new Order_Object();
                    zOrder = AnalysicOnFields(zOrderOrigin);
                    Rule_Data.AnalysisRule("80001", "", zOrder);
                    zStatusDone = CheckOrder(zOrder);
                }
            }
            if (chkRule3000.Checked)
            {
                if (!zStatusDone) // Định dạng theo luật chính xác
                {
                    zListRule = Rule_Data.List_Rule(3);
                    foreach (DataRow zRow in zListRule.Rows)
                    {
                        string zFormat = zRow["Format"].ToString();
                        zOrder = new Order_Object();
                        zOrder = AnalysicOnFields(zOrderOrigin);
                        Rule_Data.AnalysisRule("30000", zFormat, zOrder);
                        zOrder.RuleID = zRow["RuleID"].ToString();
                        zStatusDone = CheckOrder(zOrder);
                        if (zStatusDone)
                            break;
                    }
                }
            }

            if (chkRule6000.Checked)
            {
                if (!zStatusDone) // Định dạng theo luật chính xác và nhiều tờ khai
                {
                    zListRule = Rule_Data.List_Rule(6);
                    foreach (DataRow zRow in zListRule.Rows)
                    {
                        string zFormat = zRow["Format"].ToString();
                        zOrder = new Order_Object();
                        zOrder = AnalysicOnFields(zOrderOrigin);
                        Rule_Data.AnalysisRule("60000", zFormat, zOrder);
                        zOrder.RuleID = zRow["RuleID"].ToString();
                        zStatusDone = CheckOrder(zOrder);
                        if (zStatusDone)
                            break;
                    }
                }
            }

            if (zStatusDone)
            {
                // Cập nhật lỗi sai của người viết lệnh gởi tiền
                //Lệ Phí Thủ Tục Hải Quan 20.000đ/1 tờ Khai,
                if (zOrder.Header.SoTien == 20000 && zOrder.Header.TKThuNS_ID == "7111")
                {
                    txt_TK_Thu_NS.Text = "3511";
                    zOrder.Header.TKThuNS_ID = "3511";
                    if (zOrder.Items.Count == 1)
                    {
                        zOrder.Items[0].NDKT_ID = "2663";
                        zOrder.Items[0].NDKT_Name = Rule_Data.NDKT_Name("2663");
                    }
                }
                // trường hợp trên lệnh chuyển tiền có nhiều Lệ Phí Thủ Tục Hải Quan 20.000đ/1 tờ Khai
                if (zOrder.Header.ToKhai == "9999999999")
                {
                    zOrder.Header.LH_ID = "99999";
                    zOrder.Header.LH_Name = "";
                    if (zOrder.Header.NgayDK == null)
                        zOrder.Header.NgayDK = DateTime.Now;

                }
                for (int i = 0; i < zOrder.Items.Count; i++)
                {
                    if (zOrder.Items[i].ChuongID == "999")
                        zOrder.Items[i].ChuongID = "754";
                    if (zOrder.Items[i].NDKT_ID == "3052")
                    {
                        zOrder.Items[i].NDKT_ID = "2663";
                        zOrder.Items[i].NDKT_Name = Rule_Data.NDKT_Name("2663");
                    }
                    if (zOrder.Items[i].NDKT_ID == "2035" || zOrder.Items[i].NDKT_ID == "2036" || zOrder.Items[i].NDKT_ID == "2037" || zOrder.Items[i].NDKT_ID == "2038" || zOrder.Items[i].NDKT_ID == "2039")
                    {
                        zOrder.Items[i].NDKT_ID = "2021";
                        zOrder.Items[i].NDKT_Name = Rule_Data.NDKT_Name("2021");
                    }
                }

                if (Style == "Auto")
                {
                    if (zOrder.Save())
                    {
                        OrderTranfer_Info zOrderTranfer = new OrderTranfer_Info();
                        zOrderTranfer.trref = zOrder.Header.trref;
                        zOrderTranfer.StatusAnalysis = int.Parse(zOrder.RuleID);
                        zOrderTranfer.UpdateStatus();
                        GV_ListOrder.Rows[RowIndex].Cells["Message"].Value = "Done";
                        GV_ListOrder.Rows[RowIndex].Cells["StatusAnalysis"].Value = zOrder.RuleID;
                    }
                }
                else
                {
                    GV_ListOrder.Rows[RowIndex].Cells["Message"].Value = "Done";
                    GV_ListOrder.Rows[RowIndex].Cells["StatusAnalysis"].Value = zOrder.RuleID;
                }
            }
            else
            {
                GV_ListOrder.Rows[RowIndex].Cells["Message"].Value = "Fail";
            }

            if (Style == "Normal")
            {
                LoadToToolBox(zOrder);
                StringBuilder remark = new StringBuilder();
                remark.AppendLine(GV_ListOrder.Rows[RowIndex].Cells["remark"].Value.ToString());
                remark.AppendLine("=========After======");
                remark.AppendLine(zOrder.Remark);
                txt_remark.Text = remark.ToString();

                GV_Analysis.Rows.Clear();
                GV_Analysis.Rows.Add(50);
                DataGridViewRow nRowView = GV_Analysis.Rows[0];
                nRowView.Cells["Field_1"].Value = "Chứng từ số";
                nRowView.Cells["Field_2"].Value = zOrder.Header.trref;

                //======================PHAN TICH NOI DUNG VA TIM KIEM THONG SO=============================
                int k = 2;
                nRowView = GV_Analysis.Rows[k];
                nRowView.Cells["Field_1"].Value = "******";
                nRowView.Cells["Field_2"].Value = "***NỘI DUNG***";
                nRowView.Cells["Field_3"].Value = "******";
                k = 3;

                for (int i = 0; i < zOrder.Remark_Items.Count; i++)
                {
                    Item_Bill zItem = (Item_Bill)zOrder.Remark_Items[i];
                    nRowView = GV_Analysis.Rows[k + i];
                    nRowView.Cells["Field_1"].Value = zItem.Content;
                    nRowView.Cells["Field_2"].Value = zItem.Value;
                    nRowView.Cells["Field_3"].Value = zItem.CategoryName;
                    nRowView.Cells["Field_4"].Value = zItem.CorrectValue + " " + zItem.CorrectID + " " + zItem.CorrectName;
                    if (i > 40)
                        break;
                }

            }
        }
        private Order_Object AnalysicOnFields(OrderTranfer_Info OrderOrigin)
        {

            Order_Object zOrder = new Order_Object();

            string zTKNganSach = "";
            string zTK_KhoBac = "";
            string zMaQHNS = "";
            string zName_16 = OrderOrigin.name_16;
            zName_16 = Function.RemoveCharNotNumber(zName_16);
            if (zName_16.Trim() == "")
                zName_16 = "7111";

            if (zName_16.Length > 4)
            {
                zTKNganSach = zName_16.Substring(0, 4);
                if (zName_16.Length >= 11)
                    zMaQHNS = zName_16.Substring(zName_16.Length - 7, 7).Trim();
                zTK_KhoBac = zTKNganSach + ".0." + zMaQHNS;
            }
            if (zName_16.Length == 4)
            {
                string zDepartmentName = GV_ListOrder.Rows[_Index_Selected].Cells["bencust"].Value.ToString().ToUpper();
                zMaQHNS = OrderTranfer_Data.SearchMaQHNS_Of_Department(zDepartmentName);

                zTKNganSach = zName_16;
                zTK_KhoBac = zTKNganSach + ".0." + zMaQHNS;
            }

            GovTax_Account_Info zTaxAccount = new GovTax_Account_Info(zTK_KhoBac);
            if (zTaxAccount.AccountName.Trim().Length > 5)
            {
                zOrder.Header.TKThuNS_ID = zTaxAccount.TKNS;
                zOrder.Header.TKThuNS_Name = zTaxAccount.AccountName;
                zOrder.Header.CQThu_ID = zTaxAccount.DepartmentID;
                zOrder.Header.CQThu_Name = zTaxAccount.DepartmentName;
                zOrder.Header.DVSDNS_ID = zTaxAccount.QHNS;
                zOrder.Header.LoaiThue_ID = zTaxAccount.CategoryTax;
                zOrder.Header.NHNN_ID = zTaxAccount.TreasuryID;
                zOrder.Header.NHNN_Name = zTaxAccount.TreasuryName;
                zOrder.Header.DBHC_ID = zTaxAccount.DBHC;

                //GV_ListOrder.Rows[_Index_Selected].DefaultCellStyle.BackColor = Color.White;
            }
            if (zOrder.Header.TKThuNS_ID.Length == 0)
            {
                if (zName_16.Length == 4)
                    zOrder.Header.TKThuNS_ID = zName_16;
            }
            zOrder.Header.trref = OrderOrigin.trref;
            zOrder.Header.NHPV_ID = OrderOrigin.rembk1;
            zOrder.Header.NHPV_Name = OrderOrigin.remntbk;
            zOrder.Header.TenCty = OrderOrigin.ordcust;
            zOrder.Header.HinhThucThu = 2;
            zOrder.Header.SoTien = OrderOrigin.traamt;
            zOrder.trdate = OrderOrigin.trdate;
            zOrder.Header.SectionNo = OrderOrigin.SectionNo;
            string zRemark = OrderOrigin.remark;
            for (int i = 0; i < _ListNeedRemove.Count; i++)
            {
                zRemark = zRemark.Replace(_ListNeedRemove[i], "");
            }
            zOrder.Remark = zRemark;

            return zOrder;
        }

        private bool CheckOrder(Order_Object OrderCheck)
        {
            Message_System("   -- Rule : " + OrderCheck.RuleID + "------");
            bool zError = false;
            bool zStatusDone = false;
            if (CheckOrder_MST_NNT_DVSDNS_TKThuNS(OrderCheck) > 0)
            {
                zError = true;
            }
            else
            {
                if (OrderCheck.Header.LoaiThue_ID == "04")
                {
                    if (CheckOrder_TK_NDK_LH(OrderCheck) > 0)
                        zError = true;

                }
            }
            if (!zError)
            {
                if (CheckOrderItems(OrderCheck) == 0)
                {
                    GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Rule : " + OrderCheck.RuleID + " Done";
                    _AmountSuccess++;
                    zStatusDone = true;
                }

            }
            return zStatusDone;
        }
        private int CheckOrder_MST_NNT_DVSDNS_TKThuNS(Order_Object OrderCheck)
        {
            // check mã số thuế, ngày nộp tiền, đơn vị sd ngân sách, tài khoản ngân sách
            int zResult = 0;
            string zMessage = "";
            if (OrderCheck.Header.MST.Length == 10 || OrderCheck.Header.MST.Length == 13)
            {
            }
            else
            {
                zMessage += "Mã số thuế/";
                zResult++;
            }
            if (OrderCheck.Header.NgayNT == null)
            {
                OrderCheck.Header.NgayNT = OrderCheck.trdate;
            }
            if (OrderCheck.Header.DVSDNS_ID.Length == 0)
            {
                zMessage += "Cơ quan sử dụng ngân sách/";
                zResult++;
            }
            if (OrderCheck.Header.TKThuNS_ID.Length == 0)
            {
                zMessage += "Tài khoản thu ngân sách/";
                zResult++;
            }
            if (OrderCheck.Header.DBHC_ID.Length == 0)
            {
                zMessage += "DBHC/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);


            return zResult;

        }
        private int CheckOrder_TK_NDK_LH(Order_Object OrderCheck)
        {
            // checjk tờ khai- ngày đăng kí- loại hình-- loại thuế 04
            int zResult = 0;
            string zMessage = "";
            if (OrderCheck.Header.LH_ID.Length != 3)
            {
                zMessage += "Loại Hình xuất nhập khẩu/";
                zResult++;
            }
            if (OrderCheck.Header.NgayDK == null)
            {
                //if (!Kiemtratruonghopdatbiet(OrderCheck.Remark.ToUpper())) // Em viet ham loai ra nhung truong hop dat biet
                {
                    OrderCheck.Header.NgayDK = OrderCheck.Header.NgayNT;
                }

                //zMessage += "Ngày đăng ký/";
                //zResult++;
            }
            if (OrderCheck.Header.ToKhai.Length == 0)
            {
                zMessage += "Tờ khai/";
                zResult++;
            }
            else
            {
                if(OrderCheck.Header.ToKhai.Length >11)
                {
                    OrderCheck.Header.ToKhai = OrderCheck.Header.ToKhai.Substring(0, 11);
                }
                if (OrderCheck.Header.SoTien % 20000 == 0 && OrderCheck.Header.SoTien > 0 && OrderCheck.Header.TKThuNS_ID == "3511")
                {
                    if (OrderCheck.Items.Count > 1)
                    {
                        if (OrderCheck.Items[0].NDKT_ID == "2663")
                        {
                            if (OrderCheck.Header.ToKhai != "9999999999")
                            {
                                zMessage += "CT nhiều Tờ khai/";
                                zResult++;
                            }
                        }
                    }

                }
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);


            return zResult;

        }
        private int CheckOrderItems(Order_Object OrderCheck)
        {
            Order_Detail_Info zDetail, zDetailTop;
            int zResult = 0;
            string zMessage = "";

            if (OrderCheck.Items.Count == 0)
            {
                zMessage += "0_ALL/";
                zResult++;
            }
            if (OrderCheck.Items.Count == 1)
            {
                zDetail = OrderCheck.Items[0];
                if (zDetail.SoTien == 0)
                    zDetail.SoTien = OrderCheck.Header.SoTien;
            }
            double zSumMoney = 0;
            for (int i = 0; i < OrderCheck.Items.Count; i++)
            {
                zDetail = OrderCheck.Items[i];
                zSumMoney += zDetail.SoTien;
                if (zDetail.ChuongID.Length == 0)
                {
                    if (i >= 1)
                    {
                        if (OrderCheck.Items[0].ChuongID.Length != 0)
                        {
                            zDetail.ChuongID = OrderCheck.Items[0].ChuongID;
                        }
                        else
                        {
                            zMessage += "C/";
                            zResult++;
                        }
                    }
                    else
                    {
                        if (OrderCheck.Remark.Contains("TNCN"))
                        {
                            OrderCheck.Items[0].ChuongID = "757";
                        }
                        else
                        {
                            zMessage += "C/";
                            zResult++;
                        }
                    }
                }

                if (zDetail.NDKT_ID.Length == 0)
                {
                    zMessage += "TM/";
                    zResult++;
                }
                else
                {
                    //2862,2863, 2864, 4944, 1601,1602,1603
                    if (zDetail.NDKT_ID == "2862" || zDetail.NDKT_ID == "2863" || zDetail.NDKT_ID == "2864" ||
                        zDetail.NDKT_ID == "4944" || zDetail.NDKT_ID == "1601" || zDetail.NDKT_ID == "1602" ||
                        zDetail.NDKT_ID == "1603")
                    {
                        zMessage += "TM ("+ zDetail.NDKT_ID + ") VÌ PHẢI ĐƯA VỀ PHƯỜNG/";
                        zResult++;
                    }
                }

                if (zDetail.NDKT_Name.Length == 0)
                {
                    zMessage += "TM_NAME/";
                    zResult++;
                }

                if (zDetail.SoTien == 0)
                {
                    zMessage += "ST/";
                    zResult++;
                }
                if (OrderCheck.Header.LoaiThue_ID == "01")
                {
                    if (zDetail.KyThue.Length == 0)
                    {
                        zDetail.KyThue = DateTime.Now.ToString("MM/yyyy");
                    }
                }



            }


            if (zSumMoney != OrderCheck.Header.SoTien)
            {
                zMessage += "TOTAL/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            return zResult;
        }
        private int CountCharSpecial(string Content, char CharSpecial)
        {
            int zResult = 0;
            for (int i = 0; i < Content.Length; i++)
            {
                if (Content[i] == CharSpecial)
                    zResult++;
            }
            return zResult;
        }



        #endregion

        private void btn_SaveFail_Click(object sender, EventArgs e)
        {
           
                _Index_Selected = 0;
                AnalysicOrder(0, "Normal");

            
        }
    }
}
