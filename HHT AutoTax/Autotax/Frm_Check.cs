﻿using HHT.BNK;
using HHT.Library.Bank;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HHT_AutoTax
{
    public partial class Frm_Check : Form
    {
        public Frm_Check()
        {
            InitializeComponent();
        }

        private void Frm_Check_Load(object sender, EventArgs e)
        {
            Picker_From.Value = DateTime.Now;
            DataTable zTable_SQL = Order_Data.List_CT(Picker_From.Value, Picker_From.Value);
            DataTable zTable_Excel = Order_Data.List_CT_Excel(Picker_From.Value, Picker_From.Value);
            InitListView_SQL(LV_DataSQL);
            LoadDataToListView_SQL(zTable_SQL);
            InitListView_Excel(LV_DataExcel);
            LoadDataToListView_Excel(zTable_Excel);
            InnitListView_Message(LV_Message);
            lbl_s3.Text = (int.Parse(lbl_s2.Text) - int.Parse(lbl_s1.Text)).ToString();
            cb_loai.SelectedIndex = 0;

        }
        private void InnitListView_Message(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số BT";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thông Báo";
            colHead.Width = 480;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitListView_SQL(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số BT";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày Tờ Khai";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại Hình";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);
        }


        private void InitListView_Excel(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại Hình";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Loại Hình";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Tên Công Ty";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);



            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }

        public void LoadDataToListView_SQL(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_DataSQL;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoBT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoCT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MST"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ToKhai"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NgayDK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LHID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zMoney = double.Parse(nRow["SoTien"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);


            }

            this.Cursor = Cursors.Default;
            lbl_s1.Text = n.ToString();
        }


        public void LoadDataToListView_Excel(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_DataExcel;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoCT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoTK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Ngay"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LHXNK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TenLH"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MaSoThue"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TenCTy"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TongTien"].ToString().Trim();
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);


            }

            this.Cursor = Cursors.Default;
            lbl_s2.Text = n.ToString();
        }

        private void btn_Import_Click(object sender, EventArgs e)
        {
            Frm_ImportFileWeb im = new Frm_ImportFileWeb();
            im.ShowDialog();
        }

        private void btn_View_Click(object sender, EventArgs e)
        {
            if (rd_auto.Checked)
            {
                double zMoneyfrom = 0;
                double zMoneyTo = 0;
                int zOrderCatergory = 0;

                double.TryParse(txt_MoneyFrom.Text, out zMoneyfrom);
                double.TryParse(txt_MoneyTo.Text, out zMoneyTo);
                zOrderCatergory = cb_loai.SelectedIndex;

                LoadDataToListView_SQL(Order_Data.List_CT(Picker_From.Value, Picker_From.Value, zMoneyfrom, zMoneyTo, zOrderCatergory));
                LoadDataToListView_Excel(Order_Data.List_Excel(Picker_From.Value, Picker_From.Value, zMoneyfrom, zMoneyTo, zOrderCatergory));

            }
            else
            {

            }


        }

        private void btn_Apply_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < LV_DataSQL.Items.Count; i++)
            {
                string zSCT = LV_DataSQL.Items[i].SubItems[2].Text;
                string zSBT = LV_DataSQL.Items[i].SubItems[1].Text;
                BNK_Order zOrder = new BNK_Order(zSCT);
                OrderWeb_Info zOrderWeb = new OrderWeb_Info(zSCT);

                string zMessage = "";
                if (zOrder.Ma_ST != zOrderWeb.MaSoThue)
                {
                    zMessage += "Vui Lòng Kiểm Tra Lại Mã Số Thuế;";
                }
                if (zOrder.ToKhai != zOrderWeb.SoTK)
                {
                    zMessage += "Vui Lòng Kiểm Tra Tờ Khai;";
                }
                if (zOrder.NgayDK != null && zOrderWeb.Ngay != null)
                {
                    DateTime zDate = DateTime.ParseExact(zOrderWeb.Ngay, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (zOrder.NgayDK != zDate && zOrderWeb.Ngay != null)
                    {
                        zMessage += "Vui Lòng Kiểm Tra Lại Ngày Đăng Ký;";
                    }
                }
                if (zOrder.LH_ID != zOrderWeb.LHXNK)
                {
                    zMessage += "Vui Lòng Kiểm Tra Lại LHXNK;";
                }
                if (zOrder.SoTien != zOrderWeb.TongTien)
                {
                    zMessage += "Vui Lòng Kiểm Tra Lại Số Chứng Từ;";
                }
                DataTable zTable1 = OrderTranfer_Data.List_SQL(zSCT);
                DataTable zTable2 = OrderTranfer_Data.List_Excel(zSCT);
                if (zTable1.Rows.Count == zTable2.Rows.Count)
                {
                    for (int j = 0; j < zTable1.Rows.Count; j++)
                    {

                        Check zCheck = new Check(zSCT);
                        Check_Web zCheckWeb = new Check_Web(zSCT);
                        if (zCheck.NDKT != zCheckWeb.NDKT)
                        {
                            zMessage += "Vui lòng kiểm tra lại  ";
                        }
                    }

                }
                else
                {
                    zMessage += "Vui lòng kiểm tra lại ";
                }
                if (zMessage.Length > 0)
                {
                    Add_Items_Message(zSCT, zMessage, zSBT);
                }
            }
            MessageBox.Show("Thành Công");
            this.Cursor = Cursors.Default;
        }

        private void Add_Items_Message(string SCT, string Message, string SBT)
        {
            ListView LV = LV_Message;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Text = (LV_Message.Items.Count + 1).ToString();

            lvi.ForeColor = Color.DarkBlue;
            lvi.BackColor = Color.White;

            lvi.ImageIndex = 0;

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = SCT;
            lvi.SubItems.Add(lvsi);


            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = SBT;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = Message;
            lvi.SubItems.Add(lvsi);

            LV.Items.Add(lvi);
        }

        private void LV_Message_ItemActivate(object sender, EventArgs e)
        {
            string SCT = LV_Message.SelectedItems[0].SubItems[1].Text;
            FrmCheckCT frm = new FrmCheckCT();
            frm._SCT = SCT;
            frm.ShowDialog();
        }

        private void btn_ImportIpiccas_Click(object sender, EventArgs e)
        {
            Frm_ImportIPICAS frm = new Frm_ImportIPICAS();
            frm.ShowDialog();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Frm_Check_SizeChanged(object sender, EventArgs e)
        {
            PN_right.Width = this.Width / 2;
        }
    }
}
