﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;

namespace HHT_AutoTax
{
    public partial class Frm_ImportDataCheck_V2 : Form
    {
        public Frm_ImportDataCheck_V2()
        {
            InitializeComponent();
        }

        private void Frm_ImportDataCheck_V2_Load(object sender, EventArgs e)
        {
            InitListView_Left(LV_Data_Web);
            InitListView_Right(LV_IPCAS);

            InitListView_Left(LV_ItemMutil);
            InitLV_Report(LV_Report);

            InitListView_Left(LV_Web_Fail);
            InitListView_Right(LV_IPCAS_Fail);

            InitListView_Left(LV_Data_Web_Copy);
            InitListView_Right(LV_IPCAS_Copy);

            InitListView_CheckVer2(LV_Check_Ver2);
        }
        private void InitListView_Left(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CQThu";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Loại Thuế";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tờ Khai";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày TK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "LHXNK";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Người Nộp Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "NVNL";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitListView_Right(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Số BT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "tomgntno";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày KB";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TK NSNN";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "cnclflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "cncltrflg";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Status";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "StatusCheck";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }
        private void InitLV_Report(ListView LV)
        {
            ColumnHeader colHead;

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "IPCAS";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "AgriTax";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Kết Quả";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phân Tích";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Số Lượng AuTo";
            //colHead.Width = 120;
            //colHead.TextAlign = HorizontalAlignment.Center;
            //LV.Columns.Add(colHead);

            for (int i = 0; i < 50; i++)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Text = "...";

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);
            }
        }
        private void btn_ImportAgriTax_Click(object sender, EventArgs e)
        {
            Frm_ImportFileWeb frm = new Frm_ImportFileWeb();
            frm.ShowDialog();
        }

        private void Frm_ImportDataCheck_V2_SizeChanged(object sender, EventArgs e)
        {
            LV_Data_Web.Width = this.Width / 2 - panel1.Width / 2;
            LV_Data_Web_Copy.Width = this.Width / 2 - panel1.Width / 2;
            LV_Web_Fail.Width = this.Width / 2 - panel1.Width / 2;
        }
        private void LoadDataToListView_Left(ListView LV, DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            //_LePhiHQ_Excel = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MaSoThue"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoCT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zMoney = double.Parse(nRow["TongTien"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NgayKB"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                string zTNNSNN = nRow["TKNSNN"].ToString().Trim();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTNNSNN;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MaCQT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LoaiThue"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoTK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NgayTK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LHXNK"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TenCTy"].ToString().Trim().ToUpper();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MANV"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);

                if (zMoney % 20000 == 0 && zTNNSNN == "3511")
                {
                    //_LePhiHQ_Excel++;
                }

            }



            this.Cursor = Cursors.Default;
        }
        private void LoadDataToListView_Right(ListView LV, DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            //_AmountIPCAS = n;
            //_LePhiHQ_IPCAS = 0;
            //_MoneyIPCAS = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["Rem"].ToString().Trim();
                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["tomgntno"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zMoney = double.Parse(nRow["trccyamt"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                //  DateTime zTrdate = (DateTime)nRow["trdt"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["trdt"].ToString();
                lvi.SubItems.Add(lvsi);

                string zTNNSNN = nRow["trcdnm"].ToString().Trim();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTNNSNN;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["cnclflg"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["cncltrflg"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);

                if (zMoney % 20000 == 0)
                {
                    // _LePhiHQ_IPCAS++;
                }
                // _MoneyIPCAS += zMoney;

                //if (nRow["So_CT"].ToString().Trim().Length > 0)
                //    _AmountAuto_IPICAS++;
            }

            this.Cursor = Cursors.Default;
        }

        private void DayForOrder_DateSelected(object sender, DateRangeEventArgs e)
        {
            DateTime zDate = DayForOrder.SelectionStart;
            DataTable zTable = CheckOrder_Data.OrderOnWeb(zDate);
            LoadDataToListView_Left(LV_Data_Web, zTable);

            zTable = CheckOrder_Data.IPCAS_(zDate);
            LoadDataToListView_Right(LV_IPCAS, zTable);

            zTable = CheckOrder_Data.OrderOnWeb_ItemMutil(zDate);
            LoadDataToListView_Left(LV_ItemMutil, zTable);

            FillDataCash();
            FillData_6280ITL();
            Fill_LCT();
        }
        //---------------------------
        private int _CashAmount = 0;
        private double _CashToTal = 0;
        private int _CashAmount_63280ITL = 0;
        private double _CashToTal_6280ITL = 0;
        private ArrayList MST_SoCT(string Rem)
        {
            ArrayList zResult = new ArrayList();
            string zCode1 = "MST:";
            string zCode2 = "-SO CT:";
            int zIndexMST = Rem.IndexOf(zCode1);
            int zIndexSoCT = Rem.IndexOf(zCode2);
            if (zIndexMST > 0 && zIndexSoCT > 0)
            {
                string zMST = Rem.Substring(zIndexMST + zCode1.Length, zIndexSoCT - (zIndexMST + zCode1.Length)).Trim();
                string zSoCT = Rem.Substring(zIndexSoCT + zCode2.Length, Rem.Length - (zIndexSoCT + zCode2.Length + 1)).Trim();

                if (zMST.Length == 10 || zMST.Length == 13)
                    zResult.Add(zMST);
                if (zSoCT.Length == 7)
                    zResult.Add(zSoCT);
            }
            return zResult;
        }
        private void FillDataCash()
        {
            _CashAmount = 0;
            _CashToTal = 0;
            string zSoCT_IPCAST;
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                zSoCT_IPCAST = LV_IPCAS.Items[i].SubItems[2].Text.Trim();
                if (zSoCT_IPCAST.Length == 0)
                {
                    ArrayList zList = MST_SoCT(LV_IPCAS.Items[i].Tag.ToString());
                    if (zList.Count == 2)
                    {

                        LV_IPCAS.Items[i].SubItems[1].Text = zList[0].ToString();
                        LV_IPCAS.Items[i].SubItems[2].Text = zList[1].ToString();
                        LV_IPCAS.Items[i].SubItems[10].Text = "1";
                        LV_IPCAS.Items[i].ForeColor = Color.Green;
                        _CashAmount++;
                        _CashToTal += double.Parse(LV_IPCAS.Items[i].SubItems[5].Text);
                    }
                }

            }
        }
        private void FillData_6280ITL()
        {
            _CashAmount_63280ITL = 0;
            _CashToTal_6280ITL = 0;
            string z6280ITL;
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                if (LV_IPCAS.Items[i].SubItems[4].Text.Length >= 7)
                {
                    z6280ITL = LV_IPCAS.Items[i].SubItems[4].Text.Trim().Substring(0, 7);

                    if (z6280ITL == "6280ITL")
                    {
                        LV_IPCAS.Items[i].SubItems[10].Text = "2";
                        _CashAmount_63280ITL++;
                        _CashToTal_6280ITL += double.Parse(LV_IPCAS.Items[i].SubItems[5].Text);
                    }
                }

            }
        }
        private void Fill_LCT()
        {
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                string ztrref = LV_IPCAS.Items[i].SubItems[4].Text;
                string zstatus = LV_IPCAS.Items[i].SubItems[10].Text;
                if (zstatus == "2")
                {
                    Order_Info zOrderInfo = new Order_Info(ztrref);
                    LV_IPCAS.Items[i].SubItems[1].Text = zOrderInfo.MST;
                    LV_IPCAS.Items[i].SubItems[2].Text = zOrderInfo.So_CT;
                    LV_IPCAS.Items[i].SubItems[3].Text = zOrderInfo.So_BT;
                    LV_IPCAS.Items[i].Tag = zOrderInfo;
                    if (zOrderInfo.MST.Length == 0)
                    {
                        LV_IPCAS.Items[i].SubItems[1].Text = OrderTranfer_Data.GetMST(ztrref);
                    }
                }

            }
        }

        private void btn_BeginCheck_Click(object sender, EventArgs e)
        {
            lbl_AmountCheck.Text = "0";
            timer1.Start();

        }
        private int _IndexBegin = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            string zSoCT_Web = LV_Data_Web.Items[_IndexBegin].SubItems[2].Text.Trim();
            string zSoCT_SQL = "";
            for (int i = 0; i < LV_IPCAS.Items.Count; i++)
            {
                string zMessage = "";
                zSoCT_SQL = LV_IPCAS.Items[i].SubItems[2].Text.Trim();
                string zStatus = LV_IPCAS.Items[i].SubItems[10].Text.Trim();
                if (zSoCT_SQL == zSoCT_Web)
                {
                    LV_IPCAS.Items[i].SubItems[0].Text = _IndexBegin.ToString();
                    LV_Data_Web.Items[_IndexBegin].SubItems[13].Text = "1";
                    LV_IPCAS.Items[i].SubItems[10].Text = "1";
                    bool zCheckFail = false;
                    if (zStatus == "1")
                    {
                        if (LV_Data_Web.Items[_IndexBegin].SubItems[3].Text != LV_IPCAS.Items[i].SubItems[5].Text)
                        {
                            zCheckFail = true;
                            zMessage = "TOTAL/";
                        }
                    }
                    else
                    {
                        Order_Check zOrderCheck = new Order_Check(LV_IPCAS.Items[i].SubItems[2].Text.Trim());

                        if (LV_Data_Web.Items[_IndexBegin].SubItems[1].Text != zOrderCheck.Ma_ST)
                        {
                            if (zOrderCheck.Ma_ST.Length == 13)
                            {
                                string zMST = zOrderCheck.Ma_ST.Substring(0, 10) + "-" + zOrderCheck.Ma_ST.Substring(10, 3);
                                if (zMST != LV_Data_Web.Items[_IndexBegin].SubItems[1].Text)
                                {
                                    zCheckFail = true;
                                    zMessage = "MST/";
                                }
                            }
                            else
                            {
                                if (LV_Data_Web.Items[_IndexBegin].SubItems[1].Text != zOrderCheck.Ma_ST)
                                {
                                    zCheckFail = true;
                                    zMessage = "MST/";
                                }
                            }
                        }
                        if (LV_Data_Web.Items[_IndexBegin].SubItems[3].Text != zOrderCheck.SoTien.ToString("#,###,###"))
                        {
                            zCheckFail = true;
                            zMessage = "MST/";
                        }
                        if (LV_Data_Web.Items[_IndexBegin].SubItems[5].Text != zOrderCheck.TK_ThuNS)
                        {
                            zCheckFail = true;
                            zMessage = "TK_ThuNS/";
                        }
                        if (LV_Data_Web.Items[_IndexBegin].SubItems[6].Text != zOrderCheck.CQThuID)
                        {
                            zCheckFail = true;
                            zMessage = "CQThuID/";
                        }
                        if (LV_Data_Web.Items[_IndexBegin].SubItems[8].Text != zOrderCheck.ToKhai)
                        {
                            zCheckFail = true;
                            zMessage = "ToKhai/";
                        }
                        if (LV_Data_Web.Items[_IndexBegin].SubItems[10].Text != zOrderCheck.LH_ID)
                        {
                            zCheckFail = true;
                            zMessage = "LH_ID/";
                        }
                    }
                    if (zCheckFail == true)
                    {
                        Add_Items_Fail(LV_Web_Fail, LV_Data_Web.Items[_IndexBegin], "");
                        Add_Items_Fail(LV_IPCAS_Fail, LV_IPCAS.Items[i], zMessage);
                    }

                    int zAmount = int.Parse(lbl_AmountCheck.Text);
                    zAmount++;

                    lbl_AmountCheck.Text = zAmount.ToString("#,###");
                    // break;
                }
            }


            _IndexBegin++;
            if (_IndexBegin < LV_Data_Web.Items.Count)
                timer1.Start();
            else
            {
                for (int i = 0; i < LV_Data_Web.Items.Count; i++)
                {
                    if (LV_Data_Web.Items[i].SubItems[13].Text.Trim().Length == 0)
                    {
                        Add_Items_Fail(LV_Data_Web_Copy, LV_Data_Web.Items[i], "");
                    }
                }
                for (int i = 0; i < LV_IPCAS.Items.Count; i++)
                {
                    if (LV_IPCAS.Items[i].SubItems[11].Text.Trim().Length == 0 && LV_IPCAS.Items[i].SubItems[10].Text != "99")
                    {
                        Add_Items_Fail(LV_IPCAS_Copy, LV_IPCAS.Items[i], "");
                    }
                }
                MessageBox.Show("Done");
            }
        }
        private void Add_Items_Fail(ListView LV, ListViewItem Item_Fail, string MessageErr)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Text = (LV.Items.Count + 1).ToString();
            for (int i = 1; i < Item_Fail.SubItems.Count; i++)
            {
                lvsi = new ListViewItem.ListViewSubItem();
                if (MessageErr.Length > 0)
                {
                    if (i == Item_Fail.SubItems.Count - 1)
                        lvsi.Text = MessageErr;
                    else
                        lvsi.Text = Item_Fail.SubItems[i].Text;
                }
                else
                    lvsi.Text = Item_Fail.SubItems[i].Text;
                lvi.SubItems.Add(lvsi);

            }

            LV.Items.Add(lvi);
        }

        private int _AmountDone = 0;
        private void btn_FillCheck_Click(object sender, EventArgs e)
        {
            _IndexBegin = 0;
            _AmountDone = 0;
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Stop();
            string zMST = LV_Data_Web_Copy.Items[_IndexBegin].SubItems[1].Text.Trim();
            string zMST_SQL = "";
            bool zFound = false;
            for (int i = 0; i < LV_IPCAS_Copy.Items.Count; i++)
            {
                string zMessage = "";
                zMST_SQL = LV_IPCAS_Copy.Items[i].SubItems[1].Text.Trim();
                //string zStatus = LV_IPCAS.Items[i].SubItems[9].Text.Trim();
                if (zMST == zMST_SQL)
                {


                    LV_IPCAS_Copy.Items[i].Remove();
                    LV_Data_Web_Copy.Items[_IndexBegin].Remove();
                    zFound = true;

                    _AmountDone++;
                    break;
                }
            }
            if (!zFound)
                _IndexBegin++;
            lbl_AmountCheck.Text = _AmountDone.ToString("#,###");
            if (_IndexBegin < LV_Data_Web_Copy.Items.Count)
                timer2.Start();
            else
            {
                MessageBox.Show("Done");
            }
        }
        //----------------------------

        private void btn_ImportIPICAS_Click(object sender, EventArgs e)
        {
            Frm_ImportIPICAS zIPCAS = new Frm_ImportIPICAS();
            zIPCAS.ShowDialog();
        }
        //----------------------------

        //------ CHECK CHẠY LẦN 2 
        private void InitListView_CheckVer2(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số BT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Tiền";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);
        }

        private void LoadDataToListView_Check(ListView LV, DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            //_LePhiHQ_Excel = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["MST"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoCT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["SoBT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double zSoTien = double.Parse(nRow["SoTien"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zSoTien.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);
            }



            this.Cursor = Cursors.Default;
        }

        private void btn_View_Click(object sender, EventArgs e)
        {
            DataTable zTable = Order_Data.CheckVer2(Picker_From.Value,Picker_To.Value);
            LoadDataToListView_Check(LV_Check_Ver2,zTable);
        }
    }
}
