﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using OfficeOpenXml;
using HHT.Library.System;
using HHT.Library.Bank;

namespace HHT_AutoTax
{
    public partial class Frm_Import_Excel : Form
    {
        private bool _IsPostback;
        public Frm_Import_Excel()
        {
            InitializeComponent();
        }

        private void Frm_Import_Excel_Load(object sender, EventArgs e)
        {
            txt_MaxSesion.Text = OrderTranfer_Data.MaxSection().ToString();
            LoadDataToToolbox.ComboBoxData(CoUsers, "SELECT UserKey,FullName FROM SYS_User WHERE SoftWareKey = 1 ", "---- Choose User ----");
        }

        private void btn_Often_Click(object sender, EventArgs e)
        {
            _IsPostback = false;
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txt_Excel.Text = zOpf.FileName;
                    System.IO.FileInfo zFileImport = new System.IO.FileInfo(zOpf.FileName);
                    txt_Excel.Tag = zFileImport.Name;
                    ExcelPackage package = new OfficeOpenXml.ExcelPackage(zFileImport);

                    foreach (var sheet in package.Workbook.Worksheets)
                    {
                        cbo_Sheet.Items.Add(sheet.Name);
                    }
                    package.Dispose();

                    cbo_Sheet.SelectedIndex = 0;
                    DataTable zTable = Import_Excel.ToTable(txt_Excel.Text, cbo_Sheet.Text);
                    LoadDataListView_Excel(zTable);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            _IsPostback = true;
        }
        void LoadDataListView_Excel(DataTable InTable)
        {
            this.Cursor = Cursors.WaitCursor;
            list_Name.Items.Clear();
            list_Name.Columns.Clear();

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            list_Name.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Message";
            colHead.Width = 100;
            list_Name.Columns.Add(colHead);

            DataRow rHead = InTable.Rows[0];
            for (int i = 0; i < InTable.Columns.Count; i++)
            {
                colHead = new ColumnHeader();
                colHead.Text = rHead[i].ToString();
                colHead.Width = 100;
                list_Name.Columns.Add(colHead);
            }

            for (int i = 1; i < InTable.Rows.Count; i++)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;
                lvi = new ListViewItem();
                lvi.Text = i.ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                for (int o = 0; o < InTable.Columns.Count; o++)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = InTable.Rows[i][o].ToString().Trim();
                    lvi.SubItems.Add(lvsi);
                }

                list_Name.Items.Add(lvi);
            }
            txt_Amount_Excel.Text = (InTable.Rows.Count - 1).ToString();
            this.Cursor = Cursors.Default;
        }
        private void cbo_Sheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_IsPostback)
            {
                DataTable zTable = Import_Excel.ToTable(txt_Excel.Text, cbo_Sheet.Text);
                LoadDataListView_Excel(zTable);
            }
        }

        private void btn_Up_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string SQL = "";
            for (int i = 0; i < list_Name.Items.Count; i++)
            {
                try
                {
                    ListViewItem item = list_Name.Items[i];
                    string trref = item.SubItems[2].Text;
                    DateTime trdate = DateTime.Parse(item.SubItems[3].Text);
                    string traccy = item.SubItems[4].Text;
                    string traamt = item.SubItems[5].Text;
                    string usdamt = item.SubItems[6].Text;
                    string ordcust = item.SubItems[7].Text;
                    string bencust = item.SubItems[8].Text;
                    string benacct = item.SubItems[9].Text;
                    string remtbk = item.SubItems[10].Text;
                    string rmbsbk = item.SubItems[11].Text;
                    string trfbk = item.SubItems[12].Text;
                    string custno = item.SubItems[13].Text;
                    string traaccd = item.SubItems[14].Text;
                    string traacbl = item.SubItems[15].Text;
                    string lchaccd = item.SubItems[16].Text;
                    string lchacbl = item.SubItems[17].Text;
                    string rtnaccd = item.SubItems[18].Text;
                    string rtnacbl = item.SubItems[19].Text;
                    string thrref = item.SubItems[20].Text;
                    DateTime remtdt = DateTime.Parse(item.SubItems[21].Text);
                    string notibrcd = item.SubItems[22].Text;
                    string dtlchg = item.SubItems[23].Text;
                    string ddno = item.SubItems[24].Text;
                    string trsrtcd = item.SubItems[25].Text;
                    string bopid = item.SubItems[26].Text;
                    string bopcd = item.SubItems[27].Text;
                    string cpcntry = item.SubItems[28].Text;
                    string openflg = item.SubItems[29].Text;
                    string closflg = item.SubItems[30].Text;
                    string paidflg = item.SubItems[31].Text;
                    string obkflg = item.SubItems[32].Text;
                    string lchacfg = item.SubItems[33].Text;
                    string rtnacfg = item.SubItems[34].Text;
                    string autoflag = item.SubItems[35].Text;
                    string speedfg = item.SubItems[36].Text;
                    string remark = item.SubItems[37].Text;
                    string subunit = item.SubItems[38].Text;
                    string domestic = item.SubItems[39].Text;
                    string oder = item.SubItems[40].Text;
                    string traaccd_n = item.SubItems[41].Text;
                    string remntbk = item.SubItems[42].Text;
                    string rembk1 = item.SubItems[43].Text;
                    string feebk = item.SubItems[44].Text;
                    string bustp = item.SubItems[45].Text;
                    string fee_flag = item.SubItems[46].Text;
                    string reference = item.SubItems[47].Text;
                    string tax_code = item.SubItems[48].Text;
                    string name_1 = item.SubItems[49].Text;
                    string name_2 = item.SubItems[50].Text;
                    string name_3 = item.SubItems[51].Text;
                    string name_4 = item.SubItems[52].Text;
                    string name_5 = item.SubItems[53].Text;
                    string name_6 = item.SubItems[54].Text;
                    string name_7 = item.SubItems[55].Text;
                    string name_8 = item.SubItems[56].Text;
                    string name_9 = item.SubItems[57].Text;
                    string name_10 = item.SubItems[58].Text;
                    string name_11 = item.SubItems[59].Text;
                    string name_12 = item.SubItems[60].Text;
                    string name_13 = item.SubItems[61].Text;
                    string name_14 = item.SubItems[62].Text;
                    string name_15 = item.SubItems[63].Text;
                    string name_16 = item.SubItems[64].Text;

                    //insert
                    SQL = "INSERT BNK_OrderTranfer ";
                    SQL += " (trref," +
                        "trdate ," +
                        "traccy ," +
                        "traamt ," +
                        "usdamt ," +
                        "ordcust ," +
                        "bencust ," +
                        "benacct ," +
                        "remtbk ," +
                        "rmbsbk ," +
                        "trfbk ," +
                        "custno ," +
                        "traaccd ," +
                        "traacbl," +
                        "lchaccd," +
                        "lchacbl," +
                        "rtnaccd," +
                        "rtnacbl," +
                        "thrref ," +
                        "remtdt ," +
                        "notibrcd," +
                        "dtlchg," +
                        "ddno," +
                        "trsrtcd," +
                        "bopid," +
                        "bopcd," +
                        "cpcntry," +
                        "openflg," +
                        "closflg," +
                        "paidflg," +
                        "obkflg," +
                        "lchacfg," +
                        "rtnacfg," +
                        "autoflag," +
                        "speedfg," +
                        "remark ," +
                        "subunit," +
                        "domestic," +
                        "oder," +
                        "traaccd_n," +
                        "remntbk ," +
                        "rembk1 ," +
                        "feebk ," +
                        "bustp," +
                        "fee_flag," +
                        "reference," +
                        "tax_code," +
                        "name_1 ," +
                        "name_2 ," +
                        "name_3 ," +
                        "name_4 ," +
                        "name_5 ," +
                        "name_6 ," +
                        "name_7 ," +
                        "name_8 ," +
                        "name_9 ," +
                        "name_10 ," +
                        "name_11 ," +
                        "name_12 ," +
                        "name_13 ," +
                        "name_14 ," +
                        "name_15 ," +
                        "name_16 ," +
                        "SectionNo ) ";

                    SQL += " VALUES ";
                    SQL += " ('" + trref + "','"
                        + trdate.ToString("yyyy-MM-dd") + "','"
                        + traccy + "','"
                        + traamt + "','"
                        + usdamt + "','"
                        + ordcust + "','"
                        + bencust + "','"
                        + benacct + "','"
                        + remtbk + "','"
                        + rmbsbk + "','"
                        + trfbk + "','"
                        + custno + "','"
                        + traaccd + "','"
                        + traacbl + "','"
                        + lchaccd + "','"
                        + lchacbl + "','"
                        + rtnaccd + "','"
                        + rtnacbl + "','"
                        + thrref + "','"
                        + remtdt.ToString("yyyy-MM-dd") + "','"
                        + notibrcd + "','"
                        + dtlchg + "','"
                        + ddno + "','"
                        + trsrtcd + "','"
                        + bopid + "','"
                        + bopcd + "','"
                        + cpcntry + "','"
                        + openflg + "','"
                        + closflg + "','"
                        + paidflg + "','"
                        + obkflg + "','"
                        + lchacfg + "','"
                        + rtnacfg + "','"
                        + autoflag + "','"
                        + speedfg + "','"
                        + remark + "',N'"
                        + subunit + "','"
                        + domestic + "','"
                        + oder + "','"
                        + traaccd_n + "',N'"
                        + remntbk + "','"
                        + rembk1 + "','"
                        + feebk + "','"
                        + bustp + "','"
                        + fee_flag + "','"
                        + reference + "','"
                        + tax_code + "','"
                        + name_1 + "','"
                        + name_2 + "','"
                        + name_3 + "','"
                        + name_4 + "','"
                        + name_5 + "','"
                        + name_6 + "','"
                        + name_7 + "','"
                        + name_8 + "','"
                        + name_9 + "','"
                        + name_10 + "','"
                        + name_11 + "','"
                        + name_12 + "','"
                        + name_13 + "','"
                        + name_14 + "','"
                        + name_15 + "','"
                        + name_16 + "','"
                        + txt_MaxSesion.Text + "') ";

                    string zMessage = Data_Access.InsertToTable(SQL);

                    if (zMessage.Length == 0)
                    {
                        list_Name.Items[i].SubItems[1].Text = "Thành công";
                    }
                    else
                    {
                        list_Name.Items[i].BackColor = Color.Red;
                        if (zMessage.ToLower().Contains("cannot insert duplicate key"))
                            list_Name.Items[i].SubItems[1].Text = "Đã đưa lệnh này vào rồi";
                        else
                            list_Name.Items[i].SubItems[1].Text = zMessage;
                    }
                }
                catch (Exception ex)
                {
                    list_Name.Items[i].SubItems[1].Text = ex.ToString();
                }
            }

            txt_Amout_SQL.Text = OrderTranfer_Data.AmountSection(int.Parse(txt_MaxSesion.Text)).ToString();
            if (txt_Amout_SQL.Text != "0")
            {
                SectionImport_Info zSection = new SectionImport_Info(int.Parse(txt_MaxSesion.Text));
                zSection.SectionNo = int.Parse(txt_MaxSesion.Text);
                zSection.AmountData = int.Parse(txt_Amout_SQL.Text);
                zSection.FileName = txt_Excel.Tag.ToString() + "-" + cbo_Sheet.Text;
                zSection.UserKey = CoUsers.SelectedValue.ToString();
                zSection.Save();
            }
            this.Cursor = Cursors.Default;
        }

    }
}
