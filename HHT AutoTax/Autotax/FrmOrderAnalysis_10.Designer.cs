﻿namespace HHT_AutoTax
{
    partial class FrmOrderAnalysis_10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.GV_DataDetail = new System.Windows.Forms.DataGridView();
            this.label35 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_LHXNK_Name = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue_Name = new System.Windows.Forms.TextBox();
            this.txt_CQQLThu_Name = new System.Windows.Forms.TextBox();
            this.txt_DBHC_Name = new System.Windows.Forms.TextBox();
            this.txt_LHXNK = new System.Windows.Forms.TextBox();
            this.txt_DVSDNS_Name = new System.Windows.Forms.TextBox();
            this.txt_TK_Name = new System.Windows.Forms.TextBox();
            this.txt_NgayDk = new System.Windows.Forms.TextBox();
            this.txt_NguoiNT = new System.Windows.Forms.TextBox();
            this.txt_ToKhaiSo = new System.Windows.Forms.TextBox();
            this.txt_NH_PhucVu_Name = new System.Windows.Forms.TextBox();
            this.txt_LoaiThue = new System.Windows.Forms.TextBox();
            this.txt_CQQLThu_ID = new System.Windows.Forms.TextBox();
            this.txt_DVSDNS_ID = new System.Windows.Forms.TextBox();
            this.txt_DBHC_ID = new System.Windows.Forms.TextBox();
            this.txt_TK_Thu_NS = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.txt_MaSoThue = new System.Windows.Forms.TextBox();
            this.txt_NgayNT = new System.Windows.Forms.TextBox();
            this.txt_NH_PhucVu = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbo_HinhThucThu = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_NHNN_Name = new System.Windows.Forms.TextBox();
            this.txt_NHNN_ID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Save = new System.Windows.Forms.Button();
            this.txt_SoChungTu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GV_Analysis = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txt_remark = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LB_Log = new System.Windows.Forms.ListBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txt_ItemRepeat = new System.Windows.Forms.TextBox();
            this.txt_GroupSort = new System.Windows.Forms.TextBox();
            this.btn_App_Group = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Panel_Hide = new System.Windows.Forms.Panel();
            this.btn_Hide = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.chkRule8000 = new System.Windows.Forms.CheckBox();
            this.chkRule6000 = new System.Windows.Forms.CheckBox();
            this.chkRule3000 = new System.Windows.Forms.CheckBox();
            this.chkRule1000 = new System.Windows.Forms.CheckBox();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Total_Record = new System.Windows.Forms.Label();
            this.lbl_Record_Error = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_Record_Success = new System.Windows.Forms.Label();
            this.PanelLeftBottom = new System.Windows.Forms.Panel();
            this.btn_SaveFail = new System.Windows.Forms.Button();
            this.btn_Apply = new System.Windows.Forms.Button();
            this.GV_ListOrder = new System.Windows.Forms.DataGridView();
            this.Timer_Auto = new System.Windows.Forms.Timer(this.components);
            this.Panel_Right.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_DataDetail)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Analysis)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.Panel_Hide.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.PanelLeftBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_ListOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.White;
            this.Panel_Right.Controls.Add(this.tabControl1);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(660, 0);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(572, 551);
            this.Panel_Right.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(572, 551);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(564, 525);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lệnh CT";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.GV_DataDetail);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 481);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(541, 146);
            this.panel1.TabIndex = 7;
            // 
            // GV_DataDetail
            // 
            this.GV_DataDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_DataDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_DataDetail.Location = new System.Drawing.Point(0, 22);
            this.GV_DataDetail.Name = "GV_DataDetail";
            this.GV_DataDetail.Size = new System.Drawing.Size(541, 124);
            this.GV_DataDetail.TabIndex = 5;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.LightGray;
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(541, 22);
            this.label35.TabIndex = 4;
            this.label35.Text = "Thông Tin Chi Tiết";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.txt_LHXNK_Name);
            this.panel4.Controls.Add(this.textBox29);
            this.panel4.Controls.Add(this.txt_LoaiThue_Name);
            this.panel4.Controls.Add(this.txt_CQQLThu_Name);
            this.panel4.Controls.Add(this.txt_DBHC_Name);
            this.panel4.Controls.Add(this.txt_LHXNK);
            this.panel4.Controls.Add(this.txt_DVSDNS_Name);
            this.panel4.Controls.Add(this.txt_TK_Name);
            this.panel4.Controls.Add(this.txt_NgayDk);
            this.panel4.Controls.Add(this.txt_NguoiNT);
            this.panel4.Controls.Add(this.txt_ToKhaiSo);
            this.panel4.Controls.Add(this.txt_NH_PhucVu_Name);
            this.panel4.Controls.Add(this.txt_LoaiThue);
            this.panel4.Controls.Add(this.txt_CQQLThu_ID);
            this.panel4.Controls.Add(this.txt_DVSDNS_ID);
            this.panel4.Controls.Add(this.txt_DBHC_ID);
            this.panel4.Controls.Add(this.txt_TK_Thu_NS);
            this.panel4.Controls.Add(this.textBox14);
            this.panel4.Controls.Add(this.txt_MaSoThue);
            this.panel4.Controls.Add(this.txt_NgayNT);
            this.panel4.Controls.Add(this.txt_NH_PhucVu);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(3, 155);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(541, 326);
            this.panel4.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ngày Nộp Thuế";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 95);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Địa chỉ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 300);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 15);
            this.label25.TabIndex = 0;
            this.label25.Text = "Loại Hình XNK";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 274);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 15);
            this.label24.TabIndex = 0;
            this.label24.Text = "Ngày DK";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 248);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 15);
            this.label23.TabIndex = 0;
            this.label23.Text = "Tờ Khai Số";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 222);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 15);
            this.label22.TabIndex = 0;
            this.label22.Text = "Loại Thuế";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 196);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 15);
            this.label21.TabIndex = 0;
            this.label21.Text = "CQ QL Thu";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 169);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "Mã DBHC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "DVSDNS";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 15);
            this.label19.TabIndex = 0;
            this.label19.Text = "TK Thu NS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã Số Thuế";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "NH Phục Vụ NNT";
            // 
            // txt_LHXNK_Name
            // 
            this.txt_LHXNK_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LHXNK_Name.Location = new System.Drawing.Point(205, 297);
            this.txt_LHXNK_Name.Name = "txt_LHXNK_Name";
            this.txt_LHXNK_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_LHXNK_Name.TabIndex = 1;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox29.Location = new System.Drawing.Point(205, 245);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(259, 21);
            this.textBox29.TabIndex = 1;
            // 
            // txt_LoaiThue_Name
            // 
            this.txt_LoaiThue_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue_Name.Location = new System.Drawing.Point(205, 219);
            this.txt_LoaiThue_Name.Name = "txt_LoaiThue_Name";
            this.txt_LoaiThue_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_LoaiThue_Name.TabIndex = 1;
            // 
            // txt_CQQLThu_Name
            // 
            this.txt_CQQLThu_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_CQQLThu_Name.Location = new System.Drawing.Point(205, 193);
            this.txt_CQQLThu_Name.Name = "txt_CQQLThu_Name";
            this.txt_CQQLThu_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_CQQLThu_Name.TabIndex = 1;
            // 
            // txt_DBHC_Name
            // 
            this.txt_DBHC_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_Name.Location = new System.Drawing.Point(205, 166);
            this.txt_DBHC_Name.Name = "txt_DBHC_Name";
            this.txt_DBHC_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_DBHC_Name.TabIndex = 1;
            // 
            // txt_LHXNK
            // 
            this.txt_LHXNK.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LHXNK.Location = new System.Drawing.Point(110, 297);
            this.txt_LHXNK.Name = "txt_LHXNK";
            this.txt_LHXNK.Size = new System.Drawing.Size(98, 21);
            this.txt_LHXNK.TabIndex = 1;
            // 
            // txt_DVSDNS_Name
            // 
            this.txt_DVSDNS_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DVSDNS_Name.Location = new System.Drawing.Point(205, 141);
            this.txt_DVSDNS_Name.Name = "txt_DVSDNS_Name";
            this.txt_DVSDNS_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_DVSDNS_Name.TabIndex = 1;
            // 
            // txt_TK_Name
            // 
            this.txt_TK_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_TK_Name.Location = new System.Drawing.Point(205, 116);
            this.txt_TK_Name.Name = "txt_TK_Name";
            this.txt_TK_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_TK_Name.TabIndex = 1;
            // 
            // txt_NgayDk
            // 
            this.txt_NgayDk.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NgayDk.Location = new System.Drawing.Point(110, 271);
            this.txt_NgayDk.Name = "txt_NgayDk";
            this.txt_NgayDk.Size = new System.Drawing.Size(354, 21);
            this.txt_NgayDk.TabIndex = 1;
            // 
            // txt_NguoiNT
            // 
            this.txt_NguoiNT.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NguoiNT.Location = new System.Drawing.Point(205, 62);
            this.txt_NguoiNT.Name = "txt_NguoiNT";
            this.txt_NguoiNT.Size = new System.Drawing.Size(259, 21);
            this.txt_NguoiNT.TabIndex = 1;
            // 
            // txt_ToKhaiSo
            // 
            this.txt_ToKhaiSo.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_ToKhaiSo.Location = new System.Drawing.Point(110, 245);
            this.txt_ToKhaiSo.Name = "txt_ToKhaiSo";
            this.txt_ToKhaiSo.Size = new System.Drawing.Size(98, 21);
            this.txt_ToKhaiSo.TabIndex = 1;
            // 
            // txt_NH_PhucVu_Name
            // 
            this.txt_NH_PhucVu_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NH_PhucVu_Name.Location = new System.Drawing.Point(205, 8);
            this.txt_NH_PhucVu_Name.Name = "txt_NH_PhucVu_Name";
            this.txt_NH_PhucVu_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_NH_PhucVu_Name.TabIndex = 1;
            // 
            // txt_LoaiThue
            // 
            this.txt_LoaiThue.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_LoaiThue.Location = new System.Drawing.Point(110, 219);
            this.txt_LoaiThue.Name = "txt_LoaiThue";
            this.txt_LoaiThue.Size = new System.Drawing.Size(98, 21);
            this.txt_LoaiThue.TabIndex = 1;
            // 
            // txt_CQQLThu_ID
            // 
            this.txt_CQQLThu_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_CQQLThu_ID.Location = new System.Drawing.Point(110, 193);
            this.txt_CQQLThu_ID.Name = "txt_CQQLThu_ID";
            this.txt_CQQLThu_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_CQQLThu_ID.TabIndex = 1;
            // 
            // txt_DVSDNS_ID
            // 
            this.txt_DVSDNS_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DVSDNS_ID.Location = new System.Drawing.Point(110, 141);
            this.txt_DVSDNS_ID.Name = "txt_DVSDNS_ID";
            this.txt_DVSDNS_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_DVSDNS_ID.TabIndex = 1;
            // 
            // txt_DBHC_ID
            // 
            this.txt_DBHC_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC_ID.Location = new System.Drawing.Point(110, 166);
            this.txt_DBHC_ID.Name = "txt_DBHC_ID";
            this.txt_DBHC_ID.Size = new System.Drawing.Size(98, 21);
            this.txt_DBHC_ID.TabIndex = 1;
            // 
            // txt_TK_Thu_NS
            // 
            this.txt_TK_Thu_NS.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_TK_Thu_NS.Location = new System.Drawing.Point(110, 116);
            this.txt_TK_Thu_NS.Name = "txt_TK_Thu_NS";
            this.txt_TK_Thu_NS.Size = new System.Drawing.Size(98, 21);
            this.txt_TK_Thu_NS.TabIndex = 1;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.LemonChiffon;
            this.textBox14.Location = new System.Drawing.Point(110, 89);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(352, 21);
            this.textBox14.TabIndex = 1;
            // 
            // txt_MaSoThue
            // 
            this.txt_MaSoThue.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_MaSoThue.Location = new System.Drawing.Point(110, 62);
            this.txt_MaSoThue.Name = "txt_MaSoThue";
            this.txt_MaSoThue.Size = new System.Drawing.Size(98, 21);
            this.txt_MaSoThue.TabIndex = 1;
            // 
            // txt_NgayNT
            // 
            this.txt_NgayNT.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NgayNT.Location = new System.Drawing.Point(110, 35);
            this.txt_NgayNT.Name = "txt_NgayNT";
            this.txt_NgayNT.Size = new System.Drawing.Size(354, 21);
            this.txt_NgayNT.TabIndex = 1;
            // 
            // txt_NH_PhucVu
            // 
            this.txt_NH_PhucVu.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NH_PhucVu.Location = new System.Drawing.Point(110, 8);
            this.txt_NH_PhucVu.Name = "txt_NH_PhucVu";
            this.txt_NH_PhucVu.Size = new System.Drawing.Size(98, 21);
            this.txt_NH_PhucVu.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.LightGray;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(541, 22);
            this.label10.TabIndex = 3;
            this.label10.Text = "Thông Tin Về NNT";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbo_HinhThucThu);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.txt_NHNN_Name);
            this.panel3.Controls.Add(this.txt_NHNN_ID);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(541, 69);
            this.panel3.TabIndex = 2;
            // 
            // cbo_HinhThucThu
            // 
            this.cbo_HinhThucThu.FormattingEnabled = true;
            this.cbo_HinhThucThu.Items.AddRange(new object[] {
            "Tiền mặt",
            "Chuyển khoản",
            "Chuyển liên ngân hàng"});
            this.cbo_HinhThucThu.Location = new System.Drawing.Point(101, 35);
            this.cbo_HinhThucThu.Name = "cbo_HinhThucThu";
            this.cbo_HinhThucThu.Size = new System.Drawing.Size(363, 23);
            this.cbo_HinhThucThu.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "Hình Thức Thu";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "NHNN Thu";
            // 
            // txt_NHNN_Name
            // 
            this.txt_NHNN_Name.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NHNN_Name.Location = new System.Drawing.Point(205, 8);
            this.txt_NHNN_Name.Name = "txt_NHNN_Name";
            this.txt_NHNN_Name.Size = new System.Drawing.Size(259, 21);
            this.txt_NHNN_Name.TabIndex = 1;
            // 
            // txt_NHNN_ID
            // 
            this.txt_NHNN_ID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_NHNN_ID.Location = new System.Drawing.Point(101, 8);
            this.txt_NHNN_ID.Name = "txt_NHNN_ID";
            this.txt_NHNN_ID.Size = new System.Drawing.Size(107, 21);
            this.txt_NHNN_ID.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.LightGray;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(541, 22);
            this.label7.TabIndex = 1;
            this.label7.Text = "Chọn Kênh Thanh Toán";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_Save);
            this.panel2.Controls.Add(this.txt_SoChungTu);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(541, 39);
            this.panel2.TabIndex = 0;
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(394, 5);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(70, 30);
            this.btn_Save.TabIndex = 7;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = false;
            this.btn_Save.Visible = false;
            // 
            // txt_SoChungTu
            // 
            this.txt_SoChungTu.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_SoChungTu.Location = new System.Drawing.Point(101, 8);
            this.txt_SoChungTu.Name = "txt_SoChungTu";
            this.txt_SoChungTu.Size = new System.Drawing.Size(132, 21);
            this.txt_SoChungTu.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số CT";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.GV_Analysis);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(564, 525);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Phân Tích";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GV_Analysis
            // 
            this.GV_Analysis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Analysis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Analysis.Location = new System.Drawing.Point(3, 3);
            this.GV_Analysis.Name = "GV_Analysis";
            this.GV_Analysis.Size = new System.Drawing.Size(558, 519);
            this.GV_Analysis.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txt_remark);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(564, 525);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Nội Dung";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txt_remark
            // 
            this.txt_remark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_remark.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_remark.Location = new System.Drawing.Point(0, 0);
            this.txt_remark.Multiline = true;
            this.txt_remark.Name = "txt_remark";
            this.txt_remark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_remark.Size = new System.Drawing.Size(564, 525);
            this.txt_remark.TabIndex = 2;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LB_Log);
            this.tabPage4.Controls.Add(this.panel7);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(564, 525);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Log";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LB_Log
            // 
            this.LB_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LB_Log.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_Log.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LB_Log.FormattingEnabled = true;
            this.LB_Log.ItemHeight = 16;
            this.LB_Log.Location = new System.Drawing.Point(0, 0);
            this.LB_Log.Name = "LB_Log";
            this.LB_Log.Size = new System.Drawing.Size(564, 458);
            this.LB_Log.TabIndex = 0;
            this.LB_Log.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LB_Log_MouseDoubleClick);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txt_ItemRepeat);
            this.panel7.Controls.Add(this.txt_GroupSort);
            this.panel7.Controls.Add(this.btn_App_Group);
            this.panel7.Controls.Add(this.button1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 458);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(564, 67);
            this.panel7.TabIndex = 3;
            // 
            // txt_ItemRepeat
            // 
            this.txt_ItemRepeat.Location = new System.Drawing.Point(7, 33);
            this.txt_ItemRepeat.Name = "txt_ItemRepeat";
            this.txt_ItemRepeat.Size = new System.Drawing.Size(247, 20);
            this.txt_ItemRepeat.TabIndex = 1;
            // 
            // txt_GroupSort
            // 
            this.txt_GroupSort.Location = new System.Drawing.Point(7, 5);
            this.txt_GroupSort.Name = "txt_GroupSort";
            this.txt_GroupSort.Size = new System.Drawing.Size(326, 20);
            this.txt_GroupSort.TabIndex = 1;
            // 
            // btn_App_Group
            // 
            this.btn_App_Group.Location = new System.Drawing.Point(260, 31);
            this.btn_App_Group.Name = "btn_App_Group";
            this.btn_App_Group.Size = new System.Drawing.Size(75, 23);
            this.btn_App_Group.TabIndex = 0;
            this.btn_App_Group.Text = "Luật TM";
            this.btn_App_Group.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Panel_Hide
            // 
            this.Panel_Hide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.Panel_Hide.Controls.Add(this.btn_Hide);
            this.Panel_Hide.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Hide.Location = new System.Drawing.Point(623, 0);
            this.Panel_Hide.Name = "Panel_Hide";
            this.Panel_Hide.Size = new System.Drawing.Size(37, 551);
            this.Panel_Hide.TabIndex = 4;
            // 
            // btn_Hide
            // 
            this.btn_Hide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Hide.ForeColor = System.Drawing.Color.White;
            this.btn_Hide.Location = new System.Drawing.Point(4, 4);
            this.btn_Hide.Name = "btn_Hide";
            this.btn_Hide.Size = new System.Drawing.Size(31, 29);
            this.btn_Hide.TabIndex = 0;
            this.btn_Hide.Text = ">>";
            this.btn_Hide.UseVisualStyleBackColor = false;
            this.btn_Hide.Click += new System.EventHandler(this.btn_Hide_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.chkRule8000);
            this.panel6.Controls.Add(this.chkRule6000);
            this.panel6.Controls.Add(this.chkRule3000);
            this.panel6.Controls.Add(this.chkRule1000);
            this.panel6.Controls.Add(this.btn_Search);
            this.panel6.Controls.Add(this.txt_Search);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(623, 97);
            this.panel6.TabIndex = 8;
            // 
            // chkRule8000
            // 
            this.chkRule8000.AutoSize = true;
            this.chkRule8000.Checked = true;
            this.chkRule8000.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRule8000.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRule8000.Location = new System.Drawing.Point(458, 69);
            this.chkRule8000.Name = "chkRule8000";
            this.chkRule8000.Size = new System.Drawing.Size(137, 19);
            this.chkRule8000.TabIndex = 7;
            this.chkRule8000.Text = "Rule Char - Number";
            this.chkRule8000.UseVisualStyleBackColor = true;
            this.chkRule8000.Visible = false;
            // 
            // chkRule6000
            // 
            this.chkRule6000.AutoSize = true;
            this.chkRule6000.Checked = true;
            this.chkRule6000.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRule6000.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRule6000.Location = new System.Drawing.Point(458, 50);
            this.chkRule6000.Name = "chkRule6000";
            this.chkRule6000.Size = new System.Drawing.Size(83, 19);
            this.chkRule6000.TabIndex = 7;
            this.chkRule6000.Text = "Rule 6000";
            this.chkRule6000.UseVisualStyleBackColor = true;
            this.chkRule6000.Visible = false;
            // 
            // chkRule3000
            // 
            this.chkRule3000.AutoSize = true;
            this.chkRule3000.Checked = true;
            this.chkRule3000.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRule3000.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRule3000.Location = new System.Drawing.Point(458, 30);
            this.chkRule3000.Name = "chkRule3000";
            this.chkRule3000.Size = new System.Drawing.Size(83, 19);
            this.chkRule3000.TabIndex = 7;
            this.chkRule3000.Text = "Rule 3000";
            this.chkRule3000.UseVisualStyleBackColor = true;
            this.chkRule3000.Visible = false;
            // 
            // chkRule1000
            // 
            this.chkRule1000.AutoSize = true;
            this.chkRule1000.Checked = true;
            this.chkRule1000.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRule1000.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRule1000.Location = new System.Drawing.Point(458, 9);
            this.chkRule1000.Name = "chkRule1000";
            this.chkRule1000.Size = new System.Drawing.Size(83, 19);
            this.chkRule1000.TabIndex = 7;
            this.chkRule1000.Text = "Rule 1000";
            this.chkRule1000.UseVisualStyleBackColor = true;
            this.chkRule1000.Visible = false;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(218, 37);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(75, 23);
            this.btn_Search.TabIndex = 6;
            this.btn_Search.Text = "Search";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(218, 12);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(216, 20);
            this.txt_Search.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lbl_Total_Record);
            this.groupBox1.Controls.Add(this.lbl_Record_Error);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbl_Record_Success);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 90);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thống kê";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tổng Record";
            // 
            // lbl_Total_Record
            // 
            this.lbl_Total_Record.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Total_Record.Location = new System.Drawing.Point(143, 20);
            this.lbl_Total_Record.Name = "lbl_Total_Record";
            this.lbl_Total_Record.Size = new System.Drawing.Size(40, 13);
            this.lbl_Total_Record.TabIndex = 0;
            this.lbl_Total_Record.Text = "0";
            this.lbl_Total_Record.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_Record_Error
            // 
            this.lbl_Record_Error.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Record_Error.Location = new System.Drawing.Point(143, 65);
            this.lbl_Record_Error.Name = "lbl_Record_Error";
            this.lbl_Record_Error.Size = new System.Drawing.Size(40, 13);
            this.lbl_Record_Error.TabIndex = 0;
            this.lbl_Record_Error.Text = "0";
            this.lbl_Record_Error.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Phân tích thành công";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chưa phân tích được";
            // 
            // lbl_Record_Success
            // 
            this.lbl_Record_Success.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Record_Success.Location = new System.Drawing.Point(143, 42);
            this.lbl_Record_Success.Name = "lbl_Record_Success";
            this.lbl_Record_Success.Size = new System.Drawing.Size(40, 13);
            this.lbl_Record_Success.TabIndex = 0;
            this.lbl_Record_Success.Text = "0";
            this.lbl_Record_Success.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelLeftBottom
            // 
            this.PanelLeftBottom.Controls.Add(this.btn_SaveFail);
            this.PanelLeftBottom.Controls.Add(this.btn_Apply);
            this.PanelLeftBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelLeftBottom.Location = new System.Drawing.Point(0, 506);
            this.PanelLeftBottom.Name = "PanelLeftBottom";
            this.PanelLeftBottom.Size = new System.Drawing.Size(623, 45);
            this.PanelLeftBottom.TabIndex = 9;
            this.PanelLeftBottom.SizeChanged += new System.EventHandler(this.PanelLeftBottom_SizeChanged);
            // 
            // btn_SaveFail
            // 
            this.btn_SaveFail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_SaveFail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SaveFail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SaveFail.ForeColor = System.Drawing.Color.White;
            this.btn_SaveFail.Location = new System.Drawing.Point(578, 7);
            this.btn_SaveFail.Name = "btn_SaveFail";
            this.btn_SaveFail.Size = new System.Drawing.Size(39, 32);
            this.btn_SaveFail.TabIndex = 6;
            this.btn_SaveFail.UseVisualStyleBackColor = false;
            this.btn_SaveFail.Click += new System.EventHandler(this.btn_SaveFail_Click);
            // 
            // btn_Apply
            // 
            this.btn_Apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_Apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Apply.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Apply.ForeColor = System.Drawing.Color.White;
            this.btn_Apply.Location = new System.Drawing.Point(7, 7);
            this.btn_Apply.Name = "btn_Apply";
            this.btn_Apply.Size = new System.Drawing.Size(119, 32);
            this.btn_Apply.TabIndex = 4;
            this.btn_Apply.Text = "Phân tích";
            this.btn_Apply.UseVisualStyleBackColor = false;
            this.btn_Apply.Click += new System.EventHandler(this.btn_Apply_Click);
            // 
            // GV_ListOrder
            // 
            this.GV_ListOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GV_ListOrder.DefaultCellStyle = dataGridViewCellStyle1;
            this.GV_ListOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_ListOrder.Location = new System.Drawing.Point(0, 97);
            this.GV_ListOrder.Name = "GV_ListOrder";
            this.GV_ListOrder.Size = new System.Drawing.Size(623, 409);
            this.GV_ListOrder.TabIndex = 10;
            this.GV_ListOrder.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GV_ListOrder_RowEnter);
            // 
            // Timer_Auto
            // 
            this.Timer_Auto.Interval = 10;
            this.Timer_Auto.Tick += new System.EventHandler(this.Timer_Auto_Tick);
            // 
            // FrmOrderAnalysis_10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 551);
            this.Controls.Add(this.GV_ListOrder);
            this.Controls.Add(this.PanelLeftBottom);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.Panel_Hide);
            this.Controls.Add(this.Panel_Right);
            this.Name = "FrmOrderAnalysis_10";
            this.Opacity = 0.9D;
            this.Text = "Phân tích lệnh chuyển tiền 1.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmOrderAnalysis_10_Load);
            this.Panel_Right.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_DataDetail)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Analysis)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.Panel_Hide.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.PanelLeftBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_ListOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Panel Panel_Hide;
        private System.Windows.Forms.Button btn_Hide;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Total_Record;
        private System.Windows.Forms.Label lbl_Record_Error;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_Record_Success;
        private System.Windows.Forms.Panel PanelLeftBottom;
        private System.Windows.Forms.Button btn_Apply;
        private System.Windows.Forms.DataGridView GV_ListOrder;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView GV_DataDetail;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_LHXNK_Name;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox txt_LoaiThue_Name;
        private System.Windows.Forms.TextBox txt_CQQLThu_Name;
        private System.Windows.Forms.TextBox txt_DBHC_Name;
        private System.Windows.Forms.TextBox txt_LHXNK;
        private System.Windows.Forms.TextBox txt_TK_Name;
        private System.Windows.Forms.TextBox txt_NgayDk;
        private System.Windows.Forms.TextBox txt_NguoiNT;
        private System.Windows.Forms.TextBox txt_ToKhaiSo;
        private System.Windows.Forms.TextBox txt_NH_PhucVu_Name;
        private System.Windows.Forms.TextBox txt_LoaiThue;
        private System.Windows.Forms.TextBox txt_CQQLThu_ID;
        private System.Windows.Forms.TextBox txt_DBHC_ID;
        private System.Windows.Forms.TextBox txt_TK_Thu_NS;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox txt_MaSoThue;
        private System.Windows.Forms.TextBox txt_NgayNT;
        private System.Windows.Forms.TextBox txt_NH_PhucVu;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbo_HinhThucThu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_NHNN_Name;
        private System.Windows.Forms.TextBox txt_NHNN_ID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TextBox txt_SoChungTu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView GV_Analysis;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txt_remark;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListBox LB_Log;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer Timer_Auto;
        private System.Windows.Forms.TextBox txt_ItemRepeat;
        private System.Windows.Forms.TextBox txt_GroupSort;
        private System.Windows.Forms.Button btn_App_Group;
        private System.Windows.Forms.Button btn_SaveFail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_DVSDNS_Name;
        private System.Windows.Forms.TextBox txt_DVSDNS_ID;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.CheckBox chkRule6000;
        private System.Windows.Forms.CheckBox chkRule3000;
        private System.Windows.Forms.CheckBox chkRule1000;
        private System.Windows.Forms.CheckBox chkRule8000;
    }
}