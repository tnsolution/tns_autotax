﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.BNK;
using HHT.Library.System;
using HHT.Library.Bank;
using System.Data.SqlClient;
using HHT.Connect;

namespace HHT_AutoTax
{
    public partial class FrmTest : Form
    {
        public FrmTest()
        {
            InitializeComponent();
        }

        private void FrmTest_Load(object sender, EventArgs e)
        {
            textBox1.Text = "NGAY10.11.2018 NTDT MST 0301424549 NOP C554TM1901 44060218D   VA  C554TM1702 78032502D  LH XNK A11 TK10232404222 NGAY10.11.2018 KBNN Quan 2 - TP Ho Chi Minh";
            Item_Bill zItem = new Item_Bill();
            zItem.Content = textBox1.Text;
            _ListItemInBill.Add(zItem);
            ShowValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Find_C_TM_ST(textBox1.Text);
            _ListItemInBill = new ArrayList();
            Item_Bill zItem = new Item_Bill();
            zItem.Content = textBox1.Text;
            _ListItemInBill.Add(zItem);

            Analysis_New_Way(3, 12);
            // ShowValue();
            Analysis_New_Way(4, 10);
            Analysis_New_Way(10, 1);
            Analysis_New_Way(11, 2);
            Analysis_New_Date(6, 13);
            Analysis_LHXNK(3, 3);
            ShowValue();
        }
        private ArrayList _ListItemInBill = new ArrayList();
        private void Analysis_New_Way(int LengNumber, int CategoryID)
        {
            ArrayList zList = new ArrayList();

            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                Item_Bill zItemNew;
                if (zItem.CategoryID == 0)
                {
                    int j = 0;
                    int zIndex = 0;
                    int zIndexBegin = -1;
                    string zValueFound = "";
                    string zContentAnalysis = zItem.Content;
                    while (zIndex < zContentAnalysis.Length)
                    {
                        int zAscii = (int)zContentAnalysis[zIndex];
                        if ((zAscii > 47 && zAscii < 58))
                        {
                            if (zIndexBegin == -1)
                            {
                                zIndexBegin = zIndex;
                                zValueFound = zContentAnalysis[zIndex].ToString();
                            }
                            else
                            {
                                zValueFound += zContentAnalysis[zIndex].ToString();

                            }
                        }
                        else
                        {

                            if (zValueFound.Length == LengNumber && (zValueFound != "2018" && zValueFound != "2019"))
                            {
                                string zContentBefore = zContentAnalysis.Substring(0, zIndexBegin);
                                zContentAnalysis = zContentAnalysis.Substring(zIndex, zContentAnalysis.Length - zIndex);

                                zItemNew = new Item_Bill();
                                zItemNew.CategoryID = 0;
                                zItemNew.Content = zContentBefore;
                                zList.Add(zItemNew);

                                zItemNew = new Item_Bill();
                                zItemNew.CategoryID = CategoryID;
                                zItemNew.Value = zValueFound;
                                zList.Add(zItemNew);

                                zIndex = 0;
                                zIndexBegin = -1;
                                zValueFound = "";

                            }
                            else
                            {
                                zIndexBegin = -1;
                            }
                        }
                        zIndex++;
                    }
                    if (zContentAnalysis.Length > 0)
                    {
                        zItemNew = new Item_Bill();
                        zItemNew.Content = zContentAnalysis;
                        zList.Add(zItemNew);
                    }
                }
                else
                {
                    zList.Add(zItem);
                }
            }
            _ListItemInBill = zList;
        }
        private void Analysis_New_Date(int LengNumber, int CategoryID)
        {
            ArrayList zList = new ArrayList();

            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                Item_Bill zItemNew;
                if (zItem.CategoryID == 0)
                {
                    int j = 0;
                    int zIndexBegin = -1;
                    int zIndex = 0;
                    string zContentAnalysis = zItem.Content;
                    zIndexBegin = zContentAnalysis.IndexOf("2018", zIndex);
                    while (zIndexBegin >= 0)
                    {
                        if (zIndexBegin - LengNumber > 0)
                        {
                            string zDate = zContentAnalysis.Substring(zIndexBegin - LengNumber, 10);
                            bool zIsCorrectDate = false;
                            if ((zDate[2] == '.' && zDate[5] == '.') || (zDate[2] == '/' && zDate[5] == '/'))
                            {
                                zIsCorrectDate = true;
                            }

                            if (zIsCorrectDate)
                            {
                                string zContentBefore = zContentAnalysis.Substring(0, zIndexBegin - LengNumber);
                                zContentAnalysis = zContentAnalysis.Substring(zIndexBegin + 4, zContentAnalysis.Length - zIndexBegin - 4);

                                zItemNew = new Item_Bill();
                                zItemNew.CategoryID = 0;
                                zItemNew.Content = zContentBefore;
                                zList.Add(zItemNew);

                                zItemNew = new Item_Bill();
                                zItemNew.CategoryID = CategoryID;
                                zItemNew.Value = zDate;
                                zList.Add(zItemNew);
                                zIndex = 0;
                            }
                            else
                                zIndex = zIndexBegin + 4;

                        }
                        zIndexBegin = zContentAnalysis.IndexOf("2018", zIndex);
                    }
                    zItemNew = new Item_Bill();
                    zItemNew.Content = zContentAnalysis;
                    zList.Add(zItemNew);
                }
                else
                {
                    zList.Add(zItem);
                }
            }
            _ListItemInBill = zList;
        }
        private void Analysis_LHXNK(int LengNumber, int CategoryID)
        {
            ArrayList zList = new ArrayList();
            DataTable zTable = Data_Access.List_LHNXK();
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                Item_Bill zItemNew;
                if (zItem.CategoryID == 0)
                {
                    string zContentAnalysis = zItem.Content;
                    bool zIsFound = false;
                    foreach (DataRow zRow in zTable.Rows)
                    {
                        string zID = zRow["ID"].ToString();
                      
                        int zIndexBegin = zItem.Content.IndexOf(zID, 0);
                        if (zIndexBegin > 0)
                        {
                            string zContentBefore = zContentAnalysis.Substring(0, zIndexBegin);
                            zContentAnalysis = zContentAnalysis.Substring(zIndexBegin + 3, zContentAnalysis.Length - zIndexBegin - 3);

                            zItemNew = new Item_Bill();
                            zItemNew.CategoryID = 0;
                            zItemNew.Content = zContentBefore;
                            zList.Add(zItemNew);

                            zItemNew = new Item_Bill();
                            zItemNew.CategoryID = CategoryID;
                            zItemNew.Value = zID;
                            zList.Add(zItemNew);

                            zItemNew = new Item_Bill();
                            zItemNew.Content = zContentAnalysis;
                            zList.Add(zItemNew);
                            zIsFound = true;
                            break;
                        }
                       
                    }

                    if(!zIsFound)
                    {
                        zList.Add(zItem);
                    }
                }
                else
                {
                    zList.Add(zItem);
                }
            }
            _ListItemInBill = zList;
        }

        private void ShowValue()
        {
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID > 0)
                    zItem.CategoryName = ">>>>>>>>>>>>>>>>>>" + GetCategoryName(zItem.CategoryID);
                string zName = zItem.CategoryID.ToString() + ":" + zItem.CategoryName + ":" + zItem.Value + ":" + zItem.Content;
                listBox1.Items.Add(zName);
            }
        }
        public static string GetCategoryName(int ID)
        {
            string zResult = "";
            string SQL = "SELECT CategoryName FROM Item_Categories WHERE CategoryID = " + ID + "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(SQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            var zVal = zCommand.ExecuteScalar();
            if (zVal != null)
                zResult = zVal.ToString();
            zCommand.Dispose();
            zConnect.Close();
            return zResult;
        }
    }
}
