﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;
using HHT.Library.System;

namespace HHT_AutoTax
{
    public partial class FrmOrderAnalysis_Ext : Form
    {
        public int SectionNo = 0;
        private bool _IsPostback;
        CultureInfo enUS = new CultureInfo("en-US"); // is up to you

        private ArrayList _ListItemInBill;
        private OrderTranfer_Info _OrderOrigin;

        private Order_Info _OrderHeader;
        private ArrayList _OrderDetail;

        public FrmOrderAnalysis_Ext()
        {
            InitializeComponent();
        }
        private void FrmOrderAnalysis_Ext_Load(object sender, EventArgs e)
        {
            _IsPostback = false;
            InitGridView_Order(GV_ListOrder);
            InitGridView_Analysis(GV_Analysis);
            InitGridView_Order_Detail(GV_DataDetail);

            LoadDataOfOrder();


            _IsPostback = true;
        }

        #region Design Layout
        private void btn_Hide_Click(object sender, EventArgs e)
        {

            if (btn_Hide.Text == ">>")
            {
                Panel_Right.Visible = false;
                btn_Hide.Text = "<<";
            }
            else
            {
                Panel_Right.Visible = true;
                btn_Hide.Text = ">>";
            }
        }
        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("Message", "Message");
            GV.Columns.Add("StatusAnalysis", "Status");
            GV.Columns.Add("trdate", "Ngày");
            GV.Columns.Add("remark", "Nội Dung");
            GV.Columns.Add("traamt", "Số Tiền");
            GV.Columns.Add("TKTNS", "TK Thu NS");
            GV.Columns.Add("stylecust", "Loại");
            GV.Columns.Add("ordcust", "Người Nộp Thuế");
            GV.Columns.Add("bencust", "Hải Quan");
            GV.Columns.Add("trref", "Số Chứng Từ");
            GV.Columns[0].Width = 40;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 90;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 40;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 80;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[4].Width = 280;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[4].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns[5].Width = 120;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 140;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 80;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[8].Width = 280;
            GV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[8].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns[9].Width = 160;
            GV.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[9].DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            GV.Columns[10].Width = 160;
            GV.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[10].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        public void InitGridView_Analysis(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Field_1", "");
            GV.Columns.Add("Field_2", "");
            GV.Columns.Add("Field_3", "");
            GV.Columns.Add("Field_4", "");

            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 120;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 140;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 250;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;


            for (int i = 1; i <= 30; i++)
            {
                GV.Rows.Add();
            }
        }
        public void InitGridView_Order_Detail(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Chuong", "Chương");
            GV.Columns.Add("NDKT", "NDKT");
            GV.Columns.Add("Content", "Nội Dung");
            GV.Columns.Add("TienNT", "Tiền NT");
            GV.Columns.Add("KyThue", "Kỳ Thuế");

            GV.Columns[0].Width = 80;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 80;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 220;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 100;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        #endregion

        #region [Get Data ]
        private void btn_View_Click(object sender, EventArgs e)
        {
            LoadDataOfOrder();
        }
        public void LoadDataOfOrder()
        {
            GV_ListOrder.Rows.Clear();
            DataTable zListOrder = new DataTable();

            zListOrder = OrderTranfer_Data.ListAnalysis(SectionNo, 6, 0);

            int i = 0;
            int j = 0;
            foreach (DataRow nRow in zListOrder.Rows)
            {
                GV_ListOrder.Rows.Add();
                DataGridViewRow nRowView = GV_ListOrder.Rows[i];
                DateTime z_trdate = (DateTime)nRow["trdate"];
                double zMoney = double.Parse(nRow["traamt"].ToString());

                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StatusAnalysis"].Value = nRow["StatusAnalysis"].ToString();
                nRowView.Cells["trdate"].Value = z_trdate.ToString("dd/MM/yyyy");
                nRowView.Cells["trref"].Value = nRow["trref"].ToString();
                nRowView.Cells["traamt"].Value = zMoney.ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR"));
                nRowView.Cells["TKTNS"].Value = nRow["name_16"].ToString().Trim();
                nRowView.Cells["bencust"].Value = nRow["bencust"].ToString().Trim().ToUpper();
                string zNguoiNT = nRow["ordcust"].ToString().Trim().ToUpper();
                if (zNguoiNT.Contains("CTY") || zNguoiNT.Contains("CONG TY"))
                {
                    nRowView.Cells["Stylecust"].Value = "CÔNG TY";
                }
                else
                {
                    nRowView.Cells["Stylecust"].Value = "CÁ NHÂN";
                }
                nRowView.Cells["remark"].Value = nRow["remark"].ToString().Trim();
                nRowView.Cells["ordcust"].Value = nRow["ordcust"].ToString().Trim();
                i++;
                if (nRow["StatusAnalysis"].ToString() != "0")
                    j++;
            }
            lbl_Total_Record.Text = i.ToString();
            lbl_Record_Success.Text = j.ToString();
            lbl_Record_Error.Text = (i - j).ToString();
        }
        private void Message_System(string Message)
        {
            Invoke(new MethodInvoker(delegate
            {
                if (LB_Log.Items.Count > 1000)
                    LB_Log.Items.RemoveAt(0);
                LB_Log.Items.Add(DateTime.Now.ToString("dd-MM-yy hh:mm:ss.fff") + " : " + Message);
                LB_Log.SelectedIndex = LB_Log.Items.Count - 1;
            }));
        }
        private void LoadToToolBox()
        {
            GV_DataDetail.Rows.Clear();

            txt_SoChungTu.Text = _OrderHeader.trref;
            txt_NHNN_ID.Text = _OrderHeader.NHNN_ID;
            txt_NHNN_Name.Text = _OrderHeader.NHNN_Name;
            cbo_KieuThu.SelectedIndex = _OrderHeader.KieuThu;

            DateTime zDay = (DateTime)_OrderHeader.NgayNT;
            txt_NH_PhucVu.Text = _OrderHeader.NHPV_ID;
            txt_NH_PhucVu_Name.Text = _OrderHeader.NHPV_Name;
            txt_NgayNT.Text = zDay.ToString("dd/MM/yyyy");

            txt_MaSoThue.Text = _OrderHeader.Ma_ST;
            txt_NguoiNT.Text = _OrderHeader.TenCongTy;

            txt_TK_Thu_NS.Text = _OrderHeader.TK_ThuNS;
            txt_TK_Name.Text = _OrderHeader.TK_ThuNS_Name;

            txt_DBHC_ID.Text = _OrderHeader.DBHC_ID + "HH";
            txt_DBHC_Name.Text = _OrderHeader.DBHC_Name;

            txt_CQQLThu_ID.Text = _OrderHeader.CQThuID;
            txt_CQQLThu_Name.Text = _OrderHeader.CQThuName;

            txt_LHXNK.Text = _OrderHeader.LH_ID;
            txt_LHXNK_Name.Text = _OrderHeader.LH_Name;

            txt_ToKhaiSo.Text = _OrderHeader.ToKhai;

            txt_LoaiThue.Text = _OrderHeader.LoaiThueID;

            if (_OrderHeader.NgayDK != null)
            {
                zDay = (DateTime)_OrderHeader.NgayDK;
                txt_NgayDk.Text = zDay.ToString("dd/MM/yyyy");
            }
            else
                txt_NgayDk.Text = "";
            //------------------------------
            int n = _OrderDetail.Count;
            Order_Detail_Info zDetail;
            double zTotal = 0;
            int i = 0;
            for (i = 0; i < n; i++)
            {
                GV_DataDetail.Rows.Add();
                zDetail = (Order_Detail_Info)_OrderDetail[i];
                GV_DataDetail.Rows[i].Cells[0].Value = zDetail.ChuongID;
                GV_DataDetail.Rows[i].Cells[1].Value = zDetail.NDKT_ID;
                GV_DataDetail.Rows[i].Cells[2].Value = zDetail.NDKT_Name;
                GV_DataDetail.Rows[i].Cells[3].Value = zDetail.SoTien.ToString("#,###,###");
                zTotal += zDetail.SoTien;
                GV_DataDetail.Rows[i].Cells[4].Value = zDetail.KyThue;
            }
            GV_DataDetail.Rows.Add();
            GV_DataDetail.Rows[i].Cells[0].Value = "";
            GV_DataDetail.Rows[i].Cells[1].Value = "";
            GV_DataDetail.Rows[i].Cells[2].Value = "TỔNG CỘNG";
            GV_DataDetail.Rows[i].Cells[3].Value = zTotal.ToString("#,###,###");
        }
        #endregion

        private int _Index_Selected = 0;
        int _AmountSuccess = 0;
        private void GV_ListOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_IsPostback)
            {
                if (_Index_Selected != e.RowIndex)
                {
                    if (GV_ListOrder.Rows[e.RowIndex].Cells["trref"].Value != null)
                    {
                        _Index_Selected = e.RowIndex;
                        GV_Analysis.Rows.Clear();
                        GV_Analysis.Rows.Add(50);

                        Analysis_Record(2);

                        DataGridViewRow nRowView = GV_Analysis.Rows[0];
                        nRowView.Cells["Field_1"].Value = "Chứng từ số";
                        nRowView.Cells["Field_2"].Value = _OrderOrigin.trref;

                        //======================PHAN TICH NOI DUNG VA TIM KIEM THONG SO=============================
                        int k = 2;
                        nRowView = GV_Analysis.Rows[k];
                        nRowView.Cells["Field_1"].Value = "******";
                        nRowView.Cells["Field_2"].Value = "***NỘI DUNG***";
                        nRowView.Cells["Field_3"].Value = "******";
                        k = 3;


                        for (int i = 0; i < _ListItemInBill.Count; i++)
                        {
                            Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                            nRowView = GV_Analysis.Rows[k + i];
                            nRowView.Cells["Field_1"].Value = zItem.Content;
                            nRowView.Cells["Field_2"].Value = zItem.CategoryName;
                            nRowView.Cells["Field_3"].Value = zItem.Value;
                            nRowView.Cells["Field_4"].Value = zItem.Extent;

                        }

                    }

                    LoadToToolBox();
                }
            }
        }
        private void btn_Apply_Click(object sender, EventArgs e)
        {
            _AmountSuccess = 0;
            _Index_Selected = 0;
            GV_ListOrder.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            Timer_Auto.Start();
        }
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            Timer_Auto.Stop();
            string zStatus = GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value.ToString();
            if (zStatus == "5")
            {
                Analysis_Record(1);
            }
            else
                _AmountSuccess++;

            lbl_Record_Success.Text = _AmountSuccess.ToString();

            _Index_Selected++;
            if (_Index_Selected < GV_ListOrder.Rows.Count)
                Timer_Auto.Start();
            else
            {
                GV_ListOrder.Enabled = true;
                int zTotal = int.Parse(lbl_Total_Record.Text);
                lbl_Record_Error.Text = (zTotal - _AmountSuccess).ToString();
                this.Cursor = Cursors.Default;
            }
        }

        private void Analysis_Record(int Style)
        {
            string zID = GV_ListOrder.Rows[_Index_Selected].Cells["trref"].Value.ToString();
            _OrderOrigin = new OrderTranfer_Info(zID);
            _OrderOrigin.remark = GV_ListOrder.Rows[_Index_Selected].Cells["remark"].Value.ToString();
            txt_remark.Text = _OrderOrigin.remark;

            _ListItemInBill = OrderTranfer_Data.ItemAnalysis_2(_OrderOrigin.trref);

            Message_System(">>>>>>>>" + (_Index_Selected + 1).ToString() + "<<<<<<<<");
            _OrderHeader = new Order_Info();
            _OrderDetail = new ArrayList();

            RuleDetail_C_TM_ST_Ext();

            SearchOrderHeader();
            // Truong Hop Nhiều tờ khai là lệ phí hải quan
            CheckNhieuToKhai();
            int zAmountErr = 0;
            zAmountErr = CheckOrder();
            if (zAmountErr > 0)
            {
                Rule_TK_Ngay_LH();
                zAmountErr = CheckOrder();
            }
            if (zAmountErr > 0)
            {
                // Tim MST
                if (_OrderHeader.Ma_ST.Length == 0)
                {
                    FindMST();
                }
                //Find Ngay
                if (_OrderHeader.NgayDK == null)
                {
                    FindNgayDK_Ext();
                }

                // tim LH
                Item_Bill zItem = FindValueItem_OnlyOne(3);
                if (zItem.CategoryID == 3)
                {
                    _OrderHeader.LH_ID = zItem.Value;
                    _OrderHeader.LH_Name = Data_Access.LH_Name(_OrderHeader.LH_ID);
                }
                else
                {
                    FindLHXNK_Ext();
                }

                if (CheckOrderDetail() > 0)
                {
                    // Tim theo luat dac biet
                    Find_C_TM_ST();
                }

                zAmountErr = CheckOrder();
            }

            if (zAmountErr == 0)
            {
                GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Done 2";
                if (Style == 1)
                {
                    SaveOrderDone();
                    GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value = "2";
                }
            }
            else
            {
                _OrderOrigin.UpdateMST(_OrderHeader.Ma_ST);
            }

        }
        private int CategoryOrder()
        {
            if (_OrderHeader.LoaiThueID == "01")
                return 1;
            else
            {
                if (_OrderHeader.SoTien % 20000 == 0)
                    return 2;
                else
                    return 3;
            }
        }

        private void SearchOrderHeader()
        {
            _OrderHeader.trref = _OrderOrigin.trref;
            _OrderHeader.SectionNo = SectionNo;
            _OrderHeader.NHPV_ID = _OrderOrigin.remtbk;
            _OrderHeader.NHPV_Name = _OrderOrigin.remntbk;

            if (_OrderOrigin.name_16.ToString().Length == 0)
                _OrderOrigin.name_16 = "7111";
            string zName_16 = _OrderOrigin.name_16.ToString().Trim();
            string zTKNganSach = "";
            string zCQQuanLyNS = "";

            if (zName_16.Length >= 4)
            {
                zTKNganSach = zName_16.Substring(0, 4);
                if (zName_16.Length > 4)
                {
                    zCQQuanLyNS = zName_16.Substring(4, zName_16.Length - 4).Trim();
                    zCQQuanLyNS = Function.GetAllNumberInSide(zCQQuanLyNS);
                    if (zCQQuanLyNS.Length == 8 && zCQQuanLyNS[0] == '0')
                    {
                        zCQQuanLyNS = zCQQuanLyNS.Substring(1, zCQQuanLyNS.Length - 1);
                    }
                }
            }
            if (zCQQuanLyNS.Length == 0)
            {
                zCQQuanLyNS = OrderTranfer_Data.SearchDepartment(_OrderOrigin.bencust);
            }

            _OrderHeader.TK_ThuNS = zTKNganSach;
            _OrderHeader.TK_ThuNS_Name = HHT.Library.System.Data_Access.ValueOfField("SELECT AccountName FROM FNC_AccountNN WHERE AccountID ='" + zTKNganSach + "'");

            GovTax_Account_Info zCusAccount;
            zCusAccount = new GovTax_Account_Info(zTKNganSach + ".0." + zCQQuanLyNS);

            if (zCusAccount.AccountID.Length > 0)
            {
                _OrderHeader.CQThuID = zCQQuanLyNS;
               // _OrderHeader.CQThuName = zCusAccount.CustomName;
            }

           // _OrderHeader.DBHC_ID = zCusAccount.District_ID;
           // _OrderHeader.DBHC_Name = zCusAccount.Distrist_Name;
           // _OrderHeader.LoaiThueID = zCusAccount.LoaiThue;

            _OrderHeader.NHNN_ID = zCusAccount.TreasuryID;
            _OrderHeader.NHNN_Name = zCusAccount.TreasuryName;
            _OrderHeader.KieuThu = 1;

            _OrderHeader.NHPV_ID = _OrderOrigin.remtbk;
            _OrderHeader.NHPV_Name = _OrderOrigin.remntbk;
            _OrderHeader.NgayNT = _OrderOrigin.trdate;
            _OrderHeader.SoTien = _OrderOrigin.traamt;

            Item_Bill zItem;
            zItem = FindValueItem_OnlyOne(1);
            _OrderHeader.Ma_ST = zItem.Value;
            if (_OrderHeader.Ma_ST.Length > 0)
            {
                int zIndexMST = zItem.Index;
                if (zIndexMST + 3 <= _ListItemInBill.Count)
                {
                    Item_Bill zItemNgay = (Item_Bill)_ListItemInBill[zIndexMST + 2];
                    if (zItemNgay.CategoryID == 13)
                    {
                        _OrderHeader.NgayNT = DateTime.Parse(zItemNgay.Value);
                    }

                }
            }

            Item_Bill zItem1;
            zItem1 = FindValueItem_OnlyOne(13, "NGAYNT");
            if (zItem1.CategoryID == 13)
            {
                _OrderHeader.ParseNgayNT = zItem1.Value;
                _OrderHeader.NgayNT = DateTime.Parse(zItem1.Value);
            }




        }
        private void CheckNhieuToKhai()
        {
            int zAmount = FindAmountCategory(2);
            if ((zAmount > 1) && (_OrderHeader.SoTien % 20000 == 0))
            {
                double zMoney = OrderDetailTotal_ToKhai();
                if (zMoney > 0 && zMoney == _OrderHeader.SoTien)
                {
                    _OrderHeader.ToKhai = "9999999999";
                    _OrderHeader.LH_ID = "99999";
                    _OrderHeader.LH_Name = "Nhiều tờ khai là lệ phí hải quan";
                    _OrderHeader.NgayDK = DateTime.Now;
                }
            }
        }

        #region Check Info Of Invoice
        private int CheckOrder()
        {
            int zAmount = CheckOrderHeader();
            zAmount += CheckOrderTKNgayLH();
            zAmount += CheckOrderDetail();

            return zAmount;
        }
        private int CheckOrderHeader()
        {
            int zResult = 0;
            string zMessage = "";
            if (_OrderHeader.CQThuID.Length == 0)
            {
                zMessage += "CQThuID/";
                zResult++;
            }
            if (_OrderHeader.TK_ThuNS.Length == 0)
            {
                zMessage += "TK_ThuNS/";
                zResult++;
            }
            if (_OrderHeader.DBHC_Name.Length == 0)
            {
                zMessage += "DBHC_Name/";
                zResult++;
            }
            if (_OrderHeader.LoaiThueID.Length == 0)
            {
                zMessage += "LoaiThueID/";
                zResult++;
            }

            if (_OrderHeader.Ma_ST.Length == 10 || _OrderHeader.Ma_ST.Length == 13)
            {
            }
            else
            {
                zMessage += "Ma_ST/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            return zResult;
        }
        private int CheckOrderTKNgayLH()
        {
            int zResult = 0;
            string zMessage = "";
            if (_OrderHeader.LoaiThueID == "04")
            {
                if (_OrderHeader.ToKhai.Length == 0)
                {
                    zMessage += "ToKhai/";
                    zResult++;
                }
                if (!_OrderHeader.IsNgayDK)
                {
                    zMessage += "NgayDK/";
                    zResult++;
                }
                if (_OrderHeader.LH_ID.Length == 0)
                {
                    zMessage += "LH_Name/";
                    zResult++;
                }
                else
                {
                    if (_OrderHeader.LH_ID != "99999")
                    {
                        _OrderHeader.LH_Name = Data_Access.LH_Name(_OrderHeader.LH_ID);
                        if (_OrderHeader.LH_Name.Length == 0)
                        {
                            zMessage += "LH_Name/";
                            zResult++;
                        }
                    }
                }

                if (zResult > 0)
                    Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            }
            return zResult;
        }
        private int CheckOrderDetail()
        {
            Order_Detail_Info zDetail, zDetailTop;
            int zResult = 0;
            string zMessage = "";

            if (_OrderDetail.Count == 0)
            {
                zMessage += "0_ALL/";
                zResult++;
            }
            if (_OrderDetail.Count == 1)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[0];
                if (zDetail.SoTien == 0)
                    zDetail.SoTien = _OrderHeader.SoTien;
            }
            if (_OrderDetail.Count > 1)
            {
                zDetailTop = (Order_Detail_Info)_OrderDetail[0];
                // Chu y phan nay
                if (_OrderHeader.LoaiThueID == "01")
                {
                    if (zDetailTop.KyThue.Length == 0)
                    {
                        Item_Bill zTemp = FindValueItem_OnlyOne(13);
                        if (zTemp.CategoryID == 13)
                        {
                            zDetailTop.KyThue = zTemp.Value.Substring(3, 7);
                        }
                        else
                            zDetailTop.KyThue = DateTime.Now.ToString("MM/yyyy");
                    }
                }
                for (int i = 1; i < _OrderDetail.Count; i++)
                {
                    zDetail = (Order_Detail_Info)_OrderDetail[i];
                    if (zDetail.ChuongID.Length == 0)
                        zDetail.ChuongID = zDetailTop.ChuongID;
                    if (zDetail.KyThue.Length == 0)
                        zDetail.KyThue = zDetailTop.KyThue;
                }
            }

            //------------------------------
            double zSumMoney = 0;
            for (int i = 0; i < _OrderDetail.Count; i++)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[i];
                zSumMoney += zDetail.SoTien;
                if (zDetail.ChuongID.Length == 0)
                {
                    zMessage += "C/";
                    zResult++;
                }

                if (zDetail.NDKT_ID.Length == 0)
                {
                    zMessage += "TM/";
                    zResult++;
                }
                if (zDetail.NDKT_Name.Length == 0)
                {
                    zMessage += "TM_NAME/";
                    zResult++;
                }
                if (zDetail.SoTien == 0)
                {
                    zMessage += "ST/";
                    zResult++;
                }
                if (_OrderHeader.LoaiThueID == "01")
                {
                    if (zDetail.KyThue.Length == 0)
                    {
                        zMessage += "KYTHUE/";
                        zResult++;
                    }
                }
            }

            if (zSumMoney != _OrderOrigin.traamt)
            {
                zMessage += "TOTAL MONEY/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            return zResult;
        }
        #endregion

        #region Save Info Order
        private void SaveOrderDone()
        {
            _OrderHeader.StatusAnalysis = 2;
            _AmountSuccess++;

            GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value = _OrderHeader.StatusAnalysis;

            _OrderOrigin.UpdateStatus(2, CategoryOrder());

            _OrderHeader.Save();
            for (int i = 0; i < _OrderDetail.Count; i++)
            {
                Order_Detail_Info zOrderDetail = (Order_Detail_Info)_OrderDetail[i];
                zOrderDetail.trref = _OrderHeader.trref;
                zOrderDetail.Save();
            }
        }
        #endregion

        #region Find Rule
        private bool Rule_TK_Ngay_LH()
        {
            Item_Bill zItemTK, zItemNgay, zItemLHXNK;
            bool zResult = false;
            //-----------Tim To Khai---------------
            zItemTK = FindValueItem_OnlyOne(2);
            if (zItemTK.Value.Length == 0)
            {
                zItemTK = FindTK();
            }
            if (zItemTK.Value.Length >= 11)
            {
                _OrderHeader.ToKhai = zItemTK.Value;
                _OrderHeader.ToKhaiIndex = zItemTK.Index;
                if (_OrderHeader.ToKhaiIndex + 5 < _ListItemInBill.Count)
                {
                    // Rule TK_NGAY_LHXNK
                    zItemNgay = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 2];
                    zItemLHXNK = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 4];
                    if (zItemNgay.CategoryID == 13)
                    {
                        _OrderHeader.ParseNgayDK = zItemNgay.Value;
                    }
                    if (zItemLHXNK.CategoryID == 3)
                    {
                        _OrderHeader.LH_ID = zItemLHXNK.Value;
                        _OrderHeader.LH_Name = zItemLHXNK.Extent;
                    }
                    if (_OrderHeader.NgayDK == null)
                    {
                        Item_Bill zItemNgayVal = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 3];
                        string zNgayDK = Function.GetCorrectDate(zItemNgayVal.Content.Trim());
                        if (zNgayDK.Length == 10)
                        {
                            zItemNgay.CategoryID = 13;
                            zItemNgay.CategoryName = "Ngày";
                            _OrderHeader.ParseNgayDK = zNgayDK;
                        }
                    }
                    if (_OrderHeader.NgayDK == null)
                    {
                        zItemNgay = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 1];
                        if (zItemNgay.Content.Length > 10)
                        {
                            string zDateTemp = zItemNgay.Content.Substring(zItemNgay.Content.Length - 8, 8);
                            string zNgayDK = Function.GetCorrectDate(zDateTemp);
                            if (zNgayDK.Length == 10)
                                _OrderHeader.ParseNgayDK = zNgayDK;
                        }
                    }
                    if (_OrderHeader.NgayDK != null && _OrderHeader.LH_Name.Length > 4)
                    {
                        zResult = true;
                    }
                    // Rule TK_LHXNK_NGAY
                    if (!zResult)
                    {
                        zItemLHXNK = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 2];
                        zItemNgay = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 4];

                        if (zItemLHXNK.CategoryID == 3)
                        {
                            _OrderHeader.LH_ID = zItemLHXNK.Value;
                            _OrderHeader.LH_Name = zItemLHXNK.Extent;
                        }
                        else
                        {
                            Item_Bill zItemLHXNK_Val = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 3];
                            if (zItemLHXNK_Val.Content.Length >= 2)
                            {
                                string zID = zItemLHXNK.Content + zItemLHXNK_Val.Content.Substring(0, 2);
                                string zName = Data_Access.ValueOfField("SELECT Description FROM BNK_LHXNK WHERE ID ='" + zID + "'");
                                if (zName.Length > 4)
                                {
                                    zItemLHXNK.CategoryID = 3;
                                    zItemLHXNK.Value = zID;
                                    zItemLHXNK.Extent = zName;

                                    _OrderHeader.LH_ID = zItemLHXNK.Value;
                                    _OrderHeader.LH_Name = zItemLHXNK.Extent;

                                    string zDate = zItemLHXNK_Val.Content.Substring(2, zItemLHXNK_Val.Content.Length - 2);
                                    string zNgayDK = Function.GetCorrectDate(zDate.Trim());
                                    if (zNgayDK.Length == 10)
                                        _OrderHeader.ParseNgayDK = zNgayDK;
                                }
                            }
                        }
                        if (zItemNgay.CategoryID == 13)
                        {
                            _OrderHeader.ParseNgayDK = zItemNgay.Value;
                        }
                        if (_OrderHeader.NgayDK == null)
                        {
                            zItemNgay = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 3];
                            if (zItemNgay.Content.Length > 12)
                            {
                                string zDateTemp = zItemNgay.Content.Substring(zItemNgay.Content.Length - 10, 10);
                                string zNgayDK = Function.GetCorrectDate(zDateTemp);
                                if (zNgayDK.Length == 10)
                                    _OrderHeader.ParseNgayDK = zNgayDK;
                            }
                        }
                        if (_OrderHeader.NgayDK != null && _OrderHeader.LH_Name.Length > 4)
                        {
                            zResult = true;
                        }
                    }
                }
            }
            return zResult;
        }
        private void RuleDetail_C_TM_ST_Ext()
        {

            int i;
            Item_Bill zItem_C, zItem_TM, zItem_ST, zItem_KT;
            int n = _ListItemInBill.Count;

            ArrayList zTM = new ArrayList();
            int zMax = 0;
            for (i = 0; i < n; i++)
            {
                zItem_TM = (Item_Bill)_ListItemInBill[i];
                if (zItem_TM.CategoryID == 10)
                {
                    zItem_TM.Index = i;
                    zTM.Add(zItem_TM);
                }
            }
            zMax = zTM.Count;
            //------------------------------------------
            ArrayList zChuong = new ArrayList();
            for (i = 0; i < n; i++)
            {
                zItem_C = (Item_Bill)_ListItemInBill[i];
                if (zItem_C.CategoryID == 12)
                {
                    zItem_C.Index = i;
                    zChuong.Add(zItem_C);
                }
            }
            if (zMax < zChuong.Count)
                zMax = zChuong.Count;
            //------------------------------------------
            ArrayList zST = new ArrayList();
            for (i = 0; i < n; i++)
            {
                zItem_ST = (Item_Bill)_ListItemInBill[i];
                if (zItem_ST.CategoryID == 11)
                {
                    zItem_ST.Index = i;
                    zST.Add(zItem_ST);
                }
            }
            if (zMax < zST.Count)
                zMax = zST.Count;
            if (zST.Count == 0)
            {
                zST = FindValueItem_IsMoney_Ex();
            }
            //------------------------------------------
            ArrayList zKeThue = new ArrayList();
            for (i = 0; i < n; i++)
            {
                zItem_KT = (Item_Bill)_ListItemInBill[i];
                if (zItem_KT.CategoryID == 14)
                {
                    zItem_KT.Index = i;
                    zKeThue.Add(zItem_KT);
                }
            }
            if (zMax < zKeThue.Count)
                zMax = zKeThue.Count;

            if (zChuong.Count == 0 && zTM.Count == 0 && zST.Count > 0)
            {
                for (i = 0; i < _ListItemInBill.Count; i++)
                {
                    if(i == 0)
                    {
                        break;
                    }
                    zItem_ST = (Item_Bill)_ListItemInBill[i];
                    if (zItem_ST.CategoryID == 11)
                    {
                        Item_Bill zItem = (Item_Bill)_ListItemInBill[i - 1];
                        if (zItem.Content.Length == 8)
                        {
                            string[] zC_and_TM = zItem.Content.Split(',');
                            if (zC_and_TM.Length == 2)
                            {
                                zItem_C = new Item_Bill();
                                zItem_TM = new Item_Bill();
                                if (zC_and_TM[0].Length == 4)
                                {
                                    zItem_C.Value = zC_and_TM[1];
                                    zItem_TM.Value = zC_and_TM[0];

                                }
                                if (zC_and_TM[0].Length == 3)
                                {
                                    zItem_C.Value = zC_and_TM[0];
                                    zItem_TM.Value = zC_and_TM[1];
                                }
                                zItem_C.Content = Data_Access.Chuong_Name(zItem_C.Value);
                                zItem_TM.Content = Data_Access.NDKT_Name(zItem_TM.Value);
                                if (zItem_C.Content.Length > 0 && zItem_TM.Content.Length > 0)
                                {
                                    zChuong.Add(zItem_C);
                                    zTM.Add(zItem_TM);
                                }
                            }
                        }
                    }
                }
            }
            _OrderDetail = new ArrayList();
            Order_Detail_Info zDetail;

            for (i = 0; i < zMax; i++)
            {
                zDetail = new Order_Detail_Info();
                if (i < zChuong.Count)
                {
                    zItem_C = (Item_Bill)zChuong[i];
                    zDetail.ChuongID = zItem_C.Value;
                }
                if (i < zTM.Count)
                {
                    zItem_TM = (Item_Bill)zTM[i];
                    zDetail.NDKT_ID = zItem_TM.Value;
                    zDetail.NDKT_Name = zItem_TM.Extent;
                }
                if (i < zST.Count)
                {
                    zItem_ST = (Item_Bill)zST[i];
                    zDetail.SoTien = double.Parse(zItem_ST.Value);
                }
                if (_OrderHeader.LoaiThueID == "01")
                {
                    if (i < zKeThue.Count)
                    {
                        zItem_KT = (Item_Bill)zKeThue[i];
                        zDetail.KyThue = zItem_KT.Value;
                    }
                }
                _OrderDetail.Add(zDetail);
            }
            //-----------------Tim so tien-------------------


            //-----------------------------------------------
            if (_OrderDetail.Count > 1)
            {

                zDetail = (Order_Detail_Info)_OrderDetail[0];
                string str_Chuong = zDetail.ChuongID;
                string str_KyThue = zDetail.KyThue;
                for (i = 1; i < _OrderDetail.Count; i++)
                {
                    zDetail = (Order_Detail_Info)_OrderDetail[i];
                    if (zDetail.ChuongID.Length == 0)
                    {
                        zDetail.ChuongID = str_Chuong;
                    }
                    if (zDetail.KyThue.Length == 0)
                    {
                        zDetail.KyThue = str_KyThue;
                    }

                }
            }
            if (_OrderDetail.Count == 1)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[0];
                if (zDetail.SoTien == 0)
                {
                    zDetail.SoTien = _OrderHeader.SoTien;
                }
            }
            for (i = 0; i < _OrderDetail.Count; i++)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[i];
                zDetail.NDKT_Name = HHT.Library.System.Data_Access.ValueOfField("SELECT Description FROM BNK_NDKT WHERE ID ='" + zDetail.NDKT_ID + "'");
            }
        }
        private double OrderDetailTotal()
        {
            double zResult = 0;
            int i;
            Order_Detail_Info zDetail;

            for (i = 0; i < _OrderDetail.Count; i++)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[i];
                if (zDetail.ChuongID.Length == 0 || zDetail.NDKT_ID.Length == 0 || zDetail.NDKT_Name.Length == 0)
                {
                    zResult = 0;
                    break;
                }
                else
                {
                    zResult += zDetail.SoTien;
                }
            }

            return zResult;
        }
        private double OrderDetailTotal_ToKhai()
        {
            double zResult = 0;
            int i;
            Order_Detail_Info zDetail;
            Order_Detail_Info zDetail_1 = new Order_Detail_Info();
            if (_OrderDetail.Count > 0)
            {
                zDetail_1 = (Order_Detail_Info)_OrderDetail[0];
                if (zDetail_1.SoTien % 20000 == 0)
                {
                    zResult = zDetail_1.SoTien;
                    for (i = 1; i < _OrderDetail.Count; i++)
                    {
                        zDetail = (Order_Detail_Info)_OrderDetail[i];
                        if (zDetail.ChuongID == zDetail_1.ChuongID &&
                            zDetail.NDKT_ID == zDetail_1.NDKT_ID &&
                            zDetail.NDKT_Name == zDetail_1.NDKT_Name)
                        {
                            zResult += zDetail.SoTien;
                        }
                        else
                        {
                            zResult = 0;
                            break;
                        }
                    }
                }
            }
            if (zResult > 0 && zResult % 20000 == 0)
            {
                _OrderDetail = new ArrayList();
                zDetail_1.SoTien = zResult;
                _OrderDetail.Add(zDetail_1);
            }
            return zResult;
        }
        #endregion

        #region Function Check
        private bool CheckItemIsNumber(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            bool zIsCorrect = false;
            int i = 0;
            if (zRule.RuleID > 0)
            {
                for (i = 0; i < zRule.LengValue.Length; i++)
                {
                    double zNumber = 0;
                    if (In_Content.Length == zRule.LengValue[i] && double.TryParse(In_Content, out zNumber))
                    {
                        zIsCorrect = true;
                        break;
                    }
                }
            }
            return zIsCorrect;
        }
        private string CheckItemIsMoney(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            string zResult = "";
            if (zRule.RuleID > 0)
            {
                zResult = Function.GetOnlyNumberInFormat(In_Content);
                if (zResult.Length > zRule.LengMax)
                {
                    zResult = "";
                }
            }
            return zResult;
        }

        private string CheckItemIsDate(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            string zResult = "";
            if (zRule.RuleID > 0)
            {
                if (In_Content.Length <= zRule.LengMax)
                {
                    zResult = Function.GetCorrectDate(In_Content);
                }
            }
            return zResult;
        }
        private string CheckItemIsKyThue(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            string zResult = "";
            if (zRule.RuleID > 0)
            {
                if (In_Content.Length <= zRule.LengMax)
                {
                    zResult = Function.GetCorrectMonthYear(In_Content);

                }
            }
            return zResult;
        }
        private string CheckItemValue_Extension(int RuleID, int RuleStyle, string In_Content, string Query)
        {
            string zResult = "";
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            if (zRule.RuleID > 0)
            {
                for (int i = 0; i < zRule.LengValue.Length; i++)
                {
                    double zNumber = 0;
                    // Message_System("  Rule Leng : " + zRule.LengValue[i].ToString() + ":" + In_Content);
                    if (In_Content.Length == zRule.LengValue[i] && double.TryParse(In_Content, out zNumber))
                    {
                        zResult = HHT.Library.System.Data_Access.ValueOfField(Query + "'" + In_Content + "'");
                        if (zResult.Length > 5)
                        {
                            break;
                        }
                    }
                }
            }
            return zResult;
        }
        private string CheckItemValue_LHXNK(int RuleID, int RuleStyle, string In_Content, string Query)
        {
            string zResult = "";
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            bool zIsCorrect = false;
            if (zRule.RuleID > 0)
            {
                for (int i = 0; i < zRule.LengValue.Length; i++)
                {
                    if (In_Content.Length == zRule.LengValue[i])
                    {
                        zResult = HHT.Library.System.Data_Access.ValueOfField(Query + "'" + In_Content + "'");
                        if (zResult.Length > 5)
                        {
                            zIsCorrect = true;
                            break;
                        }
                    }
                }
            }
            return zResult;
        }

        private Item_Bill FindValueItem_OnlyOne(int CategoryID)
        {
            Item_Bill zResult = new Item_Bill();
            int zAmount = 0;
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == CategoryID)
                {
                    if (zResult.Value != zItem.Value)
                    {
                        zResult = zItem;
                        zResult.Index = i;
                        zAmount++;
                    }
                }
            }
            if (zAmount > 1)
                zResult = new Item_Bill();
            return zResult;
        }
        #endregion

        private Item_Bill FindValueItem_OnlyOne(int CategoryID, string WordCode)
        {
            Item_Bill zResult = new Item_Bill();
            int zAmount = 0;
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == CategoryID && WordCode == zItem.WordCode)
                {
                    if (zResult.Value != zItem.Value)
                    {
                        zResult = zItem;
                        zResult.Index = i;
                        zAmount++;
                    }
                }
            }
            if (zAmount > 1)
                zResult = new Item_Bill();
            return zResult;
        }

        private int FindAmountCategory(int CategoryID)
        {
            Item_Bill zResult = new Item_Bill();
            int zAmount = 0;
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == CategoryID)
                {
                    if (zResult.Value != zItem.Value)
                    {
                        zResult = zItem;
                        zResult.Index = i;
                        zAmount++;
                    }
                }
            }
            return zAmount;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LB_Log.Items.Clear();
        }

        //------------------------------
        private void FindMST()
        {
            for (int i = 0; i < _ListItemInBill.Count - 1; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == 0)
                {
                    Item_Bill zItem_Val = (Item_Bill)_ListItemInBill[i + 1];
                    if (zItem_Val.Content.Length >= 10)
                    {
                        DataTable zTable = Analysis_Data.ListWordCodeName(1);
                        bool zFound = false;
                        foreach (DataRow zRow in zTable.Rows)
                        {
                            if (zItem.Content.Contains(zRow["WordCode"].ToString()))
                            {
                                zFound = true;
                                break;
                            }

                        }
                        if (zFound)
                        {
                            string zTemp = "";
                            for (int j = 0; j < zItem_Val.Content.Length; j++)
                            {
                                if (zItem_Val.Content[j].ToString().Trim().Length > 0)
                                {
                                    zTemp += zItem_Val.Content[j].ToString();
                                }
                            }
                            if (zTemp.Length == 10 || zTemp.Length == 13)
                            {
                                double zNumber = 0;
                                if (double.TryParse(zTemp, out zNumber))
                                {
                                    zItem.CategoryID = 1;
                                    zItem.CategoryName = "MST";
                                    zItem.Value = zTemp;

                                    _OrderHeader.Ma_ST = zTemp;
                                }
                            }
                        }
                    }
                }

            }

        }
        private Item_Bill FindTK()
        {
            Item_Bill zResult = new Item_Bill();
            for (int i = 0; i < _ListItemInBill.Count - 1; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == 0)
                {
                    Item_Bill zItem_Val = (Item_Bill)_ListItemInBill[i + 1];
                    if (zItem_Val.Content.Length >= 10)
                    {
                        DataTable zTable = Analysis_Data.ListWordCodeName(2);
                        bool zFound = false;
                        foreach (DataRow zRow in zTable.Rows)
                        {
                            if (zItem.Content.Contains(zRow["WordCode"].ToString()))
                            {
                                zFound = true;
                                break;
                            }

                        }
                        if (zFound)
                        {
                            string zTemp = "";
                            for (int j = 0; j < zItem_Val.Content.Length; j++)
                            {
                                if (zItem_Val.Content[j].ToString().Trim().Length > 0)
                                {
                                    if (zItem_Val.Content[j] != '.')
                                        zTemp += zItem_Val.Content[j].ToString();
                                }
                            }
                            if (zTemp.Length == 12 || zTemp.Length == 11)
                            {
                                double zNumber = 0;
                                if (double.TryParse(zTemp, out zNumber))
                                {
                                    zItem.Value = zTemp;
                                    zItem.CategoryID = 2;
                                    zItem.CategoryName = "Tờ Khai";
                                    zItem.Index = i;
                                    zResult = zItem;
                                    break;
                                }
                            }
                        }
                    }
                }

            }

            return zResult;
        }
        private void FindNgayDK_Ext()
        {
            if (_OrderHeader.ToKhai.Length == 11)
            {
                if (_OrderHeader.ToKhaiIndex + 3 < _ListItemInBill.Count && _OrderHeader.ToKhaiIndex - 3 > 0)
                {
                    Item_Bill zItemNgay_1 = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 2];
                    Item_Bill zItemNgay_2 = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex - 2];
                    if (zItemNgay_1.CategoryID == 13)
                    {
                        _OrderHeader.ParseNgayDK = zItemNgay_1.Value;
                    }
                    else
                    {
                        if (zItemNgay_2.CategoryID == 13)
                        {
                            _OrderHeader.ParseNgayDK = zItemNgay_2.Value;
                        }
                    }
                }

                if (_OrderHeader.NgayDK == null)
                {
                    for (int i = 0; i < _ListItemInBill.Count - 1; i++)
                    {
                        Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                        if (zItem.CategoryID == 13)
                        {
                            if (zItem.Content.Contains("NDK"))
                            {
                                _OrderHeader.ParseNgayDK = zItem.Value;
                            }

                        }
                    }
                }
            }
        }
        private void FindLHXNK_Ext()
        {
            for (int i = 0; i < _ListItemInBill.Count - 1; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == 0)
                {
                    Item_Bill zItem_Val = (Item_Bill)_ListItemInBill[i + 1];
                    if (zItem_Val.Content.Length >= 2 && zItem.Content.Length >= 1)
                    {
                        string zID = zItem.Content.Substring(zItem.Content.Length - 1, 1);
                        zID += zItem_Val.Content.Substring(0, 2);
                        string zLH_Name = Data_Access.LH_Name(zID);
                        if (zLH_Name.Length > 4)
                        {
                            zItem.CategoryID = 3;
                            zItem.CategoryName = "Loại Hình XNK";
                            zItem.Value = zID;
                            zItem.Extent = zLH_Name;
                            _OrderHeader.LH_ID = zID;
                            _OrderHeader.LH_Name = zLH_Name;
                            break;
                        }
                    }
                }


            }
        }

        private ArrayList FindValueItem_IsMoney_Ex()
        {
            ArrayList zST = new ArrayList();
            DataTable zListWordcodeForName = Analysis_Data.ListWordCodeName_Ext(11);

            for (int k = 0; k < zListWordcodeForName.Rows.Count; k++)
            {
                string zCode = zListWordcodeForName.Rows[k]["WordCode"].ToString();
                for (int i = 0; i < _ListItemInBill.Count; i++)
                {
                    Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                    if (zItem.Content.Contains(zCode) && zItem.CategoryID == 0)
                    {
                        if (i < _ListItemInBill.Count - 1)
                        {
                            Item_Bill zItemMoney = (Item_Bill)_ListItemInBill[i + 1];
                            string zResult = Function.GetOnlyNumberInFormat(zItemMoney.Content);
                            if (zResult.Length > 4)
                            {
                                zItem.Value = zResult;
                                zItem.CategoryID = 11;
                                zItem.CategoryName = "Số Tiền";
                                zItem.Index = i;
                                zST.Add(zItem);
                            }
                        }
                    }
                }
            }
            if (zST.Count > 1)
                SortItem(zST);
            return zST;


        }

        // Tim theo luat C000TM0000 00000000
        private void Find_C_TM_ST()
        {
            string Rem = _OrderOrigin.remark.ToUpper();

            ArrayList zList = new ArrayList();
            Order_Detail_Info zDetail;
            int i = 0;
            int k = 0;
            int j = 0;
            while (i < Rem.Length - 15)
            {
                if (Rem[i] == 'C')
                {
                    zDetail = new Order_Detail_Info();
                    string zChuongID = Rem.Substring(i + 1, 3);
                    int zNumberTemp = 0;
                    if (int.TryParse(zChuongID, out zNumberTemp))
                    {
                        zDetail.ChuongID = zChuongID;
                        k = i + 4;
                        j = 0;
                        while (j < 3)
                        {
                            if (Rem[k] == ' ' || Rem[k] == ',')
                            {
                                k++;
                            }
                            else
                            {
                                if (int.TryParse(Rem[k].ToString(), out zNumberTemp))
                                {
                                    break;
                                }
                            }
                            j++;
                        }
                        if (Rem[k] == 'T' && Rem[k + 1] == 'M')
                        {
                            string zTMID = Rem.Substring(k + 2, 4);
                            if (int.TryParse(zTMID, out zNumberTemp))
                            {
                                zDetail.NDKT_ID = zTMID;

                                k = k + 6;
                                j = 0;
                                while (j < 3)
                                {
                                    if (Rem[k] == ' ' || Rem[k] == ',' || Rem[k] == ')')
                                    {
                                        k++;
                                    }
                                    else
                                    {
                                        if (int.TryParse(Rem[k].ToString(), out zNumberTemp))
                                        {
                                            break;
                                        }
                                    }
                                    j++;
                                }
                                bool zIsContinus = true;
                                j = 0;
                                int zAscii = 0;
                                string zMoney = "";
                                while (zIsContinus || j < 7)
                                {
                                    if (k + j >= Rem.Length)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        zAscii = (int)Rem[k + j];
                                        if ((zAscii > 47 && zAscii < 58))
                                        {
                                            zMoney += Rem[k + j];
                                        }
                                        else
                                        {
                                            if (zAscii != 46)
                                                break;
                                        }
                                        j++;
                                    }

                                }
                                if (zMoney.Length > 0)
                                {
                                    zDetail.SoTien = double.Parse(zMoney);
                                }
                                zList.Add(zDetail);
                            }
                        }
                    }
                }
                i++;
            }
            if (zList.Count > 0)
                _OrderDetail = new ArrayList();

            for (i = 0; i < zList.Count; i++)
            {
                zDetail = (Order_Detail_Info)zList[i];
                zDetail.ChuongName = Data_Access.Chuong_Name(zDetail.ChuongID);
                zDetail.NDKT_Name = Data_Access.NDKT_Name(zDetail.NDKT_ID);
                if (zDetail.ChuongName.Length > 0 && zDetail.NDKT_Name.Length > 0)
                {
                    _OrderDetail.Add(zDetail);
                }
            }
        }

        private void SortItem(ArrayList ListItem)
        {
            int i, j, min_idx;
            int n = ListItem.Count;
            Item_Bill zItem_min_idx, zItem_j;
            // One by one move boundary of unsorted subarray 
            for (i = 0; i < n - 1; i++)
            {
                // Find the minimum element in unsorted array 
                min_idx = i;

                zItem_min_idx = (Item_Bill)ListItem[min_idx];
                for (j = i + 1; j < n; j++)
                {
                    zItem_j = (Item_Bill)ListItem[j];
                    if (zItem_j.Index < zItem_min_idx.Index)
                        min_idx = j;
                }
                // Swap the found minimum element with the first element 
                if (min_idx != i)
                {
                    Item_Bill zTemp = (Item_Bill)ListItem[min_idx];
                    zItem_j = (Item_Bill)ListItem[i];
                    ListItem[min_idx] = zItem_j;
                    ListItem[i] = zTemp;
                }
            }
        }
    }
}
