﻿namespace HHT_AutoTax
{
    partial class FrmAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Del = new System.Windows.Forms.Button();
            this.btn_Create = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.txt_CategoryTax = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_DBHC = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_TreasuryID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_TreasuryName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_MaQHNS = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_DepartmentID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_DepartmentName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_AccountName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_AccountID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Search = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Search_Content = new System.Windows.Forms.TextBox();
            this.LV_Data = new System.Windows.Forms.ListView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Azure;
            this.panel1.Controls.Add(this.btn_Del);
            this.panel1.Controls.Add(this.btn_Create);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.txt_CategoryTax);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txt_DBHC);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txt_TreasuryID);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txt_TreasuryName);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txt_MaQHNS);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txt_DepartmentID);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txt_DepartmentName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txt_AccountName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txt_AccountID);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(602, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(308, 495);
            this.panel1.TabIndex = 0;
            // 
            // btn_Del
            // 
            this.btn_Del.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Del.Location = new System.Drawing.Point(219, 276);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.Size = new System.Drawing.Size(81, 32);
            this.btn_Del.TabIndex = 4;
            this.btn_Del.Text = "Xóa";
            this.btn_Del.UseVisualStyleBackColor = true;
            this.btn_Del.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Create
            // 
            this.btn_Create.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Create.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Create.Location = new System.Drawing.Point(36, 276);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(81, 32);
            this.btn_Create.TabIndex = 4;
            this.btn_Create.Text = "Tạo Mới";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Save.Location = new System.Drawing.Point(123, 276);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(81, 32);
            this.btn_Save.TabIndex = 4;
            this.btn_Save.Text = "Cập Nhật";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // txt_CategoryTax
            // 
            this.txt_CategoryTax.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_CategoryTax.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_CategoryTax.ForeColor = System.Drawing.Color.Navy;
            this.txt_CategoryTax.Location = new System.Drawing.Point(88, 235);
            this.txt_CategoryTax.Name = "txt_CategoryTax";
            this.txt_CategoryTax.Size = new System.Drawing.Size(208, 21);
            this.txt_CategoryTax.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(1, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 15);
            this.label9.TabIndex = 2;
            this.label9.Text = "Loại Thuế";
            // 
            // txt_DBHC
            // 
            this.txt_DBHC.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DBHC.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_DBHC.ForeColor = System.Drawing.Color.Navy;
            this.txt_DBHC.Location = new System.Drawing.Point(88, 208);
            this.txt_DBHC.Name = "txt_DBHC";
            this.txt_DBHC.Size = new System.Drawing.Size(208, 21);
            this.txt_DBHC.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(1, 211);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "DBHC";
            // 
            // txt_TreasuryID
            // 
            this.txt_TreasuryID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_TreasuryID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TreasuryID.ForeColor = System.Drawing.Color.Navy;
            this.txt_TreasuryID.Location = new System.Drawing.Point(88, 154);
            this.txt_TreasuryID.Name = "txt_TreasuryID";
            this.txt_TreasuryID.Size = new System.Drawing.Size(208, 21);
            this.txt_TreasuryID.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(1, 157);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "Mã Kho Bạc";
            // 
            // txt_TreasuryName
            // 
            this.txt_TreasuryName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_TreasuryName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TreasuryName.ForeColor = System.Drawing.Color.Navy;
            this.txt_TreasuryName.Location = new System.Drawing.Point(88, 181);
            this.txt_TreasuryName.Name = "txt_TreasuryName";
            this.txt_TreasuryName.Size = new System.Drawing.Size(208, 21);
            this.txt_TreasuryName.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(1, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tên Kho Bạc";
            // 
            // txt_MaQHNS
            // 
            this.txt_MaQHNS.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_MaQHNS.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MaQHNS.ForeColor = System.Drawing.Color.Navy;
            this.txt_MaQHNS.Location = new System.Drawing.Point(88, 73);
            this.txt_MaQHNS.Name = "txt_MaQHNS";
            this.txt_MaQHNS.Size = new System.Drawing.Size(208, 21);
            this.txt_MaQHNS.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(1, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mã QHNS";
            // 
            // txt_DepartmentID
            // 
            this.txt_DepartmentID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DepartmentID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_DepartmentID.ForeColor = System.Drawing.Color.Navy;
            this.txt_DepartmentID.Location = new System.Drawing.Point(88, 100);
            this.txt_DepartmentID.Name = "txt_DepartmentID";
            this.txt_DepartmentID.Size = new System.Drawing.Size(208, 21);
            this.txt_DepartmentID.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(1, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 15);
            this.label10.TabIndex = 2;
            this.label10.Text = "Mã CQ QL Thu";
            // 
            // txt_DepartmentName
            // 
            this.txt_DepartmentName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DepartmentName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_DepartmentName.ForeColor = System.Drawing.Color.Navy;
            this.txt_DepartmentName.Location = new System.Drawing.Point(88, 127);
            this.txt_DepartmentName.Name = "txt_DepartmentName";
            this.txt_DepartmentName.Size = new System.Drawing.Size(208, 21);
            this.txt_DepartmentName.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(1, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "CQ QL Thu";
            // 
            // txt_AccountName
            // 
            this.txt_AccountName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_AccountName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AccountName.ForeColor = System.Drawing.Color.Navy;
            this.txt_AccountName.Location = new System.Drawing.Point(88, 46);
            this.txt_AccountName.Name = "txt_AccountName";
            this.txt_AccountName.Size = new System.Drawing.Size(208, 21);
            this.txt_AccountName.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(1, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên TK";
            // 
            // txt_AccountID
            // 
            this.txt_AccountID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_AccountID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AccountID.ForeColor = System.Drawing.Color.Navy;
            this.txt_AccountID.Location = new System.Drawing.Point(88, 19);
            this.txt_AccountID.Name = "txt_AccountID";
            this.txt_AccountID.Size = new System.Drawing.Size(208, 21);
            this.txt_AccountID.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(1, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Số Hiệu TK";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.panel2.Controls.Add(this.btn_Search);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txt_Search_Content);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(602, 67);
            this.panel2.TabIndex = 1;
            // 
            // btn_Search
            // 
            this.btn_Search.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Search.Location = new System.Drawing.Point(409, 38);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(81, 25);
            this.btn_Search.TabIndex = 4;
            this.btn_Search.Text = "Tìm";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(391, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "DANH SÁCH TÀI KHOẢN THU NGÂN SÁCH";
            // 
            // txt_Search_Content
            // 
            this.txt_Search_Content.BackColor = System.Drawing.Color.White;
            this.txt_Search_Content.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search_Content.ForeColor = System.Drawing.Color.Navy;
            this.txt_Search_Content.Location = new System.Drawing.Point(16, 40);
            this.txt_Search_Content.Name = "txt_Search_Content";
            this.txt_Search_Content.Size = new System.Drawing.Size(387, 21);
            this.txt_Search_Content.TabIndex = 3;
            // 
            // LV_Data
            // 
            this.LV_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Data.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Data.ForeColor = System.Drawing.Color.Navy;
            this.LV_Data.FullRowSelect = true;
            this.LV_Data.GridLines = true;
            this.LV_Data.HideSelection = false;
            this.LV_Data.Location = new System.Drawing.Point(0, 67);
            this.LV_Data.Name = "LV_Data";
            this.LV_Data.Size = new System.Drawing.Size(602, 428);
            this.LV_Data.TabIndex = 6;
            this.LV_Data.UseCompatibleStateImageBehavior = false;
            this.LV_Data.View = System.Windows.Forms.View.Details;
            this.LV_Data.ItemActivate += new System.EventHandler(this.LV_Data_ItemActivate);
            // 
            // FrmAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 495);
            this.Controls.Add(this.LV_Data);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FrmAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tài Khoản Thu Ngân Sách";
            this.Load += new System.EventHandler(this.FrmAccount_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView LV_Data;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_AccountID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_DBHC;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_TreasuryID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_TreasuryName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_MaQHNS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_DepartmentName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_AccountName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.TextBox txt_Search_Content;
        private System.Windows.Forms.Button btn_Del;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.TextBox txt_CategoryTax;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_DepartmentID;
        private System.Windows.Forms.Label label10;
    }
}