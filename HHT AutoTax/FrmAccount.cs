﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;
namespace HHT_AutoTax
{
    public partial class FrmAccount : Form
    {
        public FrmAccount()
        {
            InitializeComponent();
        }

        private void FrmAccount_Load(object sender, EventArgs e)
        {
            InitListView(LV_Data);
            DataTable zListAccount = Bank_Data.TaiKhoanNSNN("A.TreasuryID ASC, DepartmentName ASC, AccountID ASC");
            LoadDataToListView(zListAccount);
        }
        private void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số hiệu tài khoản";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên tài khoản";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã CQ";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Cơ quan quản lý thu";
            colHead.Width = 240;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            

            colHead = new ColumnHeader();
            colHead.Text = "Mã QHNS";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mở Tại KB";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Kho Bạc";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "DBHC";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);



        }
        public void LoadDataToListView(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AccountID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AccountName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["DepartmentID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["DepartmentName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

               

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["QHNS"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TreasuryName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TreasuryID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["DBHC"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryTax"].ToString().Trim();
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }

        private void Load_Info(string ID)
        {
            GovTax_Account_Info zAccount = new GovTax_Account_Info(ID);
            txt_AccountID.Text = zAccount.AccountID;
            txt_AccountName.Text = zAccount.AccountName;
            txt_DepartmentID.Text = zAccount.DepartmentID;
            txt_DepartmentName.Text = zAccount.DepartmentName;
            txt_MaQHNS.Text = zAccount.QHNS;
            txt_TreasuryID.Text = zAccount.TreasuryID;
            txt_TreasuryName.Text = zAccount.TreasuryName;
            txt_DBHC.Text = zAccount.DBHC;
            txt_CategoryTax.Text = zAccount.CategoryTax;

        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            GovTax_Account_Info zAccount = new GovTax_Account_Info();
            zAccount.AccountID = txt_AccountID.Text;
            zAccount.AccountName = txt_AccountName.Text;
            zAccount.DepartmentID = txt_DepartmentID.Text;
            zAccount.DepartmentName = txt_DepartmentName.Text;
            zAccount.QHNS = txt_MaQHNS.Text;
            zAccount.TreasuryID = txt_TreasuryID.Text;
            zAccount.TreasuryName = txt_TreasuryName.Text;
            zAccount.DBHC = txt_DBHC.Text;
            zAccount.CategoryTax = txt_CategoryTax.Text;
            zAccount.Update();
            if (zAccount.Message.Length > 0)
            {
                MessageBox.Show(zAccount.Message);
            }
            else
            {
                DataTable zListAccount = new DataTable();
                if (txt_Search_Content.Text.Trim().Length == 0)
                    zListAccount = Bank_Data.TaiKhoanNSNN("A.TreasuryID ASC, DepartmentName ASC, AccountID ASC");
                else
                    zListAccount = Bank_Data.TaiKhoanNSNN(txt_Search_Content.Text, "A.TreasuryID ASC, DepartmentName ASC, AccountID ASC");
                LoadDataToListView(zListAccount);
            }
        }

        private void LV_Data_ItemActivate(object sender, EventArgs e)
        {
            string ID = LV_Data.SelectedItems[0].SubItems[1].Text;
            Load_Info(ID);
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            DataTable zListAccount = Bank_Data.TaiKhoanNSNN(txt_Search_Content.Text, "A.TreasuryID ASC, DepartmentName ASC, AccountID ASC");
            LoadDataToListView(zListAccount);
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            GovTax_Account_Info zAccount = new GovTax_Account_Info();
            zAccount.AccountID = txt_AccountID.Text;
            zAccount.AccountName = txt_AccountName.Text;
            zAccount.DepartmentID = txt_DepartmentID.Text;
            zAccount.DepartmentName = txt_DepartmentName.Text;
            zAccount.QHNS = txt_MaQHNS.Text;
            zAccount.TreasuryID = txt_TreasuryID.Text;
            zAccount.TreasuryName = txt_TreasuryName.Text;
            zAccount.DBHC = txt_DBHC.Text;
            zAccount.CategoryTax = txt_CategoryTax.Text;
            zAccount.Create();
            if (zAccount.Message.Length > 0)
            {
                MessageBox.Show(zAccount.Message);
            }
            else
            {
                DataTable zListAccount = new DataTable();
                if (txt_Search_Content.Text.Trim().Length == 0)
                    zListAccount = Bank_Data.TaiKhoanNSNN("A.TreasuryID ASC, DepartmentName ASC, AccountID ASC");
                else
                    zListAccount = Bank_Data.TaiKhoanNSNN(txt_Search_Content.Text, "A.TreasuryID ASC, DepartmentName ASC, AccountID ASC");
                LoadDataToListView(zListAccount);
            }
        }
    }
}
