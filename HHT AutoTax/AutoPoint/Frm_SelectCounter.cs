﻿using HHT.Connect;
using HHT.Library.System;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.Misc;

namespace HHT_AutoTax
{
    public partial class Frm_SelectCounter : Form
    {
        DataTable _DataTable;
        FileInfo FileInfo;
        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;
        int _KeyFile = 0;
        int _Key = 0;
        public Frm_SelectCounter()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            timer1.Tick += Timer1_Tick;
            timer2.Tick += Timer2_Tick;
            //timer3.Tick += Timer3_Tick;

            btn_Open.Click += Btn_Open_Click;
            btn_Del.Click += Btn_Del_Click;
            LVData.Click += LVData_Click;
            LVData.ItemActivate += LVData_ItemActivate;

            Utils.DrawLVStyle(ref LVData);
            Utils.SizeLastColumn_LV(LVData);
            InitLayout_LV(LVData);

        }

        private void Frm_SelectCounter_Load(object sender, EventArgs e)
        {
            DateTime zDate = DateTime.Now;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            dte_FromDate.Value = zFromDate;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData();
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key > 0)
            {
                if (Utils.TNMessageBox("Bạn có chắc muốn xóa", 2) == "Y")
                {
                    HHT_AutoTax.Frm_DataRecord_DLL.FileImport_Info zinfo = new Frm_DataRecord_DLL.FileImport_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message.Substring(0, 3) == "200")
                    {
                        _Key = 0;
                        Utils.TNMessageBox("Xóa thành công!", 3);
                        GV_ListOrder.Columns.Clear();
                        GV_ListOrder.Rows.Clear();
                    }
                    else
                    {
                        Utils.TNMessageBox("Lỗi!. " + zinfo.Message, 1);
                    }
                    InitData();
                }
            }
        }

        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            _Key = LVData.SelectedItems[0].Tag.ToInt();

            _IndexGV = 0;
            _DataTable = HHT_AutoTax.Frm_SelectCounter_DLL.DataRecord_Data.ListDetail(_Key);

            if (_DataTable.Rows.Count > 0)
            {
                InitGridView_Order(GV_ListOrder, _DataTable);


                _TotalGV = _DataTable.Rows.Count;
                //timer2.Start();

            }
            Cursor.Current = Cursors.Default;
        }
        private void LVData_Click(object sender, EventArgs e)
        {
            _Key = LVData.SelectedItems[0].Tag.ToInt();
        }

        #region[Listview-Left]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày tạo";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên file";
            colHead.Width = 230;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();


            DateTime zDate = dte_FromDate.Value;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);
            In_Table = HHT_AutoTax.Frm_SelectCounter_DLL.DataRecord_Data.List(zFromDate, zToDate);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["FileKey"];
                lvi.ForeColor = Color.Navy;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime date = DateTime.Parse(nRow["CreatedOn"].ToString().Trim());
                lvsi.Text = date.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FileName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        #endregion

        #region[ListView Right]

        public void InitGridView_Order(DataGridView GV, DataTable Table)
        {
            // Setup Column 
            GV.Columns.Clear();
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("UserID", "UserID");
            for (int i = 1; i < Table.Columns.Count; i++)
            {
                GV.Columns.Add(Table.Columns[i].ColumnName, Table.Columns[i].ColumnName);
                GV.Columns[Table.Columns[i].ColumnName].Width = 27;
                GV.Columns[Table.Columns[i].ColumnName].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            GV.Columns["No"].Width = 30;
            GV.Columns["UserID"].Width = 120;
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeight = 50;
            GV.EnableHeadersVisualStyles = false;
            GV.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(180, 74, 97);
            GV.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            GV.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // Du lieu
            GV.Rows.Add(Table.Rows.Count);
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                GV.Rows[i].Cells[0].Value = (i + 1).ToString();
                for (int k = 1; k <= Table.Columns.Count; k++)

                {
                    if (Table.Rows[i][k - 1].ToString() != "0")
                        GV.Rows[i].Cells[k].Value = Table.Rows[i][k - 1];
                    else
                        GV.Rows[i].Cells[k].Value = "";
                }
            }
            kryptonHeader2.Text = "Danh sách quầy tháng" + dte_FromDate.Value.Month + "/" + dte_FromDate.Value.Year;
        }


        #endregion

        #region[Import]
        private void Btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {

                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", 2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        LB_Log.Items.Clear();
                        //Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        _DataTable = new DataTable();
                        ShowLog("Đang đọc dữ liệu");

                        _DataTable = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        ShowLog("Đọc dữ liệu xong");
                        _IndexGV = 0;
                        _TotalGV = _DataTable.Rows.Count;
                        LB_Log.Items.Clear();
                        if (CheckUser(_DataTable)==true)
                        {
                            // tạo file parent
                            HHT_AutoTax.Frm_SelectCounter_DLL.FileImport_Info zinfo = new Frm_SelectCounter_DLL.FileImport_Info();
                            zinfo.FileName = FileInfo.Name;
                            zinfo.Type = 3;
                            zinfo.TypeName = "SelectCounter";
                            zinfo.Create_ServerKey();
                            _KeyFile = zinfo.FileKey;

                            //

                            //  Cursor.Current = Cursors.Default;
                            btn_Up_Click(null, null);
                        }   
                        else
                        {
                            Utils.TNMessageBoxOK("Vui lòng kiểm tra danh sách user!", 2);
                        }    

                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(), 4);
                    }
                }
            }
        }

        private SqlConnection _Connect;
        private void btn_Up_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            zError = 0;
            zSuccess = 0;
            txt_Amount_Excel.Text = _DataTable.Rows.Count.ToString("n0");
            _TotalGV = _DataTable.Rows.Count;
            ShowLog("Tổng dòng dữ liệu: " + _DataTable.Rows.Count.ToString());




            string zConnectionString = ConnectDataBase.ConnectionString;
            _Connect = new SqlConnection(zConnectionString);
            _Connect.Open();

            timer1.Start();

            this.Cursor = Cursors.Default;
        }

        private bool CheckUser( DataTable Table)
        {
            ShowLog("Kiểm tra danh sách user");
            bool zCheck = true;
            DataTable zTableUser = HHT_AutoTax.Frm_SelectCounter_DLL.DataRecord_Data.ListUser();
            foreach (DataRow r in _DataTable.Rows)
            {
                string zUser = r["2"].ToString();
                DataRow[] Array = zTableUser.Select("UserName='"+ zUser+"'");
                if(Array.Length==0)
                {
                    zCheck = false;
                    ShowLog(r["2"].ToString() +": Không chưa khai báo");
                }    

            }
            ShowLog("Kiểm tra xong user");

            return zCheck;
        }

    private void Timer1_Tick(object sender, EventArgs e)
    {
        // check befor save
        timer1.Stop();
        if (_IndexGV < _TotalGV)
        {
            DataRow r = _DataTable.Rows[_IndexGV];

            string UserID = r[1].ToString();
            ShowLog("Dang xử lý user:  " + UserID.ToString());
            int k = 2;
            int Counter = 0;
            DateTime zDate = dte_SelectDate.Value;
            for (int i = 1; i <= 31; i++)
            {
                //string CheckDate = i + "/" + zDate.Month + "/" + zDate.Year;
                string CheckDate = i + "/" + zDate.Month + "/" + zDate.Year;
                DateTime DateWrite = DateTime.MinValue;
                if (DateTime.TryParse(CheckDate, out DateWrite))
                {

                }
                if (DateWrite != DateTime.MinValue)
                {
                    if (r[k].ToString() == "1")
                    {
                        Counter = 1;
                    }
                    else
                    {
                        Counter = 0;
                    }

                    string zSQL = "INSERT INTO [dbo].[POI_SelectCounter] ("
+ " AutoKey ,DateWrite ,UserID ,Counter ,FileKey ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
+ " VALUES ( "
+ "@AutoKey ,@DateWrite ,@UserID ,@Counter ,@FileKey ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) "; string zResult = "";

                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, _Connect);
                        zCommand.CommandType = CommandType.Text;
                        string AutoKey = Guid.NewGuid().ToString();
                        zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = new Guid(AutoKey);
                        if (DateWrite == null)
                            zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = DBNull.Value;
                        else
                            zCommand.Parameters.Add("@DateWrite", SqlDbType.Date).Value = DateWrite;
                        zCommand.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = UserID;
                        zCommand.Parameters.Add("@Counter", SqlDbType.Int).Value = Counter;
                        zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _KeyFile;
                        zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = 0;
                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = "";//CreatedBy;
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = "";// CreatedName;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ""; //ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ""; //ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                        zResult = "201 Created";
                    }
                    catch (Exception Err)
                    {
                        zResult = "501 " + Err.ToString();
                    }

                    if (zResult.Substring(0, 3) == "201")
                    {
                        zSuccess++;
                        ShowLog("Đã xử lý ngày  " + (k).ToString("n0"));
                        ShowValue(zSuccess, 0);
                    }
                    else
                    {
                        zError++;
                        ShowLog("Lỗi ngày : " + (k).ToString("n0") + ": Liên hệ IT");
                        ShowValue(0, zError);
                    }

                }
                k++;
            }
            _IndexGV++;
            timer1.Start();
        }
        else
        {
            timer1.Stop();
            if (zError > 0)
            {
                //timer2.Stop();
                //PicLoading.Visible = false;
                Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!", 2);
            }
            else

            {
                ShowLog("Tổng ngày đúng: " + zSuccess.ToString());
                ShowLog(" Tổng ngày lỗi: " + zError.ToString());
                ShowLog("Xong");

                //_IndexGV = 0;
                //zError = 0;
                //zSuccess = 0;
                //lbl_Sql.Text = "Số lượng đã lưu";
                //txt_Sql.Text = "0";


                //timer2.Start();
            }
            _Connect.Close();
        }
    }

    private void Timer2_Tick(object sender, EventArgs e)
    {
        // check befor save
        timer2.Stop();
        if (_IndexGV < _TotalGV)
        {
            DataRow r = _DataTable.Rows[_IndexGV];
            int i = _IndexGV;
            GV_ListOrder.Rows.Add();
            //GV_ListOrder.Rows[i].Tag = zProduct;
            GV_ListOrder.Rows[i].Cells["No"].Value = (i + 1).ToString();
            GV_ListOrder.Rows[i].Cells["Counter"].Tag = r["Counter"];
            GV_ListOrder.Rows[i].Cells["Point"].Value = r["Point"];
            GV_ListOrder.Rows[i].Cells["MA_CN_THU"].Value = r["MA_CN_THU"];
            GV_ListOrder.Rows[i].Cells["TEN_CN_THU"].Value = r["TEN_CN_THU"];
            GV_ListOrder.Rows[i].Cells["KBNN_THU"].Value = r["KBNN_THU"];
            GV_ListOrder.Rows[i].Cells["KIHIEU_CT"].Value = r["KIHIEU_CT"];
            GV_ListOrder.Rows[i].Cells["SO_CT"].Value = r["SO_CT"];
            DateTime NGAY_CT = DateTime.Parse(r["NGAY_CT"].ToString());
            GV_ListOrder.Rows[i].Cells["NGAY_CT"].Value = NGAY_CT.ToString("dd/MM/yyyy");
            GV_ListOrder.Rows[i].Cells["MNH_VI"].Value = r["MNH_VI"];
            GV_ListOrder.Rows[i].Cells["SO_BUTTOAN"].Value = r["SO_BUTTOAN"];
            GV_ListOrder.Rows[i].Cells["NGUOI_NOPTHUE"].Value = r["NGUOI_NOPTHUE"];
            GV_ListOrder.Rows[i].Cells["TK_NO"].Value = r["TK_NO"];
            GV_ListOrder.Rows[i].Cells["TK_THUNS"].Value = r["TK_THUNS"];
            GV_ListOrder.Rows[i].Cells["MA_CQTHU"].Value = r["MA_CQTHU"];
            GV_ListOrder.Rows[i].Cells["DBHC"].Value = r["DBHC"];
            GV_ListOrder.Rows[i].Cells["TT"].Value = r["TT"];
            double TONGTIEN = 0;
            TONGTIEN = double.Parse(r["TONGTIEN"].ToString());
            GV_ListOrder.Rows[i].Cells["TONGTIEN"].Value = TONGTIEN.ToString("n0");
            GV_ListOrder.Rows[i].Cells["NV_HUY"].Value = r["NV_HUY"];

            ShowCount(_IndexGV + 1);
            _IndexGV++;

            timer2.Start();
        }
        else
        {
            timer2.Stop();

        }
    }
    void ShowLog(string Text)
    {
        Invoke(new MethodInvoker(delegate
        {
            LB_Log.Items.Add(DateTime.Now.ToString("hh:mm:ss.fff") + " : " + Text);
            LB_Log.SelectedIndex = LB_Log.Items.Count - 1;
        }));
    }
    void ShowValue(int Success, int Error)
    {
        Invoke(new MethodInvoker(delegate
        {
            if (Success > 0)
                txt_Sql.Text = zSuccess.ToString("n0");
            if (Error > 0)
                txt_Error.Text = Error.ToString("n0");
        }));
    }
    void ShowCount(int Count)
    {
        Invoke(new MethodInvoker(delegate
        {

            lbl_count.Text = Count.ToString("n0") + "/" + _TotalGV.ToString("n0");

        }));
    }
    #endregion

    #region [Custom Control Box Form]
    #region [Dùng kéo rê form]

    private bool dragging = false;
    private Point dragCursorPoint;
    private Point dragFormPoint;
    private bool isMax = true;

    private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
    {
        dragging = true;
        dragCursorPoint = Cursor.Position;
        dragFormPoint = this.Location;
    }

    private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
    {
        if (dragging)
        {
            Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
            this.Location = Point.Add(dragFormPoint, new Size(dif));
        }
    }

    private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
    {
        dragging = false;
    }
    #endregion

    private void btnMini_Click(object sender, EventArgs e)
    {
        this.WindowState = FormWindowState.Minimized;
        isMax = false;
    }

    private void btnMax_Click(object sender, EventArgs e)
    {
        if (!isMax)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            this.FormBorderStyle = FormBorderStyle.None;
            isMax = true;
            return;
        }
        if (isMax)
        {
            this.Height = 600;
            this.Width = 800;
            // this.FormBorderStyle = FormBorderStyle.Sizable;
            this.ControlBox = false;
            this.Location = new Point(
                (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
            isMax = false;
            return;
        }
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
        this.Close();
    }
    #endregion
}
}
namespace HHT_AutoTax.Frm_SelectCounter_DLL
{

    public class DataRecord_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM [dbo].[POI_FileImport] WHERE  RecordStatus != 99 AND Type=3 ORDER BY CreatedOn DESC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListDetail(int FileKey)
        {

            DataTable zTable = new DataTable();
            string zSQL = @"
--DECLARE @FileKey int =10

DECLARE @cols NVARCHAR(MAX)
DECLARE @query NVARCHAR(MAX)

CREATE TABLE #CHAMCONG (
	USERID NVARCHAR(500),  TENCOT NVARCHAR(500), GIATRI NVARCHAR(500)
)
INSERT INTO #CHAMCONG

select UserID,DAY(DateWrite) AS TENCOT ,Counter AS GIATRI  from  [dbo].[POI_SelectCounter] where ReCordStatus <> 99 AND FileKey= @FileKey

SET @cols = STUFF((SELECT ',' + QUOTENAME(TENCOT) FROM #CHAMCONG GROUP BY LEN(TENCOT), TENCOT
					FOR XML PATH(''), TYPE
					).value(N'.[1]', N'NVARCHAR(MAX)') 
				,1,1,'');


SET @query = '
			SELECT  UserID,' + @cols + ' 
			FROM 
            (
                SELECT * FROM #CHAMCONG  
			) X
            PIVOT 
            (
                 MAX(GIATRI)
				 FOR TENCOT in (' + @cols + ')
            ) P 

            ORDER BY UserID'

EXECUTE(@query)
DROP TABLE #CHAMCONG ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListUser()
        {

            DataTable zTable = new DataTable();
            string zSQL = @"
	SELECT UserName, FullName FROM SYS_User WHERE SoftWareKey=2 AND Activate =1";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }

    public class FileImport_Info
    {

        #region [ Field Name ]
        private int _FileKey = 0;
        private string _FileName = "";
        private int _AmountData = 0;
        private int _AmountAnalysis = 0;
        private int _AmountFaile = 0;
        private string _UserKey = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public FileImport_Info()
        {

        }
        public FileImport_Info(int FileKey)
        {
            string zSQL = "SELECT * FROM [dbo].[POI_FileImport] WHERE FileKey = @FileKey AND RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _FileKey = int.Parse(zReader["FileKey"].ToString());
                    _FileName = zReader["FileName"].ToString();
                    _AmountData = int.Parse(zReader["AmountData"].ToString());
                    _AmountAnalysis = int.Parse(zReader["AmountAnalysis"].ToString());
                    _AmountFaile = int.Parse(zReader["AmountFaile"].ToString());
                    _UserKey = zReader["UserKey"].ToString();
                    _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString();
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public int FileKey
        {
            get { return _FileKey; }
            set { _FileKey = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public int AmountData
        {
            get { return _AmountData; }
            set { _AmountData = value; }
        }
        public int AmountAnalysis
        {
            get { return _AmountAnalysis; }
            set { _AmountAnalysis = value; }
        }
        public int AmountFaile
        {
            get { return _AmountFaile; }
            set { _AmountFaile = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public int Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[POI_FileImport] ("
        + " FileName ,AmountData ,AmountAnalysis ,AmountFaile ,UserKey ,Type ,TypeName ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileName ,@AmountData ,@AmountAnalysis ,@AmountFaile ,@UserKey ,@Type ,@TypeName ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT FileKey FROM POI_FileImport WHERE FileKey = SCOPE_IDENTITY() ";
            int zResult = 0;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _FileKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[POI_FileImport] ("
        + " FileKey ,FileName ,AmountData ,AmountAnalysis ,AmountFaile ,UserKey ,Type ,TypeName ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileKey ,@FileName ,@AmountData ,@AmountAnalysis ,@AmountFaile ,@UserKey ,@Type ,@TypeName ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE [dbo].[POI_FileImport] SET "
                        + " FileName = @FileName,"
                        + " AmountData = @AmountData,"
                        + " AmountAnalysis = @AmountAnalysis,"
                        + " AmountFaile = @AmountFaile,"
                        + " UserKey = @UserKey,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE FileKey = @FileKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[POI_FileImport] Set RecordStatus = 99 WHERE FileKey = @FileKey
                            DELETE FROM POI_DataRecord  WHERE FileKey = @FileKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[POI_FileImport] WHERE FileKey = @FileKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
