﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.Misc;

namespace HHT_AutoTax
{
    public partial class Frm_MainPoint : Form
    {
        private int IDBank = 1;//đông sài gòn
        //private int IDBank = 2; //Hải an hải phòng
        private string NameBank = "";
        private string IDSoftWare = "";
        private int _Exit = 0;
        public Frm_MainPoint()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //right- Point
            btn_DataRecord.Click += Btn_DataRecord_Click;
            btn_DataTax.Click += Btn_DataTax_Click;
            btn_SelectCounter.Click += Btn_SelectCounter_Click;
            btn_Point.Click += Btn_Point_Click;
        }

        private void Frm_MainPoint_Load(object sender, EventArgs e)
        {
            if (IDBank == 1)
            {
                NameBank = "Đông Sài Gòn";

            }
            if (IDBank == 2)
            {
                NameBank = "Hải An";
            }

            //HeaderControl.Text = "Agribank " + NameBank;
            lbl_WorkDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            IDSoftWare = "POINT";
            bool zCheckClient = false;
            zCheckClient = CheckClient();
            if (zCheckClient == true)
            {
                Panel_Point.Visible = true;
            }
            else
            {

                Panel_Point.Visible = false;
            }

        }
        #region[Auto-Point]
        private void Btn_DataRecord_Click(object sender, EventArgs e)
        {
            Frm_DataRecord frm = new Frm_DataRecord();
            frm.ShowDialog();
        }
        private void Btn_DataTax_Click(object sender, EventArgs e)
        {
            Frm_DataTax frm = new Frm_DataTax();
            frm.ShowDialog();
        }
        private void Btn_SelectCounter_Click(object sender, EventArgs e)
        {
            Frm_SelectCounter frm = new Frm_SelectCounter();
            frm.ShowDialog();
        }
        private void Btn_Point_Click(object sender, EventArgs e)
        {
            Frm_Point frm = new Frm_Point();
            frm.ShowDialog();
        }
        #endregion

        #region [Dùng kéo rê form]

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            if (Utils.TNMessageBox("Bạn có muốn thoát phần mềm !", 2) == "Y")
            {
                _Exit = 1;
                //if (SessionUser.UserLogin != null && SessionUser.UserLogin.Key != "")
                //{
                //    User_Info zUser = new User_Info(SessionUser.UserLogin.Name);
                //    zUser.UpdateIsLogin(0);// hạ cờ đăng nhập
                //    Log_Login(3, "Đã đăng xuất phần mềm");
                //}
                this.Close();
            }
            else
            {
                _Exit = 0;
            }
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Exit == 0)
                e.Cancel = true;
            else
                e.Cancel = false;
            //if (MessageBox.Show("Bạn có muốn thoát phần mềm !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{

            //    e.Cancel = false;
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }
        #endregion


        private bool CheckClient()
        {
            DateTime Exp = DateTime.MinValue;
            DateTime zDatetime = DateTime.Now; // ngày hiện tại
            bool zResult = false;

            if (IDBank == 1) // đông sài gòn
            {
                if (IDSoftWare == "TAX")
                {
                    Exp = DateTime.Parse("22/04/2022");
                }
                if (IDSoftWare == "POINT")
                {
                    Exp = DateTime.Parse("31/08/2021");
                }
            }
            if (IDBank == 2) // hải an
            {
                if (IDSoftWare == "TAX")
                {
                    Exp = DateTime.Parse("12/06/2022");
                }
                if (IDSoftWare == "POINT")
                {
                    Exp = DateTime.MinValue;
                }
            }
            if (zDatetime <= Exp)
            {
                zResult = true; // còn hạn
            }
            else if (zDatetime > Exp & zDatetime <= Exp.AddMonths(1))
            {
                zResult = true; // hết hạn còn sd được 30 ngày
                Utils.TNMessageBoxOK("Phần mềm đã hết hạn ngày " + Exp.ToString("dd-MM-yyyy") + ".Vui lòng liên hệ nhà cung cấp!", 2);
            }
            else if ((zDatetime > Exp & zDatetime > Exp.AddMonths(1)) || Exp == DateTime.MinValue)
            {
                zResult = false; //quá hạn 1 tháng ngưng dịch vụ
                Utils.TNMessageBoxOK("Phần mềm đã bị khóa.Vui lòng liên hệ nhà cung cấp!", 2);
            }
            return zResult;
        }

    }
}
