﻿using HHT.Connect;
using HHT.Library.System;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.Misc;

namespace HHT_AutoTax
{
    public partial class Frm_DataRecord : Form
    {
        DataTable _DataTable;
        FileInfo FileInfo;
        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;
        int _KeyFile = 0;
        int _Key = 0;
        public Frm_DataRecord()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            timer1.Tick += Timer1_Tick;
            timer2.Tick += Timer2_Tick;
            //timer3.Tick += Timer3_Tick;

            btn_Open.Click += Btn_Open_Click;
            btn_Del.Click += Btn_Del_Click;
            LVData.Click += LVData_Click;
            LVData.ItemActivate += LVData_ItemActivate;

            Utils.DrawLVStyle(ref LVData);
            Utils.SizeLastColumn_LV(LVData);
            InitLayout_LV(LVData);
            InitGridView_Order(GV_ListOrder);
        }



        private void Frm_DataRecord_Load(object sender, EventArgs e)
        {
            DateTime zDate = DateTime.Now;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            dte_FromDate.Value = zFromDate;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            string zSQL = @"
SELECT UserName,UserName + '-' +FullName
FROM SYS_User 
WHERE SoftWareKey = 2 Order BY UserName";
            TNS.Misc.LoadDataToToolbox.KryptonComboBox(cbo_GDV, zSQL, "--- Tất cả---");
            cbo_GDV.SelectedIndex = 1;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData();
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key > 0)
            {
                if (Utils.TNMessageBox("Bạn có chắc muốn xóa", 2) == "Y")
                {
                    HHT_AutoTax.Frm_DataRecord_DLL.FileImport_Info zinfo = new Frm_DataRecord_DLL.FileImport_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message.Substring(0, 3) == "200")
                    {
                        _Key = 0;
                        Utils.TNMessageBox("Xóa thành công!", 3);
                        GV_ListOrder.Rows.Clear();
                        lbl_count.Text = "0/0";
                    }
                    else
                    {
                        Utils.TNMessageBox("Lỗi!. " + zinfo.Message, 1);
                    }
                    InitData();
                }
            }
        }

        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            _Key = LVData.SelectedItems[0].Tag.ToInt();
            DisplayData();

        }
        private void LVData_Click(object sender, EventArgs e)
        {
            _Key = LVData.SelectedItems[0].Tag.ToInt();
        }

        #region[Listview-Left]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày tạo";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên file";
            colHead.Width = 230;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();

            DateTime zDate = dte_FromDate.Value;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);
            In_Table = HHT_AutoTax.Frm_DataRecord_DLL.DataRecord_Data.List(zFromDate, zToDate);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["FileKey"];
                lvi.ForeColor = Color.Navy;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime date = DateTime.Parse(nRow["CreatedOn"].ToString().Trim());
                lvsi.Text = date.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FileName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        #endregion

        #region[ListView Right]

        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("Counter", "Quầy");
            GV.Columns.Add("Point", "Điểm");
            GV.Columns.Add("buscd", "buscd (C)");
            GV.Columns.Add("bchkcd", "bchkcd (E)");
            GV.Columns.Add("stscd", "stscd (J)");
            GV.Columns.Add("opndt", "opndt (L)");
            GV.Columns.Add("crtusr", "crtusr (S)");
            GV.Columns.Add("crtdt", "crtdt (W)");
            GV.Columns.Add("bchknm", "bchknm (AB)");
            GV.Columns.Add("Index_3", "Index_3 (AE)");
            GV.Columns.Add("Index_8", "Index_8 (AJ)");
            GV.Columns.Add("Index_9", "Index_9 (AK)");

            GV.Columns.Add("Index_15", "Index_15 (AQ)");
            GV.Columns.Add("name_2", "name_2 (AW)");

            GV.Columns["No"].Width = 60;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Counter"].Width = 60;
            GV.Columns["Counter"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Point"].Width = 60;
            GV.Columns["Point"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["buscd"].Width = 60;
            GV.Columns["buscd"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["bchkcd"].Width = 80;
            GV.Columns["bchkcd"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["stscd"].Width = 80;
            GV.Columns["stscd"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["opndt"].Width = 80;
            GV.Columns["opndt"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["crtusr"].Width = 100;
            GV.Columns["crtusr"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV.Columns["crtdt"].Width = 130;
            GV.Columns["crtdt"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;



            GV.Columns["bchknm"].Width = 180;
            GV.Columns["bchknm"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["bchknm"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Index_3"].Width = 150;
            GV.Columns["Index_3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Index_3"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Index_8"].Width = 150;
            GV.Columns["Index_8"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Index_8"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Index_9"].Width = 150;
            GV.Columns["Index_9"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Index_9"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Index_15"].Width = 150;
            GV.Columns["Index_15"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Index_15"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["name_2"].Width = 150;
            GV.Columns["name_2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["name_2"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            //""
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeight = 50;
            GV.EnableHeadersVisualStyles = false;
            GV.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(180, 74, 97);
            GV.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            GV.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }

        private void DisplayData()
        {
            _IndexGV = 0;
            _DataTable = HHT_AutoTax.Frm_DataRecord_DLL.DataRecord_Data.ListDetail(_Key,cbo_GDV.SelectedValue.ToString());

            if (_DataTable.Rows.Count > 0)
            {
                GV_ListOrder.Rows.Clear();
                _TotalGV = _DataTable.Rows.Count;
                timer2.Start();

            }
        }

        #endregion

        #region[Import]
        private void Btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {

                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", 2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        LB_Log.Items.Clear();
                        //Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        _DataTable = new DataTable();
                        ShowLog("Đang đọc dữ liệu");

                        _DataTable = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        ShowLog("Đọc dữ liệu xong");
                        // tạo file parent
                        HHT_AutoTax.Frm_DataRecord_DLL.FileImport_Info zinfo = new Frm_DataRecord_DLL.FileImport_Info();
                        zinfo.FileName = FileInfo.Name;
                        zinfo.Type = 1;
                        zinfo.TypeName = "DataRecord";
                        zinfo.Create_ServerKey();
                        _KeyFile = zinfo.FileKey;

                        //

                        //  Cursor.Current = Cursors.Default;
                        btn_Up_Click(null, null);
                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(), 4);
                    }
                }
            }
        }

        private SqlConnection _Connect;
        private void btn_Up_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            zError = 0;
            zSuccess = 0;
            txt_Amount_Excel.Text = _DataTable.Rows.Count.ToString("n0");
            txt_Sql.Text = "0";
            txt_Error.Text = "0";
            _IndexGV = 0;
            _TotalGV = _DataTable.Rows.Count;
            ShowLog("Tổng dòng dữ liệu: " + _DataTable.Rows.Count.ToString());


            string zConnectionString = ConnectDataBase.ConnectionString;
            _Connect = new SqlConnection(zConnectionString);
            _Connect.Open();

            timer1.Start();


            this.Cursor = Cursors.Default;
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer1.Stop();
            if (_IndexGV < _TotalGV)
            {
                DataRow r = _DataTable.Rows[_IndexGV];

                string stt = r[0].ToString().Trim();
                string bchk = r[1].ToString().Trim();
                string buscd = r[2].ToString().Trim();// C - modul
                string subcd = r[3].ToString().Trim();
                string bchkcd = r[4].ToString().Trim();// E
                string hbrcd = r[5].ToString().Trim();
                string brcd = r[6].ToString().Trim();
                string trref = r[7].ToString().Trim();
                string trseq = r[8].ToString().Trim();
                string stscd = r[9].ToString().Trim(); // J

                string troffcd = r[10].ToString().Trim();
                string opndt = r[11].ToString().Trim(); // L
                string glseq = r[12].ToString().Trim();// M
                string receiptno = r[13].ToString().Trim();
                string ibflg = r[14].ToString().Trim();
                string rlbrcd = r[15].ToString().Trim();
                string rltrref = r[16].ToString().Trim();
                string rltrseq = r[17].ToString().Trim();
                string crtusr = r[18].ToString().Trim();// S - user
                string uptusr = r[19].ToString().Trim();

                string cncusr = r[20].ToString().Trim();
                string apvusr = r[21].ToString().Trim();
                DateTime crtdt = DateTime.MinValue;
                if (DateTime.TryParse(r[22].ToString().Trim(), out crtdt))
                {

                }
                if (crtdt == DateTime.MinValue)
                {
                    if (DateTime.TryParse(r[11].ToString().Trim(), out crtdt))
                    {

                    }
                }
                string apvdt = r[23].ToString().Trim();
                string bchkdt = r[24].ToString().Trim();
                string bchktm = r[25].ToString().Trim();
                string bchkusr = r[26].ToString().Trim();
                string bchknm = r[27].ToString().Trim(); //  AB
                string Index_1 = r[28].ToString().Trim();
                string Index_2 = r[29].ToString().Trim();

                string Index_3 = r[30].ToString().Trim();// AE
                string Index_4 = r[31].ToString().Trim();
                string Index_5 = r[32].ToString().Trim();
                string Index_6 = r[33].ToString().Trim();
                string Index_7 = r[34].ToString().Trim();
                string Index_8 = r[35].ToString().Trim();// AJ
                string Index_9 = r[36].ToString().Trim();// AK 
                string Index_10 = r[37].ToString().Trim();
                string Index_11 = r[38].ToString().Trim();
                string Index_12 = r[39].ToString().Trim();

                string Index_13 = r[40].ToString().Trim();
                string Index_14 = r[41].ToString().Trim();
                string Index_15 = r[42].ToString().Trim();// AQ
                string Index_16 = r[43].ToString().Trim();
                string Index_17 = r[44].ToString().Trim();
                string Index_18 = r[45].ToString().Trim();
                string Index_19 = r[46].ToString().Trim();
                string name_1 = r[47].ToString().Trim();
                string name_2 = r[48].ToString().Trim();// AW
                string Index_20 = r[49].ToString().Trim();

                string name_3 = r[50].ToString().Trim();
                string name_4 = r[51].ToString().Trim();
                string name_5 = r[52].ToString().Trim();
                string name_6 = r[53].ToString().Trim();
                string name_7 = r[54].ToString().Trim();
                string name_8 = r[55].ToString().Trim();
                string name_9 = r[56].ToString().Trim();
                string name_10 = r[57].ToString().Trim();
                string name_11 = r[58].ToString().Trim();
                string name_12 = r[59].ToString().Trim();

                string name_13 = r[60].ToString().Trim();
                string name_14 = r[61].ToString().Trim();
                string name_15 = r[62].ToString().Trim();
                string name_16 = r[63].ToString().Trim();
                string name_17 = r[64].ToString().Trim();
                string name_18 = r[65].ToString().Trim();
                string name_19 = r[66].ToString().Trim();
                string name_20 = r[67].ToString().Trim();
                string name_21 = r[68].ToString().Trim();
                string name_22 = r[69].ToString().Trim();

                string name_24 = r[70].ToString().Trim();
                string name_25 = r[71].ToString().Trim();
                string name_26 = r[72].ToString().Trim();
                string name_27 = r[73].ToString().Trim();
                string name_28 = r[74].ToString().Trim();
                string name_29 = r[75].ToString().Trim();
                string name_30 = r[76].ToString().Trim();
                string name_31 = r[77].ToString().Trim();
                string name_32 = r[78].ToString().Trim();
                string name_34 = r[79].ToString().Trim();

                string name_33 = r[80].ToString().Trim();
                string name_35 = r[81].ToString().Trim();
                string name_23 = r[82].ToString().Trim();
                string name_36 = r[83].ToString().Trim();
                string name_37 = r[84].ToString().Trim();
                string name_38 = r[85].ToString().Trim();
                string name_39 = r[86].ToString().Trim();

                int Counter = HHT_AutoTax.Frm_DataRecord_DLL.DataRecord_Data.Counter(_Connect, crtusr, crtdt);
                float [] zArray = HHT_AutoTax.Frm_DataRecord_DLL.DataRecord_Data.SetupPoint(buscd, bchkcd, Counter, name_2, Index_8, Index_3, Index_9, Index_15,
                    bchknm, stscd, glseq);

                float Point = zArray[0];
                int TypeKey = 0;
                string TypeName = "";
                if(zArray[1]==1)
                {
                    TypeKey = 1;
                    TypeName = "Hạch toán";
                }
                else
                {
                    TypeKey = 0;
                    TypeName = "Không Hạch toán";
                }    

                string zSQL = "INSERT INTO [dbo].[POI_DataRecord] ("
+ " AutoKey ,stt ,bchk ,buscd ,subcd ,bchkcd ,hbrcd ,brcd ,trref ,trseq ,stscd ,troffcd ,opndt ,glseq ,receiptno ,ibflg ,rlbrcd ,rltrref ,rltrseq ,crtusr ,uptusr ,cncusr ,apvusr ,crtdt ,apvdt ,bchkdt ,bchktm ,bchkusr ,bchknm ,Index_1 ,Index_2 ,Index_3 ,Index_4 ,Index_5 ,Index_6 ,Index_7 ,Index_8 ,Index_9 ,Index_10 ,Index_11 ,Index_12 ,Index_13 ,Index_14 ,Index_15 ,Index_16 ,Index_17 ,Index_18 ,Index_19 ,name_1 ,name_2 ,Index_20 ,name_3 ,name_4 ,name_5 ,name_6 ,name_7 ,name_8 ,name_9 ,name_10 ,name_11 ,name_12 ,name_13 ,name_14 ,name_15 ,name_16 ,name_17 ,name_18 ,name_19 ,name_20 ,name_21 ,name_22 ,name_24 ,name_25 ,name_27 ,name_26 ,name_28 ,name_29 ,name_30 ,name_31 ,name_32 ,name_34 ,name_33 ,name_35 ,name_23 ,name_36 ,name_37 ,name_38 ,name_39 ,Counter ,Point, TypeKey, TypeName, FileKey ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
+ " VALUES ( "
+ "@AutoKey ,@stt ,@bchk ,@buscd ,@subcd ,@bchkcd ,@hbrcd ,@brcd ,@trref ,@trseq ,@stscd ,@troffcd ,@opndt ,@glseq ,@receiptno ,@ibflg ,@rlbrcd ,@rltrref ,@rltrseq ,@crtusr ,@uptusr ,@cncusr ,@apvusr ,@crtdt ,@apvdt ,@bchkdt ,@bchktm ,@bchkusr ,@bchknm ,@Index_1 ,@Index_2 ,@Index_3 ,@Index_4 ,@Index_5 ,@Index_6 ,@Index_7 ,@Index_8 ,@Index_9 ,@Index_10 ,@Index_11 ,@Index_12 ,@Index_13 ,@Index_14 ,@Index_15 ,@Index_16 ,@Index_17 ,@Index_18 ,@Index_19 ,@name_1 ,@name_2 ,@Index_20 ,@name_3 ,@name_4 ,@name_5 ,@name_6 ,@name_7 ,@name_8 ,@name_9 ,@name_10 ,@name_11 ,@name_12 ,@name_13 ,@name_14 ,@name_15 ,@name_16 ,@name_17 ,@name_18 ,@name_19 ,@name_20 ,@name_21 ,@name_22 ,@name_24 ,@name_25 ,@name_27 ,@name_26 ,@name_28 ,@name_29 ,@name_30 ,@name_31 ,@name_32 ,@name_34 ,@name_33 ,@name_35 ,@name_23 ,@name_36 ,@name_37 ,@name_38 ,@name_39 ,@Counter ,@Point ,@TypeKey ,@TypeName ,@FileKey ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";

                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, _Connect);
                    zCommand.CommandType = CommandType.Text;
                    string AutoKey = Guid.NewGuid().ToString();
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = new Guid(AutoKey);
                    zCommand.Parameters.Add("@stt", SqlDbType.Int).Value = stt;
                    zCommand.Parameters.Add("@bchk", SqlDbType.NVarChar).Value = bchk;
                    zCommand.Parameters.Add("@buscd", SqlDbType.NVarChar).Value = buscd;
                    zCommand.Parameters.Add("@subcd", SqlDbType.NVarChar).Value = subcd;
                    zCommand.Parameters.Add("@bchkcd", SqlDbType.NVarChar).Value = bchkcd;
                    zCommand.Parameters.Add("@hbrcd", SqlDbType.NVarChar).Value = hbrcd;
                    zCommand.Parameters.Add("@brcd", SqlDbType.NVarChar).Value = brcd;
                    zCommand.Parameters.Add("@trref", SqlDbType.NVarChar).Value = trref;
                    zCommand.Parameters.Add("@trseq", SqlDbType.NVarChar).Value = trseq;
                    zCommand.Parameters.Add("@stscd", SqlDbType.NVarChar).Value = stscd;
                    zCommand.Parameters.Add("@troffcd", SqlDbType.NVarChar).Value = troffcd;
                    zCommand.Parameters.Add("@opndt", SqlDbType.NVarChar).Value = opndt;
                    zCommand.Parameters.Add("@glseq", SqlDbType.NVarChar).Value = glseq;
                    zCommand.Parameters.Add("@receiptno", SqlDbType.NVarChar).Value = receiptno;
                    zCommand.Parameters.Add("@ibflg", SqlDbType.NVarChar).Value = ibflg;
                    zCommand.Parameters.Add("@rlbrcd", SqlDbType.NVarChar).Value = rlbrcd;
                    zCommand.Parameters.Add("@rltrref", SqlDbType.NVarChar).Value = rltrref;
                    zCommand.Parameters.Add("@rltrseq", SqlDbType.NVarChar).Value = rltrseq;
                    zCommand.Parameters.Add("@crtusr", SqlDbType.NVarChar).Value = crtusr;
                    zCommand.Parameters.Add("@uptusr", SqlDbType.NVarChar).Value = uptusr;
                    zCommand.Parameters.Add("@cncusr", SqlDbType.NVarChar).Value = cncusr;
                    zCommand.Parameters.Add("@apvusr", SqlDbType.NVarChar).Value = apvusr;
                    zCommand.Parameters.Add("@crtdt", SqlDbType.DateTime).Value = crtdt;
                    zCommand.Parameters.Add("@apvdt", SqlDbType.NVarChar).Value = apvdt;
                    zCommand.Parameters.Add("@bchkdt", SqlDbType.NVarChar).Value = bchkdt;
                    zCommand.Parameters.Add("@bchktm", SqlDbType.NVarChar).Value = bchktm;
                    zCommand.Parameters.Add("@bchkusr", SqlDbType.NVarChar).Value = bchkusr;
                    zCommand.Parameters.Add("@bchknm", SqlDbType.NVarChar).Value = bchknm;
                    zCommand.Parameters.Add("@Index_1", SqlDbType.NVarChar).Value = Index_1;
                    zCommand.Parameters.Add("@Index_2", SqlDbType.NVarChar).Value = Index_2;
                    zCommand.Parameters.Add("@Index_3", SqlDbType.NVarChar).Value = Index_3;
                    zCommand.Parameters.Add("@Index_4", SqlDbType.NVarChar).Value = Index_4;
                    zCommand.Parameters.Add("@Index_5", SqlDbType.NVarChar).Value = Index_5;
                    zCommand.Parameters.Add("@Index_6", SqlDbType.NVarChar).Value = Index_6;
                    zCommand.Parameters.Add("@Index_7", SqlDbType.NVarChar).Value = Index_7;
                    zCommand.Parameters.Add("@Index_8", SqlDbType.NVarChar).Value = Index_8;
                    zCommand.Parameters.Add("@Index_9", SqlDbType.NVarChar).Value = Index_9;
                    zCommand.Parameters.Add("@Index_10", SqlDbType.NVarChar).Value = Index_10;
                    zCommand.Parameters.Add("@Index_11", SqlDbType.NVarChar).Value = Index_11;
                    zCommand.Parameters.Add("@Index_12", SqlDbType.NVarChar).Value = Index_12;
                    zCommand.Parameters.Add("@Index_13", SqlDbType.NVarChar).Value = Index_13;
                    zCommand.Parameters.Add("@Index_14", SqlDbType.NVarChar).Value = Index_14;
                    zCommand.Parameters.Add("@Index_15", SqlDbType.NVarChar).Value = Index_15;
                    zCommand.Parameters.Add("@Index_16", SqlDbType.NVarChar).Value = Index_16;
                    zCommand.Parameters.Add("@Index_17", SqlDbType.NVarChar).Value = Index_17;
                    zCommand.Parameters.Add("@Index_18", SqlDbType.NVarChar).Value = Index_18;
                    zCommand.Parameters.Add("@Index_19", SqlDbType.NVarChar).Value = Index_19;
                    zCommand.Parameters.Add("@name_1", SqlDbType.NVarChar).Value = name_1;
                    zCommand.Parameters.Add("@name_2", SqlDbType.NVarChar).Value = name_2;
                    zCommand.Parameters.Add("@Index_20", SqlDbType.NVarChar).Value = Index_20;
                    zCommand.Parameters.Add("@name_3", SqlDbType.NVarChar).Value = name_3;
                    zCommand.Parameters.Add("@name_4", SqlDbType.NVarChar).Value = name_4;
                    zCommand.Parameters.Add("@name_5", SqlDbType.NVarChar).Value = name_5;
                    zCommand.Parameters.Add("@name_6", SqlDbType.NVarChar).Value = name_6;
                    zCommand.Parameters.Add("@name_7", SqlDbType.NVarChar).Value = name_7;
                    zCommand.Parameters.Add("@name_8", SqlDbType.NVarChar).Value = name_8;
                    zCommand.Parameters.Add("@name_9", SqlDbType.NVarChar).Value = name_9;
                    zCommand.Parameters.Add("@name_10", SqlDbType.NVarChar).Value = name_10;
                    zCommand.Parameters.Add("@name_11", SqlDbType.NVarChar).Value = name_11;
                    zCommand.Parameters.Add("@name_12", SqlDbType.NVarChar).Value = name_12;
                    zCommand.Parameters.Add("@name_13", SqlDbType.NVarChar).Value = name_13;
                    zCommand.Parameters.Add("@name_14", SqlDbType.NVarChar).Value = name_14;
                    zCommand.Parameters.Add("@name_15", SqlDbType.NVarChar).Value = name_15;
                    zCommand.Parameters.Add("@name_16", SqlDbType.NVarChar).Value = name_16;
                    zCommand.Parameters.Add("@name_17", SqlDbType.NVarChar).Value = name_17;
                    zCommand.Parameters.Add("@name_18", SqlDbType.NVarChar).Value = name_18;
                    zCommand.Parameters.Add("@name_19", SqlDbType.NVarChar).Value = name_19;
                    zCommand.Parameters.Add("@name_20", SqlDbType.NVarChar).Value = name_20;
                    zCommand.Parameters.Add("@name_21", SqlDbType.NVarChar).Value = name_21;
                    zCommand.Parameters.Add("@name_22", SqlDbType.NVarChar).Value = name_22;
                    zCommand.Parameters.Add("@name_24", SqlDbType.NVarChar).Value = name_24;
                    zCommand.Parameters.Add("@name_25", SqlDbType.NVarChar).Value = name_25;
                    zCommand.Parameters.Add("@name_27", SqlDbType.NVarChar).Value = name_27;
                    zCommand.Parameters.Add("@name_26", SqlDbType.NVarChar).Value = name_26;
                    zCommand.Parameters.Add("@name_28", SqlDbType.NVarChar).Value = name_28;
                    zCommand.Parameters.Add("@name_29", SqlDbType.NVarChar).Value = name_29;
                    zCommand.Parameters.Add("@name_30", SqlDbType.NVarChar).Value = name_30;
                    zCommand.Parameters.Add("@name_31", SqlDbType.NVarChar).Value = name_31;
                    zCommand.Parameters.Add("@name_32", SqlDbType.NVarChar).Value = name_32;
                    zCommand.Parameters.Add("@name_34", SqlDbType.NVarChar).Value = name_34;
                    zCommand.Parameters.Add("@name_33", SqlDbType.NVarChar).Value = name_33;
                    zCommand.Parameters.Add("@name_35", SqlDbType.NVarChar).Value = name_35;
                    zCommand.Parameters.Add("@name_23", SqlDbType.NVarChar).Value = name_23;
                    zCommand.Parameters.Add("@name_36", SqlDbType.NVarChar).Value = name_36;
                    zCommand.Parameters.Add("@name_37", SqlDbType.NVarChar).Value = name_37;
                    zCommand.Parameters.Add("@name_38", SqlDbType.NVarChar).Value = name_38;
                    zCommand.Parameters.Add("@name_39", SqlDbType.NVarChar).Value = name_39;
                    zCommand.Parameters.Add("@Counter", SqlDbType.Int).Value = Counter;
                    zCommand.Parameters.Add("@Point", SqlDbType.Float).Value = Point;
                    zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = TypeKey ;
                    zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = TypeName;
                    zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _KeyFile;
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = 0;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = "";//CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = "";// CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ""; //ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ""; //ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    zResult = "201 Created";
                }
                catch (Exception Err)
                {
                    zResult = "501 " + Err.ToString();
                }

                if (zResult.Substring(0, 3) == "201")
                {
                    zSuccess++;

                    ShowLog("Đã xử lý dòng " + (_IndexGV + 1).ToString("n0"));
                    ShowValue(zSuccess, 0);
                }
                else
                {
                    zError++;
                    ShowLog("Lỗi dòng : " + (_IndexGV + 1).ToString("n0") + ": Liên hệ IT");
                    ShowValue(0, zError);
                }
                _IndexGV++;
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                if (zError > 0)
                {
                    //timer2.Stop();
                    //PicLoading.Visible = false;
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!", 2);
                }
                else

                {
                    ShowLog("Tổng dòng đúng: " + zSuccess.ToString());
                    ShowLog("Lỗi dòng lỗi: " + zError.ToString());
                    ShowLog("Xong");

                    //_IndexGV = 0;
                    //zError = 0;
                    //zSuccess = 0;
                    //lbl_Sql.Text = "Số lượng đã lưu";
                    //txt_Sql.Text = "0";


                    //timer2.Start();
                }
                _Connect.Close();
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer2.Stop();
            if (_IndexGV < _TotalGV)
            {
                DataRow r = _DataTable.Rows[_IndexGV];
                int i = _IndexGV;
                GV_ListOrder.Rows.Add();
                //GV_ListOrder.Rows[i].Tag = zProduct;
                GV_ListOrder.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_ListOrder.Rows[i].Cells["Counter"].Value = r["Counter"];
                GV_ListOrder.Rows[i].Cells["Point"].Value = r["Point"];
                GV_ListOrder.Rows[i].Cells["buscd"].Value = r["buscd"];
                GV_ListOrder.Rows[i].Cells["bchkcd"].Value = r["bchkcd"];
                GV_ListOrder.Rows[i].Cells["stscd"].Value = r["stscd"];
                GV_ListOrder.Rows[i].Cells["opndt"].Value = r["opndt"];
                GV_ListOrder.Rows[i].Cells["crtusr"].Value = r["crtusr"];
                GV_ListOrder.Rows[i].Cells["crtdt"].Value = r["crtdt"];
                GV_ListOrder.Rows[i].Cells["bchknm"].Value = r["bchknm"];
                GV_ListOrder.Rows[i].Cells["Index_3"].Value = r["Index_3"];
                GV_ListOrder.Rows[i].Cells["Index_8"].Value = r["Index_8"];
                GV_ListOrder.Rows[i].Cells["Index_9"].Value = r["Index_9"];
                GV_ListOrder.Rows[i].Cells["Index_15"].Value = r["Index_15"];
                GV_ListOrder.Rows[i].Cells["name_2"].Value = r["name_2"];

                ShowCount(_IndexGV + 1);
                _IndexGV++;

                timer2.Start();
            }
            else
            {
                timer2.Stop();
                Cursor.Current = Cursors.Default;
            }
        }
        void ShowLog(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                LB_Log.Items.Add(DateTime.Now.ToString("hh:mm:ss.fff") + " : " + Text);
                LB_Log.SelectedIndex = LB_Log.Items.Count - 1;
            }));
        }
        void ShowValue(int Success, int Error)
        {
            Invoke(new MethodInvoker(delegate
            {
                if (Success > 0)
                    txt_Sql.Text = zSuccess.ToString("n0");
                if (Error > 0)
                    txt_Error.Text = Error.ToString("n0");
            }));
        }
        void ShowCount(int Count)
        {
            Invoke(new MethodInvoker(delegate
            {

                lbl_count.Text = Count.ToString("n0") + "/" + _TotalGV.ToString("n0");

            }));
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }

}

namespace HHT_AutoTax.Frm_DataRecord_DLL
{

    public class DataRecord_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM [dbo].[POI_FileImport] WHERE  RecordStatus != 99 AND Type=1 ORDER BY CreatedOn DESC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListDetail(int FileKey,string crtusr)
        {

            DataTable zTable = new DataTable();
            string zFilter = "";
            if (crtusr != "0")
            {
                zFilter = " AND crtusr = @crtusr ";
            }
            string zSQL = @"SELECT Counter,Point,buscd,bchkcd,stscd,opndt,
                            crtusr,crtdt,bchknm,Index_3,Index_8,Index_9,Index_15,name_2
                            FROM[dbo].[POI_DataRecord]
                            WHERE RecordStatus != 99 AND FileKey = FileKey @Customer ORDER BY opndt ASC ";
            zSQL = zSQL.Replace("@Customer", zFilter); ;
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                zCommand.Parameters.Add("@crtusr", SqlDbType.NVarChar).Value = crtusr;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //Lay cong quay cua ngay hom do
        public static int Counter(SqlConnection Connect, string UserID, DateTime Date)
        {
            int zResult = 0;
            string zSQL = @"DECLARE  @Result INT =0;
                            SELECT @Result = ISNULL(Counter,0)
                            FROM [dbo].[POI_SelectCounter]
                            WHERE RecordStatus != 99 
                            AND UserID = @UserID
                            AND DateWrite = @Date 
                            SELECT @Result
                            ";
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, Connect);
                zCommand.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = UserID;
                zCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Date;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static float[] SetupPoint(string Module, string Code, int Counter, string AW,
                                    string AJ, string AE, string AK, string AQ, string AB, string J, string M)
        {
            float[] zArray = new float[2];
            float zResult = 0;
            float zHachToan = 1;


            //Phần hạch toán
            if (Module == "DP")
            {
                if (Code == "13101")
                {
                    if (AW.IndexOf("6280-201") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                    else
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "13102")
                {
                    if (AE.IndexOf("6280") >= 0 && AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    }
                    if (AE.IndexOf("6280") == -1 && AJ.IndexOf("Cash") >= 0)
                    {

                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 4;
                    }
                    if (AJ.IndexOf("C.C.A") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "13103")
                {
                    if (AE.IndexOf("6280") >= 0 && AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 3;
                    }
                    if (AE.IndexOf("6280") == -1 && AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 4;
                    }
                    if (AJ.IndexOf("C.C.A") >= 0 || AJ.IndexOf("Customer's A/C") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 2;
                    }
                }
                if (Code == "13104")
                {
                    if (AE.IndexOf("6280") >= 0 && AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 4;
                    }
                    if (AE.IndexOf("6280") >= 0 && (AJ.IndexOf("C.C.A") >= 0 || AJ.IndexOf("Customer's A/C") >= 0))
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 3;
                    }
                    if (AE.IndexOf("6280") >= -1 & AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 4;
                    }
                    if (AE.IndexOf("6280") >= -1 && (AJ.IndexOf("C.C.A") >= 0 || AJ.IndexOf("Customer's A/C") >= 0))
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 3;
                    }
                }
                if (Code == "13105")
                {
                    if (AE.IndexOf("6280") >= 0 & AJ.IndexOf("Cash") >= 0)
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    if (AE.IndexOf("6280") == -1 & AJ.IndexOf("Cash") >= 0)
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 3;
                    if (AJ.IndexOf("Customer's A/C") >= 0)
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                }
                if (Code == "13109")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "13110")
                {
                    if (AK.IndexOf("Cash serv.charge") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    }
                    if (AK.IndexOf("A/C services charge") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "13111")
                {
                    if (AK.IndexOf("Cash serv.charge") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    }
                    if (AK.IndexOf("A/C services charge") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                    if (AK.IndexOf("C.C.A(Service Charge)") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }

                }
                if (Code == "13112")
                {
                    if (AK.IndexOf("Cash serv.charge") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    }
                    if (AK.IndexOf("A/C services charge") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "13113")
                {
                    if (AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    }
                    if (AJ.IndexOf("C.C.A") >= 0 || AJ.IndexOf("Customer's A/C") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                    if (AJ.IndexOf("CCA") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 25;
                        else
                            zResult = 25;
                    }
                }
                if (Code == "13114")
                {
                    if (AJ.IndexOf("C.C.A") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }

                }
                if (Code == "13117")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "13118")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "13404")
                {
                    if (AJ.IndexOf("Cash") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 2;
                    }
                    if (AJ.IndexOf("C.C.A") >= 0 || AJ.IndexOf("Customer's A/C") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }

                }
                if (Code == "13601")
                {
                    if (Counter == 1)
                        zResult = 4;
                    else
                        zResult = 5;
                }
                if (Code == "13900")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "13120")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "13124")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "13125")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "13902")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 2;
                }

            }
            if (Module == "FX")
            {
                if (Code == "15101")
                {
                    if (AE.IndexOf("C") >= 0 & AQ.IndexOf("OTHER") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 2;
                    }
                    if (AE.IndexOf("C") >= 0 & (AQ.IndexOf("CUST") >= 0 || AQ.IndexOf("TRAN") >= 0))
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 2;
                    }
                    if (AE.IndexOf("D") >= 0 & AQ.IndexOf("TRAN") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 2;
                    }
                    if (AE.IndexOf("C") >= 0 & AQ.IndexOf("CASH") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 4;
                    }

                }
                if (Code == "15102")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "15103")
                {
                    if (AB.IndexOf("Tra soát") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 3;
                    }
                }
                if (Code == "15111")
                {
                    if (AB.IndexOf("Thu phí Chuyển tiền đi") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "15112")
                {
                    if (AB.IndexOf("Thu phí Nhận tiền đến") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "15121")
                {
                    if (AB.IndexOf("Hoàn phí Chuyển tiền đi") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "15122")
                {
                    if (AB.IndexOf("Hoàn phí Nhận tiền đến") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "15132")
                {
                    if (AB.IndexOf("Nhận tiền đến Tài khoản") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                }
                if (Code == "15142")
                {
                    if (AQ.IndexOf("CUST") >= 0 || AQ.IndexOf("TRAN") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 1;
                        else
                            zResult = 1;
                    }
                    if (AQ.IndexOf("CASH") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 3;
                        else
                            zResult = 4;
                    }
                }
                if (Code == "15301")
                {
                    if (AJ.IndexOf("CASH") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 3;
                    }
                    if (AJ.IndexOf("CUST") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 2;
                    }
                }
                if (Code == "15302")
                {
                    if (AJ.IndexOf("CASH") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 3;
                    }
                    if (AJ.IndexOf("CUST") >= 0 || AJ.IndexOf("TRAN") >= 0)
                    {
                        if (Counter == 1)
                            zResult = 2;
                        else
                            zResult = 2;
                    }

                }
                if (Code == "15212")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
            }
            if (Module == "EI")
            {
                if (Code == "19104")
                {
                    if (Counter == 1)
                        zResult = 6;
                    else
                        zResult = 8;
                }
            }
            if (Module == "GA")
            {
                if (Code == "20100")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "20300")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "20302")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "20500")
                {
                    if (Counter == 1)
                        zResult = 5;
                    else
                        zResult = 5;
                }
                if (Code == "20600")
                {
                    if (Counter == 1)
                        zResult = 5;
                    else
                        zResult = 5;
                }
                if (Code == "20700")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "20400")
                {
                    if (Counter == 1)
                        zResult = 40;
                    else
                        zResult = 40;
                }
                if (Code == "20200")
                {
                    if (Counter == 1)
                        zResult = 50;
                    else
                        zResult = 50;
                }
            }
            if (Module == "GL")
            {
                if (Code == "18102")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "18103")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "18104")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
            }
            if (Module == "LN")
            {
                if (Code == "14104")
                {
                    if (Counter == 1)
                        zResult = 5;
                    else
                        zResult = 5;
                }
                if (Code == "14106")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "14110")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "14117")
                {
                    if (Counter == 1)
                        zResult = 2;
                    else
                        zResult = 2;
                }
                if (Code == "14118")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "14113")
                {
                    if (Counter == 1)
                        zResult = 4;
                    else
                        zResult = 4;
                }
                if (Code == "14111")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "14112")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "14114")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
            }
            if (Module == "TF")
            {
                if (Code == "16102")
                {
                    if (Counter == 1)
                        zResult = 5;
                    else
                        zResult = 5;
                }
                if (Code == "16105")
                {
                    if (Counter == 1)
                        zResult = 3;
                    else
                        zResult = 3;
                }
                if (Code == "16111")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
                if (Code == "16212")
                {
                    if (Counter == 1)
                        zResult = 1;
                    else
                        zResult = 1;
                }
            }
            if (J.IndexOf("Cancel") >= 0)
            {
                if (Counter == 1)
                    zResult = -2;
                else
                    zResult = -2;
            }

            //Phan khong hach toan
            if (Module == "DP")
            {
                if (Code == "13101")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "13104")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "13107")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "13108")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 2;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 2;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "13901")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }

            }
            if (Module == "FX")
            {
                if (Code == "15103")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }

            }
            if (Module == "GA")
            {
                if (Code == "20501")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "20302")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }

            }
            if (Module == "GL")
            {
                if (Code == "18105")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }

            }
            if (Module == "LN")
            {
                if (Code == "14105")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "14120")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = float.Parse("0.5");
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = float.Parse("0.5");
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "14125")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "14107")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "14121")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
            }
            if (Module == "TF")
            {
                if (Code == "16104")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                    }
                }
            }
            if (Module == "CD")
            {
                if (Code == "22101")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "22102")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 2;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 2;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "22103")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                    }
                }
            }
            if (Module == "CM")
            {
                if (Code == "11101")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "11102")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 30;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 30;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "11103")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "11104")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 10;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 10;
                            zHachToan = 0;
                        }
                    }
                }
            }
            if (Module == "EI")
            {
                if (Code == "21201")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 3;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "21202")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "21203")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 2;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 2;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "21204")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 5;
                            zHachToan = 0;
                        }
                    }
                }
                if (Code == "21206")
                {
                    if (M == "0")
                    {
                        if (Counter == 1)
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                        else
                        {
                            zResult = 1;
                            zHachToan = 0;
                        }
                    }
                }
            }


            zArray[0] = zResult;
            zArray[1] = zHachToan;
            return zArray;
        }
    }

    public class FileImport_Info
    {

        #region [ Field Name ]
        private int _FileKey = 0;
        private string _FileName = "";
        private int _AmountData = 0;
        private int _AmountAnalysis = 0;
        private int _AmountFaile = 0;
        private string _UserKey = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public FileImport_Info()
        {

        }
        public FileImport_Info(int FileKey)
        {
            string zSQL = "SELECT * FROM [dbo].[POI_FileImport] WHERE FileKey = @FileKey AND RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _FileKey = int.Parse(zReader["FileKey"].ToString());
                    _FileName = zReader["FileName"].ToString();
                    _AmountData = int.Parse(zReader["AmountData"].ToString());
                    _AmountAnalysis = int.Parse(zReader["AmountAnalysis"].ToString());
                    _AmountFaile = int.Parse(zReader["AmountFaile"].ToString());
                    _UserKey = zReader["UserKey"].ToString();
                    _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString();
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public int FileKey
        {
            get { return _FileKey; }
            set { _FileKey = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public int AmountData
        {
            get { return _AmountData; }
            set { _AmountData = value; }
        }
        public int AmountAnalysis
        {
            get { return _AmountAnalysis; }
            set { _AmountAnalysis = value; }
        }
        public int AmountFaile
        {
            get { return _AmountFaile; }
            set { _AmountFaile = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public int Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[POI_FileImport] ("
        + " FileName ,AmountData ,AmountAnalysis ,AmountFaile ,UserKey ,Type ,TypeName ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileName ,@AmountData ,@AmountAnalysis ,@AmountFaile ,@UserKey ,@Type ,@TypeName ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT FileKey FROM POI_FileImport WHERE FileKey = SCOPE_IDENTITY() ";
            int zResult = 0;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _FileKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[POI_FileImport] ("
        + " FileKey ,FileName ,AmountData ,AmountAnalysis ,AmountFaile ,UserKey ,Type ,TypeName ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileKey ,@FileName ,@AmountData ,@AmountAnalysis ,@AmountFaile ,@UserKey ,@Type ,@TypeName ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE [dbo].[POI_FileImport] SET "
                        + " FileName = @FileName,"
                        + " AmountData = @AmountData,"
                        + " AmountAnalysis = @AmountAnalysis,"
                        + " AmountFaile = @AmountFaile,"
                        + " UserKey = @UserKey,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE FileKey = @FileKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[POI_FileImport] Set RecordStatus = 99 WHERE FileKey = @FileKey
                            DELETE FROM POI_SelectCounter  WHERE FileKey = @FileKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[POI_FileImport] WHERE FileKey = @FileKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
