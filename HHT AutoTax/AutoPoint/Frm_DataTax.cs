﻿using HHT.Connect;
using HHT.Library.System;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.Misc;

namespace HHT_AutoTax
{
    public partial class Frm_DataTax : Form
    {
        DataTable _DataTable;
        FileInfo FileInfo;
        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;
        int _KeyFile = 0;
        int _Key = 0;
        public Frm_DataTax()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            timer1.Tick += Timer1_Tick;
            timer2.Tick += Timer2_Tick;
            //timer3.Tick += Timer3_Tick;

            btn_Open.Click += Btn_Open_Click;
            btn_Del.Click += Btn_Del_Click;
            LVData.Click += LVData_Click;
            LVData.ItemActivate += LVData_ItemActivate;

            Utils.DrawLVStyle(ref LVData);
            Utils.SizeLastColumn_LV(LVData);
            InitLayout_LV(LVData);
            InitGridView_Order(GV_ListOrder);
        }
        private void Frm_DataTax_Load(object sender, EventArgs e)
        {
            DateTime zDate = DateTime.Now;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            dte_FromDate.Value = zFromDate;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            string zSQL = @"
SELECT UserName,UserName + '-' +FullName
FROM SYS_User 
WHERE SoftWareKey = 2 Order BY UserName";
            TNS.Misc.LoadDataToToolbox.KryptonComboBox(cbo_GDV, zSQL, "--- Tất cả---");
            cbo_GDV.SelectedIndex = 1;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData();
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key > 0)
            {
                if (Utils.TNMessageBox("Bạn có chắc muốn xóa", 2) == "Y")
                {
                    HHT_AutoTax.Frm_DataRecord_DLL.FileImport_Info zinfo = new Frm_DataRecord_DLL.FileImport_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message.Substring(0, 3) == "200")
                    {
                        _Key = 0;
                        Utils.TNMessageBox("Xóa thành công!", 3);
                        GV_ListOrder.Rows.Clear();
                        lbl_count.Text = "0/0";
                    }
                    else
                    {
                        Utils.TNMessageBox("Lỗi!. " + zinfo.Message, 1);
                    }
                    InitData();
                }
            }
        }

        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            _Key = LVData.SelectedItems[0].Tag.ToInt();
            DisplayData();
          
        }
        private void LVData_Click(object sender, EventArgs e)
        {
            _Key = LVData.SelectedItems[0].Tag.ToInt();
        }

        #region[Listview-Left]
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày tạo";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên file";
            colHead.Width = 230;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();

            DateTime zDate = dte_FromDate.Value;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);
            In_Table = HHT_AutoTax.Frm_DataTax_DLL.DataRecord_Data.List(zFromDate, zToDate);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["FileKey"];
                lvi.ForeColor = Color.Navy;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime date = DateTime.Parse(nRow["CreatedOn"].ToString().Trim());
                lvsi.Text = date.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FileName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        #endregion

        #region[ListView Right]

        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("Counter", "Quầy");
            GV.Columns.Add("Point", "Điểm");
            GV.Columns.Add("MA_CN_THU", "MA CN THU");
            GV.Columns.Add("TEN_CN_THU", "TEN CN THU");
            GV.Columns.Add("KBNN_THU", "KBNN thu");
            GV.Columns.Add("KIHIEU_CT", "Ký hiệu chứng từ");
            GV.Columns.Add("SO_CT", "Số chứng từ");
            GV.Columns.Add("NGAY_CT", "Ngày chứng từ");
            GV.Columns.Add("MNH_VI", "Mnh vi");
            GV.Columns.Add("SO_BUTTOAN", "Số bút toán");
            GV.Columns.Add("NGUOI_NOPTHUE", "Tên người nộp thuế");
            GV.Columns.Add("TK_NO", "TK nợ");
            GV.Columns.Add("TK_THUNS", "TK thu NSNN");
            GV.Columns.Add("MA_CQTHU", "Mã cơ quan thu");

            GV.Columns.Add("DBHC", "ĐBHC");
            GV.Columns.Add("TT", "TT");
            GV.Columns.Add("TONGTIEN", "Tổng tiền");
            GV.Columns.Add("NV_HUY", "Mã nhân viên hủy");

            GV.Columns["No"].Width = 60;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Counter"].Width = 60;
            GV.Columns["Counter"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Point"].Width = 60;
            GV.Columns["Point"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["NGAY_CT"].Width = 100;
            GV.Columns["NGAY_CT"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["NGUOI_NOPTHUE"].Width = 200;
            GV.Columns["NGUOI_NOPTHUE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["NGUOI_NOPTHUE"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["TEN_CN_THU"].Width = 200;
            GV.Columns["TEN_CN_THU"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TEN_CN_THU"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            //GV.Columns["opndt"].Width = 80;
            GV.Columns["TONGTIEN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeight = 50;
            GV.EnableHeadersVisualStyles = false;
            GV.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(180, 74, 97);
            GV.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            GV.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }

        private void DisplayData()
        {
            _IndexGV = 0;
            _DataTable = HHT_AutoTax.Frm_DataTax_DLL.DataRecord_Data.ListDetail(_Key, cbo_GDV.SelectedValue.ToString()) ;

            if (_DataTable.Rows.Count > 0)
            {
                GV_ListOrder.Rows.Clear();
                _TotalGV = _DataTable.Rows.Count;
                timer2.Start();

            }
        }

        #endregion

        #region[Import]
        private void Btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {

                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", 2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        LB_Log.Items.Clear();
                        //Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        _DataTable = new DataTable();
                        ShowLog("Đang đọc dữ liệu");

                        _DataTable = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        ShowLog("Đọc dữ liệu xong");
                        // tạo file parent
                        HHT_AutoTax.Frm_DataTax_DLL.FileImport_Info zinfo = new Frm_DataTax_DLL.FileImport_Info();
                        zinfo.FileName = FileInfo.Name;
                        zinfo.Type = 2;
                        zinfo.TypeName = "DataTax";
                        zinfo.Create_ServerKey();
                        _KeyFile = zinfo.FileKey;

                        //

                        //  Cursor.Current = Cursors.Default;
                        btn_Up_Click(null, null);
                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(), 4);
                    }
                }
            }
        }

        private SqlConnection _Connect;
        private void btn_Up_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            zError = 0;
            zSuccess = 0;
            txt_Amount_Excel.Text = _DataTable.Rows.Count.ToString("n0");
            txt_Sql.Text = "0";
            txt_Error.Text = "0";
            _IndexGV = 0;
            _TotalGV = _DataTable.Rows.Count;
            ShowLog("Tổng dòng dữ liệu: " + _DataTable.Rows.Count.ToString());


            string zConnectionString = ConnectDataBase.ConnectionString;
            _Connect = new SqlConnection(zConnectionString);
            _Connect.Open();

            timer1.Start();

            this.Cursor = Cursors.Default;
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer1.Stop();
            if (_IndexGV < _TotalGV)
            {
                DataRow r = _DataTable.Rows[_IndexGV];

                string STT = r[0].ToString();
                //DateTime trdate = DateTime.Parse(item.SubItems[3].Text);
                string MA_CN_THU = r[1].ToString();
                string TEN_CN_THU = r[2].ToString();
                string KBNN_THU = r[3].ToString();
                string KIHIEU_CT = r[4].ToString();
                string SO_CT = r[5].ToString();
                DateTime NGAY_CT = DateTime.MinValue;
                if (DateTime.TryParse(r[6].ToString(), out NGAY_CT))
                {

                }
                string MNH_VI = r[7].ToString();
                string SO_BUTTOAN = r[8].ToString();
                string NGUOI_NOPTHUE = r[9].ToString();

                string TK_NO = r[10].ToString();
                string TK_THUNS = r[11].ToString();
                string MA_CQTHU = r[12].ToString();
                string DBHC = r[13].ToString();
                string TT = r[14].ToString();
                double TONGTIEN = 0;
                if (double.TryParse(r[15].ToString(), out TONGTIEN))
                {

                }
                string NV_HUY = r[16].ToString();

                int Counter = HHT_AutoTax.Frm_DataTax_DLL.DataRecord_Data.Counter(_Connect, MNH_VI, NGAY_CT);
                int Point = 1; // fix


                string zSQL = "INSERT INTO [dbo].[POI_DataTax] ("
+ " AutoKey ,STT ,MA_CN_THU ,TEN_CN_THU ,KBNN_THU ,KIHIEU_CT ,SO_CT ,NGAY_CT ,MNH_VI ,SO_BUTTOAN ,NGUOI_NOPTHUE ,TK_NO ,TK_THUNS ,MA_CQTHU ,DBHC ,TT ,TONGTIEN ,NV_HUY ,Counter ,Point ,FileKey ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
+ " VALUES ( "
+ "@AutoKey ,@STT ,@MA_CN_THU ,@TEN_CN_THU ,@KBNN_THU ,@KIHIEU_CT ,@SO_CT ,@NGAY_CT ,@MNH_VI ,@SO_BUTTOAN ,@NGUOI_NOPTHUE ,@TK_NO ,@TK_THUNS ,@MA_CQTHU ,@DBHC ,@TT ,@TONGTIEN ,@NV_HUY ,@Counter ,@Point ,@FileKey ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
                string zResult = "";
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, _Connect);
                    zCommand.CommandType = CommandType.Text;
                    string AutoKey = Guid.NewGuid().ToString();
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = new Guid(AutoKey);
                    zCommand.Parameters.Add("@STT", SqlDbType.NVarChar).Value = STT;
                    zCommand.Parameters.Add("@MA_CN_THU", SqlDbType.NVarChar).Value = MA_CN_THU;
                    zCommand.Parameters.Add("@TEN_CN_THU", SqlDbType.NVarChar).Value = TEN_CN_THU;
                    zCommand.Parameters.Add("@KBNN_THU", SqlDbType.NVarChar).Value = KBNN_THU;
                    zCommand.Parameters.Add("@KIHIEU_CT", SqlDbType.NVarChar).Value = KIHIEU_CT;
                    zCommand.Parameters.Add("@SO_CT", SqlDbType.NVarChar).Value = SO_CT;
                    if (NGAY_CT == null)
                        zCommand.Parameters.Add("@NGAY_CT", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@NGAY_CT", SqlDbType.DateTime).Value = NGAY_CT;
                    zCommand.Parameters.Add("@MNH_VI", SqlDbType.NVarChar).Value = MNH_VI;
                    zCommand.Parameters.Add("@SO_BUTTOAN", SqlDbType.NVarChar).Value = SO_BUTTOAN;
                    zCommand.Parameters.Add("@NGUOI_NOPTHUE", SqlDbType.NVarChar).Value = NGUOI_NOPTHUE;
                    zCommand.Parameters.Add("@TK_NO", SqlDbType.NVarChar).Value = TK_NO;
                    zCommand.Parameters.Add("@TK_THUNS", SqlDbType.NVarChar).Value = TK_THUNS;
                    zCommand.Parameters.Add("@MA_CQTHU", SqlDbType.NVarChar).Value = MA_CQTHU;
                    zCommand.Parameters.Add("@DBHC", SqlDbType.NVarChar).Value = DBHC;
                    zCommand.Parameters.Add("@TT", SqlDbType.NVarChar).Value = TT;
                    zCommand.Parameters.Add("@TONGTIEN", SqlDbType.Float).Value = TONGTIEN;
                    zCommand.Parameters.Add("@NV_HUY", SqlDbType.NVarChar).Value = NV_HUY;
                    zCommand.Parameters.Add("@Counter", SqlDbType.Int).Value = Counter;
                    zCommand.Parameters.Add("@Point", SqlDbType.Float).Value = Point;
                    zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _KeyFile;
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = 0;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = "";//CreatedBy;
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = "";// CreatedName;
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ""; //ModifiedBy;
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ""; //ModifiedName;
                    zResult = zCommand.ExecuteNonQuery().ToString();
                    zCommand.Dispose();
                    zResult = "201 Created";
                }
                catch (Exception Err)
                {
                    zResult = "501 " + Err.ToString();
                }

                if (zResult.Substring(0, 3) == "201")
                {
                    zSuccess++;

                    ShowLog("Đã xử lý dòng " + (_IndexGV + 1).ToString("n0"));
                    ShowValue(zSuccess, 0);
                }
                else
                {
                    zError++;
                    ShowLog("Lỗi dòng : " + (_IndexGV + 1).ToString("n0") + ": Liên hệ IT");
                    ShowValue(0, zError);
                }
                _IndexGV++;
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                if (zError > 0)
                {
                    //timer2.Stop();
                    //PicLoading.Visible = false;
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!", 2);
                }
                else

                {
                    ShowLog("Tổng dòng đúng: " + zSuccess.ToString());
                    ShowLog("Lỗi dòng lỗi: " + zError.ToString());
                    ShowLog("Xong");

                    //_IndexGV = 0;
                    //zError = 0;
                    //zSuccess = 0;
                    //lbl_Sql.Text = "Số lượng đã lưu";
                    //txt_Sql.Text = "0";


                    //timer2.Start();
                }
                _Connect.Close();
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer2.Stop();
            if (_IndexGV < _TotalGV)
            {
                DataRow r = _DataTable.Rows[_IndexGV];
                int i = _IndexGV;
                GV_ListOrder.Rows.Add();
                //GV_ListOrder.Rows[i].Tag = zProduct;
                GV_ListOrder.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_ListOrder.Rows[i].Cells["Counter"].Value = r["Counter"];
                GV_ListOrder.Rows[i].Cells["Point"].Value = r["Point"];
                GV_ListOrder.Rows[i].Cells["MA_CN_THU"].Value = r["MA_CN_THU"];
                GV_ListOrder.Rows[i].Cells["TEN_CN_THU"].Value = r["TEN_CN_THU"];
                GV_ListOrder.Rows[i].Cells["KBNN_THU"].Value = r["KBNN_THU"];
                GV_ListOrder.Rows[i].Cells["KIHIEU_CT"].Value = r["KIHIEU_CT"];
                GV_ListOrder.Rows[i].Cells["SO_CT"].Value = r["SO_CT"];
                DateTime NGAY_CT = DateTime.Parse(r["NGAY_CT"].ToString());
                GV_ListOrder.Rows[i].Cells["NGAY_CT"].Value = NGAY_CT.ToString("dd/MM/yyyy");
                GV_ListOrder.Rows[i].Cells["MNH_VI"].Value = r["MNH_VI"];
                GV_ListOrder.Rows[i].Cells["SO_BUTTOAN"].Value = r["SO_BUTTOAN"];
                GV_ListOrder.Rows[i].Cells["NGUOI_NOPTHUE"].Value = r["NGUOI_NOPTHUE"];
                GV_ListOrder.Rows[i].Cells["TK_NO"].Value = r["TK_NO"];
                GV_ListOrder.Rows[i].Cells["TK_THUNS"].Value = r["TK_THUNS"];
                GV_ListOrder.Rows[i].Cells["MA_CQTHU"].Value = r["MA_CQTHU"];
                GV_ListOrder.Rows[i].Cells["DBHC"].Value = r["DBHC"];
                GV_ListOrder.Rows[i].Cells["TT"].Value = r["TT"];
                double TONGTIEN = 0;
                TONGTIEN = double.Parse(r["TONGTIEN"].ToString());
                GV_ListOrder.Rows[i].Cells["TONGTIEN"].Value = TONGTIEN.ToString("n0");
                GV_ListOrder.Rows[i].Cells["NV_HUY"].Value = r["NV_HUY"];

                ShowCount(_IndexGV + 1);
                _IndexGV++;

                timer2.Start();
            }
            else
            {
                timer2.Stop();
                Cursor.Current = Cursors.Default;
            }
        }
        void ShowLog(string Text)
        {
            Invoke(new MethodInvoker(delegate
            {
                LB_Log.Items.Add(DateTime.Now.ToString("hh:mm:ss.fff") + " : " + Text);
                LB_Log.SelectedIndex = LB_Log.Items.Count - 1;
            }));
        }
        void ShowValue(int Success, int Error)
        {
            Invoke(new MethodInvoker(delegate
            {
                if (Success > 0)
                    txt_Sql.Text = zSuccess.ToString("n0");
                if (Error > 0)
                    txt_Error.Text = Error.ToString("n0");
            }));
        }
        void ShowCount(int Count)
        {
            Invoke(new MethodInvoker(delegate
            {

                lbl_count.Text = Count.ToString("n0") + "/" + _TotalGV.ToString("n0");

            }));
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
namespace HHT_AutoTax.Frm_DataTax_DLL
{

    public class DataRecord_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM [dbo].[POI_FileImport] WHERE  RecordStatus != 99 AND Type=2 ORDER BY CreatedOn DESC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListDetail(int FileKey,string MNH_VI)
        {

            DataTable zTable = new DataTable();
            string zFilter = "";
            if(MNH_VI!="0")
            {
                zFilter = " AND MNH_VI = @MNH_VI ";
            }    
            string zSQL = @"SELECT  *
                            FROM[dbo].[POI_DataTax]
                            WHERE RecordStatus != 99 AND FileKey = FileKey @Customer
                            ORDER BY NGAY_CT ASC ";

            zSQL = zSQL.Replace("@Customer", zFilter); 
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                zCommand.Parameters.Add("@MNH_VI", SqlDbType.NVarChar).Value = MNH_VI;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //Lay cong quay cua ngay hom do
        public static int Counter(SqlConnection Connect,string UserID, DateTime Date)
        {
            int zResult = 0;
            string zSQL = @"DECLARE  @Result INT =0;
                            SELECT @Result = ISNULL(Counter,0)
                            FROM [dbo].[POI_SelectCounter]
                            WHERE RecordStatus != 99 
                            AND UserID = @UserID
                            AND DateWrite = @Date 
                            SELECT @Result
                            ";
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, Connect);
                zCommand.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = UserID;
                zCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Date;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }

    public class FileImport_Info
    {

        #region [ Field Name ]
        private int _FileKey = 0;
        private string _FileName = "";
        private int _AmountData = 0;
        private int _AmountAnalysis = 0;
        private int _AmountFaile = 0;
        private string _UserKey = "";
        private int _Type = 0;
        private string _TypeName = "";
        private int _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public FileImport_Info()
        {

        }
        public FileImport_Info(int FileKey)
        {
            string zSQL = "SELECT * FROM [dbo].[POI_FileImport] WHERE FileKey = @FileKey AND RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _FileKey = int.Parse(zReader["FileKey"].ToString());
                    _FileName = zReader["FileName"].ToString();
                    _AmountData = int.Parse(zReader["AmountData"].ToString());
                    _AmountAnalysis = int.Parse(zReader["AmountAnalysis"].ToString());
                    _AmountFaile = int.Parse(zReader["AmountFaile"].ToString());
                    _UserKey = zReader["UserKey"].ToString();
                    _Type = int.Parse(zReader["Type"].ToString());
                    _TypeName = zReader["TypeName"].ToString();
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]
        public int FileKey
        {
            get { return _FileKey; }
            set { _FileKey = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public int AmountData
        {
            get { return _AmountData; }
            set { _AmountData = value; }
        }
        public int AmountAnalysis
        {
            get { return _AmountAnalysis; }
            set { _AmountAnalysis = value; }
        }
        public int AmountFaile
        {
            get { return _AmountFaile; }
            set { _AmountFaile = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public int Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[POI_FileImport] ("
        + " FileName ,AmountData ,AmountAnalysis ,AmountFaile ,UserKey ,Type ,TypeName ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileName ,@AmountData ,@AmountAnalysis ,@AmountFaile ,@UserKey ,@Type ,@TypeName ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT FileKey FROM POI_FileImport WHERE FileKey = SCOPE_IDENTITY() ";
            int zResult = 0;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _FileKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO [dbo].[POI_FileImport] ("
        + " FileKey ,FileName ,AmountData ,AmountAnalysis ,AmountFaile ,UserKey ,Type ,TypeName ,RecordStatus ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileKey ,@FileName ,@AmountData ,@AmountAnalysis ,@AmountFaile ,@UserKey ,@Type ,@TypeName ,@RecordStatus ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE [dbo].[POI_FileImport] SET "
                        + " FileName = @FileName,"
                        + " AmountData = @AmountData,"
                        + " AmountAnalysis = @AmountAnalysis,"
                        + " AmountFaile = @AmountFaile,"
                        + " UserKey = @UserKey,"
                        + " Type = @Type,"
                        + " TypeName = @TypeName,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE FileKey = @FileKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName;
                zCommand.Parameters.Add("@AmountData", SqlDbType.Int).Value = _AmountData;
                zCommand.Parameters.Add("@AmountAnalysis", SqlDbType.Int).Value = _AmountAnalysis;
                zCommand.Parameters.Add("@AmountFaile", SqlDbType.Int).Value = _AmountFaile;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = _TypeName;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "501 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[POI_FileImport] Set RecordStatus = 99 WHERE FileKey = @FileKey
                            DELETE FROM POI_DataRecord  WHERE FileKey = @FileKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM [dbo].[POI_FileImport] WHERE FileKey = @FileKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}