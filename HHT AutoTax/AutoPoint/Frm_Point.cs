﻿using HHT.Connect;
using HHT.Library.System;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.Misc;

namespace HHT_AutoTax
{
    public partial class Frm_Point : Form
    {
        DataTable _DataTable;
        int _IndexGV = 0;
        int _TotalGV = 0;
        public Frm_Point()
        {
            InitializeComponent();

            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            timer2.Tick += Timer2_Tick;
            btn_Export.Click += Btn_Export_Click;



        }

        private void Frm_Point_Load(object sender, EventArgs e)
        {


            dte_FromDate.Value = DateTime.Now;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GV_ListOrder.Rows.Clear();
            GV_ListOrder.Columns.Clear();
            InitGridView_Order(GV_ListOrder);
            DisplayData();
        }
        private void DisplayData()
        {
            DateTime zDate = dte_FromDate.Value;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            _IndexGV = 0;
            //_XepHang = 0;
            _DataTable = HHT_AutoTax.Frm_Point_DLL.DataRecord_Data.List(zFromDate, zToDate);

            if (_DataTable.Rows.Count > 0)
            {
                _TotalGV = _DataTable.Rows.Count;
               // _XepHang = _TotalGV;
                timer2.Start();

            }
        }
        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "XẾP HẠNG");
            GV.Columns.Add("UserID", "USER");
            GV.Columns.Add("UserName", "GIAO DỊCH VIÊN");
            GV.Columns.Add("ThucHien", "SỐ BÚT TOÁN THỰC HIỆN");
            GV.Columns.Add("Huy", "SỐ BÚT TOÁN HỦY");
            GV.Columns.Add("Agritax", "SỐ BÚT TOÁN AGRITAX");
            GV.Columns.Add("KhongHachToan", "SỐ BÚT TOÁN KHÔNG HẠCH TOÁN");
            GV.Columns.Add("Tong", "TỔNG ĐIỂM QUY ĐỔI");
            GV.Columns.Add("Note", "GHI CHÚ");


            GV.Columns["No"].Width = 130;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["UserID"].Width = 130;
            GV.Columns["UserID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["UserName"].Width = 320;
            GV.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ThucHien"].Width = 130;
            GV.Columns["ThucHien"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            GV.Columns["Huy"].Width = 130;
            GV.Columns["Huy"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Agritax"].Width = 130;
            GV.Columns["Agritax"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["KhongHachToan"].Width = 130;
            GV.Columns["KhongHachToan"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Tong"].Width = 130;
            GV.Columns["Tong"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Note"].Width = 130;
            GV.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //""
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeight = 50;
            GV.EnableHeadersVisualStyles = false;
            GV.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(180, 74, 97);
            GV.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            GV.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        private int _XepHang = 0;
        private void Timer2_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer2.Stop();
            if (_IndexGV < _TotalGV)
            {
                DataRow r = _DataTable.Rows[_IndexGV];
                int i = _IndexGV;
                GV_ListOrder.Rows.Add();
                //GV_ListOrder.Rows[i].Tag = zProduct;
                GV_ListOrder.Rows[i].Cells["No"].Value = (_IndexGV+1).ToString();
                GV_ListOrder.Rows[i].Cells["UserID"].Value = r["UserName"];
                GV_ListOrder.Rows[i].Cells["UserName"].Value = r["FullName"];
                float zNumber = 0;
                zNumber = float.Parse(r["ThucHien"].ToString());
                GV_ListOrder.Rows[i].Cells["ThucHien"].Value = zNumber.ToString("n0");
                zNumber = 0;
                zNumber = float.Parse(r["Huy"].ToString());
                GV_ListOrder.Rows[i].Cells["Huy"].Value = zNumber.ToString("n0");
                zNumber = 0;
                zNumber = float.Parse(r["Agritax"].ToString());
                GV_ListOrder.Rows[i].Cells["Agritax"].Value = zNumber.ToString("n0");
                zNumber = 0;
                zNumber = float.Parse(r["KhongHachToan"].ToString());
                GV_ListOrder.Rows[i].Cells["KhongHachToan"].Value = zNumber.ToString("n0");
                zNumber = 0;
                zNumber = float.Parse(r["Tong"].ToString());
                GV_ListOrder.Rows[i].Cells["Tong"].Value = zNumber.ToString("n0");
                GV_ListOrder.Rows[i].Cells["Note"].Value = r["Note"];
                _IndexGV++;
                _XepHang--;

                timer2.Start();
            }
            else
            {
                timer2.Stop();
                Cursor.Current = Cursors.Default;
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách chấm điểm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.FileName;
                DataTable dt = new DataTable();

                foreach (DataGridViewColumn col in GV_ListOrder.Columns)
                {
                    dt.Columns.Add(col.HeaderText);
                }


                DataGridViewRow row;

                for (int i = 0; i < GV_ListOrder.RowCount - 1; i++)
                {
                    row = GV_ListOrder.Rows[i];
                    DataRow dRow = dt.NewRow();

                    int k = 0;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dRow[cell.ColumnIndex] = cell.Value;
                        k++;
                    }
                    dt.Rows.Add(dRow);
                }
                string Message = ExportTableToExcel(dt, Path);
                if (Message != "OK")
                    Utils.TNMessageBoxOK(Message, 4);
                else
                {
                    Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                    if (Utils.TNMessageBox(Message, 1) == "Y")
                    {
                        Process.Start(Path);
                    }
                }
            }
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }

                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("CHẤM CÔNG");



                    worksheet.Cells["A4"].LoadFromDataTable(zTable, true);
                    
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột

                    worksheet.Column(1).Width = 15;
                    worksheet.Column(2).Width = 20;
                    worksheet.Column(3).Width = 40;
                    worksheet.Column(4).Width = 15;
                    worksheet.Column(5).Width = 15;
                    worksheet.Column(6).Width = 15;
                    worksheet.Column(7).Width = 15;
                    worksheet.Column(8).Width = 15;

                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    worksheet.Column(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(5).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                    worksheet.Cells["C1"].Value = "SỐ BÚT TOÁN CỦA GIAO DỊCH VIÊN VÀ KIỂM SOÁT ";
                    worksheet.Cells["C1"].Style.WrapText = false;
                    worksheet.Cells["C1"].Style.Font.Bold = true;
                    worksheet.Cells["C1"].Style.Font.Name = "Times New Roman";
                    worksheet.Cells["C1"].Style.Font.Size = 16;
                    worksheet.Row(1).Height = 30;

                    worksheet.Cells["C2"].Value = "Tháng " + dte_FromDate.Value.Month + "/" + dte_FromDate.Value.Year;
                    worksheet.Cells["C2"].Style.WrapText = false;
                    worksheet.Cells["C2"].Style.Font.Bold = true;
                    worksheet.Cells["C2"].Style.Font.Name = "Times New Roman";
                    worksheet.Cells["C2"].Style.Font.Size = 13;
                    worksheet.Cells["C2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Row(2).Height = 30;

                    worksheet.Row(3).Height = 20;
                    worksheet.Row(3).Style.WrapText = true;
                    worksheet.Row(3).Style.Font.Bold = true;
                    worksheet.Row(3).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    worksheet.Row(4).Height = 60;
                    worksheet.Row(4).Style.WrapText = true;
                    worksheet.Row(4).Style.Font.Bold = true;
                    worksheet.Row(4).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
namespace HHT_AutoTax.Frm_Point_DLL
{

    public class DataRecord_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare  @FromDate datetime='2021-01-01 00:00:00'
--declare  @ToDate datetime='2021-01-31 23:59:59'

CREATE TABLE #TAM
(
UserName nvarchar(50),
FullName nvarchar(500),
ThucHien float,
Huy float,
Agritax float,
KhongHachToan float
)
INSERT INTO #TAM
SELECT UserName, FullName,
[dbo].[ButToanThucHien] (@FromDate,@ToDate,UserName) AS ThucHien,
[dbo].[ButToanHuy] (@FromDate,@ToDate,UserName) AS Huy,
[dbo].[ButToanAgritax] (@FromDate,@ToDate,UserName) AS Agritax,
[dbo].[ButToanKhongHachToan](@FromDate,@ToDate,UserName) AS KhongHachToan
FROM SYS_User WHERE SoftWareKey=2

SELECT 
UserName,
FullName,
ThucHien,
Huy,
Agritax,
KhongHachToan,
(ThucHien + Huy + Agritax + KhongHachToan) AS Tong, ''Note
FROM #TAM
ORDER BY Tong DESC

DROP TABLE #TAM";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }

}
