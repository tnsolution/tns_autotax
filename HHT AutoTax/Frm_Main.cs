﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNS.Misc;

namespace HHT_AutoTax
{
    public partial class Frm_Main : Form
    {
        private int IDBank = 1;//đông sài gòn
        //private int IDBank = 2; //Hải an hải phòng
        private string NameBank = "";
        private string IDSoftWare = "";
        private int _Exit = 0;
        public Frm_Main()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //---- Layout
            this.Panel_AutoTax.Location = new System.Drawing.Point(17, 44);
            this.Panel_Point.Location = new System.Drawing.Point(17, 44);

            //left
            btn_AutoTax.Click += Btn_AutoTax_Click;
            btn_AutoPoint.Click += Btn_AutoPoint_Click;


            //right - Autotax
            btn_OrderAnalysis.Click += Btn_OrderAnalysis_Click;
            btn_CheckCT.Click += Btn_CheckCT_Click;
            btn_OrderTranfer.Click += Btn_OrderTranfer_Click;
            btn_ImportDataWeb.Click += Btn_ImportDataWeb_Click;
            btn_Config.Click += Btn_Config_Click;
            btn_TKThuNS.Click += Btn_TKThuNS_Click;

            //right- Point
            btn_DataRecord.Click += Btn_DataRecord_Click;
            btn_DataTax.Click += Btn_DataTax_Click;
            btn_SelectCounter.Click += Btn_SelectCounter_Click;
            btn_Point.Click += Btn_Point_Click;


        }



        private void Frm_Main_Load(object sender, EventArgs e)
        {
            if (IDBank == 1)
            {
                NameBank = "Đông Sài Gòn";

            }
            if (IDBank == 2)
            {
                NameBank = "Hải An";
            }

            HeaderControl.Text = "Agribank " + NameBank;
            lbl_WorkDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Panel_AutoTax.Visible = false;
            Panel_Point.Visible = false;

        }
        private void Btn_AutoTax_Click(object sender, EventArgs e)
        {
            IDSoftWare = "TAX";
            bool zCheckClient = false;
            zCheckClient = CheckClient();
            if (zCheckClient == true)
            {
                Panel_AutoTax.Visible = true;
                Panel_Point.Visible = false;
            }
            else 
            {

                Panel_AutoTax.Visible = false;
                Panel_Point.Visible = false;
            }
        }
        private void Btn_AutoPoint_Click(object sender, EventArgs e)
        {
            IDSoftWare = "POINT";
            bool zCheckClient = false;
            zCheckClient = CheckClient();
            if (zCheckClient == true)
            {
                Panel_AutoTax.Visible = false;
                Panel_Point.Visible = true;
            }
            else
            {

                Panel_AutoTax.Visible = false;
                Panel_Point.Visible = false;
            }
            
        }

        #region AutoTax -Right
        private void Btn_Config_Click(object sender, EventArgs e)
        {
            Frm_SetupRule frm = new Frm_SetupRule();
            frm.ShowDialog();
        }

        private void Btn_ImportDataWeb_Click(object sender, EventArgs e)
        {
            Frm_Check frm = new Frm_Check();
            frm.ShowDialog();
        }

        private void Btn_OrderTranfer_Click(object sender, EventArgs e)
        {
            FrmOrder frm = new FrmOrder();
            frm.ShowDialog();
        }

        private void Btn_CheckCT_Click(object sender, EventArgs e)
        {
            Frm_ImportDataCheck_V2 frm = new Frm_ImportDataCheck_V2();
            frm.ShowDialog();
        }

        private void Btn_OrderAnalysis_Click(object sender, EventArgs e)
        {
            FrmOrderTranfer frm = new FrmOrderTranfer();
            frm.ShowDialog();
        }
        private void Btn_TKThuNS_Click(object sender, EventArgs e)
        {
            FrmAccount frm = new FrmAccount();
            frm.ShowDialog();
        }

        #endregion


        #region[Auto-Point]
        private void Btn_DataRecord_Click(object sender, EventArgs e)
        {
            Frm_DataRecord frm = new Frm_DataRecord();
            frm.ShowDialog();
        }
        private void Btn_DataTax_Click(object sender, EventArgs e)
        {
            Frm_DataTax frm = new Frm_DataTax();
            frm.ShowDialog();
        }
        private void Btn_SelectCounter_Click(object sender, EventArgs e)
        {
            Frm_SelectCounter frm = new Frm_SelectCounter();
            frm.ShowDialog();
        }
        private void Btn_Point_Click(object sender, EventArgs e)
        {
            Frm_Point frm = new Frm_Point();
            frm.ShowDialog();
        }
        #endregion

        #region [Dùng kéo rê form]

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            if (Utils.TNMessageBox("Bạn có muốn thoát phần mềm !", 2) == "Y")
            {
                _Exit = 1;
                //if (SessionUser.UserLogin != null && SessionUser.UserLogin.Key != "")
                //{
                //    User_Info zUser = new User_Info(SessionUser.UserLogin.Name);
                //    zUser.UpdateIsLogin(0);// hạ cờ đăng nhập
                //    Log_Login(3, "Đã đăng xuất phần mềm");
                //}
                this.Close();
            }
            else
            {
                _Exit = 0;
            }
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Exit == 0)
                e.Cancel = true;
            else
                e.Cancel = false;
            //if (MessageBox.Show("Bạn có muốn thoát phần mềm !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{

            //    e.Cancel = false;
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }
        #endregion


        private bool CheckClient()
        {
            DateTime Exp = DateTime.MinValue;
            DateTime zDatetime = DateTime.Now; // ngày hiện tại
            bool zResult = false;

            if (IDBank == 1) // đông sài gòn
            {
                if (IDSoftWare == "TAX")
                {
                    Exp = DateTime.Parse("22/04/2022");
                }
                if(IDSoftWare=="POINT")
                {
                    Exp = DateTime.Parse("31/07/2021");
                } 
            }
            if (IDBank == 2) // hải an
            {
                if (IDSoftWare == "TAX")
                {
                    Exp = DateTime.Parse("12/06/2022");
                }
                if (IDSoftWare == "POINT")
                {
                    Exp = DateTime.MinValue;
                }
            }
            if (zDatetime <= Exp)
            {
                zResult = true; // còn hạn
            }
            else if (zDatetime > Exp & zDatetime <= Exp.AddMonths(1))
            {
                zResult = true; // hết hạn còn sd được 30 ngày
                Utils.TNMessageBoxOK("Phần mềm đã hết hạn ngày "+ Exp.ToString("dd-MM-yyyy") + ".Vui lòng liên hệ nhà cung cấp!", 2);
            }
            else if ((zDatetime > Exp & zDatetime > Exp.AddMonths(1)) || Exp == DateTime.MinValue)
            {
                zResult = false; //quá hạn 1 tháng ngưng dịch vụ
                Utils.TNMessageBoxOK("Phần mềm đã bị khóa.Vui lòng liên hệ nhà cung cấp!", 2);
            }
            return zResult;
        }

    }
}
