﻿
namespace HHT_AutoTax
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.lbl_EmployeeName = new System.Windows.Forms.Label();
            this.lbl_WorkDate = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel_Left = new System.Windows.Forms.Panel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_AutoPoint = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_AutoTax = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton3 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Setup = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Order = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Report = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.Panel_Point = new System.Windows.Forms.Panel();
            this.btn_Point = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SelectCounter = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_DataTax = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_DataRecord = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_AutoTax = new System.Windows.Forms.Panel();
            this.btn_ImportDataWeb = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_OrderAnalysis = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_TKThuNS = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Config = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CheckCT = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_OrderTranfer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            this.Header.SuspendLayout();
            this.panel_Left.SuspendLayout();
            this.Panel_Right.SuspendLayout();
            this.Panel_Point.SuspendLayout();
            this.Panel_AutoTax.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1266, 42);
            this.HeaderControl.TabIndex = 186;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Agribank";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // Header
            // 
            this.Header.AutoScroll = true;
            this.Header.Controls.Add(this.lbl_EmployeeName);
            this.Header.Controls.Add(this.lbl_WorkDate);
            this.Header.Controls.Add(this.label15);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 42);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderPrimary;
            this.Header.Size = new System.Drawing.Size(1266, 65);
            this.Header.StateCommon.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Header.StateCommon.Color2 = System.Drawing.Color.LightGreen;
            this.Header.StateCommon.ColorAngle = 75F;
            this.Header.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Rounding5;
            this.Header.TabIndex = 187;
            // 
            // lbl_EmployeeName
            // 
            this.lbl_EmployeeName.AutoSize = true;
            this.lbl_EmployeeName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_EmployeeName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_EmployeeName.ForeColor = System.Drawing.Color.Green;
            this.lbl_EmployeeName.Location = new System.Drawing.Point(12, 12);
            this.lbl_EmployeeName.Name = "lbl_EmployeeName";
            this.lbl_EmployeeName.Size = new System.Drawing.Size(95, 16);
            this.lbl_EmployeeName.TabIndex = 182;
            this.lbl_EmployeeName.Text = "Người dùng : ";
            this.lbl_EmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_WorkDate
            // 
            this.lbl_WorkDate.AutoSize = true;
            this.lbl_WorkDate.BackColor = System.Drawing.Color.Transparent;
            this.lbl_WorkDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_WorkDate.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_WorkDate.Location = new System.Drawing.Point(123, 37);
            this.lbl_WorkDate.Name = "lbl_WorkDate";
            this.lbl_WorkDate.Size = new System.Drawing.Size(72, 16);
            this.lbl_WorkDate.TabIndex = 183;
            this.lbl_WorkDate.Text = "01/01/2000";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Green;
            this.label15.Location = new System.Drawing.Point(12, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 16);
            this.label15.TabIndex = 183;
            this.label15.Text = "Ngày Làm Việc :";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.kryptonHeader2.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 723);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.Size = new System.Drawing.Size(1266, 45);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader2.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.kryptonHeader2.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader2.TabIndex = 188;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "\r\n2021 © TNS. All rights reserved.";
            this.kryptonHeader2.Values.Image = null;
            // 
            // panel_Left
            // 
            this.panel_Left.AutoScroll = true;
            this.panel_Left.BackColor = System.Drawing.Color.Transparent;
            this.panel_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Left.Controls.Add(this.kryptonHeader1);
            this.panel_Left.Controls.Add(this.btn_AutoPoint);
            this.panel_Left.Controls.Add(this.btn_AutoTax);
            this.panel_Left.Controls.Add(this.kryptonButton3);
            this.panel_Left.Controls.Add(this.kryptonButton2);
            this.panel_Left.Controls.Add(this.btn_Setup);
            this.panel_Left.Controls.Add(this.btn_Order);
            this.panel_Left.Controls.Add(this.kryptonButton1);
            this.panel_Left.Controls.Add(this.btn_Report);
            this.panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Left.Location = new System.Drawing.Point(0, 107);
            this.panel_Left.Name = "panel_Left";
            this.panel_Left.Size = new System.Drawing.Size(263, 616);
            this.panel_Left.TabIndex = 189;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.Size = new System.Drawing.Size(261, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.TabIndex = 186;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Menu";
            // 
            // btn_AutoPoint
            // 
            this.btn_AutoPoint.Location = new System.Drawing.Point(125, 36);
            this.btn_AutoPoint.Name = "btn_AutoPoint";
            this.btn_AutoPoint.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_AutoPoint.Size = new System.Drawing.Size(110, 95);
            this.btn_AutoPoint.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_AutoPoint.StateCommon.Border.Rounding = 10;
            this.btn_AutoPoint.StateCommon.Border.Width = 1;
            this.btn_AutoPoint.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoPoint.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoPoint.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoPoint.StateCommon.Content.LongText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_AutoPoint.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AutoPoint.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoPoint.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_AutoPoint.TabIndex = 185;
            this.btn_AutoPoint.Tag = "MEN_0001";
            this.btn_AutoPoint.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_AutoPoint.Values.Image")));
            this.btn_AutoPoint.Values.Text = "Chấm điểm\r\n bút toán";
            // 
            // btn_AutoTax
            // 
            this.btn_AutoTax.Location = new System.Drawing.Point(9, 36);
            this.btn_AutoTax.Name = "btn_AutoTax";
            this.btn_AutoTax.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_AutoTax.Size = new System.Drawing.Size(110, 95);
            this.btn_AutoTax.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_AutoTax.StateCommon.Border.Rounding = 10;
            this.btn_AutoTax.StateCommon.Border.Width = 1;
            this.btn_AutoTax.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoTax.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoTax.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AutoTax.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_AutoTax.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_AutoTax.TabIndex = 185;
            this.btn_AutoTax.Tag = "MEN_0001";
            this.btn_AutoTax.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_AutoTax.Values.Image")));
            this.btn_AutoTax.Values.Text = "AutoTax";
            // 
            // kryptonButton3
            // 
            this.kryptonButton3.Location = new System.Drawing.Point(125, 339);
            this.kryptonButton3.Name = "kryptonButton3";
            this.kryptonButton3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton3.Size = new System.Drawing.Size(110, 95);
            this.kryptonButton3.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonButton3.StateCommon.Border.Rounding = 10;
            this.kryptonButton3.StateCommon.Border.Width = 1;
            this.kryptonButton3.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton3.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton3.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton3.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.kryptonButton3.TabIndex = 185;
            this.kryptonButton3.Tag = "MEN_0001";
            this.kryptonButton3.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton3.Values.Image")));
            this.kryptonButton3.Values.Text = "Thẩm định giá";
            // 
            // kryptonButton2
            // 
            this.kryptonButton2.Location = new System.Drawing.Point(9, 238);
            this.kryptonButton2.Name = "kryptonButton2";
            this.kryptonButton2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton2.Size = new System.Drawing.Size(110, 95);
            this.kryptonButton2.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonButton2.StateCommon.Border.Rounding = 10;
            this.kryptonButton2.StateCommon.Border.Width = 1;
            this.kryptonButton2.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton2.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton2.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton2.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.kryptonButton2.TabIndex = 185;
            this.kryptonButton2.Tag = "MEN_0001";
            this.kryptonButton2.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton2.Values.Image")));
            this.kryptonButton2.Values.Text = "Thế chấp T.sản";
            // 
            // btn_Setup
            // 
            this.btn_Setup.Location = new System.Drawing.Point(9, 339);
            this.btn_Setup.Name = "btn_Setup";
            this.btn_Setup.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Setup.Size = new System.Drawing.Size(110, 95);
            this.btn_Setup.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Setup.StateCommon.Border.Rounding = 10;
            this.btn_Setup.StateCommon.Border.Width = 1;
            this.btn_Setup.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Setup.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Setup.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Setup.TabIndex = 185;
            this.btn_Setup.Tag = "MEN_0001";
            this.btn_Setup.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Setup.Values.Image")));
            this.btn_Setup.Values.Text = "Cài đặt";
            // 
            // btn_Order
            // 
            this.btn_Order.Location = new System.Drawing.Point(9, 137);
            this.btn_Order.Name = "btn_Order";
            this.btn_Order.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Order.Size = new System.Drawing.Size(110, 95);
            this.btn_Order.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Order.StateCommon.Border.Rounding = 10;
            this.btn_Order.StateCommon.Border.Width = 1;
            this.btn_Order.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Order.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Order.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Order.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Order.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Order.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Order.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Order.TabIndex = 185;
            this.btn_Order.Tag = "MEN_0008";
            this.btn_Order.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Order.Values.Image")));
            this.btn_Order.Values.Text = "Nhân sự";
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(125, 137);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton1.Size = new System.Drawing.Size(110, 95);
            this.kryptonButton1.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonButton1.StateCommon.Border.Rounding = 10;
            this.kryptonButton1.StateCommon.Border.Width = 1;
            this.kryptonButton1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton1.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.kryptonButton1.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.kryptonButton1.TabIndex = 185;
            this.kryptonButton1.Tag = "MEN_0010";
            this.kryptonButton1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton1.Values.Image")));
            this.kryptonButton1.Values.Text = "Chấm công";
            // 
            // btn_Report
            // 
            this.btn_Report.Location = new System.Drawing.Point(125, 238);
            this.btn_Report.Name = "btn_Report";
            this.btn_Report.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Report.Size = new System.Drawing.Size(110, 95);
            this.btn_Report.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Report.StateCommon.Border.Rounding = 10;
            this.btn_Report.StateCommon.Border.Width = 1;
            this.btn_Report.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Report.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Report.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Report.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Report.TabIndex = 185;
            this.btn_Report.Tag = "MEN_0010";
            this.btn_Report.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Report.Values.Image")));
            this.btn_Report.Values.Text = "P.Tích K.Hàng";
            // 
            // Panel_Right
            // 
            this.Panel_Right.AutoScroll = true;
            this.Panel_Right.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Right.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Panel_Right.BackgroundImage")));
            this.Panel_Right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Panel_Right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Right.Controls.Add(this.Panel_Point);
            this.Panel_Right.Controls.Add(this.Panel_AutoTax);
            this.Panel_Right.Controls.Add(this.txtTitle);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(263, 107);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(1003, 616);
            this.Panel_Right.TabIndex = 190;
            // 
            // Panel_Point
            // 
            this.Panel_Point.Controls.Add(this.btn_Point);
            this.Panel_Point.Controls.Add(this.btn_SelectCounter);
            this.Panel_Point.Controls.Add(this.btn_DataTax);
            this.Panel_Point.Controls.Add(this.btn_DataRecord);
            this.Panel_Point.Location = new System.Drawing.Point(27, 269);
            this.Panel_Point.Name = "Panel_Point";
            this.Panel_Point.Size = new System.Drawing.Size(386, 111);
            this.Panel_Point.TabIndex = 195;
            // 
            // btn_Point
            // 
            this.btn_Point.Location = new System.Drawing.Point(284, 10);
            this.btn_Point.Name = "btn_Point";
            this.btn_Point.Size = new System.Drawing.Size(90, 90);
            this.btn_Point.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Point.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Point.StateCommon.Back.ColorAngle = 75F;
            this.btn_Point.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Point.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Point.StateCommon.Border.Rounding = 10;
            this.btn_Point.StateCommon.Border.Width = 2;
            this.btn_Point.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Point.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Point.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Point.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Point.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Point.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Point.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Point.TabIndex = 209;
            this.btn_Point.Tag = "SUB_0206";
            this.btn_Point.Values.Text = "4. Điểm\r\n bút toán";
            // 
            // btn_SelectCounter
            // 
            this.btn_SelectCounter.Location = new System.Drawing.Point(8, 9);
            this.btn_SelectCounter.Name = "btn_SelectCounter";
            this.btn_SelectCounter.Size = new System.Drawing.Size(90, 90);
            this.btn_SelectCounter.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SelectCounter.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_SelectCounter.StateCommon.Back.ColorAngle = 75F;
            this.btn_SelectCounter.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_SelectCounter.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_SelectCounter.StateCommon.Border.Rounding = 10;
            this.btn_SelectCounter.StateCommon.Border.Width = 2;
            this.btn_SelectCounter.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_SelectCounter.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SelectCounter.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_SelectCounter.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_SelectCounter.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SelectCounter.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SelectCounter.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_SelectCounter.TabIndex = 204;
            this.btn_SelectCounter.Tag = "SUB_0201";
            this.btn_SelectCounter.Values.Text = "1.Nhập \r\nQuầy User";
            // 
            // btn_DataTax
            // 
            this.btn_DataTax.Location = new System.Drawing.Point(192, 9);
            this.btn_DataTax.Name = "btn_DataTax";
            this.btn_DataTax.Size = new System.Drawing.Size(90, 90);
            this.btn_DataTax.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_DataTax.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_DataTax.StateCommon.Back.ColorAngle = 75F;
            this.btn_DataTax.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_DataTax.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_DataTax.StateCommon.Border.Rounding = 10;
            this.btn_DataTax.StateCommon.Border.Width = 2;
            this.btn_DataTax.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DataTax.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataTax.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_DataTax.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_DataTax.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataTax.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataTax.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataTax.TabIndex = 207;
            this.btn_DataTax.Tag = "SUB_0205";
            this.btn_DataTax.Values.Text = "3. Thuế liên\r\nN.Hàng";
            // 
            // btn_DataRecord
            // 
            this.btn_DataRecord.Location = new System.Drawing.Point(100, 9);
            this.btn_DataRecord.Name = "btn_DataRecord";
            this.btn_DataRecord.Size = new System.Drawing.Size(90, 90);
            this.btn_DataRecord.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_DataRecord.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_DataRecord.StateCommon.Back.ColorAngle = 75F;
            this.btn_DataRecord.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_DataRecord.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_DataRecord.StateCommon.Border.Rounding = 10;
            this.btn_DataRecord.StateCommon.Border.Width = 2;
            this.btn_DataRecord.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_DataRecord.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataRecord.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_DataRecord.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_DataRecord.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataRecord.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataRecord.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_DataRecord.TabIndex = 206;
            this.btn_DataRecord.Tag = "SUB_0203";
            this.btn_DataRecord.Values.Text = "2. Dữ liệu\r\nbút toán";
            // 
            // Panel_AutoTax
            // 
            this.Panel_AutoTax.Controls.Add(this.btn_ImportDataWeb);
            this.Panel_AutoTax.Controls.Add(this.btn_OrderAnalysis);
            this.Panel_AutoTax.Controls.Add(this.btn_TKThuNS);
            this.Panel_AutoTax.Controls.Add(this.btn_Config);
            this.Panel_AutoTax.Controls.Add(this.btn_CheckCT);
            this.Panel_AutoTax.Controls.Add(this.btn_OrderTranfer);
            this.Panel_AutoTax.Location = new System.Drawing.Point(27, 54);
            this.Panel_AutoTax.Name = "Panel_AutoTax";
            this.Panel_AutoTax.Size = new System.Drawing.Size(290, 200);
            this.Panel_AutoTax.TabIndex = 195;
            // 
            // btn_ImportDataWeb
            // 
            this.btn_ImportDataWeb.Location = new System.Drawing.Point(192, 101);
            this.btn_ImportDataWeb.Name = "btn_ImportDataWeb";
            this.btn_ImportDataWeb.Size = new System.Drawing.Size(90, 90);
            this.btn_ImportDataWeb.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ImportDataWeb.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_ImportDataWeb.StateCommon.Back.ColorAngle = 75F;
            this.btn_ImportDataWeb.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_ImportDataWeb.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_ImportDataWeb.StateCommon.Border.Rounding = 10;
            this.btn_ImportDataWeb.StateCommon.Border.Width = 2;
            this.btn_ImportDataWeb.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ImportDataWeb.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ImportDataWeb.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_ImportDataWeb.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_ImportDataWeb.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ImportDataWeb.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_ImportDataWeb.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_ImportDataWeb.TabIndex = 209;
            this.btn_ImportDataWeb.Tag = "SUB_0206";
            this.btn_ImportDataWeb.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_ImportDataWeb.Values.Image")));
            this.btn_ImportDataWeb.Values.Text = "Khách hàng";
            // 
            // btn_OrderAnalysis
            // 
            this.btn_OrderAnalysis.Location = new System.Drawing.Point(8, 9);
            this.btn_OrderAnalysis.Name = "btn_OrderAnalysis";
            this.btn_OrderAnalysis.Size = new System.Drawing.Size(90, 90);
            this.btn_OrderAnalysis.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_OrderAnalysis.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_OrderAnalysis.StateCommon.Back.ColorAngle = 75F;
            this.btn_OrderAnalysis.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_OrderAnalysis.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_OrderAnalysis.StateCommon.Border.Rounding = 10;
            this.btn_OrderAnalysis.StateCommon.Border.Width = 2;
            this.btn_OrderAnalysis.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderAnalysis.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderAnalysis.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_OrderAnalysis.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_OrderAnalysis.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderAnalysis.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderAnalysis.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_OrderAnalysis.TabIndex = 204;
            this.btn_OrderAnalysis.Tag = "SUB_0201";
            this.btn_OrderAnalysis.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_OrderAnalysis.Values.Image")));
            this.btn_OrderAnalysis.Values.Text = "Phân tích";
            // 
            // btn_TKThuNS
            // 
            this.btn_TKThuNS.Location = new System.Drawing.Point(100, 101);
            this.btn_TKThuNS.Name = "btn_TKThuNS";
            this.btn_TKThuNS.Size = new System.Drawing.Size(90, 90);
            this.btn_TKThuNS.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TKThuNS.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_TKThuNS.StateCommon.Back.ColorAngle = 75F;
            this.btn_TKThuNS.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_TKThuNS.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_TKThuNS.StateCommon.Border.Rounding = 10;
            this.btn_TKThuNS.StateCommon.Border.Width = 2;
            this.btn_TKThuNS.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TKThuNS.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TKThuNS.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_TKThuNS.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_TKThuNS.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TKThuNS.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_TKThuNS.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_TKThuNS.TabIndex = 208;
            this.btn_TKThuNS.Tag = "SUB_0204";
            this.btn_TKThuNS.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_TKThuNS.Values.Image")));
            this.btn_TKThuNS.Values.Text = "TK N.sách";
            // 
            // btn_Config
            // 
            this.btn_Config.Location = new System.Drawing.Point(8, 101);
            this.btn_Config.Name = "btn_Config";
            this.btn_Config.Size = new System.Drawing.Size(90, 90);
            this.btn_Config.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Config.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_Config.StateCommon.Back.ColorAngle = 75F;
            this.btn_Config.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Config.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Config.StateCommon.Border.Rounding = 10;
            this.btn_Config.StateCommon.Border.Width = 2;
            this.btn_Config.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Config.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Config.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Config.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_Config.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Config.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Config.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_Config.TabIndex = 205;
            this.btn_Config.Tag = "SUB_0202";
            this.btn_Config.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Config.Values.Image")));
            this.btn_Config.Values.Text = "Tập luật";
            // 
            // btn_CheckCT
            // 
            this.btn_CheckCT.Location = new System.Drawing.Point(192, 9);
            this.btn_CheckCT.Name = "btn_CheckCT";
            this.btn_CheckCT.Size = new System.Drawing.Size(90, 90);
            this.btn_CheckCT.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CheckCT.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CheckCT.StateCommon.Back.ColorAngle = 75F;
            this.btn_CheckCT.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_CheckCT.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_CheckCT.StateCommon.Border.Rounding = 10;
            this.btn_CheckCT.StateCommon.Border.Width = 2;
            this.btn_CheckCT.StateCommon.Content.Image.Effect = ComponentFactory.Krypton.Toolkit.PaletteImageEffect.Light;
            this.btn_CheckCT.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CheckCT.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CheckCT.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_CheckCT.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_CheckCT.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CheckCT.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_CheckCT.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_CheckCT.TabIndex = 207;
            this.btn_CheckCT.Tag = "SUB_0205";
            this.btn_CheckCT.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_CheckCT.Values.Image")));
            this.btn_CheckCT.Values.Text = "Kiểm duyệt";
            // 
            // btn_OrderTranfer
            // 
            this.btn_OrderTranfer.Location = new System.Drawing.Point(100, 9);
            this.btn_OrderTranfer.Name = "btn_OrderTranfer";
            this.btn_OrderTranfer.Size = new System.Drawing.Size(90, 90);
            this.btn_OrderTranfer.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_OrderTranfer.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_OrderTranfer.StateCommon.Back.ColorAngle = 75F;
            this.btn_OrderTranfer.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_OrderTranfer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_OrderTranfer.StateCommon.Border.Rounding = 10;
            this.btn_OrderTranfer.StateCommon.Border.Width = 2;
            this.btn_OrderTranfer.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderTranfer.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderTranfer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_OrderTranfer.StateCommon.Content.ShortText.MultiLine = ComponentFactory.Krypton.Toolkit.InheritBool.True;
            this.btn_OrderTranfer.StateCommon.Content.ShortText.MultiLineH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderTranfer.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_OrderTranfer.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far;
            this.btn_OrderTranfer.TabIndex = 206;
            this.btn_OrderTranfer.Tag = "SUB_0203";
            this.btn_OrderTranfer.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_OrderTranfer.Values.Image")));
            this.btn_OrderTranfer.Values.Text = "Lệnh CT";
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(1001, 30);
            this.txtTitle.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtTitle.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.txtTitle.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.txtTitle.TabIndex = 188;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "";
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.panel_Left);
            this.Controls.Add(this.kryptonHeader2);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Main";
            this.Text = "Frm_Main";
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            this.Header.ResumeLayout(false);
            this.Header.PerformLayout();
            this.panel_Left.ResumeLayout(false);
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Point.ResumeLayout(false);
            this.Panel_AutoTax.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Header;
        private System.Windows.Forms.Label lbl_EmployeeName;
        private System.Windows.Forms.Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel panel_Left;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Setup;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Report;
        private System.Windows.Forms.Panel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_AutoPoint;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_AutoTax;
        private System.Windows.Forms.Panel Panel_Point;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Point;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SelectCounter;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_DataTax;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_DataRecord;
        private System.Windows.Forms.Panel Panel_AutoTax;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ImportDataWeb;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_TKThuNS;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Config;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CheckCT;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_OrderTranfer;
        private System.Windows.Forms.Label lbl_WorkDate;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_OrderAnalysis;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
    }
}