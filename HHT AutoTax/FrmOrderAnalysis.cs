﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HHT.Library.Bank;
using HHT.Library.System;

namespace HHT_AutoTax
{
    public partial class FrmOrderAnalysis : Form
    {
        public int SectionNo = 0;
        private bool _IsPostback;
        CultureInfo enUS = new CultureInfo("en-US"); // is up to you

        private ArrayList _ListItemInBill;
        private OrderTranfer_Info _OrderOrigin;

        private Order_Info _OrderHeader;
        private ArrayList _OrderDetail;

        public FrmOrderAnalysis()
        {
            InitializeComponent();
        }
        private void FrmOrderAnalysis_Load(object sender, EventArgs e)
        {
            _IsPostback = false;
            InitGridView_Order(GV_ListOrder);
            InitGridView_Analysis(GV_Analysis);
            InitGridView_Order_Detail(GV_DataDetail);
            cbo_StyleAnalysis.SelectedIndex = 1;
            cbo_StyleFee.SelectedIndex = 0;
            LoadDataOfOrder();

            _IsPostback = true;
        }

        #region Design Layout
        private void btn_Hide_Click(object sender, EventArgs e)
        {

            if (btn_Hide.Text == ">>")
            {
                Panel_Right.Visible = false;
                btn_Hide.Text = "<<";
            }
            else
            {
                Panel_Right.Visible = true;
                btn_Hide.Text = ">>";
            }
        }
        public void InitGridView_Order(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("Message", "Message");
            GV.Columns.Add("StatusAnalysis", "Status");
            GV.Columns.Add("trdate", "Ngày");
            GV.Columns.Add("remark", "Nội Dung");
            GV.Columns.Add("traamt", "Số Tiền");
            GV.Columns.Add("TKTNS", "TK Thu NS");
            GV.Columns.Add("stylecust", "Loại");
            GV.Columns.Add("ordcust", "Người Nộp Thuế");
            GV.Columns.Add("bencust", "Hải Quan");
            GV.Columns.Add("trref", "Số Chứng Từ");
            GV.Columns[0].Width = 40;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 90;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 40;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 80;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[4].Width = 280;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[4].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns[5].Width = 120;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 140;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 80;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[8].Width = 280;
            GV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[8].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns[9].Width = 160;
            GV.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[9].DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            GV.Columns[10].Width = 160;
            GV.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[10].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        public void InitGridView_Analysis(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Field_1", "");
            GV.Columns.Add("Field_2", "");
            GV.Columns.Add("Field_3", "");
            GV.Columns.Add("Field_4", "");

            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 120;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 140;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 250;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;


            for (int i = 1; i <= 30; i++)
            {
                GV.Rows.Add();
            }
        }
        public void InitGridView_Order_Detail(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("Chuong", "Chương");
            GV.Columns.Add("NDKT", "NDKT");
            GV.Columns.Add("Content", "Nội Dung");
            GV.Columns.Add("TienNT", "Tiền NT");
            GV.Columns.Add("KyThue", "Kỳ Thuế");

            GV.Columns[0].Width = 80;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 80;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 220;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 100;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        #endregion

        #region [Get Data ]
        private void btn_View_Click(object sender, EventArgs e)
        {
            LoadDataOfOrder();
        }
        public void LoadDataOfOrder()
        {
            GV_ListOrder.Rows.Clear();
            DataTable zListOrder = new DataTable();

            zListOrder = OrderTranfer_Data.ListAnalysis(SectionNo, cbo_StyleAnalysis.SelectedIndex, cbo_StyleFee.SelectedIndex);

            int i = 0;
            int j = 0;
            foreach (DataRow nRow in zListOrder.Rows)
            {
                GV_ListOrder.Rows.Add();
                DataGridViewRow nRowView = GV_ListOrder.Rows[i];
                DateTime z_trdate = (DateTime)nRow["trdate"];
                double zMoney = double.Parse(nRow["traamt"].ToString());

                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StatusAnalysis"].Value = nRow["StatusAnalysis"].ToString();
                nRowView.Cells["trdate"].Value = z_trdate.ToString("dd/MM/yyyy");
                nRowView.Cells["trref"].Value = nRow["trref"].ToString();
                nRowView.Cells["traamt"].Value = zMoney.ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR"));
                nRowView.Cells["TKTNS"].Value = nRow["name_16"].ToString().Trim();
                nRowView.Cells["bencust"].Value = nRow["bencust"].ToString().Trim().ToUpper();
                string zNguoiNT = nRow["ordcust"].ToString().Trim().ToUpper();
                if (zNguoiNT.Contains("CTY") || zNguoiNT.Contains("CONG TY"))
                {
                    nRowView.Cells["Stylecust"].Value = "CÔNG TY";
                }
                else
                {
                    nRowView.Cells["Stylecust"].Value = "CÁ NHÂN";
                }
                nRowView.Cells["remark"].Value = nRow["remark"].ToString().Trim();
                nRowView.Cells["ordcust"].Value = nRow["ordcust"].ToString().Trim();
                i++;
                if (nRow["StatusAnalysis"].ToString() != "0")
                    j++;
            }
            lbl_Total_Record.Text = i.ToString();
            lbl_Record_Success.Text = j.ToString();
            lbl_Record_Error.Text = (i - j).ToString();
        }
        private void Message_System(string Message)
        {
            Invoke(new MethodInvoker(delegate
            {
                LB_Log.Items.Add(DateTime.Now.ToString("dd-MM-yy hh:mm:ss.fff") + " : " + Message);
            }));
        }
        private void LoadToToolBox()
        {
            GV_DataDetail.Rows.Clear();

            txt_SoChungTu.Text = _OrderHeader.trref;
            txt_NHNN_ID.Text = _OrderHeader.NHNN_ID;
            txt_NHNN_Name.Text = _OrderHeader.NHNN_Name;
            cbo_KieuThu.SelectedIndex = _OrderHeader.KieuThu;

            DateTime zDay = (DateTime)_OrderHeader.NgayNT;
            txt_NH_PhucVu.Text = _OrderHeader.NHPV_ID;
            txt_NH_PhucVu_Name.Text = _OrderHeader.NHPV_Name;
            txt_NgayNT.Text = zDay.ToString("dd/MM/yyyy");

            txt_MaSoThue.Text = _OrderHeader.Ma_ST;
            txt_NguoiNT.Text = _OrderHeader.TenCongTy;

            txt_TK_Thu_NS.Text = _OrderHeader.TK_ThuNS;
            txt_TK_Name.Text = _OrderHeader.TK_ThuNS_Name;

            txt_DBHC_ID.Text = _OrderHeader.DBHC_ID + "HH";
            txt_DBHC_Name.Text = _OrderHeader.DBHC_Name;

            txt_CQQLThu_ID.Text = _OrderHeader.CQThuID;
            txt_CQQLThu_Name.Text = _OrderHeader.CQThuName;

            txt_LHXNK.Text = _OrderHeader.LH_ID;
            txt_LHXNK_Name.Text = _OrderHeader.LH_Name;

            txt_ToKhaiSo.Text = _OrderHeader.ToKhai;

            txt_LoaiThue.Text = _OrderHeader.LoaiThueID;

            if (_OrderHeader.NgayDK != null)
            {
                zDay = (DateTime)_OrderHeader.NgayDK;
                txt_NgayDk.Text = zDay.ToString("dd/MM/yyyy");
            }
            else
                txt_NgayDk.Text = "";
            //------------------------------
            int n = _OrderDetail.Count;
            Order_Detail_Info zDetail;
            double zTotal = 0;
            int i = 0;
            for (i = 0; i < n; i++)
            {
                GV_DataDetail.Rows.Add();
                zDetail = (Order_Detail_Info)_OrderDetail[i];
                GV_DataDetail.Rows[i].Cells[0].Value = zDetail.ChuongID;
                GV_DataDetail.Rows[i].Cells[1].Value = zDetail.NDKT_ID;
                GV_DataDetail.Rows[i].Cells[2].Value = zDetail.NDKT_Name;
                GV_DataDetail.Rows[i].Cells[3].Value = zDetail.SoTien.ToString("#,###,###");
                zTotal += zDetail.SoTien;
                GV_DataDetail.Rows[i].Cells[4].Value = zDetail.KyThue;
            }
            GV_DataDetail.Rows.Add();
            GV_DataDetail.Rows[i].Cells[0].Value = "";
            GV_DataDetail.Rows[i].Cells[1].Value = "";
            GV_DataDetail.Rows[i].Cells[2].Value = "TỔNG CỘNG";
            GV_DataDetail.Rows[i].Cells[3].Value = zTotal.ToString("#,###,###");
        }
        #endregion

        private int _Index_Selected = 0;
        int _AmountSuccess = 0;
        private void GV_ListOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_IsPostback)
            {
                if (_Index_Selected != e.RowIndex)
                {
                    if (GV_ListOrder.Rows[e.RowIndex].Cells["trref"].Value != null)
                    {
                        _Index_Selected = e.RowIndex;
                        GV_Analysis.Rows.Clear();
                        GV_Analysis.Rows.Add(50);

                        Analysis_Record(2);

                        DataGridViewRow nRowView = GV_Analysis.Rows[0];
                        nRowView.Cells["Field_1"].Value = "Chứng từ số";
                        nRowView.Cells["Field_2"].Value = _OrderOrigin.trref;

                        //======================PHAN TICH NOI DUNG VA TIM KIEM THONG SO=============================
                        int k = 2;
                        nRowView = GV_Analysis.Rows[k];
                        nRowView.Cells["Field_1"].Value = "******";
                        nRowView.Cells["Field_2"].Value = "***NỘI DUNG***";
                        nRowView.Cells["Field_3"].Value = "******";
                        k = 3;

                        for (int i = 0; i < _ListItemInBill.Count; i++)
                        {
                            Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                            nRowView = GV_Analysis.Rows[k + i];
                            nRowView.Cells["Field_1"].Value = zItem.Content;
                            nRowView.Cells["Field_2"].Value = zItem.CategoryName;
                            nRowView.Cells["Field_3"].Value = zItem.Value;
                            nRowView.Cells["Field_4"].Value = zItem.Extent;

                        }

                    }

                    LoadToToolBox();
                }
            }
        }
        private void btn_Apply_Click(object sender, EventArgs e)
        {
            _AmountSuccess = 0;
            _Index_Selected = 0;
            GV_ListOrder.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            Timer_Auto.Start();
        }
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            Timer_Auto.Stop();
            string zStatus = GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value.ToString();
            if (zStatus == "0")
            {
                Analysis_Record(1);
            }
            else
                _AmountSuccess++;

            lbl_Record_Success.Text = _AmountSuccess.ToString();

            _Index_Selected++;
            if (_Index_Selected < GV_ListOrder.Rows.Count)
                Timer_Auto.Start();
            else
            {
                GV_ListOrder.Enabled = true;
                int zTotal = int.Parse(lbl_Total_Record.Text);
                lbl_Record_Error.Text = (zTotal - _AmountSuccess).ToString();
                this.Cursor = Cursors.Default;
            }
        }

        private void Analysis_Record(int Style)
        {
            string zID = GV_ListOrder.Rows[_Index_Selected].Cells["trref"].Value.ToString();
            _OrderOrigin = new OrderTranfer_Info(zID);
            _OrderOrigin.remark = GV_ListOrder.Rows[_Index_Selected].Cells["remark"].Value.ToString();
            txt_remark.Text = _OrderOrigin.remark;

            Message_System(">>>>>>>>" + (_Index_Selected + 1).ToString() + "<<<<<<<<");
            Analysis_Basic();

            int zAmountErr = 0;
            SearchOrderHeader();
            if (CheckOrderHeader() == 0)
            {
                RuleDetail_C_TM_ST();
                if (CheckOrderDetail() > 0)
                {
                    zAmountErr++;
                }
                if (_OrderHeader.LoaiThueID == "04")
                {
                    Rule_TK_Ngay_LH();
                    if (CheckOrderTKNgayLH() > 0)
                        zAmountErr++;
                }

            }
            else
                zAmountErr++;

            if (zAmountErr == 0)
            {
                _OrderHeader.StatusAnalysis = 1;
                _AmountSuccess++;
                if (Style == 1)
                {
                    GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value = _OrderHeader.StatusAnalysis;
                    GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Done 1";
                    _OrderOrigin.UpdateStatus(1, CategoryOrder());
                    if (txt_ToKhaiSo.Text.Length > 11)
                    {
                        string result = txt_ToKhaiSo.Text.Substring(0, 11);
                        txt_ToKhaiSo.Text = result;
                        _OrderHeader.ToKhai = result;
                        _OrderHeader.Save();
                    }

                    if (_OrderHeader.NgayDK != null)
                    {
                        DateTime zDay = _OrderOrigin.trdate;
                        zDay = (DateTime)_OrderHeader.NgayDK;
                        txt_NgayDk.Text = zDay.ToString("dd/MM/yyyy");
                    }
                    if (_OrderHeader.SoTien == 20000 && _OrderHeader.TK_ThuNS == "7111")
                    {
                        txt_TK_Thu_NS.Text = "3511";
                        _OrderHeader.TK_ThuNS = txt_TK_Thu_NS.Text;
                        _OrderHeader.Save();
                    }
                    if (_OrderHeader.SoTien > 20000 && _OrderHeader.SoTien % 20000 == 0 && _OrderHeader.TK_ThuNS == "3511")
                    {
                        _OrderHeader.ToKhai = "9999999999";
                        _OrderHeader.LH_ID = "99999";
                        _OrderHeader.NgayDK = DateTime.Now;
                        _OrderHeader.LH_Name = "";
                        _OrderHeader.Save();
                        _OrderHeader.ToKhai = txt_ToKhaiSo.Text;
                        _OrderHeader.NgayDK = DateTime.Parse(txt_NgayDk.Text);
                        _OrderHeader.LH_ID = txt_LHXNK.Text;
                        _OrderHeader.LH_Name = "";
                    }
                    _OrderHeader.Save();
                    for (int i = 0; i < _OrderDetail.Count; i++)
                    {
                        Order_Detail_Info zOrderDetail = (Order_Detail_Info)_OrderDetail[i];
                        zOrderDetail.trref = _OrderHeader.trref;
                        zOrderDetail.Save();
                    }
                }
                else
                    GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Done 1";

            }
            //----------------------------------------
            if (Style == 2)
            {
                if (zAmountErr > 0)
                {
                    _OrderHeader.StatusAnalysis = 2;
                    RuleDetail_C_TM_ST_Ext();
                }
            }
            //-----------------------LUAT MO RONG---------------------
            if (Style == 1 && zAmountErr > 0)
            {
                OrderTranfer_Detail_Info zTranferDetail;

                for (int k = 0; k < _ListItemInBill.Count; k++)
                {
                    zTranferDetail = new OrderTranfer_Detail_Info();
                    zTranferDetail.trref = _OrderOrigin.trref;
                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[k];

                    zTranferDetail.CategoryID = zItemValue.CategoryID;
                    zTranferDetail.ContentValue = zItemValue.Value;
                    zTranferDetail.ContentItem = zItemValue.Content;
                    zTranferDetail.Create();

                }
                _OrderOrigin.UpdateStatus(5, CategoryOrder());
                GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Done 5";
                GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value = 5;
            }
         //=======================================


        }
        private int CategoryOrder()
        {
            if (_OrderHeader.LoaiThueID == "01")
                return 1;
            else
            {
                if (_OrderHeader.SoTien % 20000 == 0)
                    return 2;
                else
                    return 3;
            }
        }

        private void Analysis_Basic()
        {
            _ListItemInBill = Function.SplipItem(_OrderOrigin.remark.ToUpper());

            DataTable zListCategory = Analysis_Data.ListCategory();
            foreach (DataRow zRowCategory in zListCategory.Rows)
            {
                int zCategoryID = (int)zRowCategory["CategoryID"];
                int zStyle = (int)zRowCategory["Style"];
                string zQuery = zRowCategory["Query"].ToString().Trim();
                DataTable zListWordcodeForName = Analysis_Data.ListWordCodeName(zCategoryID);

                for (int i = 0; i < zListWordcodeForName.Rows.Count; i++)
                {
                    bool zIsFound = false;
                    string zCode = zListWordcodeForName.Rows[i]["WordCode"].ToString();
                    int zRuleID = int.Parse(zListWordcodeForName.Rows[i]["RuleID"].ToString());
                    int zLengMax = int.Parse(zListWordcodeForName.Rows[i]["LengMax"].ToString());
                    for (int j = 0; j < _ListItemInBill.Count - 1; j++)
                    {
                        Item_Bill zItem = (Item_Bill)_ListItemInBill[j];
                        if (!zItem.IsNumber && zItem.Content.Length >= zCode.Length && zItem.CategoryID == 0)
                        {
                            // Find Rule MST, TK,......
                            string zTemp = "";
                            if (zStyle == 1)
                            {
                                zTemp = zItem.Content.Substring(zItem.Content.Length - zCode.Length, zCode.Length);
                                if (zTemp == zCode && j < _ListItemInBill.Count - 1)
                                {
                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    string zItemValue_Content = zItemValue.Content;
                                    if (zCategoryID == 1 || zCategoryID == 2)
                                    {
                                        int k = 0;
                                        while (k < zItemValue.Content.Length)
                                        {
                                            if (zItemValue.Content[k] == '/' || zItemValue.Content[k] == '\\' || zItemValue.Content[k] == '-')
                                            {
                                                break;
                                            }
                                            k++;
                                        }
                                        zItemValue_Content = zItemValue.Content.Substring(0, k);

                                    }
                                    if (zCategoryID == 1 && zItemValue.Content.Length == 14)
                                    {
                                        int zMST_ext = 0;
                                        if (zItemValue.Content[10] == '-' && int.TryParse(zItemValue.Content.Substring(11, 3), out zMST_ext))
                                        {
                                            zItemValue_Content = zItemValue.Content.Substring(0, 10) + zItemValue.Content.Substring(11, 3);

                                        }
                                    }
                                    if (CheckItemIsNumber(zRuleID, 2, zItemValue_Content))
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.WordCode = zCode;
                                        zItem.Value = zItemValue_Content;
                                        zIsFound = true;
                                    }
                                }
                            }
                            // Find rule LH XUAT NHAP KHAU
                            if (zStyle == 2)
                            {
                                if (zItem.Content.Contains(zCode))
                                {
                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    string zLH_ID = zItem.Content.Substring(zItem.Content.Length - 1, 1) + zItemValue.Content;
                                    string zLH_Name = CheckItemValue_LHXNK(zRuleID, 2, zLH_ID, zQuery);
                                    if (zLH_Name.Length > 5)
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.Value = zLH_ID;
                                        zItem.Extent = zLH_Name;
                                        zItem.WordCode = zCode;
                                        zIsFound = true;
                                    }
                                }
                            }
                            //// TM, CHUONG
                            if (zStyle == 3) // 
                            {
                                zTemp = zItem.Content.Substring(zItem.Content.Length - zCode.Length, zCode.Length);
                                if (zTemp == zCode && j < _ListItemInBill.Count - 1)
                                {
                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    string zName = CheckItemValue_Extension(zRuleID, 2, zItemValue.Content, zQuery);
                                    if (zName.Length > 5)
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.Value = zItemValue.Content;
                                        zItem.Extent = zName;
                                        zItem.WordCode = zCode;
                                        zIsFound = true;
                                    }
                                }
                            }
                            if (zStyle == 7) // 
                            {
                                zTemp = zItem.Content.Substring(zItem.Content.Length - zCode.Length, zCode.Length);
                                if (zTemp == zCode && j < _ListItemInBill.Count - 1)
                                {
                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    
                                    if (zItemValue.Content.Length == 3)
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.Value = zItemValue.Content;
                                        zItem.WordCode = zCode;
                                        zIsFound = true;
                                    }
                                }
                            }
                            // Find Money
                            if (zStyle == 4)
                            {
                                zTemp = zItem.Content.Substring(zItem.Content.Length - zCode.Length, zCode.Length);
                                if (zTemp == zCode && j < _ListItemInBill.Count - 1)
                                {

                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    string zMoney = CheckItemIsMoney(zRuleID, 2, zItemValue.Content);
                                    if (zMoney.Length >= 4)
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.WordCode = zCode;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.Value = zMoney;
                                        zIsFound = true;
                                    }
                                }
                            }
                            // Find Ngay
                            if (zStyle == 5)
                            {
                                zTemp = zItem.Content.Substring(zItem.Content.Length - zCode.Length, zCode.Length);
                                if (zTemp == zCode && j < _ListItemInBill.Count - 1)
                                {
                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    string zDate = CheckItemIsDate(zRuleID, 2, zItemValue.Content);
                                    if (zDate.Length == 10)
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.Value = zDate;
                                        zItem.WordCode = zCode;
                                        zIsFound = true;
                                    }
                                }
                            }
                            // Find Ky Thuế
                            if (zStyle == 6)
                            {
                                zTemp = zItem.Content.Substring(zItem.Content.Length - zCode.Length, zCode.Length);
                                if (zTemp == zCode && j < _ListItemInBill.Count - 1 && zItem.Content.Length <= zLengMax)
                                {
                                    Item_Bill zItemValue = (Item_Bill)_ListItemInBill[j + 1];
                                    string zDate = CheckItemIsKyThue(zRuleID, 2, zItemValue.Content);
                                    if (zDate.Length == 7)
                                    {
                                        zItem.CategoryID = zCategoryID;
                                        zItem.CategoryName = zRowCategory["CategoryName"].ToString();
                                        zItem.Value = zDate;
                                        zItem.WordCode = zCode;
                                        zIsFound = true;
                                    }
                                }
                            }
                        }
                    }
                    if (zIsFound)
                        break;
                }
            }

        }
        private void SearchOrderHeader()
        {
            _OrderHeader = new Order_Info();
            _OrderDetail = new ArrayList();

            _OrderHeader.trref = _OrderOrigin.trref;
            _OrderHeader.SectionNo = SectionNo;
            _OrderHeader.NHPV_ID = _OrderOrigin.remtbk;
            _OrderHeader.NHPV_Name = _OrderOrigin.remntbk;

            if (_OrderOrigin.name_16.ToString().Length == 0)
                _OrderOrigin.name_16 = "7111";
            string zName_16 = _OrderOrigin.name_16.ToString().Trim();
            string zTKNganSach = "";
            string zCQQuanLyNS = "";

            if (zName_16.Length >= 4)
            {
                zTKNganSach = zName_16.Substring(0, 4);
                if (zName_16.Length > 4)
                {
                    zCQQuanLyNS = zName_16.Substring(4, zName_16.Length - 4).Trim();
                    zCQQuanLyNS = Function.GetAllNumberInSide(zCQQuanLyNS);
                    if (zCQQuanLyNS.Length == 8 && zCQQuanLyNS[0] == '0')
                    {
                        zCQQuanLyNS = zCQQuanLyNS.Substring(1, zCQQuanLyNS.Length - 1);
                    }
                }
            }
            if (zCQQuanLyNS.Length == 0)
            {
                zCQQuanLyNS = OrderTranfer_Data.SearchDepartment(_OrderOrigin.bencust);
            }

            _OrderHeader.TK_ThuNS = zTKNganSach;
            _OrderHeader.TK_ThuNS_Name = HHT.Library.System.Data_Access.ValueOfField("SELECT AccountName FROM FNC_AccountNN WHERE AccountID ='" + zTKNganSach + "'");

            GovTax_Account_Info zCusAccount;
            zCusAccount = new GovTax_Account_Info(zTKNganSach + ".0." + zCQQuanLyNS);

            if (zCusAccount.AccountID.Length > 0)
            {
                _OrderHeader.CQThuID = zCQQuanLyNS;
               // _OrderHeader.CQThuName = zCusAccount.CustomName;
            }

         //   _OrderHeader.DBHC_ID = zCusAccount.District_ID;
         //   _OrderHeader.DBHC_Name = zCusAccount.Distrist_Name;
        //    _OrderHeader.LoaiThueID = zCusAccount.LoaiThue;

            _OrderHeader.NHNN_ID = zCusAccount.TreasuryID;
            _OrderHeader.NHNN_Name = zCusAccount.TreasuryName;
            _OrderHeader.KieuThu = 1;

            _OrderHeader.NHPV_ID = _OrderOrigin.remtbk;
            _OrderHeader.NHPV_Name = _OrderOrigin.remntbk;
            _OrderHeader.NgayNT = _OrderOrigin.trdate;
            _OrderHeader.SoTien = _OrderOrigin.traamt;

            Item_Bill zItem;
            zItem = FindValueItem_OnlyOne(1);
            _OrderHeader.Ma_ST = zItem.Value;
            if (_OrderHeader.Ma_ST.Length > 0)
            {
                int zIndexMST = zItem.Index;
                if (zIndexMST + 3 <= _ListItemInBill.Count)
                {
                    Item_Bill zItemNgay = (Item_Bill)_ListItemInBill[zIndexMST + 2];
                    if (zItemNgay.CategoryID == 13)
                    {
                        _OrderHeader.NgayNT = DateTime.Parse(zItemNgay.Value);
                    }

                }
            }

            Item_Bill zItem1;
            zItem1 = FindValueItem_OnlyOne(13, "NGAYNT");
            if (zItem1.CategoryID == 13)
            {
                _OrderHeader.ParseNgayNT = zItem1.Value;
                _OrderHeader.NgayNT = DateTime.Parse(zItem1.Value);
            }
        }

        #region Check Info Of Invoice
        private int CheckOrderHeader()
        {
            int zResult = 0;
            string zMessage = "";
            if (_OrderHeader.CQThuID.Length == 0)
            {
                zMessage += "CQThuID/";
                zResult++;
            }
            if (_OrderHeader.TK_ThuNS.Length == 0)
            {
                zMessage += "TK_ThuNS/";
                zResult++;
            }
            if (_OrderHeader.DBHC_Name.Length == 0)
            {
                zMessage += "DBHC_Name/";
                zResult++;
            }
            if (_OrderHeader.LoaiThueID.Length == 0)
            {
                zMessage += "LoaiThueID/";
                zResult++;
            }

            if (_OrderHeader.Ma_ST.Length == 10 || _OrderHeader.Ma_ST.Length == 13)
            {
            }
            else
            {
                zMessage += "Ma_ST/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            return zResult;
        }
        private int CheckOrderTKNgayLH()
        {
            int zResult = 0;
            string zMessage = "";
            if (_OrderHeader.ToKhai.Length == 0)
            {
                zMessage += "ToKhai/";
                zResult++;
            }
            if (!_OrderHeader.IsNgayDK)
            {
                zMessage += "NgayDK/";
                zResult++;
            }
            if (_OrderHeader.LH_Name.Length == 0)
            {
                zMessage += "LH_Name/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            return zResult;
        }
        private int CheckOrderDetail()
        {
            Order_Detail_Info zDetail, zDetailTop;
            int zResult = 0;
            string zMessage = "";

            if (_OrderDetail.Count == 0)
            {
                zMessage += "0_ALL/";
                zResult++;
            }
            if (_OrderDetail.Count == 1)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[0];
                if (zDetail.SoTien == 0)
                    zDetail.SoTien = _OrderHeader.SoTien;
            }
            if (_OrderDetail.Count > 1)
            {
                zDetailTop = (Order_Detail_Info)_OrderDetail[0];
                // Chu y phan nay
                if (_OrderHeader.LoaiThueID == "01")
                {
                    if (zDetailTop.KyThue.Length == 0)
                    {
                        Item_Bill zTemp = FindValueItem_OnlyOne(13);
                        if (zTemp.CategoryID == 13)
                        {
                            zDetailTop.KyThue = zTemp.Value.Substring(3, 7);
                        }
                        else
                            zDetailTop.KyThue = DateTime.Now.ToString("MM/yyyy");
                    }
                }
                for (int i = 1; i < _OrderDetail.Count; i++)
                {
                    zDetail = (Order_Detail_Info)_OrderDetail[i];
                    if (zDetail.ChuongID.Length == 0)
                        zDetail.ChuongID = zDetailTop.ChuongID;
                    if (zDetail.KyThue.Length == 0)
                        zDetail.KyThue = zDetailTop.KyThue;
                }
            }

            //------------------------------
            double zSumMoney = 0;
            for (int i = 0; i < _OrderDetail.Count; i++)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[i];
                zSumMoney += zDetail.SoTien;
                if (zDetail.ChuongID.Length == 0)
                {
                    zMessage += "C/";
                    zResult++;
                }

                if (zDetail.NDKT_ID.Length == 0)
                {
                    zMessage += "TM/";
                    zResult++;
                }
                if (zDetail.NDKT_Name.Length == 0)
                {
                    zMessage += "TM_NAME/";
                    zResult++;
                }
                if (zDetail.SoTien == 0)
                {
                    zMessage += "ST/";
                    zResult++;
                }
                if (_OrderHeader.LoaiThueID == "01")
                {
                    if (zDetail.KyThue.Length == 0)
                    {
                        zMessage += "KYTHUE/";
                        zResult++;
                    }
                }
            }

            if (zSumMoney != _OrderOrigin.traamt)
            {
                zMessage += "TOTAL/";
                zResult++;
            }
            if (zResult > 0)
                Message_System("   -- (" + zResult.ToString() + ")" + zMessage);
            return zResult;
        }
        #endregion

        #region Find Rule
        private bool Rule_TK_Ngay_LH()
        {
            Item_Bill zItemTK, zItemNgay, zItemLHXNK;
            bool zResult = false;
            //-----------Tim To Khai---------------
            zItemTK = FindValueItem_OnlyOne(2);
            if (zItemTK.Value.Length >= 11)
            {
                _OrderHeader.ToKhai = zItemTK.Value;
                _OrderHeader.ToKhaiIndex = zItemTK.Index;
                if (_OrderHeader.ToKhaiIndex + 5 < _ListItemInBill.Count)
                {
                    // Rule TK_NGAY_LHXNK
                    zItemNgay = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 2];
                    zItemLHXNK = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 4];
                    if (zItemNgay.CategoryID == 13)
                    {
                        _OrderHeader.ParseNgayDK = zItemNgay.Value;
                    }
                    if (zItemLHXNK.CategoryID == 3)
                    {
                        _OrderHeader.LH_ID = zItemLHXNK.Value;
                        _OrderHeader.LH_Name = zItemLHXNK.Extent;
                    }
                    if (_OrderHeader.NgayDK != null && _OrderHeader.LH_Name.Length > 4)
                    {
                        zResult = true;
                    }
                    // Rule TK_LHXNK_NGAY
                    if (!zResult)
                    {
                        zItemLHXNK = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 2];
                        zItemNgay = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 4];

                        if (zItemLHXNK.CategoryID == 3)
                        {
                            _OrderHeader.LH_ID = zItemLHXNK.Value;
                            _OrderHeader.LH_Name = zItemLHXNK.Extent;
                        }
                        else
                        {
                            Item_Bill zItemLHXNK_Val = (Item_Bill)_ListItemInBill[_OrderHeader.ToKhaiIndex + 3];
                            if (zItemLHXNK_Val.Content.Length >= 2)
                            {
                                string zID = zItemLHXNK.Content + zItemLHXNK_Val.Content.Substring(0, 2);
                                string zName = Data_Access.ValueOfField("SELECT Description FROM BNK_LHXNK WHERE ID ='" + zID + "'");
                                if (zName.Length > 4)
                                {
                                    zItemLHXNK.CategoryID = 3;
                                    zItemLHXNK.Value = zID;
                                    zItemLHXNK.Extent = zName;

                                    _OrderHeader.LH_ID = zItemLHXNK.Value;
                                    _OrderHeader.LH_Name = zItemLHXNK.Extent;

                                    string zDate = zItemLHXNK_Val.Content.Substring(2, zItemLHXNK_Val.Content.Length - 2);
                                    string zNgayDK = Function.GetCorrectDate(zDate.Trim());
                                    if (zNgayDK.Length == 10)
                                        _OrderHeader.ParseNgayDK = zNgayDK;
                                }
                            }
                        }
                        if (zItemNgay.CategoryID == 13)
                        {
                            _OrderHeader.ParseNgayDK = zItemNgay.Value;
                        }

                        if (_OrderHeader.NgayDK != null && _OrderHeader.LH_Name.Length > 4)
                        {
                            zResult = true;
                        }
                    }
                }
            }
            return zResult;
        }
        private bool RuleDetail_C_TM_ST()
        {
            int i;
            Item_Bill zItem;
            ArrayList zListDetail = new ArrayList();
            int[] zListID = new int[] { 10, 11, 12, 14 };
            for (i = 0; i < _ListItemInBill.Count; i++)
            {
                zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CheckCategoryID_InList(zListID))
                {
                    zItem.Index = i;
                    zListDetail.Add(zItem);
                }
            }
            string zListSort = "";
            Item_Bill zItem1, zItem2;
            string strEmpty = ";0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0";
            if (zListDetail.Count > 1)
            {
                zItem1 = (Item_Bill)zListDetail[0];
                zListSort = zItem1.CategoryID.ToString();
                for (i = 1; i < zListDetail.Count; i++)
                {
                    zItem1 = (Item_Bill)zListDetail[i - 1];
                    zItem2 = (Item_Bill)zListDetail[i];
                    int zSpace = zItem2.Index - zItem1.Index;
                    if (zSpace < 10)
                        zListSort += strEmpty.Substring(0, zSpace) + ";" + zItem2.CategoryID;
                }
            }
            Message_System("    " + zListSort);
            txt_GroupSort.Text = zListSort;
            bool zResult = false;
            Order_Detail_Info zDetail;
            DataTable zRule_Detail = Rule_Data.RuleGroup(1);
            foreach (DataRow zRow in zRule_Detail.Rows)
            {
                // Message_System("       -" + zRow["GroupSort"].ToString());
                if (zListSort == zRow["GroupSort"].ToString())
                {
                    string[] zItemRepeat = zRow["ItemRepeat"].ToString().Split(';');
                    int k = 0;
                    for (i = 0; i < zItemRepeat.Length; i++)
                    {
                        int zAmount = int.Parse(zItemRepeat[i]);
                        zDetail = new Order_Detail_Info();
                        for (int j = 0; j < zAmount; j++)
                        {
                            zItem = (Item_Bill)zListDetail[k];
                            k++;
                            switch (zItem.CategoryID)
                            {
                                case 10:
                                    zDetail.NDKT_ID = zItem.Value;
                                    zDetail.NDKT_Name = zItem.Extent;
                                    break;
                                case 11:
                                    zDetail.SoTien = double.Parse(zItem.Value);
                                    break;
                                case 12:
                                    zDetail.ChuongID = zItem.Value;
                                    break;
                                case 14:
                                    zDetail.KyThue = zItem.Value;
                                    break;
                            }

                        }
                        _OrderDetail.Add(zDetail);
                    }
                    zResult = true;
                    break;
                }
            }
            return zResult;
        }

        private void RuleDetail_C_TM_ST_Ext()
        {

            int i;
            Item_Bill zItem_C, zItem_TM, zItem_ST, zItem_KT;
            int n = _ListItemInBill.Count;

            ArrayList zTM = new ArrayList();
            int zMax = 0;
            for (i = 0; i < n; i++)
            {
                zItem_TM = (Item_Bill)_ListItemInBill[i];
                if (zItem_TM.CategoryID == 10)
                {
                    zItem_TM.Index = i;
                    zTM.Add(zItem_TM);
                }
            }
            zMax = zTM.Count;
            //------------------------------------------
            ArrayList zChuong = new ArrayList();
            for (i = 0; i < n; i++)
            {
                zItem_C = (Item_Bill)_ListItemInBill[i];
                if (zItem_C.CategoryID == 12)
                {
                    zItem_C.Index = i;
                    zChuong.Add(zItem_C);
                }
            }
            if (zMax < zChuong.Count)
                zMax = zChuong.Count;
            //------------------------------------------
            ArrayList zST = new ArrayList();
            for (i = 0; i < n; i++)
            {
                zItem_ST = (Item_Bill)_ListItemInBill[i];
                if (zItem_ST.CategoryID == 11)
                {
                    zItem_ST.Index = i;
                    zST.Add(zItem_ST);
                }
            }
            if (zMax < zST.Count)
                zMax = zST.Count;
            //------------------------------------------
            ArrayList zKeThue = new ArrayList();
            for (i = 0; i < n; i++)
            {
                zItem_KT = (Item_Bill)_ListItemInBill[i];
                if (zItem_KT.CategoryID == 14)
                {
                    zItem_KT.Index = i;
                    zKeThue.Add(zItem_KT);
                }
            }
            if (zMax < zKeThue.Count)
                zMax = zKeThue.Count;

            _OrderDetail = new ArrayList();
            Order_Detail_Info zDetail;


            for (i = 0; i < zMax; i++)
            {
                zDetail = new Order_Detail_Info();
                if (i < zChuong.Count)
                {
                    zItem_C = (Item_Bill)zChuong[i];
                    zDetail.ChuongID = zItem_C.Value;
                }
                if (i < zTM.Count)
                {
                    zItem_TM = (Item_Bill)zTM[i];
                    zDetail.NDKT_ID = zItem_TM.Value;
                    zDetail.NDKT_Name = zItem_TM.Extent;
                }
                if (i < zST.Count)
                {
                    zItem_ST = (Item_Bill)zST[i];
                    zDetail.SoTien = double.Parse(zItem_ST.Value);
                }
                if (_OrderHeader.LoaiThueID == "01")
                {
                    if (i < zKeThue.Count)
                    {
                        zItem_KT = (Item_Bill)zKeThue[i];
                        zDetail.KyThue = zItem_KT.Value;
                    }
                }
                _OrderDetail.Add(zDetail);
            }

            if (_OrderDetail.Count > 1)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[0];
                string str_Chuong = zDetail.ChuongID;
                string str_KyThue = zDetail.KyThue;
                for (i = 1; i < _OrderDetail.Count; i++)
                {
                    zDetail = (Order_Detail_Info)_OrderDetail[i];
                    if (zDetail.ChuongID.Length == 0)
                    {
                        zDetail.ChuongID = str_Chuong;
                    }
                    if (zDetail.KyThue.Length == 0)
                    {
                        zDetail.KyThue = str_KyThue;
                    }
                }
            }
            if (_OrderDetail.Count == 1)
            {
                zDetail = (Order_Detail_Info)_OrderDetail[0];
                if (zDetail.SoTien == 0)
                {
                    zDetail.SoTien = _OrderHeader.SoTien;
                }
            }

        }
        #endregion

        #region Function Check
        private bool CheckItemIsNumber(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            bool zIsCorrect = false;
            int i = 0;
            if (zRule.RuleID > 0)
            {
                for (i = 0; i < zRule.LengValue.Length; i++)
                {
                    double zNumber = 0;
                    if (In_Content.Length == zRule.LengValue[i] && double.TryParse(In_Content, out zNumber))
                    {
                        zIsCorrect = true;
                        break;
                    }
                }
            }
            return zIsCorrect;
        }
        private string CheckItemIsMoney(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            string zResult = "";
            if (zRule.RuleID > 0)
            {
                zResult = Function.GetOnlyNumberInFormat(In_Content);
                if (zResult.Length > zRule.LengMax)
                {
                    zResult = "";
                }
            }
            return zResult;
        }
        private string CheckItemIsDate(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            string zResult = "";
            if (zRule.RuleID > 0)
            {
                if (In_Content.Length <= zRule.LengMax)
                {
                    zResult = Function.GetCorrectDate(In_Content);
                }
            }
            return zResult;
        }
        private string CheckItemIsKyThue(int RuleID, int RuleStyle, string In_Content)
        {
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            string zResult = "";
            if (zRule.RuleID > 0)
            {
                if (In_Content.Length <= zRule.LengMax)
                {
                    zResult = Function.GetCorrectMonthYear(In_Content);

                }
            }
            return zResult;
        }
        private string CheckItemValue_Extension(int RuleID, int RuleStyle, string In_Content, string Query)
        {
            string zResult = "";
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            if (zRule.RuleID > 0)
            {
                for (int i = 0; i < zRule.LengValue.Length; i++)
                {
                    double zNumber = 0;
                    // Message_System("  Rule Leng : " + zRule.LengValue[i].ToString() + ":" + In_Content);
                    if (In_Content.Length == zRule.LengValue[i] && double.TryParse(In_Content, out zNumber))
                    {
                        zResult = HHT.Library.System.Data_Access.ValueOfField(Query + "'" + In_Content + "'");
                        if (zResult.Length > 5)
                        {
                            break;
                        }
                    }
                }
            }
            return zResult;
        }
        private string CheckItemValue_LHXNK(int RuleID, int RuleStyle, string In_Content, string Query)
        {
            string zResult = "";
            Rule_Info zRule = new Rule_Info(RuleID, RuleStyle);
            bool zIsCorrect = false;
            if (zRule.RuleID > 0)
            {
                for (int i = 0; i < zRule.LengValue.Length; i++)
                {
                    if (In_Content.Length == zRule.LengValue[i])
                    {
                        zResult = HHT.Library.System.Data_Access.ValueOfField(Query + "'" + In_Content + "'");
                        if (zResult.Length > 5)
                        {
                            zIsCorrect = true;
                            break;
                        }
                    }
                }
            }
            return zResult;
        }

        private Item_Bill FindValueItem_OnlyOne(int CategoryID)
        {
            Item_Bill zResult = new Item_Bill();
            int zAmount = 0;
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == CategoryID)
                {
                    if (zResult.Value != zItem.Value)
                    {
                        zResult = zItem;
                        zResult.Index = i;
                        zAmount++;
                    }
                }
            }
            if (zAmount > 1)
                zResult = new Item_Bill();
            return zResult;
        }
        #endregion

        #region Save By Hand
        private void btn_Save_Click(object sender, EventArgs e)
        {
            SaveData_Normal();
            int zAmountErr = CheckOrderHeader();
            zAmountErr += CheckOrderTKNgayLH();
            zAmountErr += CheckOrderDetail();
            if (zAmountErr == 0)
            {
                _OrderOrigin.UpdateStatus(10, CategoryOrder());
                _OrderHeader.StatusAnalysis = 10;

                GV_ListOrder.Rows[_Index_Selected].Cells["Message"].Value = "Done 10";
                GV_ListOrder.Rows[_Index_Selected].Cells["StatusAnalysis"].Value = _OrderHeader.StatusAnalysis;
            }
            else
            {
                MessageBox.Show("Xin vui lòng kiểm tra lại thông tin !");
            }
        }
        private void SaveData_Normal()
        {
            _OrderHeader = new Order_Info();

            _OrderHeader.CQThuID = txt_CQQLThu_ID.Text;
            _OrderHeader.CQThuName = txt_CQQLThu_Name.Text;
            _OrderHeader.DBHC_ID = txt_DBHC_ID.Text.Replace("HH", " ").Trim();
            _OrderHeader.DBHC_Name = txt_DBHC_Name.Text;
            _OrderHeader.KieuThu = 2;
            _OrderHeader.LH_ID = txt_LHXNK.Text;
            _OrderHeader.LH_Name = txt_LHXNK_Name.Text;
            _OrderHeader.LoaiThueID = txt_LoaiThue.Text;
            _OrderHeader.LoaiThueName = txt_LoaiThue_Name.Text;
            _OrderHeader.Ma_ST = txt_MaSoThue.Text;

            _OrderHeader.ParseNgayDK = txt_NgayDk.Text;
            _OrderHeader.ParseNgayNT = txt_NgayNT.Text;

            _OrderHeader.NHNN_ID = txt_NHNN_ID.Text;
            _OrderHeader.NHNN_Name = txt_NHNN_Name.Text;
            _OrderHeader.NHPV_ID = txt_NH_PhucVu.Text;

            _OrderHeader.StatusAnalysis = 20;
            _OrderHeader.SoTien = _OrderOrigin.traamt;
            _OrderHeader.TenCongTy = txt_NguoiNT.Text;
            _OrderHeader.TK_ThuNS = txt_TK_Thu_NS.Text;
            _OrderHeader.TK_ThuNS_Name = txt_TK_Name.Text;
            _OrderHeader.trref = txt_SoChungTu.Text;
            _OrderHeader.ToKhai = txt_ToKhaiSo.Text;

            _OrderDetail = new ArrayList();
            Order_Detail_Info zDetail;

            int n = GV_DataDetail.Rows.Count;
            double zTienNT = 0;
            for (int i = 0; i < n - 1; i++)
            {
                if (GV_DataDetail.Rows[i].Cells["Chuong"].Value == null)
                    break;

                zDetail = new Order_Detail_Info();
                if (GV_DataDetail.Rows[i].Cells["Chuong"].Value != null)
                    zDetail.ChuongID = GV_DataDetail.Rows[i].Cells["Chuong"].Value.ToString();
                if (GV_DataDetail.Rows[i].Cells["NDKT"].Value != null)
                    zDetail.NDKT_ID = GV_DataDetail.Rows[i].Cells["NDKT"].Value.ToString();
                if (GV_DataDetail.Rows[i].Cells["Content"].Value != null)
                    zDetail.NDKT_Name = GV_DataDetail.Rows[i].Cells["Content"].Value.ToString();
                if (GV_DataDetail.Rows[i].Cells["KyThue"].Value != null)
                    zDetail.KyThue = GV_DataDetail.Rows[i].Cells["KyThue"].Value.ToString();
                zTienNT = 0;
                double.TryParse(GV_DataDetail.Rows[i].Cells["TienNT"].Value.ToString(), out zTienNT);

                zDetail.SoTien = zTienNT;
                zDetail.trref = _OrderHeader.trref;

                _OrderDetail.Add(zDetail);

            }


        }
        #endregion

        private Item_Bill FindValueItem_OnlyOne(int CategoryID, string WordCode)
        {
            Item_Bill zResult = new Item_Bill();
            int zAmount = 0;
            for (int i = 0; i < _ListItemInBill.Count; i++)
            {
                Item_Bill zItem = (Item_Bill)_ListItemInBill[i];
                if (zItem.CategoryID == CategoryID && WordCode == zItem.WordCode)
                {
                    if (zResult.Value != zItem.Value)
                    {
                        zResult = zItem;
                        zResult.Index = i;
                        zAmount++;
                    }
                }
            }
            if (zAmount > 1)
                zResult = new Item_Bill();
            return zResult;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LB_Log.Items.Clear();
        }

        private void btn_App_Group_Click(object sender, EventArgs e)
        {
            Rule_Group_Info zRule_Group = new Rule_Group_Info();
            zRule_Group.GroupID = 1;
            zRule_Group.GroupSort = txt_GroupSort.Text;
            zRule_Group.ItemRepeat = txt_ItemRepeat.Text;
            zRule_Group.Version = 1;
            zRule_Group.Rank = 10;
            zRule_Group.Create();
            if (zRule_Group.Message.Length == 0)
                MessageBox.Show("Update Success!");
        }
        private void btn_SaveFail_Click(object sender, EventArgs e)
        {
            if (GroupSearch.Visible)
                GroupSearch.Visible = false;
            else
                GroupSearch.Visible = true;
        }

        private void PanelLeftBottom_SizeChanged(object sender, EventArgs e)
        {
            btn_SaveFail.Left = PanelLeftBottom.Width - btn_SaveFail.Width - 10;
        }
    }
}
