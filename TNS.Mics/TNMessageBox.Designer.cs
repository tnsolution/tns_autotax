﻿namespace TNS.Misc
{
    partial class TNMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TNMessageBox));
            this.FormTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Success = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.btnOK = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnCancel = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Warning = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.Panel_Error = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.Panel_Footer = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.Panel_Split = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.Panel_Info = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.txt_Description = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Success)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Warning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Footer)).BeginInit();
            this.Panel_Footer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Split)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Info)).BeginInit();
            this.SuspendLayout();
            // 
            // FormTitle
            // 
            this.FormTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.FormTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.FormTitle.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Primary;
            this.FormTitle.Location = new System.Drawing.Point(0, 0);
            this.FormTitle.Name = "FormTitle";
            this.FormTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.FormTitle.Size = new System.Drawing.Size(500, 36);
            this.FormTitle.TabIndex = 222;
            this.FormTitle.Values.Description = "";
            this.FormTitle.Values.Heading = "Thông báo";
            this.FormTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("FormTitle.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.Style = ComponentFactory.Krypton.Toolkit.PaletteButtonStyle.Inherit;
            this.btnMini.ToolTipStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ToolTip;
            this.btnMini.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Generic;
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Visible = false;
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.Style = ComponentFactory.Krypton.Toolkit.PaletteButtonStyle.Inherit;
            this.btnMax.ToolTipStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ToolTip;
            this.btnMax.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Generic;
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Style = ComponentFactory.Krypton.Toolkit.PaletteButtonStyle.Inherit;
            this.btnClose.ToolTipStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ToolTip;
            this.btnClose.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Generic;
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Visible = false;
            // 
            // Panel_Success
            // 
            this.Panel_Success.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Success.Location = new System.Drawing.Point(20, 36);
            this.Panel_Success.Name = "Panel_Success";
            this.Panel_Success.Size = new System.Drawing.Size(20, 164);
            this.Panel_Success.StateCommon.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Panel_Success.StateCommon.Color2 = System.Drawing.Color.Green;
            this.Panel_Success.StateCommon.ColorAngle = 45F;
            this.Panel_Success.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Rounding4;
            this.Panel_Success.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.Panel_Success.TabIndex = 223;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(254, 8);
            this.btnOK.Name = "btnOK";
            this.btnOK.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnOK.Size = new System.Drawing.Size(100, 35);
            this.btnOK.TabIndex = 226;
            this.btnOK.Values.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(146, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btnCancel.Size = new System.Drawing.Size(100, 35);
            this.btnCancel.TabIndex = 226;
            this.btnCancel.Values.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Panel_Warning
            // 
            this.Panel_Warning.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Warning.Location = new System.Drawing.Point(60, 36);
            this.Panel_Warning.Name = "Panel_Warning";
            this.Panel_Warning.Size = new System.Drawing.Size(20, 164);
            this.Panel_Warning.StateCommon.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Panel_Warning.StateCommon.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Panel_Warning.StateCommon.ColorAngle = 45F;
            this.Panel_Warning.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Rounding4;
            this.Panel_Warning.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.Panel_Warning.TabIndex = 227;
            // 
            // Panel_Error
            // 
            this.Panel_Error.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Error.Location = new System.Drawing.Point(40, 36);
            this.Panel_Error.Name = "Panel_Error";
            this.Panel_Error.Size = new System.Drawing.Size(20, 164);
            this.Panel_Error.StateCommon.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Panel_Error.StateCommon.Color2 = System.Drawing.Color.Red;
            this.Panel_Error.StateCommon.ColorAngle = 45F;
            this.Panel_Error.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Rounding4;
            this.Panel_Error.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.Panel_Error.TabIndex = 228;
            // 
            // Panel_Footer
            // 
            this.Panel_Footer.Controls.Add(this.btnOK);
            this.Panel_Footer.Controls.Add(this.btnCancel);
            this.Panel_Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel_Footer.Location = new System.Drawing.Point(0, 200);
            this.Panel_Footer.Name = "Panel_Footer";
            this.Panel_Footer.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Panel_Footer.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.Panel_Footer.Size = new System.Drawing.Size(500, 50);
            this.Panel_Footer.TabIndex = 229;
            // 
            // Panel_Split
            // 
            this.Panel_Split.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Split.Location = new System.Drawing.Point(80, 36);
            this.Panel_Split.Name = "Panel_Split";
            this.Panel_Split.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.Panel_Split.Size = new System.Drawing.Size(5, 164);
            this.Panel_Split.StateCommon.Color1 = System.Drawing.Color.White;
            this.Panel_Split.StateCommon.Color2 = System.Drawing.Color.Transparent;
            this.Panel_Split.StateCommon.ColorAngle = 75F;
            this.Panel_Split.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Sigma;
            this.Panel_Split.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.Panel_Split.TabIndex = 232;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "iconfinder_help-browser_118806.png");
            this.imageList1.Images.SetKeyName(1, "iconfinder_dialog-warning_118940.png");
            this.imageList1.Images.SetKeyName(2, "iconfinder_accepted_48_10249.png");
            this.imageList1.Images.SetKeyName(3, "iconfinder_software-update-urgent_118956.png");
            // 
            // Panel_Info
            // 
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Info.Location = new System.Drawing.Point(0, 36);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(20, 164);
            this.Panel_Info.StateCommon.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Panel_Info.StateCommon.Color2 = System.Drawing.Color.Blue;
            this.Panel_Info.StateCommon.ColorAngle = 45F;
            this.Panel_Info.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Rounding4;
            this.Panel_Info.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.Inherit;
            this.Panel_Info.TabIndex = 235;
            // 
            // txt_Description
            // 
            this.txt_Description.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Description.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.txt_Description.Location = new System.Drawing.Point(85, 36);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_Description.Size = new System.Drawing.Size(415, 164);
            this.txt_Description.TabIndex = 236;
            this.txt_Description.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TNMessageBox
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(500, 250);
            this.Controls.Add(this.txt_Description);
            this.Controls.Add(this.Panel_Split);
            this.Controls.Add(this.Panel_Warning);
            this.Controls.Add(this.Panel_Error);
            this.Controls.Add(this.Panel_Success);
            this.Controls.Add(this.Panel_Info);
            this.Controls.Add(this.FormTitle);
            this.Controls.Add(this.Panel_Footer);
            this.Font = new System.Drawing.Font("Tahoma", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TNMessageBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "THÔNG BÁO";
            this.Load += new System.EventHandler(this.TNMessageBox_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Success)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Warning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Footer)).EndInit();
            this.Panel_Footer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Split)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel_Info)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader FormTitle;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Success;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnOK;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCancel;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Warning;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Error;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Footer;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Split;
        private System.Windows.Forms.ImageList imageList1;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel Panel_Info;
        private System.Windows.Forms.TextBox txt_Description;
    }
}